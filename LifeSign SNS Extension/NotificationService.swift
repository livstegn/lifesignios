//
//  NotificationService.swift
//  LifeSign Notification Service Extension
//
//  Created by Simon Jensen on 20/07/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//


import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {

        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
            
        if let listData = request.content.userInfo["aps"] as? [String: Any],
            let snsCategory = listData["category"] as? String,
            let bestAttemptContent = bestAttemptContent {
            
                switch(snsCategory) {
                
                case "INVITATION":
                    let snsSenderName = listData["sendername"] as? String ?? NSLocalizedString("(No sender)", comment: "Notification for an invitation - default if lacking sender")
                    let snsGroupName = listData["groupname"] as? String ?? NSLocalizedString("(No group)", comment: "Notification for an invitation - default if lacking group")
                    let bodyText = NSLocalizedString("#1# has invited you to the group #2#", comment: "Notification for an invitation - body")
                    let bodyText1 = bodyText.replacingOccurrences(of: "#1#", with: snsSenderName)
                    bestAttemptContent.body = bodyText1.replacingOccurrences(of: "#2#", with: snsGroupName)
                    bestAttemptContent.title = NSLocalizedString("Invitation", comment: "Notification for an invitation - title")

                    // Always call the completion handler when done.
                    contentHandler(bestAttemptContent)
                    
                case "FEEDBACK":
                    var snsSenderName = listData["sendername"] as? String ?? NSLocalizedString("(No sender)", comment: "Notification for feedback - default if lacking sender")
                    if (snsSenderName == "Self") {
                        snsSenderName = NSLocalizedString("You", comment: "Notification for feedback - yourself as sender")
                    }
                    let bodyText = NSLocalizedString("#1# gave sign of life", comment: "Notification for feedback - body")
                    bestAttemptContent.body = bodyText.replacingOccurrences(of: "#1#", with: snsSenderName)
                    bestAttemptContent.title = NSLocalizedString("Sign of life", comment: "Notification for feedback - title")

                    // Always call the completion handler when done.
                    contentHandler(bestAttemptContent)
                    
                default:
                    // Modify the notification content here...
                    bestAttemptContent.title = "\(bestAttemptContent.title) [modified]"
                    // Always call the completion handler when done.
                    contentHandler(bestAttemptContent)
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            
            contentHandler(bestAttemptContent)
        }
    }

}
