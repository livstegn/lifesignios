
INSTALLATION AF AWS SDK

1) Åben et terminal vindue - gå ind i folderen med 'LifeSign' projektet
2) Installer Cocoapods:  $sudo gem install cocoapods
3) De nødvendige pods er allerede defineret i 'Podfile'
4) Installer nødvendige AWS Mobile komponenter:  $pod install --repo-update
5) Luk 'LifeSign' projektet i Xcode (hvis åbent)
6) Åben 'LifeSign' ved at klikke på `LifeSign.xcworkspace` i 'LifeSign' projekt folderen 
