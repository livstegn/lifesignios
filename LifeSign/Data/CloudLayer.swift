//
//  CloudLayer.swift
//  Lifesign-Models
//
//  Created by Peter Rosendahl on 19/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation

import AWSCore
import AWSSNS
import AWSCognitoIdentityProvider

protocol CloudLayer {
    
    /* Reference to DeviceLayer */
    //func setDeviceLayer(layer: DeviceLayer?)
    
    /* Lifesign API ved AWS */
    func setupCredentials(authMethod: AuthenticationMethod) -> Bool
    func setAuthorizationToken(token: String?)
    func createUser(user: User) -> User
    func updateUser(userid: String, user: User) -> User
    func getUser(userid: String) -> User?
    func deleteUser(userid: String) -> String?  // Return errorcode???
    func addCaregroup(userid: String, caregroup: Caregroup) -> String?  // Return groupID
    func updateCaregroup(userid: String, caregroup: Caregroup) -> String?  // Return groupID
    func deleteCaregroup(caregroupid: String) -> String?  // Return errorcode???
    func addLifesign(userid: String, userMinutesBeforeUTC : Int) -> String
    func getCaregroups(userid: String) -> [Caregroup]?
    func getCaregroup(caregroupid: String) -> Caregroup?
    func addGroupmember(userId: String, status: GroupMemberStatusType, caregroupId: String, memberId: String, isMonitor: Bool, isMonitored: Bool, isOwner: Bool) -> String?
    func removeGroupmember(userId: String, caregroupId: String, memberId: String) -> String?  // return/errorcode
    func updateGroupmember(groupmember: GroupMember) -> GroupMember?
    func getGroupmember(userId: String, caregroupId: String) -> GroupMember?
    func getGroupmembers(caregroupId: String) -> [GroupMember]?
    func addGroupMessage(userid: String, caregroupId: String, messagetext: String) ->String?
    func getGroupMessages(caregroupId: String) -> [Message]?
    func getMessageGroup(messageId: String) -> String?
    func getUserID(email: String) -> String?
    func getUsers() -> [User]
    func addSupervision(supervision: Supervision) -> String?
    func updateSupervision(supervision: Supervision) -> String?
    func deleteSupervision(supervision: Supervision) -> String?  // Return errorcode???
    func getSupervisions(groupmemberId: String) -> [Supervision]?
    func getSupervision(groupmemberId: String, supervisionId: String) -> Supervision?
    func addCheckpoint(checkpoint: Checkpoint) -> String
    func getCheckpoints(supervisionid: String) -> [Checkpoint]?
    func deleteCheckpoints(supervisionid: String) -> String?
    func addGroupInvitation(fromUserId: String, toUserId: String, caregroupId: String, invitation: String) -> String?  // Return invitationID
    func getLifesigns(userid: String) -> [LifeSign]?
    func getAlarms(supervisionid: String) -> [Alarm]?
    func getLocation(trackingId: String, locationsId: String) -> Location?
    func getLocations(supervision: Supervision) -> [Location]
    func addLocation(location: Location) ->String?
    func getMainStatusList(userId: String, caregroupId: String) -> [MainStatus] 
    func updateTracking(tracking: Tracking) -> String?
    func getTracking(trackingid: String) -> Tracking?
    func addTracking(tracking: Tracking) -> String?
    func getTrackings(supervision: Supervision) -> [Tracking]
    func updateGroupInvitation(userId: String, invitationId: String, status: InvitationStatusType) -> Invitation
    func pushGroupInvitation(userId: String, invitationId: String, fromUser: String, toGroup: String)
    func haveGroupInvitations(userId: String) -> Bool
    func getGroupInvitation(userId: String, invitationId: String) -> Invitation?
    func getGroupInvitations(userId: String) -> [Invitation]?
    func pushFeedback(userId: String, caregroupId: String, fromUser: String)
    func getAppStatus(userid: String) -> Status?
    func setAppStatus(userid: String, status: AppStatusType) -> Status?

    /* AWS Cognito API-kald */
    func signIn(email: String, password: String) -> NSError?
    //func signup(userName: String, email: String, password: String) -> SignupResult
    func newsignup(userName: String, email: String, password: String) -> SignupResult
    func signOut()
    func deleteCognitoUser() -> NSError?
    func forgotPassword(email: String) -> NSError?
    func confirmForgotPassword(email: String, confirmationCodeValue: String, password: String) -> NSError?
    func changeEmail(email: String, newEmail: String) -> NSError?
    func confirmNewEmail(email: String, code: String) -> NSError?
    func changePassword(email: String, password: String, newPassword: String) -> NSError?
    func getUserSessionState() -> UserSessionStatusType
    func isCloudSessionSignedIn() -> Bool
    func getCloudSessionIdToken() -> String?
    func getCloudSessionUserName() -> String?
    
    /* AWS SNS API-kald */
    func snsAuthneticateUser(userId: String, email: String)
    func snsCreatePlatformARN(userId: String, email: String) -> String?
    func snsCreateEndpointARN(token: String, platformARN: String) -> String?
    func snsCreateTopicARN(userID: String) -> String?
    func snsCreateSubscriptionARN(endpointARN: String, topicARN: String) -> String?
    
}

class CloudLayerImplementation: CloudLayer {
    
    let trace = ErrorTraces()
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled / PerformanceTraces () = trace enabled, runtime collected
    
    var awsUser: AWSCognitoIdentityUser?
    var awsPool: AWSCognitoIdentityUserPool?
    var passwordAuthenticationCompletion: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>?
    var awsUserSession: AWSCognitoIdentityUserSession?
    var awsAuthorizationToken : String?
    var awsClient : AWSLifesignClient?
    var currentAuthMethod: AuthenticationMethod?

    let awsSessionFilename = "awsSession"
    let xmlExtension = "xml"
    var sns : AWSSNS?
    
    var successCounter = 0

    init() {
        trace.begin()
        self.currentAuthMethod = AuthenticationMethod.Unknown
        
        awsClient = AWSLifesignClient.default()
        
        trace.end()
    }

    func setAuthorizationToken(token: String?) {
        trace.begin()
        self.awsAuthorizationToken = token
        trace.end()
    }
    
    func setupCredentials(authMethod: AuthenticationMethod) -> Bool {
        
        trace.begin()
        
        switch(authMethod) {
            
        case AuthenticationMethod.Identity:  // User authentication with Facebook (Identity provider)
            
            trace.output("Authentication: IDENTITY")
            break
 
            
        case AuthenticationMethod.User:
            
            trace.output("Authentication: USER")
            
           
            // setup service configuration
            let serviceConfiguration = AWSServiceConfiguration(region: CognitoIdentityUserPoolRegion, credentialsProvider: nil)
            
            // create pool configuration
            let poolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: CognitoIdentityUserPoolAppClientId,
                                                                            clientSecret: CognitoIdentityUserPoolAppClientSecret,
                                                                            poolId: CognitoIdentityUserPoolId)
            // initialize user pool client
            AWSCognitoIdentityUserPool.register(with: serviceConfiguration, userPoolConfiguration: poolConfiguration, forKey: AWSCognitoUserPoolsSignInProviderKey)
            
            awsClient = AWSLifesignClient.default()
            awsPool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            
            if (awsUser == nil) {
                awsUser = awsPool?.currentUser()
            }
            break

        default:
            trace.output("Authentication: DEFAULT??")
            break
        }
        
        self.currentAuthMethod = authMethod
        
        trace.output("Authentication: Completed.")
        trace.end()
        
        return true
    }
    
    /* Lifesign API ved AWS */
    func getUser(userid: String) -> User? {
        
        trace.begin()
        
        var user : User?
        
        let headerParameters = headerNoToken() as [AnyHashable : Any]   // Must be NoToken to allow check of email BEFORE login
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error as NSError? {
                //self.trace.error("Error occurred: \(error)")
                DispatchQueue.main.async {
                    Helper.httpErrorHandler(error: error)
                }
                return nil
            }
            else
                if let result = task?.result as? AWSUser {
                    self.trace.output(result.description ?? "")
                    if result is AWSUser {
                        user = User()
                        user!.userId = result.partitionkey ?? ""
                        user!.email = result.dataSk ?? ""
                        user!.userName = result.name ?? ""
                        user!.deviceId = result.deviceid ?? ""
                        user!.platform = PlatformType.enumFromString(string: result.platform ?? PlatformType.iOS.description) ?? PlatformType.iOS
                        user!.cellno = result.cellno ?? ""
                        user!.userMinutesBeforeUTC = Int(truncating: result.userminutesbeforeutc ?? 0)
                        self.trace.output("CloudLayer::postUser UserId from post = \(String(describing: result.partitionkey ?? ""))")
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}")
        return user
    }
    
    func deleteUser(userid: String) -> String? {
        trace.begin()
        self.trace.output("CloudLayer - deleteUser")
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        
        awsClient!.invokeHTTPRequest("DELETE", urlString: "/users/{userid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result as? AWSUser {
                    self.trace.output(result.description ?? "")
            }
            return nil
            }.waitUntilFinished()
        
        trace.end()
        return "test rc"
    }
    
    func addCaregroup(userid: String, caregroup: Caregroup) ->String? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["caregroupname"] = caregroup.caregroupName
        queryParameters["status"] = caregroup.status.description
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        
        var caregroupID: String?
        
        // Find the caregroups (only the id's) that the user has relations to
        //var caregroupdIds = [String]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output("Result: \(String(describing: result.description))" )

                    if result is String {
                        self.trace.output("result matched!")
                        caregroupID = result.description
                        self.trace.output("CloudLayer::addCaregroup POST Caregroup caregroupID = \(String(describing: caregroupID))")
                    }
                    self.trace.output("CaregroupID: \(String(describing: caregroupID))")
            }
            
            return nil

            }.waitUntilFinished()
        
        print("return caregroupID: \(String(describing: caregroupID))")

        trace.end()
        self.trace.output(start: before, text: "POST /users/{userid}/caregroups")

        return caregroupID
    }


    func updateCaregroup(userid: String, caregroup: Caregroup) ->String? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["caregroupname"] = caregroup.caregroupName
        queryParameters["status"] = caregroup.status.description
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        pathParameters["caregroupid"] = caregroup.caregroupId
        
        var caregroupID: String?
        
        // Find the caregroups (only the id's) that the user has relations to
        //var caregroupdIds = [String]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/caregroups/{caregroupid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        caregroupID = result.description
                        self.trace.output("CloudLayer::updateCaregroup PATCH Caregroup caregroupID = \(String(describing: caregroupID))")
                    }
            }
            return nil
            }.waitUntilFinished()
        
        trace.end()
        self.trace.output(start: before, text: "PATCH /users/{userid}/caregroups")
        
        return caregroupID
    }
    
    func deleteCaregroup(caregroupid: String) -> String? {
        trace.begin()
        self.trace.output("CloudLayer - deleteCaregroup")

        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = caregroupid

        
        awsClient!.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/caregroups/{caregroupid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self).continueWith { (task: AWSTask?) -> AnyObject? in
        if let error = task?.error {
            self.trace.error("Error occurred: \(error)")
            return nil
        }
        else if let result = task?.result as? AWSCaregroup {
            self.trace.output(result.description)
        }
        return nil
        }.waitUntilFinished()
 
        trace.end()
        return "test rc"
    }
    
    func addLifesign(userid: String, userMinutesBeforeUTC: Int) -> String {
        trace.begin()
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["userminutesbeforeutc"] = userMinutesBeforeUTC
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid

        var lifesignID = ""
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/lifesigns", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLifesign.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        lifesignID = result.description
                        self.trace.output("CloudLayer::addLifesign POST Lifesign for lifesignID = \(String(describing: lifesignID))")
                    }
            }
            return nil
            }.waitUntilFinished()

        
        
        trace.end()

        self.trace.output(start: before, text: "POST /users/{userid}/lifesigns")

        return lifesignID
    }
    
    func getCaregroups(userid: String) -> [Caregroup]? {

        trace.begin()
        trace.output(">> getCareGroups \(userid)>>")
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        
        // Find the caregroups (only the id's) that the user has relations to
        var caregroupdIds = [String]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error as NSError? {
                //self.trace.error("Error occurred: \(error)")
                DispatchQueue.main.async {
                    Helper.httpErrorHandler(error: error)
                }
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSCaregroup]  {
                            caregroupdIds.append(x.sortkey ?? "")
                            self.trace.output("CloudLayer::getCaregroups GET Caregroups key = \(String(describing: x.sortkey ?? ""))")
                        }
                    }
                    else if result is AWSCaregroup {
                        let cg = task?.result as? AWSCaregroup
                        caregroupdIds.append(cg?.sortkey ?? "")
                        self.trace.output("CloudLayer::getCaregroups GET Caregroups key = \(String(describing: cg?.sortkey ?? ""))")
                    }
            }
            return nil
            }.waitUntilFinished()

        self.trace.output(start: before, text: "GET /users/{userid}/caregroups " + userid)
        var caregroups = [Caregroup]()

        // Read all data about each caregroup

        for caregroupId in caregroupdIds {
            pathParameters = [:]
            pathParameters["userid"] = userid // TODO ikke nødvendigt?
            pathParameters["caregroupid"] = caregroupId
 
            let before2 = Date()
            awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self).continueWith { (task: AWSTask?) -> AnyObject? in
                if let error = task?.error {
                    self.trace.error("Error occurred: \(error)")
                    return nil
                }
                else
                    if let result = task?.result {
                        self.trace.output(result.description ?? "")
                        if result is AWSCaregroup {
                            let cg = Caregroup()
                            cg.caregroupId = result.partitionkey ?? ""
                            cg.caregroupName = result.dataSk ?? ""
                            let cgResult : AWSCaregroup = (task?.result)! as! AWSCaregroup  // << SIMON
                            cg.status = CaregroupStatusType.enumFromString(string: cgResult.status ?? CaregroupStatusType.Inactive.description) ?? CaregroupStatusType.Inactive
                            
                            caregroups.append(cg)
                            self.trace.output("CloudLayer::getCaregroups GET Caregroups name = \(String(describing: cg.caregroupName))")
                        }
                }
                return nil
                }.waitUntilFinished()
            self.trace.output(start: before2, text: "GET /users/{userid}/caregroups/{caregroupid} " + caregroupId)
        }
        
        trace.end()

        return caregroups
    }

    func getCaregroup(caregroupid: String) -> Caregroup? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = caregroupid
        
        // Read all data about each caregroup
        
        let caregroup = Caregroup()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is AWSCaregroup {
                        caregroup.caregroupId = result.partitionkey ?? ""
                        caregroup.caregroupName = result.dataSk ?? ""
                        let cgResult : AWSCaregroup = (task?.result)! as! AWSCaregroup  // << SIMON
                        caregroup.status = CaregroupStatusType.enumFromString(string: cgResult.status ?? CaregroupStatusType.Inactive.description) ?? CaregroupStatusType.Inactive
                        
                        self.trace.output("GET Caregroup name = \(String(describing: caregroup.caregroupName))")
                    }
                }
                return nil
            }.waitUntilFinished()

        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}")

        trace.end()
        
        return caregroup
    }
    
    
    func addGroupmember(userId: String, status: GroupMemberStatusType, caregroupId: String, memberId: String, isMonitor: Bool, isMonitored: Bool, isOwner: Bool) -> String? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["status"] = status.description
        queryParameters["memberid"] = memberId    // Optional! Add to re-use current caregroupmemberId. If parameter is not included, then Lambda will create new number

        if (isMonitored) {
            queryParameters["isMonitored"] = "1"
        } else {
            queryParameters["isMonitored"] = "0"
        }
        if (isMonitor) {
            queryParameters["isMonitor"] = "1"
        } else {
            queryParameters["isMonitor"] = "0"
        }
        if (isOwner) {
            queryParameters["isOwner"] = "1"
        } else {
            queryParameters["isOwner"] = "0"
        }

        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId
        pathParameters["caregroupid"] = caregroupId
        
        // Find the members in the caregroup
        var groupmemberId : String?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/members", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        groupmemberId = result.description
                        self.trace.output("CloudLayer::addGroupmember POST userId: \(String(describing: userId)) as groupmemberId: \(String(describing: groupmemberId))")
                    }
                }
                return nil
            }
        }.waitUntilFinished()

        self.trace.output(start: before, text: "POST /users/{userid}/caregroups/{caregroupid}/members")
        trace.end()
        
        return groupmemberId
    }

//  func updateGroupmember(userId: String, caregroupId: String, status: GroupMemberStatusType, isMonitor: Bool, isMonitored: Bool, isOwner: Bool) -> GroupMember? {
    func updateGroupmember(groupmember: GroupMember) -> GroupMember? {

//    func updateGroupmember(userId: String, caregroupId: String, status: GroupMemberStatusType) -> GroupMember? {
        
        trace.begin()
        
        trace.output("userid = \(groupmember.userid)")
        trace.output("caregroupid = \(groupmember.caregroupId)")
        trace.output("groupmemberid = \(groupmember.groupmemberId)")

        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["status"] = groupmember.status.description
        if (groupmember.isMonitored) {
            queryParameters["ismonitored"] = "1"
        } else {
            queryParameters["ismonitored"] = "0"
        }
        if (groupmember.isMonitor) {
            queryParameters["ismonitor"] = "1"
        } else {
            queryParameters["ismonitor"] = "0"
        }
        if (groupmember.isOwner) {
            queryParameters["isowner"] = "1"
        } else {
            queryParameters["isowner"] = "0"
        }
        
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = groupmember.userid
        pathParameters["caregroupid"] = groupmember.caregroupId
        pathParameters["groupmemberid"] = "dummy"  // A parameter is needed by API Gateway, but not by Lambda
        if(groupmember.groupmemberId != "") {
            pathParameters["groupmemberid"] = groupmember.groupmemberId
        }
        
        trace.output("pathParams = \(pathParameters)")
        trace.output("queryParams = \(queryParameters)")

        let groupMember = GroupMember()
        
        awsClient!.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{groupmemberid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                if let result = task?.result as? AWSGroupMember {
                    self.trace.output(result.description )
                    if result is AWSGroupMember {
                        groupMember.userid = result.partitionkey ?? ""
                        groupMember.caregroupId = result.sortkey ?? ""
                        groupMember.groupmemberId = result.dataSk ?? ""
                        groupMember.status = GroupMemberStatusType.enumFromString(string: result.status ?? GroupMemberStatusType.Inactivated.description) ?? GroupMemberStatusType.Inactivated
                        
                        groupMember.isMonitored = (result.ismonitored?.boolValue ?? false)
                        groupMember.isMonitor = (result.ismonitor?.boolValue ?? false)
                        groupMember.isOwner = (result.isowner?.boolValue ?? false)
                        
                    }
                }
                return nil
            }

        }.waitUntilFinished()

        trace.end()

        return groupMember
    }
    
    func getGroupmember(userId: String, caregroupId: String) -> GroupMember?  {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId
        pathParameters["caregroupid"] = caregroupId
        pathParameters["memberid"] = "dummy"
        
        // Find the members in the caregroup
        let groupmember = GroupMember()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                if let result = task?.result as? AWSGroupMember {  // << SIMON
                    self.trace.output(result.description)
                    if result is AWSGroupMember {
                        groupmember.userid = result.partitionkey ?? ""
                        groupmember.caregroupId = result.sortkey ?? ""
                        groupmember.groupmemberId = result.dataSk ?? ""
                        groupmember.status = GroupMemberStatusType.enumFromString(string: result.status ?? GroupMemberStatusType.Inactivated.description) ?? GroupMemberStatusType.Inactivated
                        groupmember.isMonitored = (result.ismonitored?.boolValue ?? false)
                        groupmember.isMonitor = (result.ismonitor?.boolValue ?? false)
                        groupmember.isOwner = (result.isowner?.boolValue ?? false)
                    }
                }
                return nil
            }
        }.waitUntilFinished()
        
        self.trace.output(start: before, text: "/users/{userid}/caregroups/{caregroupid}/members/{memberid} " + userId)

        trace.end()
    
        return groupmember
    }
    
    func getGroupmembers(caregroupId: String) -> [GroupMember]? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = caregroupId

        // Find the members in the caregroup
        var memberlist = [GroupMember]()
        var groupmembers = [GroupMember]()

        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSGroupMember]  {
                            let groupmember = GroupMember()
                            groupmember.userid = x.partitionkey ?? ""
                            groupmember.caregroupId = x.sortkey ?? ""
                            memberlist.append(groupmember)
                        }
                    }
                }
                return nil
            }.waitUntilFinished()

        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members " + caregroupId)

        for member in memberlist {
            let groupmember = self.getGroupmember(userId: member.userid, caregroupId: member.caregroupId)
            groupmembers.append(groupmember!)
        }
        

        trace.end()

        return groupmembers
    }

    func removeGroupmember(userId: String, caregroupId: String, memberId: String) -> String?  { // return/errorcode
        trace.begin()
        self.trace.output("CloudLayer - removeGroupmember")
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId
        pathParameters["caregroupid"] = caregroupId
        pathParameters["memberid"] = memberId

        awsClient!.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result as? AWSGroupMember {
                    self.trace.output(result.description ?? "")
            }
            return nil
            }.waitUntilFinished()
        
        trace.end()
        return "test RC"
    }
    
    func addGroupMessage(userid: String, caregroupId: String, messagetext: String) ->String? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["messagetext"] = messagetext
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        pathParameters["caregroupid"] = caregroupId
        
        var messageID: String?

        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/messages", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSMessage.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        messageID = result.description
                        self.trace.output("CloudLayer::addGroupMessage POST Message messageID = \(String(describing: messageID))")
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/messages")
        trace.end()
        
        return messageID
    }
    
    
    func getGroupMessages(caregroupId: String) -> [Message]? {
 
//        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = caregroupId
        
        // Find the messages in the caregroup
        var groupmessages = [Message]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/messages", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSMessage.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error as NSError? {
                //self.trace.error("Error occurred: \(error)")
                DispatchQueue.main.async {
                    Helper.httpErrorHandler(error: error)
                }
                return nil
            }
            else
                if let result = task?.result {
//                    self.trace.output(result.description ?? "")
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSMessage]  {
                            let gmsg = Message()
                            //gmsg.xx = x.partitionkey ?? ""
                            gmsg.messageId = x.sortkey ?? ""
                            //gmsg.xxx = x.dataSk ?? ""
                            gmsg.messagetext = x.messagetext ?? ""
                            gmsg.senderid = x.senderid ?? ""
                            gmsg.systemgenerated = (x.systemgenerated?.boolValue ?? false)
                            gmsg.systemtextid = x.systemtextid ?? ""
                            
                            //self.trace.output(x.createddate ?? "")
                            let dateString = x.createddate
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
                            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            let now = dateFormatter.string( from: Date() )
                            let date = dateFormatter.date(from: dateString ?? now )
                            if date != nil {
                                //self.trace.output(dateFormatter.string(from: date! ))
                                gmsg.createddate = date!
                            }
                            groupmessages.append(gmsg)
//                            self.trace.output("CloudLayer::getGroupMessages GET getGroupMessages key = \(String(describing: gmsg.messageId))")
                        }
                    }
                }
                return nil
            }.waitUntilFinished()
        
//        trace.end()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/messages")

        return groupmessages
        
    }
    
    func getMessageGroup(messageId: String) -> String? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = "dummy"
        pathParameters["messageid"] = messageId
        
        // Find the members in the caregroup
        var caregroupId: String? = nil
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/messages/{messageid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSMessage.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSMessage]  {
                            caregroupId = x.partitionkey ?? ""
                        }
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/messages/{messageid}")
        
        trace.end()
        
        return caregroupId
    }
    

    func addSupervision(supervision: Supervision) ->String? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["status"] = supervision.status.description
        queryParameters["countdowntime"] = Helper.hhColonMm(timeofday: supervision.countdownTime)
        if (supervision.deadline1 != nil) {
            queryParameters["deadline1"] = Helper.hhColonMm(timeofday: supervision.deadline1!)
        } else {
            queryParameters["deadline1"] = " "
        }
        if (supervision.deadline2 != nil) {
            queryParameters["deadline2"] = Helper.hhColonMm(timeofday: supervision.deadline2!)
        } else {
            queryParameters["deadline2"] = " "
        }
        if (supervision.deadline3 != nil) {
            queryParameters["deadline3"] = Helper.hhColonMm(timeofday: supervision.deadline3!)
        } else {
            queryParameters["deadline3"] = " "
        }
        if (supervision.interval != nil) {
            queryParameters["interval"] = Helper.hhColonMm(timeofday: supervision.interval!)
        } else {
            queryParameters["interval"] = " "
        }
        queryParameters["supervisiontype"] = supervision.supervisionType.description
        if (supervision.periodStart != nil) {
            queryParameters["periodstart"] = Helper.utcTime(date: supervision.periodStart!)
        } else {
            queryParameters["periodstart"] = " "
        }
        if (supervision.periodEnd != nil) {
            queryParameters["periodend"] = Helper.utcTime(date: supervision.periodEnd!)
        } else {
            queryParameters["periodend"] = " "
        }
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = supervision.groupmemberId
        pathParameters["userid"] = supervision.userId
        pathParameters["caregroupid"] = supervision.caregroupId
        if (supervision.endingType != nil) {
            queryParameters["endingtype"] = supervision.endingType!.description
        } else {
            queryParameters["endingtype"] = " "
        }
        if (supervision.wifi != nil) {
            queryParameters["wifi"] = supervision.wifi!
        } else {
            queryParameters["wifi"] = " "
        }
        if (supervision.locationInterval != nil) {
            queryParameters["locationinterval"] = supervision.locationInterval!
        } else {
            queryParameters["locationinterval"] = " "
        }
        if (supervision.locationLatitude != nil) {
            queryParameters["locationlatitude"] = supervision.locationLatitude!
        } else {
            queryParameters["locationlatitude"] = " "
        }
        if (supervision.locationLongitude != nil) {
            queryParameters["locationlongitude"] = supervision.locationLongitude!
        } else {
            queryParameters["locationlongitude"] = " "
        }
        if (supervision.trackingId != nil) {
            queryParameters["trackingid"] = supervision.trackingId!
        } else {
            queryParameters["trackingid"] = " "
        }

        var supervisionId: String?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions",
                                     pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self).continueWith { (task: AWSTask?) -> AnyObject? in
                                        if let error = task?.error {
                                            self.trace.error("Error occurred: \(error)")
                                            return nil
                                        }
                                        else if let result = task?.result as? AWSSupervision {
                                            self.trace.output(result.description)
                                            if result is AWSSupervision {  // if let result = task?.result as? AWSGroupMember
                                                supervisionId = result.sortkey
                                            }
                                            self.trace.output("CloudLayer::addSupervision POST Message messageID = \(String(describing: supervisionId))")
                                            return nil
                                        } else {
                                            self.trace.output(task?.description)
                                            return nil
                                        }
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "POST /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions")
        trace.end()
        
        return supervisionId
    }
    
    func updateSupervision(supervision: Supervision) -> String? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["status"] = supervision.status.description
        queryParameters["countdowntime"] = Helper.hhColonMm(timeofday: supervision.countdownTime)
        if (supervision.deadline1 ?? -1 > -1) {
            queryParameters["deadline1"] = Helper.hhColonMm(timeofday: supervision.deadline1!)
        } else {
            queryParameters["deadline1"] = " "
        }
        if (supervision.deadline2 ?? -1 > -1) {
            queryParameters["deadline2"] = Helper.hhColonMm(timeofday: supervision.deadline2!)
        } else {
            queryParameters["deadline2"] = " "
        }
        if (supervision.deadline3 ?? -1 > -1) {
            queryParameters["deadline3"] = Helper.hhColonMm(timeofday: supervision.deadline3!)
        } else {
            queryParameters["deadline3"] = " "
        }
        if (supervision.interval ?? -1 > -1) {
            queryParameters["interval"] = Helper.hhColonMm(timeofday: supervision.interval!)
        } else {
            queryParameters["interval"] = " "
        }
        queryParameters["supervisiontype"] = supervision.supervisionType.description
        if (supervision.periodStart != nil) {
            queryParameters["periodstart"] = Helper.utcTime(date: supervision.periodStart!)
        } else {
            queryParameters["periodstart"] = " "
        }
        if (supervision.periodEnd != nil) {
            queryParameters["periodend"] = Helper.utcTime(date: supervision.periodEnd!)
        } else {
            queryParameters["periodend"] = " "
        }
        var pathParameters:[String:Any] = [:]
        pathParameters["supervisionid"] = supervision.supervisionId
        pathParameters["memberid"] = supervision.groupmemberId
        pathParameters["userid"] = supervision.userId
        pathParameters["caregroupid"] = supervision.caregroupId
        if (supervision.endingType != nil) {
            queryParameters["endingtype"] = supervision.endingType!.description
        } else {
            queryParameters["endingtype"] = " "
        }
        if (supervision.wifi != nil) {
            queryParameters["wifi"] = supervision.wifi!
        } else {
            queryParameters["wifi"] = " "
        }
        if (supervision.locationInterval != nil) {
            queryParameters["locationinterval"] = supervision.locationInterval!
        } else {
            queryParameters["locationinterval"] = " "
        }
        if (supervision.locationLatitude != nil) {
            queryParameters["locationlatitude"] = supervision.locationLatitude!
        } else {
            queryParameters["locationlatitude"] = " "
        }
        if (supervision.locationLongitude != nil) {
            queryParameters["locationlongitude"] = supervision.locationLongitude!
        } else {
            queryParameters["locationlongitude"] = " "
        }
        if (supervision.trackingId != nil) {
            queryParameters["trackingid"] = supervision.trackingId!
        } else {
            queryParameters["trackingid"] = " "
        }

        var supervisionId: String?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}",
                                     pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self).continueWith { (task: AWSTask?) -> AnyObject? in
                                        if let error = task?.error {
                                            self.trace.error("Error occurred: \(error)")
                                            return nil
                                        }
                                        else
                                            if let result = task?.result {
                                                self.trace.output(result.description ?? "")
                                                if result is String {
                                                    supervisionId = result.description
                                                    self.trace.output("CloudLayer::updateSupervision PATCH Message messageID = \(String(describing: supervisionId))")
                                                    supervision.supervisionId = supervisionId!
                                                }
                                        }
                                        return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "PATCH /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}")
        trace.end()
        
        return supervisionId
    }

    func deleteSupervision(supervision: Supervision) -> String? {
        trace.begin()
        self.trace.output("CloudLayer - deleteSupervision")
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = supervision.userId
        pathParameters["caregroupid"] = supervision.caregroupId
        pathParameters["memberid"] = supervision.groupmemberId
        pathParameters["supervisionid"] = supervision.supervisionId

        awsClient!.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result as? AWSSupervision {
                    self.trace.output(result.description ?? "")
            }
            return nil
            }.waitUntilFinished()
        
        trace.end()
        return "test rc"
    }

    func getSupervisions(groupmemberId: String) -> [Supervision]? {

        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = groupmemberId
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = "dummy"
        
        var supervisions = [Supervision]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSSupervision]  {
                            let sv = Supervision()
                            
                            sv.groupmemberId = x.partitionkey!
                            sv.supervisionId = x.sortkey!
                            sv.status = SupervisionStatusType.enumFromString(string: x.dataSk!)!
                            sv.deadline1 = x.deadline1 == nil ? nil : Helper.intervalFromTime(time: x.deadline1!)
                            sv.deadline2 = x.deadline2 == nil ? nil : Helper.intervalFromTime(time: x.deadline2!)
                            sv.deadline3 = x.deadline3 == nil ? nil : Helper.intervalFromTime(time: x.deadline3!)
                            sv.countdownTime = Helper.intervalFromTime(time: x.countdowntime ?? "  :  ")
                            sv.interval = x.interval == nil ? nil : Helper.intervalFromTime(time: x.interval!)
                            sv.periodStart = Helper.dateFromUTC(utc: x.periodstart ?? "")
                            sv.periodEnd = Helper.dateFromUTC(utc: x.periodend ?? "")
                            sv.supervisionType = SupervisionType.enumFromString(string: x.supervisiontype ?? "")!
                            sv.userId = x.userid!
                            sv.caregroupId = x.caregroupid!
                            sv.endingType = x.endingtype == nil ? nil : SupervisionEndingType.enumFromString(string: x.endingtype!)
                            sv.wifi = x.wifi ?? nil
                            sv.locationInterval = x.locationinterval == nil ? nil : Int(x.locationinterval!)
                            sv.locationLatitude = x.locationlatitude == nil ? nil : Double(x.locationlatitude!)
                            sv.locationLongitude = x.locationlongitude == nil ? nil : Double(x.locationlongitude!)
                            sv.trackingId = x.trackingid == nil ? nil : x.trackingid
                            self.trace.output(result.description)
                            self.trace.output(sv.description)

                            supervisions.append(sv)
                        }
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions " + groupmemberId)
        trace.end()
        
        return supervisions
        
    }
    
    func getSupervision(groupmemberId: String, supervisionId: String) -> Supervision? {

        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = groupmemberId
        pathParameters["supervisionid"] = supervisionId
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = "dummy"
        
        var supervision : Supervision?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let x = task?.result as? AWSSupervision {
                    supervision = Supervision()
                    
                    supervision!.groupmemberId = x.partitionkey!
                    supervision!.supervisionId = x.sortkey!
                    supervision!.status = SupervisionStatusType.enumFromString(string: x.dataSk!)!
                    supervision!.deadline1 = x.deadline1 == nil ? nil : Helper.intervalFromTime(time: x.deadline1!)
                    supervision!.deadline2 = x.deadline2 == nil ? nil : Helper.intervalFromTime(time: x.deadline2!)
                    supervision!.deadline3 = x.deadline3 == nil ? nil : Helper.intervalFromTime(time: x.deadline3!)
                    supervision!.countdownTime = Helper.intervalFromTime(time: x.countdowntime ?? "  :  ")
                    supervision!.interval = x.interval == nil ? nil : Helper.intervalFromTime(time: x.interval!)
                    supervision!.periodStart = Helper.dateFromUTC(utc: x.periodstart ?? "")
                    supervision!.periodEnd = Helper.dateFromUTC(utc: x.periodend ?? "")
                    supervision!.supervisionType = SupervisionType.enumFromString(string: x.supervisiontype ?? "")!
                    supervision!.userId = x.userid!
                    supervision!.caregroupId = x.caregroupid!
                    supervision!.endingType = x.endingtype == nil ? nil : SupervisionEndingType.enumFromString(string: x.endingtype!)
                    supervision!.wifi = x.wifi ?? nil
                    supervision!.locationInterval = x.locationinterval == nil ? nil : Int(x.locationinterval!)
                    supervision!.locationLatitude = x.locationlatitude == nil ? nil : Double(x.locationlatitude!)
                    supervision!.locationLongitude = x.locationlongitude == nil ? nil : Double(x.locationlongitude!)
                    supervision!.trackingId = x.trackingid == nil ? nil : x.trackingid
                    self.trace.output(supervision!.description)
                }
                return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}")
        trace.end()
        
        return supervision
    }
    
    func addCheckpoint(checkpoint: Checkpoint) -> String {
        trace.begin()
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["type"] = checkpoint.type.description
        queryParameters["time"] = Helper.utcTime(date: checkpoint.time)
        queryParameters["notifiedcount"] = checkpoint.notifiedCount
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = checkpoint.groupmemberId
        pathParameters["userid"] = checkpoint.userId
        pathParameters["caregroupid"] = checkpoint.caregroupid
        pathParameters["supervisionid"] = checkpoint.supervisionId

        var resultValue = ""
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/checkpoints", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLifesign.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error as NSError? {
                //self.trace.error("Error occurred: \(error)")
                DispatchQueue.main.async {
                    Helper.httpErrorHandler(error: error)
                }
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        resultValue = result.description
                        self.trace.output("CloudLayer::addCheckpoint POST Lifesign for supervisionId \(checkpoint.supervisionId)")
                    }
            }
            return nil
            }.waitUntilFinished()

        trace.end()

        self.trace.output(start: before, text: "POST /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/checkpoints")

        return resultValue
    }
    

    func getCheckpoints(supervisionid: String) -> [Checkpoint]? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = "dummy"
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = "dummy"
        pathParameters["supervisionid"] = supervisionid

        // Find the messages in the caregroup
        var checkpoints = [Checkpoint]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/checkpoints",
                                     pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCheckpoints.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSCheckpoints]  {
                            let c = Checkpoint()
                            
                            c.supervisionId = x.partitionkey!
                            c.type = CheckpointType.enumFromString(string: x.sortkey!)!
                            c.caregroupid = x.caregroupid!
                            c.groupmemberId = x.memberid!
                            c.notifiedCount = Int(truncating: x.notifiedcount ?? 0)
                            c.userId = x.userid!
                            c.time = Helper.dateFromUTC(utc: x.dataSk ?? "") ?? Date()
                            self.trace.output(c.description)
                            
                            checkpoints.append(c)
                        }
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/checkpoints " + supervisionid)
        trace.end()
        
        return checkpoints
    }

    func deleteCheckpoints(supervisionid: String) -> String? {
        trace.begin()
        self.trace.output("CloudLayer - deleteCheckpoints")
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = "dummy"
        pathParameters["memberid"] = "dummy"
        pathParameters["supervisionid"] = supervisionid

        awsClient!.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/checkpoints", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCheckpoints.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else if let result = task?.result as? AWSCheckpoints {
                self.trace.output(result.description)
            }
            return nil
            }.waitUntilFinished()
        
        trace.end()
        return "test rc"
    }

    
    func getLifesigns(userid: String) -> [LifeSign]? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        
        // Find the messages in the caregroup
        var lifesigns = [LifeSign]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/lifesigns", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLifesign.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSLifesign]  {
                            let l = LifeSign()
                            
                            l.userId = x.partitionkey!
                            l.lifesignId = x.sortkey!
                            l.userMinutesBeforeUTC = Int(truncating: x.userminutesbeforeutc ?? 0)
                            // l.location = x.location ?? ""
                            l.latestLifesign = Helper.dateFromUTC(utc: x.dataSk ?? "") ?? Date()
                            self.trace.output(l.description)
                            
                            lifesigns.append(l)
                        }
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/lifesigns")
        trace.end()
        
        return lifesigns
    }
    
    func getAlarms(supervisionid: String) -> [Alarm]? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = "dummy"
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = "dummy"
        pathParameters["supervisionid"] = supervisionid

        // Find the messages in the caregroup
        var alarms = [Alarm]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/alarms", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSAlarm.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSAlarm]  {
                            let a = Alarm()
                            
                            a.supervisionId = x.partitionkey!
                            a.alarmId = x.sortkey!
                            a.date = Helper.dateFromUTC(utc: x.dataSk ?? "") ?? Date()
                            a.escalationTime = Helper.dateFromUTC(utc: x.escalationtime ?? "") ?? Date()
                            a.surveiledUserId = x.surveileduser ?? ""
                            a.lastKnownPosition = x.lastknowposition ?? ""
                            a.status = AlarmStatusType.enumFromString(string: x.status!)!
                            a.caregroupId = x.caregroupid ?? ""
                            a.groupmemberId = x.memberid ?? ""
                            
                            self.trace.output(a.description)
                            
                            alarms.append(a)
                        }
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/alarms")
        trace.end()
        
        return alarms
    }

    func getLocation(trackingId: String, locationsId: String) -> Location? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = "dummy"
        pathParameters["userid"] = "dummy"
        pathParameters["caregroupid"] = "dummy"
        pathParameters["supervisionid"] = "dummy"
        pathParameters["trackingid"] = trackingId
        pathParameters["locationid"] = locationsId

        // Find the messages in the caregroup
        var location : Location?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations/{locationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLocation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else if let x = task?.result as? AWSLocation {
                location = Location()
                
                location!.trackingId = x.partitionkey!
                location!.locationId = x.sortkey!
                location!.time = Helper.dateFromUTC(utc: x.dataSk ?? "") ?? Date()
                location!.userId = x.userid ?? ""
                location!.caregroupId = x.caregroupid ?? ""
                location!.groupmemberId = x.memberid ?? ""
                location!.supervisionId = x.supervisionid ?? ""
                location!.latitude = Double(truncating: x.latitude ?? 0)
                location!.longitude = Double(truncating: x.longitude ?? 0)
                location!.address = x.address ?? ""
                location!.onSafe = (x.onsafe?.boolValue ?? false)
                location!.leftSafe = (x.leftsafe?.boolValue ?? false)
                location!.backSafe = (x.backsafe?.boolValue ?? false)

                self.trace.output(location!.description)
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations/{locationid}")
        trace.end()
        
        return location
    }
    
    func getLocations(supervision: Supervision) -> [Location] {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = supervision.groupmemberId
        pathParameters["userid"] = supervision.userId
        pathParameters["caregroupid"] = supervision.caregroupId
        pathParameters["supervisionid"] = supervision.supervisionId
        pathParameters["trackingid"] = supervision.trackingId ?? ""

        // Find the messages in the caregroup
        var locations = [Location]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLocation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSLocation]  {
                            let location = Location()
                            
                            location.trackingId = x.partitionkey!
                            location.locationId = x.sortkey!
                            location.time = Helper.dateFromUTC(utc: x.dataSk ?? "") ?? Date()
                            location.userId = x.userid ?? ""
                            location.caregroupId = x.caregroupid ?? ""
                            location.groupmemberId = x.memberid ?? ""
                            location.supervisionId = x.supervisionid ?? ""
                            location.latitude = Double(truncating: x.latitude ?? 0)
                            location.longitude = Double(truncating: x.longitude ?? 0)
                            location.address = x.address ?? ""
                            location.onSafe = (x.onsafe?.boolValue ?? false)
                            location.leftSafe = (x.leftsafe?.boolValue ?? false)
                            location.backSafe = (x.backsafe?.boolValue ?? false)

                            self.trace.output(location.description)
                            
                            locations.append(location)
                        }
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations")
        trace.end()
        
        return locations
    }
    
    func addLocation(location: Location) ->String? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = location.groupmemberId
        pathParameters["userid"] = location.userId
        pathParameters["caregroupid"] = location.caregroupId
        pathParameters["supervisionid"] = location.supervisionId
        pathParameters["trackingid"] = location.trackingId
        pathParameters["locationid"] = location.locationId

        var queryParameters:[String:Any] = [:]
        queryParameters["time"] = Helper.utcTime(date: location.time)
        queryParameters["latitude"] = location.latitude
        queryParameters["longitude"] = location.longitude
        queryParameters["address"] = (location.address != "") ? location.address :" "
        if (location.onSafe) {
            queryParameters["onsafe"] = "1"
        } else {
            queryParameters["onsafe"] = "0"
        }
        if (location.leftSafe) {
            queryParameters["leftsafe"] = "1"
        } else {
            queryParameters["leftsafe"] = "0"
        }
        if (location.backSafe) {
            queryParameters["backsafe"] = "1"
        } else {
            queryParameters["backsafe"] = "0"
        }

        let before = Date()
        var locationId : String?
        
        awsClient!.invokeHTTPRequest("PUT", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations/{locationid}",
                                     pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLocation.self).continueWith { (task: AWSTask?) -> AnyObject? in
                                        if let error = task?.error {
                                            self.trace.error("Error occurred: \(error)")
                                            return nil
                                        }
                                        else
                                            if let result = task?.result {
                                                self.trace.output(result.description ?? "")
                                                if result is String {
                                                    locationId = result.description
                                                    self.trace.output("CloudLayer::addLocation PUT = \(String(describing: locationId))")
                                                }
                                        }
                                        return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "PUT /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations/{locationid}")
        trace.end()
        
        return locationId
    }
    
    func getMainStatusList(userId: String, caregroupId: String) -> [MainStatus] {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId
        pathParameters["caregroupid"] = caregroupId

        // Find the messages in the caregroup
        var mainStatusList = [MainStatus]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/mainstatus", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSMainStatus.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error as NSError? {
                //self.trace.error("Error occurred: \(error)")
                DispatchQueue.main.async {
                    Helper.httpErrorHandler(error: error)
                }
                return nil
            }
            else
                if let result = task?.result {
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSMainStatus]  {
                            let mainStatus = MainStatus()
                            
                            mainStatus.caregroupId = x.partitionkey!
                            mainStatus.supervisionId = x.sortkey!
                            mainStatus.userId = x.dataSk!
                            mainStatus.userName = x.username!
                            mainStatus.supervisionType = SupervisionType.enumFromString(string: x.supervisiontype!) ?? SupervisionType.Ongoing
                            mainStatus.caregroupName = x.caregroupname ?? " "
                            mainStatus.groupmemberId = x.memberid ?? " "
                            mainStatus.countdownTime = Helper.dateFromUTC(utc: x.countdowntime ?? "") ?? nil
                            mainStatus.deadlineTime = Helper.dateFromUTC(utc: x.deadlinetime ?? "") ?? nil
                            mainStatus.endedTime = Helper.dateFromUTC(utc: x.endedtime ?? "") ?? nil
                            mainStatus.periodEnd = Helper.dateFromUTC(utc: x.periodend ?? "") ?? nil
                            if (x.trackingstatus == nil) {
                                mainStatus.trackingStatus = nil
                            } else {
                                if (x.trackingstatus == " ") {
                                    mainStatus.trackingStatus = nil
                                } else {
                                    mainStatus.trackingStatus = TrackingStatusType.enumFromString(string: x.trackingstatus!) ?? TrackingStatusType.Upcoming
                                }
                            }
                            mainStatus.endedTime = Helper.dateFromUTC(utc: x.endedtime ?? "") ?? nil
                            mainStatus.leftSafe = x.leftsafe ?? "0" == "1" ? true : false
                            mainStatus.onSafe = x.onsafe ?? "0" == "1" ? true : false
                            mainStatus.backSafe = x.backsafe ?? "0" == "1" ? true : false

                            self.trace.output(mainStatus.description)
                            
                            mainStatusList.append(mainStatus)
                        }
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/mainstatus")
        trace.end()
        
        return mainStatusList
    }


    func getTrackings(supervision: Supervision) -> [Tracking] {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = supervision.groupmemberId
        pathParameters["userid"] = supervision.userId
        pathParameters["caregroupid"] = supervision.caregroupId
        pathParameters["supervisionid"] = supervision.supervisionId
        pathParameters["trackingid"] = supervision.trackingId ?? ""

        // Find the messages in the caregroup
        var trackings = [Tracking]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLocation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    if result is NSArray {
                        let res = result as! NSArray
                        for x in res as! [AWSTracking]  {
                            let tracking = Tracking()
                            
                            tracking.trackingId = x.partitionkey!
                            tracking.starttime = Helper.dateFromUTC(utc: x.sortkey ?? "") ?? Date()
                            tracking.status = TrackingStatusType.enumFromString(string: x.dataSk!)!
                            tracking.onSafe = (x.onsafe?.boolValue ?? false)
                            tracking.leftSafe = (x.leftsafe?.boolValue ?? false)
                            tracking.backSafe = (x.backsafe?.boolValue ?? false)

                            tracking.userId = x.userid ?? ""
                            tracking.caregroupId = x.caregroupid ?? ""
                            tracking.groupmemberId = x.memberid ?? ""
                            tracking.supervisionId = x.supervisionid ?? ""
                            tracking.latestLocationId = x.latestlocationid == nil ? nil : x.latestlocationid

                            self.trace.output(tracking.description)
                            
                            trackings.append(tracking)
                        }
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings")
        trace.end()
        
        return trackings
    }

    func getTracking(trackingid: String) -> Tracking? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = ""
        pathParameters["userid"] = ""
        pathParameters["caregroupid"] = ""
        pathParameters["supervisionid"] = ""
        pathParameters["trackingid"] = trackingid

        var tracking : Tracking?
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSTracking.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else if let result = task?.result as? AWSTracking {
                tracking = Tracking()
                
                tracking!.trackingId = result.partitionkey!
                tracking!.starttime = Helper.dateFromUTC(utc: result.sortkey ?? "") ?? Date()
                tracking!.status = TrackingStatusType.enumFromString(string: result.dataSk!)!
                tracking!.onSafe = (result.onsafe?.boolValue ?? false)
                tracking!.leftSafe = (result.leftsafe?.boolValue ?? false)
                tracking!.backSafe = (result.backsafe?.boolValue ?? false)

                tracking!.userId = result.userid!
                tracking!.caregroupId = result.caregroupid!
                tracking!.groupmemberId = result.memberid!
                tracking!.supervisionId = result.supervisionid!
                tracking!.latestLocationId = result.latestlocationid == nil ? nil : result.latestlocationid

                self.trace.output(tracking!.description)
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid} " + trackingid)
        trace.end()
        
        return tracking
    }

    func updateTracking(tracking: Tracking) -> String? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["starttime"] = Helper.utcTime(date: tracking.starttime)
        queryParameters["status"] = tracking.status.description
        if (tracking.onSafe) {
            queryParameters["onsafe"] = "1"
        } else {
            queryParameters["onsafe"] = "0"
        }
        if (tracking.leftSafe) {
            queryParameters["leftsafe"] = "1"
        } else {
            queryParameters["leftsafe"] = "0"
        }
        if (tracking.backSafe) {
            queryParameters["backsafe"] = "1"
        } else {
            queryParameters["backsafe"] = "0"
        }
        if (tracking.latestLocationId != nil) {
            queryParameters["latestlocationid"] = tracking.latestLocationId!
        } else {
            queryParameters["latestlocationid"] = " "
        }
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = tracking.groupmemberId
        pathParameters["userid"] = tracking.userId
        pathParameters["caregroupid"] = tracking.caregroupId
        pathParameters["supervisionid"] = tracking.supervisionId
        pathParameters["trackingid"] = tracking.trackingId

        // Find the messages in the caregroup

        let before = Date()
        var trackingId : String?
        
        awsClient!.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSTracking.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        trackingId = result.description
                        self.trace.output("CloudLayer::updateTracking PATCH Tracking trackingId = \(String(describing: trackingId))")
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "PATCH /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}")
        trace.end()
        
        return trackingId
    }
    
    func addTracking(tracking: Tracking) -> String? {
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var pathParameters:[String:Any] = [:]
        pathParameters["memberid"] = tracking.groupmemberId
        pathParameters["userid"] = tracking.userId
        pathParameters["caregroupid"] = tracking.caregroupId
        pathParameters["supervisionid"] = tracking.supervisionId
        pathParameters["trackingid"] = tracking.trackingId

        var queryParameters:[String:Any] = [:]
        queryParameters["starttime"] = Helper.utcTime(date: tracking.starttime)
        queryParameters["status"] = tracking.status.description
        if (tracking.onSafe) {
            queryParameters["onsafe"] = "1"
        } else {
            queryParameters["onsafe"] = "0"
        }
        if (tracking.leftSafe) {
            queryParameters["leftsafe"] = "1"
        } else {
            queryParameters["leftsafe"] = "0"
        }
        if (tracking.backSafe) {
            queryParameters["backsafe"] = "1"
        } else {
            queryParameters["backsafe"] = "0"
        }
        if (tracking.latestLocationId != nil) {
            queryParameters["latestlocationid"] = tracking.latestLocationId!
        } else {
            queryParameters["latestlocationid"] = " "
        }

        let before = Date()
        var trackingid : String?
        
        awsClient!.invokeHTTPRequest("PUT", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}",
                                     pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLocation.self).continueWith { (task: AWSTask?) -> AnyObject? in
                                        if let error = task?.error {
                                            self.trace.error("Error occurred: \(error)")
                                            return nil
                                        }
                                        else
                                            if let result = task?.result {
                                                self.trace.output(result.description ?? "")
                                                if result is String {
                                                    trackingid = result.description
                                                    self.trace.output("CloudLayer::addTracking PUT = \(String(describing: trackingid))")
                                                }
                                        }
                                        return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "PUT /users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid} ")
        trace.end()
        
        return trackingid
    }
    
    func getUserID(email: String) -> String? {
        
        trace.begin()
        
        let headerParameters = headerNoToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["email"] = email
        
        let pathParameters:[String:Any] = [:]
        
        var userID: String?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users", pathParameters: pathParameters,
                                     queryParameters: queryParameters, headerParameters: headerParameters,
                                     body: nil, responseClass: AWSUser.self).continueWith { (task: AWSTask?) -> AnyObject? in
                                        if let error = task?.error {
                                            self.trace.error("Error occurred: \(error)")
                                            return nil
                                        }
                                        else {
                                            if let results = task?.result {
                                                self.trace.output(results.description ?? "")
                                                if results is NSArray {
                                                    let result = results as! NSArray
                                                    for x in result as! [AWSUser] {
                                                        userID = x.partitionkey
                                                        self.trace.output("CloudLayer::getUserID GET user ID = \(String(describing: userID))")
                                                    }
                                                    if (result.count > 1) {
                                                        self.trace.error("Found \(result.count) entries of \(email). User email should ONLY exist with one entry.")
                                                    }
                                                }
                                            }
                                        }
                                        return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users")
        trace.end()
        
        return userID
    }
    
    func createUser(user: User) -> User {
        
        trace.begin()
        
        let postedUser = user
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        
        queryParameters["name"] = user.userName
        queryParameters["email"] = user.email
        queryParameters["status"] = user.status.description
        queryParameters["platform"] = user.platform.description
        queryParameters["userminutesbeforeutc"] = user.userMinutesBeforeUTC

        let pathParameters:[String:Any] = [:]
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result as? AWSUser {
                    self.trace.output(result.description)
                    if result is AWSUser {
                        postedUser.userId = result.partitionkey ?? ""
                        self.trace.output("CloudLayer::createUser UserId from post = \(String(describing: result.partitionkey ?? ""))")
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "POST /users")
        trace.end()
        
        return postedUser
    }
    
    func updateUser(userid: String, user: User) -> User {
        
        trace.begin()
        
        let postedUser = user
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        
        var queryParameters:[String:Any] = [:]
        queryParameters["name"] = user.userName
        queryParameters["email"] = user.email
        queryParameters["status"] = user.status.description
        if (!user.cellno.isEmpty) { queryParameters["cellno"] = user.cellno }
        if (!user.deviceId.isEmpty) { queryParameters["deviceid"] = user.deviceId }
        queryParameters["platform"] = user.platform.description
        queryParameters["userminutesbeforeutc"] = user.userMinutesBeforeUTC

        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid    // to userId

        self.trace.output("UserId: \(userid)")
        self.trace.output("Name: \(user.userName)")
        self.trace.output("Email: \(user.email)")
        
        let before = Date()
        awsClient!.invokeHTTPRequest("PATCH", urlString: "/users/{userid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result as? AWSUser {
                    self.trace.output(result.description)
                    if result is AWSUser {
                        postedUser.userId = result.partitionkey ?? ""
                        self.trace.output("CloudLayer::updateUser UserId from post = \(String(describing: result.partitionkey ?? ""))")
                    }
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "PATCH /users/{userid}")
        trace.end()
        
        return postedUser
    }
    
    func getUsers() -> [User] {
        
        trace.begin()
        
        var users = [User]()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        let pathParameters:[String:Any] = [:]
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users", pathParameters: pathParameters,
                                     queryParameters: queryParameters, headerParameters: headerParameters,
                                     body: nil, responseClass: AWSUser.self).continueWith { (task: AWSTask?) -> AnyObject? in
                                        if let error = task?.error {
                                            self.trace.error("Error occurred: \(error)")
                                            return nil
                                        }
                                        else
                                            if let result = task?.result {
                                                if result is NSArray {
                                                    let res = result as! NSArray
                                                    for x in res as! [AWSUser]  {
                                                        let u = User()
                                                        u.userId = x.partitionkey ?? ""
                                                        u.userName = x.name ?? ""
                                                        u.cellno = x.cellno ?? ""
                                                        u.deviceId = x.deviceid ?? ""
                                                        u.email = x.dataSk ?? ""
                                                        u.userMinutesBeforeUTC = Int(truncating: x.userminutesbeforeutc ?? 0)

                                                        users.append(u)
                                                        
                                                        self.trace.output("CloudLayer::getUsers GET user with ID = \(String(describing: u.userId))")
                                                    }
                                                }
                                        }
                                        return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users")
        trace.end()
        
        return users
    }
    
    func addGroupInvitation(fromUserId: String, toUserId: String, caregroupId: String, invitation: String) -> String? {
        
        trace.begin()
        
        trace.output("from: \(fromUserId)")
        trace.output("to: \(toUserId)")
        trace.output("caregroupId: \(caregroupId)")
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        
        if(invitation != ""){
            queryParameters["invitationid"] = invitation   // Optional! Add to re-use current invitationId. If parameter is not included, then Lambda will create a new number
        }
        if(toUserId != ""){
            queryParameters["receiverid"] = toUserId
        }
        //queryParameters["groupmemberid"] = groupmemberId  //
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = fromUserId    // from
        pathParameters["caregroupid"] = caregroupId    // group to join
        
        
        var invitationId : String?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/invites", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        invitationId = result.description
                        self.trace.output("CloudLayer::addInvitation POST userId: \(String(describing: fromUserId)) as invitationId: \(String(describing: invitationId))")
                    }
                }
                return nil
            }
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "POST /users/{userid}/caregroups/{caregroupid}/invites")
        trace.end()
        
        return invitationId
    }
    

    
    /* Den original group invitation
    func addGroupInvitation(userId: String, caregroupId: String, senderId: String, invitation: String) -> String? {
        
        trace.begin()
        
        trace.output("from: \(senderId)")
        trace.output("to: \(userId)")
        trace.output("caregroupId: \(caregroupId)")
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["senderid"] = senderId   // from
        queryParameters["invitationid"] = invitation   // Optional! Add to re-use current invitationId. If parameter is not included, then Lambda will create a new number
        //queryParameters["groupmemberid"] = groupmemberId  //
        
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId    // to
        pathParameters["caregroupid"] = caregroupId    // group to join
        
        
        var invitationId : String?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/invites", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        invitationId = result.description
                        self.trace.output("CloudLayer::addInvitation POST userId: \(String(describing: userId)) as invitationId: \(String(describing: invitationId))")
                    }
                }
                return nil
            }
        }.waitUntilFinished()
        
        self.trace.output(start: before, text: "POST /users/{userid}/caregroups/{caregroupid}/invites")
        trace.end()
        
        return invitationId
    }
    */
    
    func updateGroupInvitation(userId: String, invitationId: String, status: InvitationStatusType) -> Invitation   {
        
        trace.begin()
        
        trace.output("Invitation: \(invitationId)")
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        
        var queryParameters:[String:Any] = [:]
        queryParameters["status"] = status.description
        
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId    // to userID
        pathParameters["invitationid"] = invitationId
        
        let invitation = Invitation()
        
        awsClient!.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/invites/{invitationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result as? AWSInvitation {
                    self.trace.output(result.description ?? "")
                    if result is AWSInvitation {
                        invitation.invitationToUserId = result.partitionkey ?? ""
                        invitation.invitationId = result.sortkey ?? ""
                        invitation.caregroupId = result.dataSk ?? ""
                        invitation.invitedByUserId = result.senderid ?? ""
                        invitation.status = InvitationStatusType.enumFromString(string: result.status ?? InvitationStatusType.Rejected.description) ?? InvitationStatusType.Rejected
                    }
                }
                return nil
            }.waitUntilFinished()
        
        trace.end()
        
        return invitation
    }
    
    func pushGroupInvitation(userId: String, invitationId: String, fromUser: String, toGroup: String) {

        trace.begin()
        
        trace.output("to userId: \(userId)")
        trace.output("Invitation: \(invitationId)")
        trace.output("from user: \(fromUser)")
        trace.output("group: \(toGroup)")

        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["sendername"] = fromUser   // The name of the user sending the invitation
        queryParameters["groupname"] = toGroup   //  The name of the group in the invitation
        
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId    // to userId
        pathParameters["invitationid"] = invitationId
        
        var invitationId : String?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/invites/{invitationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        invitationId = result.description
                        self.trace.output("CloudLayer::pushGroupInvitation HEAD userId: \(String(describing: userId)) as invitationId: \(String(describing: invitationId))")
                    }
                }
                return nil
            }
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "HEAD /users/{userid}/caregroups/{caregroupid}")
        trace.end()

    
    }
    
    func haveGroupInvitations(userId: String) -> Bool {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId
        
        //var invitation: AWSInvitation
        var invitations = [Invitation]()

        var haveInvitations : Bool = false
        
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/invites", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                self.trace.output("Result...")
                if let result = task?.result {
                    self.trace.output("Result: \(result.description ?? "")")
                    if result is NSArray {
                        self.trace.output("NSArray")
                        let res = result as! NSArray
                        for x in res as! [AWSInvitation]  {
                            self.trace.output("Element: \(x)")
                            if(x.status?.description == InvitationStatusType.Sent.description) {
                                let invitation = Invitation()
                                invitation.invitedByUserId = x.dataSk ?? ""
                                invitation.invitationId = x.sortkey ?? ""
                                //invitation.groupmemberId = x.partitionkey ?? ""
                                
                                invitations.append(invitation)
                                self.trace.output("#1 GET invitationId: \(String(describing: invitation.invitationId))")
                            }
                        }
                    }
                }
                return nil
            }
            
            }.waitUntilFinished()
        
        if invitations.count > 0 {
            haveInvitations = true
        }
        
        trace.output("haveInvitations = \(haveInvitations)")
        trace.end()
        
        return haveInvitations
    }
    
    func getGroupInvitation(userId: String, invitationId: String) -> Invitation? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId
        pathParameters["invitationid"] = invitationId
        
        let invitation = Invitation()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/invites/{invitationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                if let result = task?.result as? AWSInvitation {
                    self.trace.output(result.description ?? "")
                    if result is AWSInvitation {
                        invitation.invitationToUserId = result.partitionkey ?? ""
                        invitation.invitationId = result.sortkey ?? ""
                        invitation.caregroupId = result.dataSk ?? ""
                        invitation.invitedByUserId = result.senderid ?? ""
                        invitation.status = InvitationStatusType.enumFromString(string: result.status ?? InvitationStatusType.Rejected.description) ?? InvitationStatusType.Rejected
                    }
                }
                return nil
            }
        }.waitUntilFinished()
        
        trace.output("\(invitation)")
        self.trace.output(start: before, text: "GET /users/{userid}/invites/{invitationid}")
        trace.end()
        
        return invitation
    }
    
    func getGroupInvitations(userId: String) -> [Invitation]? {
        
        trace.begin()
        
        let headerParameters = headerWithToken() as [AnyHashable : Any]
        let queryParameters:[String:Any] = [:]
        
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId
        
        //var invitation: AWSInvitation
        var invitations = [Invitation]()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/invites", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else {
                if let result = task?.result {
                    self.trace.output("Result: \(result.description ?? "")")
                    if result is NSArray {
                        self.trace.output("NSArray")
                        let res = result as! NSArray
                        for x in res as! [AWSInvitation]  {
                            self.trace.output("Element: \(x)")
                            if(x.status?.description == InvitationStatusType.Sent.description) {
                                let invitation = Invitation()
                                invitation.invitationToUserId = x.partitionkey ?? ""
                                invitation.invitationId = x.sortkey ?? ""
                                invitation.caregroupId = x.dataSk ?? ""
                                invitation.invitedByUserId = x.senderid ?? ""
                                invitation.status = InvitationStatusType.enumFromString(string: x.status ?? InvitationStatusType.Rejected.description) ?? InvitationStatusType.Rejected
                                invitations.append(invitation)
                            }
                        }
                    }
                }
                return nil
            }

        }.waitUntilFinished()
        
        self.trace.output(start: before, text: "GET /users/{userid}/invites")
        trace.end()

        return invitations
    }
    
    func pushFeedback(userId: String, caregroupId: String, fromUser: String) {

        trace.begin()
        
        trace.output("userId: \(userId)")
        trace.output("user name: \(fromUser)")
        trace.output("Group: \(caregroupId)")

        let headerParameters = headerWithToken() as [AnyHashable : Any]
        var queryParameters:[String:Any] = [:]
        queryParameters["sendername"] = fromUser   // The name of the user sending the lifesign feedback
        
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userId    // Lifesign feedback from userId
        pathParameters["caregroupid"] = caregroupId
        
        var description : String?
        
        let before = Date()
        awsClient!.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/feedback", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self).continueWith { (task: AWSTask?) -> AnyObject? in
            if let error = task?.error as NSError? {
                //self.trace.error("Error occurred: \(error)")
                DispatchQueue.main.async {
                    Helper.httpErrorHandler(error: error)
                }
                return nil
            }
            else {
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is String {
                        description = result.description
                        self.trace.output("CloudLayer::pushFeedback POST userId: \(String(describing: userId)) to caregroupId: \(String(describing: caregroupId))")
                    }
                }
                return nil
            }
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "POST /users/{userid}/caregroups/{caregroupid}/feedback")
        trace.end()
        
    }
    
    func getAppStatus(userid: String) -> Status? {
        
        trace.begin()
        
        //var user : User?
        
        let headerParameters = headerNoToken() as [AnyHashable : Any]   // Must be NoToken to allow check of email BEFORE login
        let queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        
        let status = Status()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("GET", urlString: "/users/{userid}/status", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSStatus.self).continueWith { (task: AWSTask?) -> AnyObject? in
            
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is AWSStatus {
                        status.userId = result.sortkey ?? ""
                        status.lastUpdate = Helper.dateFromUTC(utc: result.createdate ?? "") ?? Date()
                        let cgResult : AWSStatus = (task?.result)! as! AWSStatus  // << SIMON
                        status.status = AppStatusType.enumFromString(string: cgResult.dataSk ?? AppStatusType.NotRunning.description) ?? AppStatusType.NotRunning
                        
                        self.trace.output("GET User status = \(String(describing: status.description))")
                    }
                }
                return nil
            }.waitUntilFinished()

        self.trace.output(start: before, text: "GET /users/{userid}/status")

        trace.end()
        
        return status
    }
    
    func setAppStatus(userid: String, status: AppStatusType) -> Status? {
        
        trace.begin()
    
        if (userid == "" ||
            self.getUserSessionState() != UserSessionStatusType.LoggedIn) {
            return nil
        }
        
        let headerParameters = headerNoToken() as [AnyHashable : Any]   // Must be NoToken to allow check of email BEFORE login
        var queryParameters:[String:Any] = [:]
        var pathParameters:[String:Any] = [:]
        pathParameters["userid"] = userid
        queryParameters["status"] = status.description
        
        let status = Status()
        
        let before = Date()
        awsClient!.invokeHTTPRequest("PUT", urlString: "/users/{userid}/status", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSStatus.self).continueWith { (task: AWSTask?) -> AnyObject? in
            
            if let error = task?.error {
                self.trace.error("Error occurred: \(error)")
                return nil
            }
            else
                if let result = task?.result {
                    self.trace.output(result.description ?? "")
                    if result is AWSStatus {
                        status.userId = result.sortkey ?? ""
                        status.lastUpdate = Helper.dateFromUTC(utc: result.dataSk ?? "") ?? Date()
                        let cgResult : AWSStatus = (task?.result)! as! AWSStatus  // << SIMON
                        status.status = AppStatusType.enumFromString(string: cgResult.dataSk ?? AppStatusType.NotRunning.description) ?? AppStatusType.NotRunning
                        
                        self.trace.output("PUT User status = \(String(describing: status.description))")
                    }
                }
                return nil
            }.waitUntilFinished()

        self.trace.output(start: before, text: "PUT /users/{userid}/status")

        trace.end()
        
        return status
    }
            
            
    
    /* AWS Cognito API-kald */
    
    func getUserSessionState() -> UserSessionStatusType {
        
        trace.begin()
        
        var status = UserSessionStatusType.Unknown
        if((UIApplication.shared.delegate as! AppDelegate).deviceLayer.getAuthMethod() != AuthenticationMethod.Unknown) {
            status = UserSessionStatusType.LoggedIn
        } else {
            status = UserSessionStatusType.LoggedOut
        }
        trace.output("status = \(status.description)")
        trace.end()
        return status
    }
    
    func signIn(email: String, password: String) -> NSError? {
        
        trace.begin()
        
        _ = self.setupCredentials(authMethod: AuthenticationMethod.User)
        
        let authDetails = AWSCognitoIdentityPasswordAuthenticationDetails(username: email, password: password)
        passwordAuthenticationCompletion?.set(result: authDetails)
        
        var loginError : NSError?
        
        let before = Date()
        awsUser?.getSession(email, password: password, validationData: nil).continueWith {[weak self] (task: AWSTask) -> AnyObject? in
            if let error = task.error as NSError? {
                //self!.trace.error("Error occurred: \(error)")
                DispatchQueue.main.async {
                    Helper.httpErrorHandler(error: error)
                }
                loginError = error as NSError
            } else {
                self!.awsUserSession = task.result
                self!.setAuthorizationToken(token: task.result?.accessToken?.tokenString)
                self!.trace.output("CloudLayer::signIn successfull login")
            }
            return nil
        }.waitUntilFinished()

        self.trace.output(start: before, text: "awsUser?.getSession")
        trace.end()
        
        return loginError
    }
    
    func newsignup(userName: String, email: String, password: String) -> SignupResult {
        
        trace.begin()
        
        var attributes = [AWSCognitoIdentityUserAttributeType]()
        
        if !email.isEmpty {
            let emailAttribute = AWSCognitoIdentityUserAttributeType()
            emailAttribute?.name = "email"
            emailAttribute?.value = email
            attributes.append(emailAttribute!)
        }
        
        let authDetails = AWSCognitoIdentityPasswordAuthenticationDetails(username: email.lowercased(), password: password)
        passwordAuthenticationCompletion?.set(result: authDetails)
        
        let signupResult = SignupResult()
        
        let before = Date()
        awsPool?.signUp(email.lowercased(), password: password, userAttributes: nil, validationData: nil).continueWith {(task: AWSTask) -> AnyObject? in
            if let error = task.error as NSError? {
                signupResult.error = error
                signupResult.status = NewUserStatusType.Unknown
            } else if let result = task.result {
                signupResult.email = email
                signupResult.user = userName
                signupResult.status = NewUserStatusType.Unknown
                
                self.trace.output("CloudLayer::signUp successfull login \(String(describing: result.description()))")
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "awsPool?.signUp")
        trace.end()
        
        return signupResult
    }
    
    func signup(userName: String, email: String, password: String) -> SignupResult {
 
        trace.begin()
        
        var attributes = [AWSCognitoIdentityUserAttributeType]()
        
        if !email.isEmpty {
            let emailAttribute = AWSCognitoIdentityUserAttributeType()
            emailAttribute?.name = "email"
            emailAttribute?.value = email
            attributes.append(emailAttribute!)
        }

        let authDetails = AWSCognitoIdentityPasswordAuthenticationDetails(username: email, password: password)
        passwordAuthenticationCompletion?.set(result: authDetails)
        
        let signupResult = SignupResult()
        
        let before = Date()
        awsPool?.signUp(userName, password: password, userAttributes: attributes, validationData: nil).continueWith {(task: AWSTask) -> AnyObject? in
            if let error = task.error as NSError? {
                signupResult.error = error
                signupResult.status = NewUserStatusType.Unknown
            } else if let result = task.result {
                signupResult.email = email
                signupResult.user = userName
                signupResult.status = NewUserStatusType.Unknown

                self.trace.output("CloudLayer::signUp successfull login \(String(describing: result.description()))")
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "awsPool?.signUp")
        trace.end()
        
        return signupResult
    }
    
    func forgotPassword(email: String) -> NSError? {
        
        trace.begin()
        
        var resultError : NSError?
        
        let before = Date()
        if let thisUser = awsPool?.getUser(email) {
            thisUser.forgotPassword().continueWith {(task: AWSTask) -> AnyObject? in
                if let error = task.error as NSError? {
                    self.trace.error("CloudLayer::forgotPassword error with response: \(error)")
                    resultError = error
                } else if let result = task.result {
                    self.trace.output("CloudLayer::forgotPassword successfull: \(result)")
                }
                return nil
                }.waitUntilFinished()
        } else {
            resultError = NSError.init(domain: "No known user with this email", code: 404, userInfo: nil)
        }
        self.trace.output(start: before, text: "forgotPassword()")

        trace.end()
        
        return resultError
    }

    func confirmForgotPassword(email: String, confirmationCodeValue: String, password: String) -> NSError? {
        
        trace.begin()
        
        var resultError : NSError?
        
        let before = Date()
        if let thisUser = awsPool?.getUser(email) {
            thisUser.confirmForgotPassword(confirmationCodeValue, password: password).continueWith {(task: AWSTask) -> AnyObject? in
                if let error = task.error as NSError? {
                    self.trace.error("CloudLayer::confirmForgotPassword error with response: \(error)")
                    resultError = error
                } else if let result = task.result {
                    self.trace.output("CloudLayer::confirmForgotPassword successfull: \(result)")
                }
                return nil
                }.waitUntilFinished()
        } else {
            resultError = NSError.init(domain: "No known user with this email", code: 404, userInfo: nil)
        }
        
        self.trace.output(start: before, text: "confirmForgotPassword")
        trace.end()
        
        return resultError
    }
    
    func changeEmail(email: String, newEmail: String) -> NSError? {
        
        trace.begin()
        
        var resultError : NSError?
        
        if (awsUser == nil) {
            resultError = NSError.init(domain: "No known user with this email", code: 404, userInfo: nil)
            return resultError
        }
        
        var attributes = [AWSCognitoIdentityUserAttributeType]()
        
        let attribute = AWSCognitoIdentityUserAttributeType()
        attribute!.name = "email"
        attribute!.value = newEmail
        trace.output("New email: \(newEmail)")
        attributes.append(attribute!)
        
        attribute!.name =  "email_verified"
        attribute!.value = "true"
        attributes.append(attribute!)
        
        let before = Date()
        
        awsUser!.update(attributes).continueWith {(task: AWSTask) -> AnyObject? in
            if let error = task.error as NSError? {
                self.trace.error("CloudLayer::changeEmail error with response: \(error)")
                resultError = error
            } else if let result = task.result {
                self.trace.output("CloudLayer::changeEmail successfull: \(result)")
            }
            return nil
        }.waitUntilFinished()

        
        self.trace.output(start: before, text: "changeEmail")
        trace.end()
        
        return resultError
    }
    
    func confirmNewEmail(email: String, code: String) -> NSError? {
        
        trace.begin()
        
        var resultError : NSError?
        
        if (awsUser == nil) {
            resultError = NSError.init(domain: "No known user with this email", code: 404, userInfo: nil)
            return resultError
        }
        
        let before = Date()
 
        awsUser!.verifyAttribute("email", code: code).continueWith {(task: AWSTask) -> AnyObject? in
            if let error = task.error as NSError? {
                self.trace.error("CloudLayer:confirmNewEmail error with response: \(error)")
                resultError = error
            } else if let result = task.result {
                self.trace.output("CloudLayer::confirmNewEmail successfull: \(result)")
            }
            return nil
        }.waitUntilFinished()
        
        self.trace.output(start: before, text: "confirmNewEmail")
        trace.end()
        
        return resultError
    }
    
    func isEmailConfirmed(email: String, newEmail: String) -> Bool {
        
        trace.begin()
        
        var resultError : NSError?
        var emailVerified : Bool = false
        
        if (awsUser == nil) {
            resultError = NSError.init(domain: "No known user with this email", code: 404, userInfo: nil)
            return false
        }
        
        let before = Date()
        
        awsUser!.getDetails().continueWith {(task: AWSTask) -> AnyObject? in
            if let error = task.error as NSError? {
                self.trace.error("CloudLayer::isEmailConfirmed error with response: \(error)")
                resultError = error
            } else if let result = task.result {
                self.trace.output("CloudLayer::isEmailConfirmed successfull: \(result)")
                
                let attributes = task.result?.userAttributes
                if let emailVerifiedAttribute = attributes?.filter({$0.name == "email_verified"}), emailVerifiedAttribute.count == 1  {
                    if (emailVerifiedAttribute[0].value == "false") {
                        self.trace.output("Email not verified")
                        emailVerified = false
                        //self.openEmailVerificationVc()
                    }else{
                        self.trace.output("Email verified")
                        emailVerified = true
                    }
                }
            }
            return nil
        }.waitUntilFinished()
        
        self.trace.output(start: before, text: "isEmailConfirmed")
        trace.end()
        
        return emailVerified
    }
    
    func changePassword(email: String, password: String, newPassword: String) -> NSError? {
        
        trace.begin()
        
        var resultError : NSError?
        
        if (awsUser == nil) {
            resultError = NSError.init(domain: "No known user with this email", code: 404, userInfo: nil)
            return resultError
        }

        let before = Date()

        awsUser!.changePassword(password, proposedPassword: newPassword).continueWith {(task: AWSTask) -> AnyObject? in
            if let error = task.error as NSError? {
                self.trace.error("CloudLayer:changePassword error with response: \(error)")
                resultError = error
            } else if let result = task.result {
                self.trace.output("CloudLayer::changePassword successfull: \(result)")
            }
            return nil
        }.waitUntilFinished()

        self.trace.output(start: before, text: "changePassword")
        trace.end()
        
        return resultError
    }

    func isCloudSessionSignedIn() -> Bool {
        
        trace.begin()
        var status = false
        
        if(awsUser == nil) {
            trace.output("No user signin with Email...")
            status = false
        } else {
            trace.output("Already signed in with Email? \(String(describing: awsUser?.isSignedIn))")
            status = awsUser!.isSignedIn
        }
        trace.end()
        return status
    }
    
    func getCloudSessionIdToken() -> String? {
        return awsUserSession?.idToken?.tokenString
    }
    
    func getCloudSessionUserName() -> String? {
        if(awsUser != nil) {
            return awsUser?.username
        } else {
            return "NONAME"
        }
    }
    
    func signOut() {
        
        // User logout
        if(self.awsUser != nil) {
            awsUser!.signOut()
            awsUser = nil
            awsPool = nil
            awsClient = nil
        }
    }
    
    func deleteCognitoUser() -> NSError? {
        
        trace.begin()
        
        var resultError : NSError?
        
        if (awsUser == nil) {
            resultError = NSError.init(domain: "No known user with this email", code: 404, userInfo: nil)
            return resultError
        }
        
        let before = Date()
        
        awsUser!.delete().continueWith {(task: AWSTask) -> AnyObject? in
            if let error = task.error as NSError? {
                self.trace.error("CloudLayer:deleteUser error with response: \(error)")
                resultError = error
            } else if let result = task.result {
                self.trace.output("CloudLayer::deleteUser successfull: \(result)")
            }
            return nil
            }.waitUntilFinished()
        
        self.trace.output(start: before, text: "deleteUser")
        trace.end()
        
        return resultError
    }
    
    func headerWithToken() -> [String : String?] {
        return [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": self.awsAuthorizationToken
        ]
    }

    func headerNoToken() -> [String : String?] {
        return [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
    }

    
    /*
    **  Amazon Simple Notification Service (SNS)
    **  Setup push notifications
    */
    
    func snsAuthneticateUser(userId: String, email: String) {
        
        trace.begin()
        
        let credentialProvider = AWSCognitoCredentialsProvider(regionType:.EUCentral1, identityPoolId: CognitoIdentityPoolId)
        trace.output("credentialProvider: \(credentialProvider)")
        
        // setup service configuration
        let serviceConfiguration = AWSServiceConfiguration(region: CognitoIdentityUserPoolRegion, credentialsProvider: credentialProvider)
        
        // create pool configuration
        let poolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: CognitoIdentityUserPoolAppClientId,
                                                                        clientSecret: CognitoIdentityUserPoolAppClientSecret,
                                                                        poolId: CognitoIdentityUserPoolId)
        
        // initialize user pool client
        AWSCognitoIdentityUserPool.register(with: serviceConfiguration, userPoolConfiguration: poolConfiguration, forKey: AWSCognitoUserPoolsSignInProviderKey)
        AWSServiceManager.default().defaultServiceConfiguration = serviceConfiguration
        trace.output("credentialProvider: \(credentialProvider)")
        
        self.successCounter += 1    //1
        
        /*
        // Retrieve your Amazon Cognito ID
        credentialProvider.getIdentityId().continueWith(block: { (task) -> AnyObject? in
            if (task.error != nil) {
                self.trace.output("Error: " + task.error!.localizedDescription)
            }
            else {
                // the task result will contain the identity id
                let cognitoId = task.result!
                self.trace.output("Cognito id: \(cognitoId)")
            }
            return task;
        })
        */
        
        trace.end()
    }
    
    func snsCreatePlatformARN(userId: String, email: String) -> String? {
        
        trace.begin()
        sns = AWSSNS.default()
        
        var platformApplicationARN : String?
        
        
        let listPlatformApplicationsInput = AWSSNSListPlatformApplicationsInput()
        sns!.listPlatformApplications(listPlatformApplicationsInput!).continueWith { (task) -> AnyObject? in
            
            if task.error != nil {
                self.trace.error("Error creating platform endpoint: \(String(describing: task.error))")
                    (UIApplication.shared.delegate as! AppDelegate).notificationHandler.errorMessage = task.error.debugDescription
                //NSAssertionHandler?(task.error as! NSAssertionHandler)
                return nil
            }
            
            self.successCounter += 1    //2
            
            let result = task.result as! AWSSNSListPlatformApplicationsResponse
            self.trace.output("listPlatformApplicationsResponse: \(result)")
            
            let platformApplications = result.platformApplications
            self.trace.output("Number of platformapplications: \(result.platformApplications?.count)")
            
            if((platformApplications?.count)! > 0) {
                for platformApplication in platformApplications! {
                    self.trace.output("platformApplicationARN: \(String(describing: platformApplication.platformApplicationArn))")
                    platformApplicationARN = platformApplication.platformApplicationArn
                }
                // There should only be one element in platformsApplications[] - else ERROR
                if((platformApplications?.count)! > 1) {
                    self.trace.error("platformApplications.count > 1 [=\(String(describing: platformApplications?.count))]")
                    return nil
                } else {
                    self.trace.output("platformApplicationARN: \(String(describing: platformApplicationARN))")
                }
            }
            else {
                self.trace.output("[AWSSNSPlatformApplication] er tom...?")
                return nil
            }
            return nil
            }.waitUntilFinished()
        
        trace.end()
        return platformApplicationARN
    
    }
    
    func snsCreateEndpointARN(token: String, platformARN: String) -> String? {
        
        trace.begin()
        var endpointARN : String?
        
        let createPlatformEndpointInput = AWSSNSCreatePlatformEndpointInput()
        createPlatformEndpointInput?.token = token
        createPlatformEndpointInput?.platformApplicationArn = platformARN
        
        sns!.createPlatformEndpoint(createPlatformEndpointInput!).continueWith { (task) -> AnyObject? in
            
            if task.error != nil {
                
                (UIApplication.shared.delegate as! AppDelegate).notificationHandler.errorMessage = task.error.debugDescription
                
                self.trace.error("Error creating platform endpoint: \(String(describing: task.error))")
                return nil
            }
            
            self.successCounter += 1    //3
            
            let result = task.result as! AWSSNSCreateEndpointResponse
            self.trace.output("createEndpointResponse: \(result)")
            
            endpointARN = result.endpointArn
            self.trace.output("endpointARN: \(String(describing: endpointARN))")
            
            return nil
            }.waitUntilFinished()
        
        trace.end()
        return endpointARN

    }
    
    func snsCreateTopicARN(userID: String) -> String? {
        trace.begin()
        
        var topicARN : String?
        
        let topicInput = AWSSNSCreateTopicInput()
        topicInput?.name = userID
        
        sns!.createTopic(topicInput!).continueWith { (task) -> AnyObject? in
            
            if task.error != nil {
                self.trace.error("Error creating topic: \(String(describing: task.error))")
                return nil
            }
            
            self.successCounter += 1    //4
            
            let result = task.result! as AWSSNSCreateTopicResponse
            self.trace.output("createEndpointResponse: \(result)")
            
            topicARN = result.topicArn
            self.trace.output("topicARN: \(String(describing: topicARN))")
            
            return nil
            }.waitUntilFinished()
        
        trace.end()
        return topicARN
        
    }
    
    func snsCreateSubscriptionARN(endpointARN: String, topicARN: String) -> String? {
        
        trace.begin()
        var subscriptionARN : String?
        
        let subscribeInput = AWSSNSSubscribeInput()
        subscribeInput?.protocols = "application"
        subscribeInput?.topicArn = topicARN
        subscribeInput?.returnSubscriptionArn = true
        subscribeInput?.endpoint = endpointARN
        
        sns!.subscribe(subscribeInput!).continueWith { (task) -> AnyObject? in
            
            if task.error != nil {
                self.trace.error("Error subscribing: \(String(describing: task.error))")
                return nil
            }
            
            self.successCounter += 1    //5
            
            let result = task.result! as AWSSNSSubscribeResponse
            subscriptionARN = result.subscriptionArn
            self.trace.output("subscriptionARN: \(String(describing: subscriptionARN))")
            
            return nil
            }.waitUntilFinished()
        
        trace.end()
        
        return subscriptionARN
    
    }

}

