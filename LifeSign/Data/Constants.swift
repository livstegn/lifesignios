//
// Copyright 2014-2018 Amazon.com,
// Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License").
// You may not use this file except in compliance with the
// License. A copy of the License is located at
//
//     http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, express or implied. See the License
// for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import AWSCognitoIdentityProvider

let CognitoIdentityUserPoolRegion: AWSRegionType = AWSRegionType.EUCentral1
let CognitoIdentityUserPoolId = "eu-central-1_7RnBPx5eQ"    //"eu-central-1_vYKeT3ASm"
let CognitoIdentityUserPoolAppClientId = "2av2g6srvrllq97mp8ibkgt8d4"    //"3le7dmjvtaoklf624rdqf8bok8"
let CognitoIdentityUserPoolAppClientSecret = "1onm7mvph9rmmqt7jasbabirs6v9n0age32d9nifuvggeh6dc9fn"    //"1qmf8kn6cojsau9e261dre0fc6k7up613dpkjj5soa4t06soc48n"

let CognitoIdentityPoolId = "eu-central-1:aac97f0d-07ef-4a79-8c39-2259060f22a3"

let AWSCognitoUserPoolsSignInProviderKey = "UserPool"

let AWSSNSPlatformApplicationARN = "arn:aws:sns:eu-central-1:329102635714:app/APNS/Lifesign"

let LifesignDeepLink = "http://www.livewebsite.dk/invites/?invite="
//let PinpointAppID = "37L57QH86F.privat.LifeSign"
