//
//  DeviceLayer.swift
//  Lifesign-Models
//
//  Created by Peter Rosendahl on 19/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftKeychainWrapper

protocol DeviceLayer {
    func savelocalUser(user: User) -> Bool
    func loadlocalUser() -> User?
    func getNewEmail() -> String?
    func getEmail() -> String?
    func getPassword() -> String?
    func getPasswordVisible() -> Bool?
    func getDeviceToken() -> String?
    func getInvitationIds() -> [String]?
    func getUserName() -> String?
    func getUserId() -> String
    func getAuthMethod() -> AuthenticationMethod
    func giveLifesign() -> Bool?
    func saveNewEmail(email: String)    // Temporary storage for new email while confirming
    func saveEmail(email: String)
    func savePassword(password: String)
    func savePasswordVisible(status: Bool)
    func saveUserName(name: String)
    func saveUserId(id: String)
    func saveAuthMethod(method: AuthenticationMethod)
    func saveDeviceToken(token: String)
    func saveInvitationIds(invitations: [String])
    func getCurrentWifiName() -> String
    func getCoordinates(address: String) -> CLLocationCoordinate2D
    func debugPreference() -> Bool
    func activityPreference() -> Bool
    func startupPreference() -> Bool
    func getLocationManager() -> CLLocationManager
    func setLifesign(status: Bool)
}

class DeviceLayerImplementation: DeviceLayer {
    
    let trace = ErrorTraces()
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    let localUserFilename = "user"
    let xmlExtension = "xml"
    var locationManager = CLLocationManager()

    var newEmail : String?     // Temporary storage for newEmail while confirming
    private var localeStrings = [String:String]()

    func getLocationManager() -> CLLocationManager {
        return self.locationManager
    }
    
    init() {
        self.trace.output( "DeviceLayerImplementation.init()")
        
        UserDefaults.standard.register(defaults: [String:Any]())
    }
    
    func debugPreference() -> Bool {
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: "debug_preference")
    }

    func activityPreference() -> Bool {
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: "activity_preference")
    }
    
    func startupPreference() -> Bool {
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: "startup_preference")
    }
    
    func savelocalUser(user: User) -> Bool
    {
        var result = false
        
        let dir = try? FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask, appropriateFor: nil, create: true)
        
        if let fileURL = dir?.appendingPathComponent(localUserFilename).appendingPathExtension(xmlExtension)
        {
            let encoder = PropertyListEncoder()
            encoder.outputFormat = .xml
            do {
                let data = try encoder.encode(user)
                try data.write(to: fileURL)
                result = true
            } catch {
                self.trace.error(error)
            }
        }
        
        return result
    }
    
    func loadlocalUser() -> User?
    {
        var user : User? = nil
        
        let dir = try? FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(localUserFilename).appendingPathExtension(xmlExtension)
        self.trace.output("loading user from \(fileURL as Any)")
        if fileURL != nil
        {
            //trace.output("loading user from", fileURL)
            if let data = try? Data(contentsOf: fileURL!) {
                let decoder = PropertyListDecoder()
                user = try! decoder.decode(User.self, from: data)
            }
        }
        
        return user
    }

    func getNewEmail() -> String? {
        return newEmail
    }
    
    
    func removeAllKeys() {
        KeychainWrapper.standard.removeAllKeys()
    }
    
    func getEmail() -> String? {
        let email : String? = KeychainWrapper.standard.string(forKey: "email")
        
        return email
    }

    func getPassword() -> String? {
        let password : String? = KeychainWrapper.standard.string(forKey: "password")
        
        return password
    }
    
    func getPasswordVisible() -> Bool? {
        let  visible : Bool? = KeychainWrapper.standard.bool(forKey: "visible")
        
        return visible
    }

    func getUserName() -> String? {
        let username : String? = KeychainWrapper.standard.string(forKey: "username")
        
        return username
    }
    
    func getUserId() -> String {
        let userid : String = KeychainWrapper.standard.string(forKey: "userid") ?? ""
        
        return userid
    }

    func getAuthMethod() -> AuthenticationMethod {
        let authmethod : String? = KeychainWrapper.standard.string(forKey: "authmethod")
        
        return AuthenticationMethod.enumFromString(string: authmethod ?? AuthenticationMethod.Unknown.description) ?? AuthenticationMethod.Unknown
    }
    
    func getDeviceToken() -> String? {
        let  devicetoken : String? = KeychainWrapper.standard.string(forKey: "devicetoken")
        
        return devicetoken
    }
    
    func getInvitationIds() -> [String]? {
        
        let invitationString : String? = KeychainWrapper.standard.string(forKey: "invitation")
        let invitations = [String]()
        
        if (invitationString == nil) {
            return invitations
        } else {
            return arrayFromString(invitationString!)
        }
    }
    
    func giveLifesign() -> Bool? {
        let  lifesign : Bool? = KeychainWrapper.standard.bool(forKey: "lifesign")
        
        return lifesign
    }
    
    func saveNewEmail(email: String) {
        self.newEmail = email.lowercased()
    }
    
    func saveEmail(email: String) {
        KeychainWrapper.standard.set(email.lowercased(), forKey: "email")
    }
    
    func savePassword(password: String) {
        KeychainWrapper.standard.set(password, forKey: "password")
    }
    
    func savePasswordVisible(status: Bool) {
        KeychainWrapper.standard.set(status, forKey: "visible")
    }

    func saveUserName(name: String) {
        KeychainWrapper.standard.set(name, forKey: "username")
    }
    
    func saveUserId(id: String) {
        KeychainWrapper.standard.set(id, forKey: "userid")
    }
    
    func saveAuthMethod(method: AuthenticationMethod) {
        let authmethod: String = method.description
        KeychainWrapper.standard.set(authmethod, forKey: "authmethod")
    }
    
    func saveDeviceToken(token: String) {
        KeychainWrapper.standard.set(token, forKey: "devicetoken")
    }
    
    func saveInvitationIds(invitations: [String]) {
        let invitationString = stringFromArray(invitations)
        if (invitationString == "") {
            KeychainWrapper.standard.removeObject(forKey: "invitation")
        } else {
            KeychainWrapper.standard.set(invitationString!, forKey: "invitation")
        }
    }

    func setLifesign(status: Bool) {
        KeychainWrapper.standard.set(status, forKey: "lifesign")
    }

    
    func stringFromArray(_ array: [String]) -> String? {
        return (try? JSONSerialization.data(withJSONObject: array, options: []))?.base64EncodedString()
    }
    
    func arrayFromString(_ string: String) -> [String]? {
        guard let data = Data(base64Encoded: string) else {
            return nil
        }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String]
    }
    
    
    func getCurrentWifiName() -> String {
        var networkName = "Unavailable"
        
        /*
        if let interface = CNCopysuppportedi() {
            for i in 0..<CFArrayGetCount(interface) {
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interface, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                if let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString), let interfaceData = unsafeInterfaceData as? [String : AnyObject] {
                    // connected wifi
                    print("BSSID: \(interfaceData["BSSID"]), SSID: \(interfaceData["SSID"]), SSIDDATA: \(interfaceData["SSIDDATA"])")
                    networkName = interfaceData["SSID"]
                } else {
                    print("Not connected")
                }
            }
        } */
        
        return networkName
    }
    
    func getCoordinates(address: String) -> CLLocationCoordinate2D {
        let olesvej20 = CLLocationCoordinate2D.init(latitude: 55.797120, longitude: 12.469080) // Olesvej 20, 2830 Virum
        var location : CLLocationCoordinate2D?
        
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            guard
                let placemarks = placemarks
            else {
                return
            }

            location = placemarks.first?.location?.coordinate
        }
        
        return location ?? olesvej20
    }
 
}
