//
//  Enums.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 03/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

enum UserSessionStatusType : Int, Codable {
    case LoggedIn = 0, LoggedOut, Unknown
    
    static var count: Int { return UserSessionStatusType.Unknown.hashValue + 1 }
    
    var description: String {
        switch self {
        case .LoggedIn: return "LoggedIn"
        case .LoggedOut : return "LoggedOut"
        case .Unknown : return "Unknown"
        }
    }
}

enum AuthenticationMethod : Int, Codable {
    case User = 0, Identity, Unknown
    
    static var count: Int { return UserSessionStatusType.Unknown.hashValue + 1 }
    
    var description: String {
        switch self {
        case .User: return "User"
        case .Identity: return "Identity"
        case .Unknown: return "Unknown"
        }
    }
    
    static func enumFromString(string:String) -> AuthenticationMethod? {
        var i = 0
        while let item = AuthenticationMethod(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}

enum StateViewStateType : Int, Codable {
    case OK = 0, Warning, Alarm
    
    static var count: Int { return StateViewStateType.Alarm.hashValue + 1 }
    
    var description: String {
        switch self {
        case .OK: return "OK"
        case .Warning : return "Warning"
        case .Alarm : return "Alarm"
        }
    }
    
    static func enumFromString(string:String) -> StateViewStateType? {
        var i = 0
        while let item = StateViewStateType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}

