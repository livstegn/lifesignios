//
//  LSContact.swift
//  ContactListView
//
//  Created by Simon Jensen on 10/06/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class LSContact: NSObject {
    
    var user:  User?
    var name:  String?
    var email:  String?
    var selected: Bool
    var isMember: Bool
    
    override init(){
        self.user = nil
        self.name = "unknown"
        self.email = "unknown"
        self.selected = false
        self.isMember = false
    }
    
}
