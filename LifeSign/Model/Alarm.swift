//
//  Alarm.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 20/10/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

class Alarm : NSObject, Codable  {
    var supervisionId : String
    var alarmId : String
    var date : Date
    var escalationTime : Date
    var surveiledUserId : String
    var lastKnownPosition : String
    var status: AlarmStatusType
    var caregroupId : String
    var groupmemberId : String
    
    enum CodingKeys: String, CodingKey
    {
        case supervisionId = "supervisionId"
        case alarmId = "alarmId"
        case date = "date"
        case escalationTime = "escalationTime"
        case surveiledUserId = "surveiledUserId"
        case lastKnownPosition = "lastKnownPosition"
        case status = "status"
        case caregroupId = "caregroupId"
        case groupmemberId = "groupmemberId"
    }
    
    override init() {
        self.supervisionId = ""
        self.alarmId = ""
        self.date = Date()
        self.escalationTime = Date()
        self.surveiledUserId = ""
        self.lastKnownPosition = ""
        self.status = AlarmStatusType.Cancelled
        self.caregroupId = ""
        self.groupmemberId = ""
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.supervisionId = try container.decodeIfPresent(String.self, forKey: .supervisionId) ?? ""
        self.alarmId = try container.decodeIfPresent(String.self, forKey: .alarmId) ?? ""
        self.date = try container.decodeIfPresent(Date.self, forKey: .date) ?? Date()
        self.escalationTime = try container.decodeIfPresent(Date.self, forKey: .escalationTime) ?? Date()
        self.surveiledUserId = try container.decodeIfPresent(String.self, forKey: .surveiledUserId) ?? ""
        self.lastKnownPosition = try container.decodeIfPresent(String.self, forKey: .lastKnownPosition) ?? ""
        self.status = try container.decodeIfPresent(AlarmStatusType.self, forKey: .status) ?? AlarmStatusType.Cancelled
        self.caregroupId = try container.decodeIfPresent(String.self, forKey: .caregroupId) ?? ""
        self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? ""
    }
}

enum AlarmStatusType : Int, Codable {
    case Due, Activated, Cancelled
    
    static var count: Int { return AlarmStatusType.Cancelled.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Due : return "due"
        case .Activated : return "activated"
        case .Cancelled : return "cancelled"
        }
    }
    
    static func enumFromString(string:String) -> AlarmStatusType? {
        var i = 0
        while let item = AlarmStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
    
}
