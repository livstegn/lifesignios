//
//  Group.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 29/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

class Caregroup : NSObject, Codable  {
    var caregroupId : String
    var caregroupName: String
    var activationdate : Date
    var status: CaregroupStatusType
    
    enum CodingKeys: String, CodingKey
    {
        case caregroupId = "caregroupId"
        case caregroupName = "caregroupName"
        case activationdate = "activationdate"
        case status = "status"
    }
    
    override init() {
        caregroupId = ""
        caregroupName = ""
        activationdate = Date()
        status = CaregroupStatusType.Inactive
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.caregroupId = try container.decodeIfPresent(String.self, forKey: .caregroupId) ?? ""
        self.caregroupName = try container.decodeIfPresent(String.self, forKey: .caregroupName) ?? ""
        self.activationdate = try container.decodeIfPresent(Date.self, forKey: .activationdate) ?? Date()
        self.status = try container.decodeIfPresent(CaregroupStatusType.self, forKey: .status) ?? CaregroupStatusType.Inactive
    }

    init(_ caregroupId: String, _ caregroupName: String, _ activationdate: Date, _ status: CaregroupStatusType) {
        self.caregroupId = caregroupId
        self.caregroupName = caregroupName
        self.activationdate = activationdate
        self.status = status
    }
}

enum CaregroupStatusType : Int, Codable {
    case Active = 0, Inactive
    
    static var count: Int { return CaregroupStatusType.Inactive.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Active : return "active"
        case .Inactive : return "inactive"
        }
    }
    
    static func enumFromString(string:String) -> CaregroupStatusType? {
        var i = 0
        while let item = CaregroupStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}
