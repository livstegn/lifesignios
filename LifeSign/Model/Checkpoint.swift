//
//  Checkpoint.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 18/08/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

class Checkpoint : NSObject, Codable  {
    var supervisionId : String
    var type: CheckpointType
    var time : Date
    var userId : String
    var caregroupid : String
    var groupmemberId : String
    var notifiedCount : Int
    
    enum CodingKeys: String, CodingKey
    {
        case supervisionId = "supervisionId"
        case type = "type"
        case time = "time"
        case userId = "userId"
        case caregroupid = "caregroupid"
        case groupmemberId = "groupmemberId"
        case notifiedCount = "notifiedCount"
    }
    
    override init() {
        self.supervisionId = ""
        self.type = CheckpointType.Deadline
        self.time = Date()
        self.userId = ""
        self.caregroupid = ""
        self.groupmemberId = ""
        self.notifiedCount = 0
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.supervisionId = try container.decodeIfPresent(String.self, forKey: .supervisionId) ?? ""
        self.type = try container.decodeIfPresent(CheckpointType.self, forKey: .type) ?? CheckpointType.Deadline
        self.time = try container.decodeIfPresent(Date.self, forKey: .time) ?? Date()
        self.userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        self.caregroupid = try container.decodeIfPresent(String.self, forKey: .caregroupid) ?? ""
        self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? ""
        self.notifiedCount = try container.decodeIfPresent(Int.self, forKey: .notifiedCount) ?? 0
    }

    init(supervision : Supervision, type: CheckpointType, time : Date, notifiedCount : Int)  {
        self.supervisionId = supervision.supervisionId
        self.type = type
        self.time = time
        self.userId = supervision.userId
        self.caregroupid = supervision.caregroupId
        self.groupmemberId = supervision.groupmemberId
        self.notifiedCount = notifiedCount
    }
}

enum CheckpointType : Int, Codable {
    case Countdown = 0, Deadline, Ended, Location
    
    static var count: Int { return CheckpointType.Location.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Countdown : return "countdown"
        case .Deadline : return "deadline"
        case .Ended : return "ended"
        case .Location : return "location"
        }
    }
    
    static func enumFromString(string:String) -> CheckpointType? {
        var i = 0
        while let item = CheckpointType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
    
}
