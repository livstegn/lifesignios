//
//  Groupmember.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 29/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

class GroupMember : NSObject, Codable  {
    /*
     groupmemberid (PK)
     status
     userid (FK)
     isMonitored
     var isMonitor
     var isOwner
     caregroupid (FK)
    */
    var groupmemberId : String
    var status: GroupMemberStatusType
    var userid : String
    var isMonitored: Bool
    var isMonitor: Bool
    var isOwner: Bool
    var caregroupId : String
    
    enum CodingKeys: String, CodingKey
    {
        case groupmemberId = "groupmemberId"
        case status = "status"
        case userid = "userid"
        case isMonitored = "isMonitored"
        case isMonitor = "isMonitor"
        case isOwner = "isOwner"
        case caregroupId = "caregroupId"
    }
    
    override init() {
        groupmemberId = ""
        status = GroupMemberStatusType.Inactivated
        userid = ""
        isMonitored = false
        isMonitor = false
        isOwner = false
        caregroupId = ""
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? ""
        self.status = try container.decodeIfPresent(GroupMemberStatusType.self, forKey: .status) ?? GroupMemberStatusType.Inactivated
        self.userid = try container.decodeIfPresent(String.self, forKey: .userid) ?? ""
        self.isMonitored = try container.decodeIfPresent(Bool.self, forKey: .isMonitored) ?? false
        self.isMonitor = try container.decodeIfPresent(Bool.self, forKey: .isMonitor) ?? false
        self.isOwner = try container.decodeIfPresent(Bool.self, forKey: .isOwner) ?? false
        self.caregroupId = try container.decodeIfPresent(String.self, forKey: .caregroupId) ?? ""
    }

    init(_ groupmemberId : String, _ caregroupId : String, _ status: GroupMemberStatusType, _ userid : String, _ isMonitored: Bool, _ isMonitor: Bool, _ isOwner: Bool)  {
        self.groupmemberId = groupmemberId
        self.caregroupId = caregroupId
        self.status = status
        self.userid = userid
        self.isMonitored = isMonitored
        self.isMonitor = isMonitor
        self.isOwner = isOwner
    }
}

enum GroupMemberStatusType : Int, Codable {
    case Waiting = 0, Invited, Active, Inactivated
    
    static var count: Int { return GroupMemberStatusType.Inactivated.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Waiting : return "waiting"
        case .Invited : return "invited"
        case .Active : return "active"
        case .Inactivated : return "inactivated"
        }
    }
    
    static func enumFromString(string:String) -> GroupMemberStatusType? {
        var i = 0
        while let item = GroupMemberStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}
