//
//  Helper.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 17/06/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation
import UIKit
import BTNavigationDropdownMenu
import AWSCognitoIdentityProvider

class Helper {
    
    static func hhColonMm (timeofday: TimeInterval) -> String {
        let hours = Int(timeofday / 3600)
        let minutes = Int((timeofday - Double(hours * 3600)) / 60)
        let hhColonMm = String(format: "%02d:%02d", hours, minutes)
        
        return hhColonMm
    }
    
    static func utcTime (date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter.timeZone = TimeZone(identifier: "UTC")!
        
        let date = formatter.string(from: date)
        return date
    }

    static func uiTime (date: Date) -> String {
        let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM HH:mm"
        formatter.timeZone = calendar.timeZone

        let date = formatter.string(from: date)
        return date
    }

    static func dateString (date: Date) -> String {
        let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM"
        formatter.timeZone = calendar.timeZone

        let date = formatter.string(from: date)
        return date
    }

    static func uiHoursMinutes (date: Date) -> String {
        let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        formatter.timeZone = calendar.timeZone

        let date = formatter.string(from: date)
        return date
    }

    static func simpleUITime (date: Date) -> String {
        let calendar = Calendar.current
        var dateString = ""
        
        if calendar.isDateInYesterday(date) {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            formatter.timeZone = calendar.timeZone
            
            dateString = formatter.string(from: date) + NSLocalizedString(" yesterday", comment: "Generic ui date term: the day before today")
        }
        else if calendar.isDateInToday(date) {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            formatter.timeZone = calendar.timeZone
            
            dateString = formatter.string(from: date) + NSLocalizedString(" today", comment: "Generic ui date term: today")
        }
        else if calendar.isDateInTomorrow(date) {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            formatter.timeZone = calendar.timeZone
            
            dateString = formatter.string(from: date) + NSLocalizedString(" tomorrow", comment: "Generic ui date term: the day after today")
        }
        else {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm dd/MM"
            formatter.timeZone = calendar.timeZone
            
            dateString = formatter.string(from: date)
        }

        return dateString
    }
    
    static func simpleUITimeNoDate (date: Date) -> String {
        var dateString = ""
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        formatter.timeZone = TimeZone(identifier: "UTC")!
        
        dateString = formatter.string(from: date)

        return dateString
    }
    
    static func dateFromUTC (utc : String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")!
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")

        let date = dateFormatter.date(from: utc)
        
        return date
    }

    static func dateYYYYMMDD_HHMM (_ dateString : String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYYMMDD HHMM"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")!
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        let date = dateFormatter.date(from: dateString)
        
        return date
    }

    static func intervalFromTime (time: String) -> TimeInterval
    {
        var interval : TimeInterval = 0
        
        if (time.count != 5) {
            interval = -1
        } else {
            let hourStr = String(time.prefix(upTo: time.index(time.startIndex, offsetBy: 2)))
            let minuteStr = String(time[time.index(time.startIndex, offsetBy: 3)...])
            let hours = Double(hourStr) ?? -1
            let minutes = Double(minuteStr) ?? -1

            if (hours < 0) {
                interval = -1
            } else if (minutes < 0) || (minutes > 59) {
                interval = -1
            } else {
                interval = Double(hours) * Double(3600) + Double(minutes) * Double(60)
            }
        }
        return interval
    }
    
    static func simpleTimeFromInterval(second: TimeInterval) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional
        formatter.allowedUnits = [.hour, .minute]
        formatter.zeroFormattingBehavior = .pad
        return formatter.string(from: second)
    }
    
    static func weightedPreferredFont(font: UIFont, weight: UIFont.Weight) -> UIFont {
        let textStyle = font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.textStyle) as! UIFont.TextStyle
        let desc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: textStyle)
        let returnFont = UIFont.systemFont(ofSize: desc.pointSize, weight: weight)
        
        return returnFont
    }
    
    static func simpleTimeMessengerStyle(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        formatter.locale = Locale.current
        return formatter.string(from: date)
    }
    
    static func groupMenuView(navigationController: UINavigationController?, model: Model) -> BTNavigationDropdownMenu {

        var items = [String]()
        let NoOfCaregroups = model.caregroups?.count ?? 0
        for index in 0..<NoOfCaregroups {
            items.append(model.caregroups?[index].caregroupName ?? "NA")
        }
        
        var title = "NA"
        if let cg = model.getCurrentCaregroup() {
            title = cg.caregroupName
        } else {
            if (items.count > 0) {
                title = items[0]
                model.setCurrentCaregroup(index: 0)
            }
        }
        //var btTitle = BTTitle.title(title)

        let menuView = BTNavigationDropdownMenu(navigationController: navigationController!, containerView: navigationController!.view, title: title, items: items)

        menuView.cellBackgroundColor = navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(named: "lsTextVIewBackground")
        menuView.cellTextLabelFont = UIFont.preferredFont(forTextStyle: .body)
        menuView.shouldKeepSelectedCellColor = true
        menuView.frame.size.width = (navigationController!.view.frame.size.width * 0.6)
        menuView.backgroundColor = UIColor(named: "lsTextVIewBackground")
        menuView.frame.size.height = 0.8 * menuView.frame.size.height
        menuView.layer.cornerRadius = 18
        menuView.layer.masksToBounds = true
        menuView.arrowTintColor = UIColor.black
        menuView.navigationBarTitleFont = UIFont.preferredFont(forTextStyle: .body)

        return menuView
    }
    
    static func httpErrorHandler(error: NSError) {
        switch error.code {
        case -1001 :
            NotificationBanner.show("The request timed out, please retry", style: .error )
        case -1005 :
            NotificationBanner.show("The network connection was lost, please retry", style: .error )
        case -1009 :
            NotificationBanner.show("The Internet connection appears to be offline, please retry", style: .error )
        case 1:
            NotificationBanner.show("Authorization error - not authorized", style: .error )
        case 20:
            NotificationBanner.show("Incorrect username or password", style: .error )
        case 3010:
            NotificationBanner.show("Remote notifications are not supported", style: .error )
        default:
            NotificationBanner.show("Unknown error occured, \(error)", style: .error )
        }
        return
    }
    
    static func frameForTabAtIndex(tabBarVC: TabBarViewController, toview: UIView, index: Int) -> CGRect {
        /*guard let tabBarSubviews = tabBarController?.tabBar.subviews else {
            return CGRect.zero
        }*/
        let tabBarSubviews = tabBarVC.tabBar.subviews

        var allItems = [UIView]()
        for tabBarItem in tabBarSubviews {
            if tabBarItem.isKind(of: NSClassFromString("UITabBarButton")!) {
                allItems.append(tabBarItem)
            }
        }
        let item = allItems[index]
        return item.superview!.convert(item.frame, to: toview)
    }
}

