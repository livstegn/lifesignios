//
//  Invitation.swift
//  LifeSign
//
//  Created by Simon Jensen on 23/07/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class Invitation: NSObject, Codable {

    var invitationId : String?
    var status : InvitationStatusType?
    var invitationToUserId: String?
    var invitedByUserId: String?
    var caregroupId: String?
    //var groupmemberId : String?
    var senderName : String?
    var caregroupName : String?
    
    var date: String?
    
    enum CodingKeys: String, CodingKey
    {
        case invitationId = "invitationId"
        case status = "status"
        case invitationToUserId = "invitationToUserId"
        case invitedByUserId = "invitedByUserId"
        case caregroupId = "caregroupId"
        //case groupmemberId = "groupmemberId"
        case senderName = "senderName"
        case caregroupName = "caregroupName"
    }
    
    override init() {
        self.invitationId = ""
        self.status = InvitationStatusType.Creating
        self.invitationToUserId = ""
        self.invitedByUserId = ""
        self.caregroupId = ""
        //self.groupmemberId = ""
        self.senderName = ""
        self.caregroupName = ""
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.invitationId = try container.decodeIfPresent(String.self, forKey: .invitationId) ?? ""
        self.status = try container.decodeIfPresent(InvitationStatusType.self, forKey: .status) ?? InvitationStatusType.Creating
        self.invitationToUserId = try container.decodeIfPresent(String.self, forKey: .invitationToUserId) ?? ""
        self.invitedByUserId = try container.decodeIfPresent(String.self, forKey: .invitedByUserId) ?? ""
        self.caregroupId = try container.decodeIfPresent(String.self, forKey: .caregroupId) ?? ""
        //self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? ""
        self.senderName = try container.decodeIfPresent(String.self, forKey: .senderName) ?? ""
        self.caregroupName = try container.decodeIfPresent(String.self, forKey: .caregroupName) ?? ""
    }
    
}

enum InvitationStatusType : Int, Codable {
    case Creating = 0, Sent, Accepted, Rejected
    
    static var count: Int { return InvitationStatusType.Rejected.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Creating : return "creating"
        case .Sent: return "sent"
        case .Accepted : return "accepted"
        case .Rejected : return "rejected"
        }
    }
    
    static func enumFromString(string: String) -> InvitationStatusType? {
        var i = 0
        while let item = InvitationStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}



