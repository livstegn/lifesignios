//
//  Lifesign.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 20/10/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

class LifeSign : NSObject, Codable  {
    var userId : String
    var lifesignId : String
    var latestLifesign : Date
    var location : String
    var userMinutesBeforeUTC : Int
    
    enum CodingKeys: String, CodingKey
    {
        case userId = "userId"
        case lifesignId = "lifesignId"
        case latestLifesign = "latestLifesign"
        case location = "location"
        case userMinutesBeforeUTC = "userMinutesBeforeUTC"
    }
    
    override init() {
        self.userId = ""
        self.lifesignId = ""
        self.latestLifesign = Date()
        self.location = ""
        self.userMinutesBeforeUTC = 0
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        self.lifesignId = try container.decodeIfPresent(String.self, forKey: .lifesignId) ?? ""
        self.latestLifesign = try container.decodeIfPresent(Date.self, forKey: .latestLifesign) ?? Date()
        self.location = try container.decodeIfPresent(String.self, forKey: .location) ?? ""
        self.userMinutesBeforeUTC = try container.decodeIfPresent(Int.self, forKey: .userMinutesBeforeUTC) ?? 0
    }

    init(_ userId : String, _ lifesignId : String, _ latestLifesign : Date, _ location : String, _ userMinutesBeforeUTC : Int) {
        self.userId = userId
        self.lifesignId = lifesignId
        self.latestLifesign = latestLifesign
        self.location = location
        self.userMinutesBeforeUTC = userMinutesBeforeUTC
    }
}
