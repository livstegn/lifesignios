//
//  Location.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 06/03/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation
import CoreLocation

class Location: NSObject, Codable {
    var trackingId : String
    var locationId : String
    var latitude: Double
    var longitude: Double
    var time: Date
    var address: String
    var supervisionId : String
    var userId : String
    var caregroupId : String
    var groupmemberId : String
    var onSafe : Bool
    var leftSafe : Bool
    var backSafe : Bool

    enum CodingKeys: String, CodingKey
    {
        case trackingId = "trackingId"
        case locationId = "locationId"
        case latitude = "latitude"
        case longitude = "longitude"
        case time = "time"
        case address = "address"
        case supervisionId = "supervisionId"
        case userId = "userId"
        case caregroupId = "caregroupId"
        case groupmemberId = "groupmemberId"
        case onSafe = "onSafe"
        case leftSafe = "leftSafe"
        case backSafe = "backSafe"
    }
    
    override init() {
        self.trackingId = ""
        self.locationId = ""
        self.latitude = 0
        self.longitude = 0
        self.time = Date()
        self.address = ""
        self.supervisionId = ""
        self.userId = ""
        self.caregroupId = ""
        self.groupmemberId = ""
        self.onSafe = false
        self.leftSafe = false
        self.backSafe = false
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.trackingId = try container.decodeIfPresent(String.self, forKey: .trackingId) ?? ""
        self.locationId = try container.decodeIfPresent(String.self, forKey: .locationId) ?? ""
        self.latitude = try container.decodeIfPresent(Double.self, forKey: .latitude) ?? 0
        self.longitude = try container.decodeIfPresent(Double.self, forKey: .longitude) ?? 0
        self.time = try container.decodeIfPresent(Date.self, forKey: .time) ?? Date()
        self.address = try container.decodeIfPresent(String.self, forKey: .address) ?? ""
        self.supervisionId = try container.decodeIfPresent(String.self, forKey: .supervisionId) ?? ""
        self.userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        self.caregroupId = try container.decodeIfPresent(String.self, forKey: .caregroupId) ?? ""
        self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? ""
        self.onSafe = try container.decodeIfPresent(Bool.self, forKey: .onSafe) ?? false
        self.leftSafe = try container.decodeIfPresent(Bool.self, forKey: .leftSafe) ?? false
        self.backSafe = try container.decodeIfPresent(Bool.self, forKey: .backSafe) ?? false
    }
    
    init (_ location: CLLocationCoordinate2D, time: Date, address: String, tracking: Tracking) {
        self.trackingId = tracking.trackingId
        self.locationId = "LOC-" + UUID().uuidString
        self.latitude = location.latitude
        self.longitude = location.longitude
        self.time = time
        self.address = address
        self.supervisionId = tracking.supervisionId
        self.userId = tracking.userId
        self.caregroupId = tracking.caregroupId
        self.groupmemberId = tracking.groupmemberId
        self.onSafe = tracking.onSafe
        self.leftSafe = tracking.leftSafe
        self.backSafe = tracking.backSafe
    }
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        return formatter
    }()
    
    var coordinates: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
