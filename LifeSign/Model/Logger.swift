//
//  Logger.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 30/09/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

protocol Logger {
    func addPerformanceLog (text: String)
    func printPerformanceLog ()

    func addCloudAPILog (start: Date, text: String)
    func printCloudAPILog ()
    func clearPerformanceLog ()
}

class LoggerImplementation : Logger {
    struct LogEntry {
        var thread : String
        var text : String
    }
    
    var performanceLogs = [Date:LogEntry]()
    var cloudAPILogs = [Date:String]()

    init() {
        self.addPerformanceLog(text: "Logger initiated")
    }
    
    func addPerformanceLog (text: String) {
        let threadStr = Thread.current.description
        let index = threadStr.firstIndex(of: ",") ?? threadStr.endIndex
        // beginning is "<NSThread: 0x600003b11fc0>{number = 1"
        let beginning = threadStr[..<index]
        var index2 = beginning.endIndex
        if let index3 = beginning.lastIndex(of: " ") {
            index2 = beginning.index(after: index3)
        }
        let range = index2..<beginning.endIndex
        let numberStr = String(beginning[range])
        let le = LogEntry(thread: numberStr, text: text)
        if le != nil {
            performanceLogs[Date()] = le
        } else {
            performanceLogs[Date()] = LogEntry(thread: "0", text: "No thread: " + text)
        }
        
    }
    
    func printPerformanceLog () {
        var previousDate : Date?
        
        print("PLOG\tTime\tms\tThread\tText")
        
        for key in performanceLogs.keys.sorted() {
            let timeString = Helper.utcTime(date: key)

            let difference = Calendar.current.dateComponents([.nanosecond], from: previousDate ?? key, to: key)
            let milliseconds = String(format: "%ld", (difference.nanosecond! / 1000000))
            if performanceLogs[key] != nil {
                let le = performanceLogs[key]!
                print("PLOG\t" + timeString + "\t" + milliseconds + "\t" + le.thread + "\t" + le.text)
            }

            previousDate = key
        }
    }

    func addCloudAPILog (start: Date, text: String) {
        let now = Date()
        let difference = Calendar.current.dateComponents([.nanosecond], from: start, to: now)
        let differenceString = String(format: " (%ld)", (difference.nanosecond! / 1000000))
        cloudAPILogs[now] = text + differenceString
    }
    
    func printCloudAPILog () {
        for key in cloudAPILogs.keys.sorted() {
            let timeString = Helper.utcTime(date: key)
            print("APILOG:: " + timeString + " : " + cloudAPILogs[key]!)
        }
    }
    
    func clearPerformanceLog () {
        performanceLogs = [Date:LogEntry]()
    }
}
