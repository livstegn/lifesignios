//
//  MainStatus.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 03/01/2021.
//  Copyright © 2021 Simon Jensen. All rights reserved.
//

import Foundation

class MainStatus : NSObject, Codable  {
    var caregroupId : String
    var supervisionId : String
    var userId : String
    var userName : String
    var supervisionType: SupervisionType
    var caregroupName : String
    var groupmemberId : String
    var countdownTime : Date?
    var deadlineTime : Date?
    var endedTime : Date?
    var periodEnd : Date?
    var trackingStatus : TrackingStatusType?
    var leftSafe : Bool
    var onSafe : Bool
    var backSafe : Bool

    let trace = NoTraces()

    enum CodingKeys: String, CodingKey
    {
        case caregroupId = "caregroupId"
        case supervisionId = "supervisionId"
        case userId = "userId"
        case userName = "userName"
        case supervisionType = "supervisionType"
        case caregroupName = "caregroupName"
        case groupmemberId = "groupmemberId"
        case countdownTime = "countdownTime"
        case deadlineTime = "deadlineTime"
        case endedTime = "endedTime"
        case periodEnd = "periodEnd"
        case trackingStatus = "trackingStatus"
        case leftSafe = "leftSafe"
        case onSafe = "onSafe"
        case backSafe = "backSafe"
    }
    
    override init() {
        self.caregroupId = ""
        self.supervisionId = ""
        self.userId = ""
        self.userName = ""
        self.supervisionType = SupervisionType.Ongoing
        self.caregroupName = ""
        self.groupmemberId = ""
        self.countdownTime = nil
        self.deadlineTime = nil
        self.endedTime = nil
        self.periodEnd = nil
        self.trackingStatus = nil
        self.leftSafe = false
        self.onSafe = false
        self.backSafe = false
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.caregroupId = try container.decodeIfPresent(String.self, forKey: .caregroupId) ?? ""
        self.supervisionId = try container.decodeIfPresent(String.self, forKey: .supervisionId) ?? ""
        self.userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        self.userName = try container.decodeIfPresent(String.self, forKey: .userName) ?? ""
        self.supervisionType = try container.decodeIfPresent(SupervisionType.self, forKey: .supervisionType) ?? SupervisionType.Ongoing
        self.caregroupName = try container.decodeIfPresent(String.self, forKey: .caregroupName) ?? ""
        self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? ""
        self.countdownTime = try container.decodeIfPresent(Date.self, forKey: .countdownTime) ?? nil
        self.deadlineTime = try container.decodeIfPresent(Date.self, forKey: .deadlineTime) ?? nil
        self.endedTime = try container.decodeIfPresent(Date.self, forKey: .endedTime) ?? nil
        self.periodEnd = try container.decodeIfPresent(Date.self, forKey: .periodEnd) ?? nil
        self.trackingStatus = try container.decodeIfPresent(TrackingStatusType.self, forKey: .trackingStatus) ?? nil
        self.onSafe = try container.decodeIfPresent(Bool.self, forKey: .onSafe) ?? false
        self.leftSafe = try container.decodeIfPresent(Bool.self, forKey: .leftSafe) ?? false
        self.backSafe = try container.decodeIfPresent(Bool.self, forKey: .backSafe) ?? false

    }
    
    init(supervision: Supervision) {
        self.caregroupId = supervision.caregroupId
        self.supervisionId = supervision.supervisionId
        self.userId = supervision.userId
        self.userName = ""
        self.supervisionType = supervision.supervisionType
        self.caregroupName = ""
        self.groupmemberId = supervision.groupmemberId
        self.countdownTime = nil
        self.deadlineTime = nil
        self.endedTime = nil
        self.periodEnd = supervision.periodEnd
        self.trackingStatus = nil
        self.leftSafe = false
        self.onSafe = false
        self.backSafe = false
    }
    
    func isWaiting (appUserid: String, now: Date) -> Bool {
        var isWaiting = false
        
        if (appUserid == self.userId) {
            if (endedTime == nil) {
                isWaiting = true
            }
        }

        return isWaiting
    }
    
    func getStateMessage(appUserid: String, now: Date) -> StateMessage {
        var sm = StateMessage ()
        
        /*
        Ongoing supervisions
            Checkpoints...............               StateMessage                           Waiting Lifesign
            Countdown  Deadline  Ended    Tracking   Severity  Type
            ---------  --------  -----    --------   --------  ---------------------------  -----------------
x            < Date     < Date                        OK        NoDueLifesignYou             Ja
x            > Date     < Date                        Warning   SoonDueLifesignYou           Ja
x            > Date     > Date                        Alarm     DueLifesignYou               Ja

        Period supervisions
            ---------  --------  -----    --------   --------  ---------------------------  -----------------
x            < Date     < Date                        OK        NoDueLifesignYou             Ja
x            < Date     < Date                        OK        EndedInTimeYou               Nej
x            > Date     < Date                        Warning   SoonDueLifesignYou           Ja
x            > Date     > Date                        Alarm     DueLifesignYou               Ja
-                                 < Date              OK        NoDueLifesignYou             Nej
x                                 < Date              OK        EndedInTimeYou               Nej
x                                 > Date              Warning   AfterDeadline                Nej
            < Date     < Date             Upcoming   OK        SafespotUpcoming             Ja
            < Date     < Date             Active     OK        SafespotActive               Ja
            < Date     < Date             Active     OK        SafespotActiveLeftSafe       Ja
            > Date     < Date             Active     Warning   SoonDueLifesignYou           Ja
x            > Date     > Date             Active     Alarm     SafespotActiveOverdue        Ja
                                 < Date   Ended      OK        SafespotEndedBackSafe        Nej
                                 < Date   Ended      OK        SafespotEndedNeverLeftSafe   Nej
                                 > Date   Ended      Warning   SafespotEndedTooLate         Nej
        */

        if (self.supervisionType == .Ongoing) {
            if (deadlineTime ?? now < now) {
                sm = StateMessage(userName, userId, caregroupName, caregroupId, appUserid != self.userId ? .DueLifesignOther : .DueLifesignYou, .Alarm, supervisionId: supervisionId)
            } else if (countdownTime ?? now < now) {
                sm = StateMessage(userName, userId, caregroupName, caregroupId, appUserid != self.userId ? .SoonDueLifesignOther : .SoonDueLifesignYou, .Warning, supervisionId: supervisionId)
            } else {
                sm = StateMessage(userName, userId, caregroupName, caregroupId, appUserid != self.userId ? .NoDueLifesignOther : .NoDueLifesignYou, .Info, supervisionId: supervisionId)
            }
        } else if (self.supervisionType == .Period) {
            if (trackingStatus == nil) {
                // Manuelt livstegn
                if (deadlineTime == nil) {
                    if (periodEnd ?? now > endedTime ?? now) {
                        sm = StateMessage(userName, userId, caregroupName, caregroupId, appUserid != self.userId ? .EndedInTimeOther : .EndedInTimeYou, .Info, supervisionId: supervisionId)
                    } else {
                        sm = StateMessage(userName, userId, caregroupName, caregroupId, .AfterDeadline, .Warning, supervisionId: supervisionId)
                    }
                }
                else if (deadlineTime! < now) {
                    sm = StateMessage(userName, userId, caregroupName, caregroupId, appUserid != self.userId ? .DueLifesignOther : .DueLifesignYou, .Alarm, supervisionId: supervisionId)
                } else if (countdownTime ?? now < now) {
                    sm = StateMessage(userName, userId, caregroupName, caregroupId, appUserid != self.userId ? .SoonDueLifesignOther : .SoonDueLifesignYou, .Warning, supervisionId: supervisionId)
                } else {
                    sm = StateMessage(userName, userId, caregroupName, caregroupId, appUserid != self.userId ? .NoDueLifesignOther : .NoDueLifesignYou, .Info, supervisionId: supervisionId)
                }
            } else {
                if (trackingStatus! == .Upcoming) {
                    sm = StateMessage(userName, userId, caregroupName, caregroupId, .SafespotUpcoming, .Info, supervisionId: supervisionId)
                } else if (trackingStatus! == .Active) {
                    if (deadlineTime ?? now < now) {
                        sm = StateMessage(userName, userId, caregroupName, caregroupId, .SafespotActiveOverdue, .Alarm, supervisionId: supervisionId)
                    } else if (countdownTime ?? now < now) {
                        sm = StateMessage(userName, userId, caregroupName, caregroupId, appUserid != self.userId ? .SoonDueLifesignOther : .SoonDueLifesignYou, .Warning, supervisionId: supervisionId)
                    } else {
                        if (leftSafe && !backSafe && !onSafe) {
                            sm = StateMessage(userName, userId, caregroupName, caregroupId, .SafespotActiveLeftSafe, .Info, supervisionId: supervisionId)
                        } else {
                            sm = StateMessage(userName, userId, caregroupName, caregroupId, .SafespotActive, .Info, supervisionId: supervisionId)
                        }
                    }
                } else if (trackingStatus! == .Ended) {
                    if (!leftSafe && onSafe) {
                        sm = StateMessage(userName, userId, caregroupName, caregroupId,.SafespotEndedNeverLeftSafe, .Info, supervisionId: supervisionId)
                    } else {
                        if (periodEnd ?? now > endedTime ?? now) {
                            sm = StateMessage(userName, userId, caregroupName, caregroupId,.SafespotEndedBackSafe, .Info, supervisionId: supervisionId)
                        } else {
                            sm = StateMessage(userName, userId, caregroupName, caregroupId, .SafespotEndedTooLate, .Warning, supervisionId: supervisionId)
                        }
                    }
                }
            }
        }

        return sm
    }
}

