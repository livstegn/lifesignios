//
//  Message.swift
//  LifeSign
//
//  Created by Bo Dalberg on 11/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

class Message : NSObject, Codable  {
/*
 messageid (PK)
 createddate
 messagetext
 sender (=userid)
 alarmid (FK)
 */
    var messageId : String
    var createddate : Date
    var messagetext : String
    var systemgenerated: Bool
    var systemtextid: String
    var senderid : String
  //  var alarmid : String

    enum CodingKeys: String, CodingKey
    {
        case messageId = "messageId"
        case createddate = "createddate"
        case messagetext = "messagetext"
        case systemgenerated = "systemgenerated"
        case systemtextid = "systemtextid"
        case senderid = "senderid"
        // husk alarm
    }
    
    override init() {
        messageId = ""
        createddate = Date()
        messagetext = ""
        systemgenerated = false
        systemtextid = ""
        senderid = ""
    }
    
}
