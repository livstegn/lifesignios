//
//  LifesignModel.swift
//  Lifesign-Model
//
//  Created by Peter Rosendahl on 18/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation
import AWSCognitoIdentityProvider
import SwiftKeychainWrapper
import CoreLocation
import UserNotifications

class Model : NSObject, CLLocationManagerDelegate {
    
    let trace = ErrorTraces()
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    var deviceLayer: DeviceLayer
    var cloudLayer: CloudLayer
    var userSessionStatus : UserSessionStatusType
    var user : User?
    var caregroups : [Caregroup]?
    private var currentGroup: Int               // Selected caregroup
    var groupmembers : [GroupMember]?
    var currentMember: Int              // Selected member
    var groupMessages : [Message]?
    var recipients : [String]?
    var stateOfSession : StateViewStateType?
    var stateMessages = [StateMessage]()
    var stateLastLifesign : Date?
    var stateNextLifesign : Date?
    private var userNameCacheDict : [String:String]
    var memberGroups = [String:Caregroup]()
    var statusViewController : ReloaderDelegate?

    private var holdUserActivitiesSeconds : Double = 28800  // Hver 8. time (tidligere: 1800)
    private var bufferUserActivityAgeSeconds : Double = 86400 // Hver 24. time
    private var groupsLastRead = Date.init(timeIntervalSince1970: 0)
    private var lifesignsLastRead = Date.init(timeIntervalSince1970: 0)
    private var lifesigns = [LifeSign]()

    // Push notification - information originate from AppDelegate
    var notificationLaunch : Bool = false   // Notification received
    var notificationData : [AnyHashable:Any]?      // Referencing notification data.
    var notificationType : String?
    var ignoreInvitation : Bool = false     // User ignore invitation
    var checkShouldBeTracking = false

    // Universal link - information originate from AppDelegate
    var universalLinkLaunch : Bool = false
    var universalLinkData : String?
    
    init (deviceLayerImplementation: DeviceLayer, cloudLayerImplementation: CloudLayer, universalLinkLaunch: Bool, universalLinkData: String?) {
        self.deviceLayer = deviceLayerImplementation
        self.cloudLayer = cloudLayerImplementation

        self.trace.output( "Model init ifm. AppDelegate")
 
        trace.begin()
        
        self.currentGroup = -1
        self.currentMember = -1
        
        userSessionStatus = UserSessionStatusType.Unknown
        user = nil
        caregroups = nil
        groupmembers = nil
        userNameCacheDict = [String:String] ()
        
        
        // Check if a session was ended without logout - and allow user to continue that session
        let currentAuthMethod = self.deviceLayer.getAuthMethod()
        trace.output("Last session: \(currentAuthMethod.description)")
        if(currentAuthMethod != AuthenticationMethod.Unknown) {
            trace.output("Continue last session: \(currentAuthMethod.description)")
            _ = cloudLayer.setupCredentials(authMethod: currentAuthMethod)
            userSessionStatus = UserSessionStatusType.LoggedIn
        }
        
        trace.end()
    }
    
    func getCurrentCaregroup () -> Caregroup? {
        var cg : Caregroup? = nil
        
        if currentGroup > -1 && currentGroup < caregroups!.count {
            cg = caregroups![currentGroup]
        } else if caregroups != nil && caregroups!.count > 0 {
            currentGroup = 0
            cg = caregroups![0]
        }
        
        return cg
    }
    
    func setCurrentCaregroup (index: Int) {
        currentGroup = index
    }
    
    func debugPreference() -> Bool {
        return deviceLayer.debugPreference()
    }
    
    func activityPreference() -> Bool {
        return deviceLayer.activityPreference()
    }

    func startupPreference() -> Bool {
        return deviceLayer.startupPreference()
    }
    func loadUserParameters() {
        
        trace.begin()
        
        if let remoteUser = cloudLayer.getUser(userid: user!.userId) {
            userSessionStatus = UserSessionStatusType.LoggedIn
            trace.output("Model:: Remote user \(remoteUser.userId) indlæst ")
            user = remoteUser
            // TODO: slettes helt: _ = deviceLayer.savelocalUser(user: user!)
        }
        
        trace.end()
    }

    func createNewUser(email: String) {
        
        trace.begin()
        
        let tempUser = User()
        
        // New user logged in, known in AWS, but not in Dynamo
        tempUser.userName = deviceLayer.getUserName() ?? "John Doe" //cloudLayer.getCloudSessionUserName() ?? ""    // XXX Returning username - not user name...???
        tempUser.email = email.lowercased()
        tempUser.cellno = ""
        tempUser.platform = PlatformType.iOS
        tempUser.deviceId = UIDevice.current.identifierForVendor!.uuidString
        
        let cloudUser = cloudLayer.createUser(user: tempUser)
        
        user = cloudUser
        
        // TODO: slettes helt: _ = deviceLayer.savelocalUser(user: localUser)
        
        trace.end()
        
    }
    
    func restoreCurrentUser() {
        
        trace.begin()
        // When the tabviewcontroller is loaded, it means that passwords have been authenticated against AWS Cognito
        // At this stage we load the Lifesign userdata based on the authenticated user profile
        
        let email : String? = deviceLayer.getEmail()
        self.trace.output( "Efter: model.deviceLayer.getEmail()")
        trace.output("Using email: \(String(describing: email))")
        
        let userID = self.getUserID(email: email!)
        self.trace.output( "Efter model.getUserID()")
        trace.output("Getting UserID: \(String(describing: userID))")
        
        self.user = self.getUser(userid: userID!)
        self.trace.output( "Efter: model.getUser()")
        
        self.loadUserParameters()
        self.trace.output( "Efter: model.loadUserParameters()")
        
        // At this stage, if the user status is not 'active' then force to active...
        if (self.user?.status != UserStatusType.Active) {
            self.user?.status = UserStatusType.Active
            self.updateUserDetails()
            
        }
        
        // Set userStatus = Active (app is running)
        _ = self.setAppStatus(userid: user!.userId, status: AppStatusType.Active)
        
        trace.end()
        
    }
    
    func getLifesignStateNew (now: Date = Date()) -> [MainStatus] {
        var mainStatusList = [MainStatus]()
        
        trace.begin()
        
        // Reset Model state-related properties
        if (self.user != nil) {
            stateLastLifesign = getLatestLifesign(userId: self.user!.userId)
            stateNextLifesign = nil
            
            getCareGroups()
            
            for caregroup in self.caregroups! {
                for status in getMainStatusList(userId: self.user!.userId, caregroupId: caregroup.caregroupId) {
                    if (status.supervisionType == .Ongoing) {
                        mainStatusList.append(status)
                    } else if (status.supervisionType == .Period) {
                        if (status.endedTime == nil) {
                            mainStatusList.append(status)
                        } else if (status.endedTime!.addingTimeInterval(24*60*60) > now) {
                            mainStatusList.append(status)
                        }
                    }

                    if (status.deadlineTime != nil && status.userId == self.user!.userId) {
                        if (stateNextLifesign == nil) {
                            stateNextLifesign = status.deadlineTime
                        } else if (status.deadlineTime! < stateNextLifesign!) {
                            stateNextLifesign = status.deadlineTime
                        }
                    }
                }
            }

        }
        
        trace.end()
        return mainStatusList
    }

    func overrideLimitsUserActivities (holdSeconds: Double, bufferAge: Double) {
        if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
            self.holdUserActivitiesSeconds = holdSeconds
            self.bufferUserActivityAgeSeconds = bufferAge
            print ("holdUserActivitiesSeconds = " + holdUserActivitiesSeconds.description)
            print ("bufferUserActivityAgeSeconds = " + bufferUserActivityAgeSeconds.description)
        }
    }
    
    var collectedUserActivites = [UserActivity]()

    func newUserActivity(userActivity: UserActivity, now: Date = Date()) {
        var giveLifesign = true
        var currentUserActivities = [UserActivity]()
        
        // Løber alle 'gemte' aktiviteter igennem
        // 'gemte' aktiviteter (collectedUserActivities) der er yngre end 24 timer gemmes (som currentUserActivities)
        for fua in collectedUserActivites {
            let age = now.timeIntervalSince(fua.time)
            if age < self.bufferUserActivityAgeSeconds {
                currentUserActivities.append(fua)
            } else {
                print("Old activity erased: " + fua.time.description + " age=" + age.description)
            }
            if (fua.status == .Forwarded && userActivity.time.timeIntervalSince(fua.time) < self.holdUserActivitiesSeconds) {
                // Vi har givet livstegn for mindre end X minutter siden.
                giveLifesign = false
            }
        }
        
        collectedUserActivites = currentUserActivities // Sørg for, at de gamle ikke længere er med...
        
        if (giveLifesign) {
            self.giveLifesignNew(lifesignTime: now)
            // Nu er der ikke grund til at gemme de gamle userActivities
            collectedUserActivites = [UserActivity]()
            userActivity.status = .Forwarded
            
            /*
            if self.statusViewController != nil {
                self.statusViewController!.reload()
            }*/
        } else {
            userActivity.status = .Held
        }
        
        collectedUserActivites.append(userActivity)
    }
    
    func getCollectedActivities() -> String
    {
        var text : String = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        
        for fua in collectedUserActivites {
            text += dateFormatter.string(from: fua.time) + " " + fua.status.description + "\n"
        }
        return text
    }
    
    func getUTCOffset () -> Int {
        let seconds = TimeZone.current.secondsFromGMT()
        let minutes = Int(seconds/60)

        return minutes
    }
    
    func giveLifesignNew (lifesignTime : Date) {
        trace.begin()
        
        trace.user(self.user)  // Temporary check to verify model.user != nil
        
        let currentOffset = getUTCOffset()
        if(self.user == nil) {
            self.user = self.getUser(userid: deviceLayer.getUserId())
        }
        if (self.user!.userMinutesBeforeUTC != currentOffset) {
            self.user!.userMinutesBeforeUTC = currentOffset
            self.updateUserDetails()
        }

        getCareGroups()
        
        var lifesignAdded = false
        var pushToGroup = [String]()
        
        for caregroup in self.caregroups! {
            var lifesignInGroup = false

            for status in getMainStatusList(userId: self.user!.userId, caregroupId: caregroup.caregroupId) {
                if (status.userId == self.user!.userId &&
                        (status.isWaiting(appUserid: self.user!.userId, now: Date())
                            || status.supervisionType == .Ongoing
                            || status.trackingStatus == nil)) {

                    if (!lifesignAdded) {
                        _ = cloudLayer.addLifesign(userid: user!.userId, userMinutesBeforeUTC: self.user!.userMinutesBeforeUTC)
                    }
                    
                    lifesignAdded = true
                    stateLastLifesign = lifesignTime
                    lifesignsLastRead = Date.init(timeIntervalSince1970: 0) // invalidate caches

                    let supervision = self.getSupervision(groupmemberId: status.groupmemberId, supervisionId: status.supervisionId)

                    if (status.supervisionType == .Ongoing) {
                        let nextDeadline = self.updateCheckpoints(supervision: supervision!, lifesign: true, now: Date())
                        if (stateNextLifesign == nil) {
                            stateNextLifesign = nextDeadline
                        } else if (nextDeadline < stateNextLifesign!) {
                            stateNextLifesign = nextDeadline
                        }
                    } else {
                        if (supervision!.endingType != SupervisionEndingType.Spot) {
                            self.endCheckpoints(supervision: supervision!, now: Date())
                        }
                    }

                    if (!lifesignInGroup) {
                        // Give feedback to all users in groups
                        lifesignInGroup = true
                        pushToGroup.append(caregroup.caregroupId)
                    }
                }
            }
        }
        
        /* Simon: Replaced outside loop by call pushFeedback() with no parameters, which sends feedback to all members in all the users groups
        for groupid in pushToGroup {
            //self.cloudLayer.pushFeedback(userId: self.user!.userId, caregroupId: groupid, fromUser: self.user!.userName)
        }*/

        if (lifesignAdded == false) {
            lifesignsLastRead = Date.init(timeIntervalSince1970: 0) // invalidate caches
            stateLastLifesign = getLatestLifesign(userId: user!.userId)
        }
        
        self.pushFeedback()

        trace.end()
    }
    
    func clearLifesignsCache () {
        lifesignsLastRead = Date.init(timeIntervalSince1970: 0)
    }

    func getLatestLifesign (userId: String) -> Date? {
        trace.begin()

        var latestLifesign : Date?

        if (self.lifesignsLastRead.distance(to: Date()) > 10) {
            self.lifesigns = cloudLayer.getLifesigns(userid: userId) ?? [LifeSign]()
        }

        for lifesign in lifesigns {
            if (latestLifesign == nil) {
                latestLifesign = lifesign.latestLifesign
            } else {
                if (latestLifesign! < lifesign.latestLifesign) {
                    latestLifesign = lifesign.latestLifesign
                }
            }
        }
        
        self.lifesignsLastRead = Date()
            
        
        trace.end()
        return latestLifesign
    }
    
    func updateUserDetails(userId: String = "") {
        
        trace.begin()
        
        // Update user logged in
        var _userId = userId
        if(_userId == "") {
            _userId = (self.user?.userId)!
            trace.output("userId initialized to self.user?.userId: \(_userId)")
        }
        
        _ = cloudLayer.updateUser(userid: _userId, user: user!)
        
        trace.end()
    }
    
    func updateGroupMemberUserDetails(userid: String, gmuser: User!) {
        
        trace.begin()
        
        _ = cloudLayer.updateUser(userid: userid, user: gmuser!)
        
        trace.end()
    }
    
    func updateGroupMemberDetails(groupMember: GroupMember) {
        
        trace.begin()
        // todo !!!
        _ = cloudLayer.updateGroupmember(groupmember: groupMember)
        trace.end()
    }
    
    func updateGroupMember(groupMember: GroupMember) {
        trace.begin()
        _ = cloudLayer.updateGroupmember(groupmember: groupMember)
        trace.end()
    }

    
    func createNewUserAsInvited(_ name: String, _ email: String, cellno: String) -> String? {
    
        // The function will just return the userId of an existing user (email) otherwise a new user is created
        
        trace.begin()

        var userId = self.getUserID(email: email)
        
        // Check if the user already exists...
        if (userId != nil) {

            // The user data exists
            trace.output("Existing userID: \(String(describing: userId))")

        } else {

            // The user does not exist, so create with status 'invited'
            let tempUser = User()
            tempUser.userName = name
            tempUser.email = email.lowercased()
            var cloudUser = cloudLayer.createUser(user: tempUser)

            tempUser.cellno = cellno
            tempUser.status = UserStatusType.Invited
            tempUser.platform = PlatformType.Unknown
            cloudUser = cloudLayer.updateUser(userid: cloudUser.userId, user: tempUser)
            
            userId = cloudUser.userId
        
            trace.output("New userID: \(String(describing: userId))")
        }
        
        trace.end()
        
        return userId
    }
    

    func signinKnownUser() -> Bool {
        
        trace.begin()
        
        let email : String?  = KeychainWrapper.standard.string(forKey: "email")
        let password : String?  = KeychainWrapper.standard.string(forKey: "password")
        
        if(email == nil || password == nil) {
            return false
        }
        
        let error = cloudLayer.signIn(email: email!, password: password!)
        
        if (error != nil) {
            userSessionStatus = UserSessionStatusType.LoggedOut
            trace.error("Model:: Background login FAILED")
            
            trace.end()
            return false
        }
        
        userSessionStatus = UserSessionStatusType.LoggedIn
        trace.output("Model:: Background login succeded")
        
        trace.end()
        return true
    }
    
    func hasKnownUser() -> Bool {
        
        trace.begin()
        
        let email : String?  = KeychainWrapper.standard.string(forKey: "email")
        let password : String?  = KeychainWrapper.standard.string(forKey: "password")
        
        if(email == nil || password == nil) {
            trace.error("NO Known user in Keychain")
            trace.end()
            return false
        }
        trace.output("Known user stored in Keychain")
        
        trace.end()
        return true
    }
    
    func signupPressed(userName: String, email: String, password: String) -> SignupResult {
        
        trace.begin()
        
        let result = cloudLayer.newsignup(userName: userName, email: email.lowercased(), password: password)
        
        trace.end()
        return result // Return result to the caller, letting the viewController react on signin error
    }
    
    func forgotPassword(email: String) -> NSError? {

        trace.begin()
        
        let result = cloudLayer.forgotPassword(email: email.lowercased())
        
        trace.end()
        return result // Return result to the caller, letting the viewController react on signin error
    }

    func confirmForgotPassword(email: String, confirmationCodeValue: String, password: String) -> NSError? {

        trace.begin()
        
        let result = cloudLayer.confirmForgotPassword(email: email.lowercased(), confirmationCodeValue: confirmationCodeValue, password: password)
        
        trace.end()
        return result // Return result to the caller, letting the viewController react on signin error
    }
    
    func changePassword(email: String, password: String, newPassword: String) -> NSError? {
        
        trace.begin()
        
        let result = cloudLayer.changePassword(email: email.lowercased(), password: password, newPassword: newPassword)
        
        if (result==nil) {  // Update keychain with new email
            deviceLayer.saveEmail(email: email.lowercased())
            deviceLayer.savePassword(password: newPassword)
        }
        trace.end()
        return result // Return result to the caller, letting the viewController react on error
    }
    
    func changeEmail(email: String, newEmail: String) -> NSError? {
        
        trace.begin()
        
        let result = cloudLayer.changeEmail(email: email.lowercased(), newEmail: newEmail.lowercased())
        trace.end()
        return result // Return result to the caller, letting the viewController react on error
    }
    
    func confirmNewEmail(email: String, code: String) -> NSError? {
        
        trace.begin()
        
        let result = cloudLayer.confirmNewEmail(email: email.lowercased(), code: code)
        
        if (result==nil) {  // Update keychain with new email
            deviceLayer.saveEmail(email: email.lowercased())
        }
        trace.end()
        return result // Return result to the caller, letting the viewController react on error
    }
    
    func signinPressed(email: String, password: String) -> NSError? {

        trace.begin()
        
        let error = cloudLayer.signIn(email: email.lowercased(), password: password)

        if (error == nil) {
            userSessionStatus = UserSessionStatusType.LoggedIn
            trace.output("Model:: User login succeded")
        }
        else {
            userSessionStatus = UserSessionStatusType.LoggedOut
            trace.error("Model:: User login FAILED")
        }
        
        trace.end()
        return error // Return error to the caller, letting the viewController react on signin error
    }
    
    func signOut() {
        
        trace.begin()
        
        cloudLayer.signOut()
        
        userSessionStatus = UserSessionStatusType.LoggedOut
        deviceLayer.saveAuthMethod(method: AuthenticationMethod.Unknown)
        trace.output("Model:: User logout success")

        trace.end()
    }
    
    func deleteCognitoUser() {
        
        trace.begin()
        
        _ = cloudLayer.deleteCognitoUser()
        userSessionStatus = UserSessionStatusType.LoggedOut
        
        trace.output("Model:: User logout succeded")
        trace.end()
    }
    
    
    
    func getUserName () -> String {
        trace.begin()
        var name : String
        
        if (user == nil) {
            name = ""
        }
        else {
            name = user!.userName
        }
        trace.end()
        return name
    }
    
    func getEmail() -> String? {
        return deviceLayer.getEmail()
    }
    
    func getPassword()->String? {
        return deviceLayer.getPassword()
    }
    
    func getUserID(email: String) -> String? {
        
        let userId = cloudLayer.getUserID(email: email.lowercased())
        if (userId != nil) {
            deviceLayer.saveUserId(id: userId!)
        }
        
        return userId
    }
    
    func getUser(userid: String) -> User? {
        return cloudLayer.getUser(userid: userid)
    }
    
    func getUsers() -> [User] {
        return cloudLayer.getUsers()
    }
    
    func getAppStatus(userid: String) -> Status? {
        return cloudLayer.getAppStatus(userid: userid)
    }
    
    func setAppStatus(userid: String, status: AppStatusType) -> Status? {
        return cloudLayer.setAppStatus(userid: userid, status: status)
    }
    
    
    func getCareGroup(caregroupId: String = "") -> Caregroup{
        
        trace.begin()
        var caregroup : Caregroup?
        
        if caregroupId == "" {
            caregroup = self.caregroups![self.currentGroup]
        }
        
        if(caregroupId != "" ) {
            trace.output("OK")
            caregroup = cloudLayer.getCaregroup(caregroupid: caregroupId)
        } else {
            trace.error("NO valid caregroupId")
        }
        
        trace.end()
        return caregroup!
    }
    
    func getCareGroups(_userId: String = "", forceRefresh: Bool = false) {

        trace.begin()
        var userId = _userId
        
        if userId == "" {
            userId = (self.user?.userId)!
        }
        
        if(userId != "" && userId != "X" ) {
            trace.output("OK")
            if (self.groupsLastRead.distance(to: Date()) > 10 || forceRefresh==true) {
                self.caregroups = cloudLayer.getCaregroups(userid: user!.userId)
                self.groupsLastRead = Date()
            }
        } else {
            trace.error("NO valid userID")
        }
        
        trace.end()
    }

    func addCaregroup(userid: String, caregroup: Caregroup) ->String? {
        trace.begin()
        self.trace.output("Model - addCaregroup")
        trace.end()
        return cloudLayer.addCaregroup(userid: userid, caregroup: caregroup)
    }

    func updateCaregroup(userid: String, caregroup: Caregroup) ->String? {
        trace.begin()
        self.trace.output("Model - updateCaregroup")
        trace.end()
        return cloudLayer.updateCaregroup(userid: userid, caregroup: caregroup)
    }
    
    func deleteCaregroup(caregroupid: String) -> String? {
        trace.begin()
        self.trace.output("Model - deleteCaregroup")
        trace.end()
        return cloudLayer.deleteCaregroup(caregroupid: caregroupid)
    }

    func addGroupmember(userId: String, status: GroupMemberStatusType, caregroupId: String, memberId: String, isMonitor: Bool, isMonitored: Bool, isOwner: Bool) -> String? {
        trace.begin()
        self.trace.output("Model - addGroupmember")
        trace.end()
        return cloudLayer.addGroupmember(userId: userId, status: status, caregroupId: caregroupId, memberId: memberId, isMonitor: isMonitor, isMonitored: isMonitored, isOwner: isOwner)
    }
    
    func addTracking(tracking: Tracking) -> String? {
        trace.begin()
        self.trace.output("Model - addTracking")
        trace.end()
        return cloudLayer.addTracking(tracking: tracking)
    }
    
    func getMainStatusList(userId: String = "dummy", caregroupId: String) -> [MainStatus] {
        trace.begin()
        self.trace.output("Model - getMainStatusList")
        let statusList = cloudLayer.getMainStatusList(userId: userId, caregroupId: caregroupId)
        trace.end()
        return statusList
    }

    func getTracking(trackingId: String) -> Tracking? {
        trace.begin()
        self.trace.output("Model - getTracking")
        let tracking = cloudLayer.getTracking(trackingid: trackingId)
        trace.end()
        return tracking
    }

    func getLocations(supervision: Supervision) -> [Location] {
        trace.begin()
        self.trace.output("Model - getLocations")
        let locations = cloudLayer.getLocations(supervision: supervision)
        trace.end()
        return locations
    }

    func getLocation(trackingId: String, locationsId: String) -> Location? {
        trace.begin()
        self.trace.output("Model - getLocations")
        let location = cloudLayer.getLocation(trackingId: trackingId, locationsId: locationsId)
        trace.end()
        return location
    }

    func addGroupmember(userId: String, status: GroupMemberStatusType, memberId: String = "") -> String? {
        
        trace.begin()
        var groupmemberId : String?
        
        if (self.currentGroup >= 0) {
            trace.output("OK")
            groupmemberId = self.addGroupmember(userId: userId, status: status, caregroupId: self.caregroups?[self.currentGroup].caregroupId ?? "", memberId: memberId, isMonitor: false, isMonitored: false, isOwner: false)
        } else {
            trace.error("NO valid currentGroup")
            return nil
        }
        
        trace.end()
        return groupmemberId
    }
    
    func addGroupmember(_userId: String = "", _caregroupId: String = "", status: GroupMemberStatusType = GroupMemberStatusType.Active, memberId: String = "") -> String? {
        
        trace.begin()
        
        var userId = _userId
        var caregroupId = _caregroupId
        //var status = _status
        var groupmemberId : String?
        
        if(userId == "") {
            userId = (self.user?.userId)!
            trace.output("userId initialized to self.user?.userId: \(userId)")
        }

        if caregroupId == "" {
            if (self.currentGroup >= 0) {
                trace.output("OK")
                caregroupId = self.caregroups?[self.currentGroup].caregroupId ?? ""
            } else {
                trace.error("NO valid currentGroup")
                return nil
            }
        }
        
        groupmemberId = self.addGroupmember(userId: userId, status: status, caregroupId: caregroupId, memberId: memberId, isMonitor: false, isMonitored: false, isOwner: false)

        trace.end()
        return groupmemberId
    }
    
    func getGroupMembers() {
        
        trace.begin()
        
        if(self.currentGroup >= 0) {
            trace.output("OK")
            self.groupmembers = cloudLayer.getGroupmembers(caregroupId: self.caregroups?[self.currentGroup].caregroupId ?? "")
        } else {
            trace.error("NO valid currentGroup")
        }
        
        trace.end()
    }
    
    func getGroupmember(userId: String, caregroupId: String) -> GroupMember? {
        let groupMember = cloudLayer.getGroupmember(userId: userId, caregroupId: caregroupId)
        
        return groupMember
    }
    
    func getGroupMembers(caregroupId: String) -> [GroupMember] {
        
        let groupMembers = cloudLayer.getGroupmembers(caregroupId: caregroupId)
        
        return groupMembers ?? [GroupMember]()
    }

    func getGroupMessages(_caregroupId: String = "") {
        trace.begin()
        
        var caregroupId: String? = nil
        
        if(_caregroupId != "") {
            caregroupId = _caregroupId
            trace.output("CaregroupID passed as parameter: \(String(describing: caregroupId))")
        } else if(self.currentGroup >= 0) {
            caregroupId = self.caregroups?[self.currentGroup].caregroupId ?? ""
            trace.output("CaregroupID comes from current group (#\(self.currentGroup)): \(String(describing: caregroupId))")
        }
        
        if(caregroupId != nil) {
            trace.output("Get group messages from caregroup: \(String(describing: caregroupId))")
            self.groupMessages = cloudLayer.getGroupMessages(caregroupId: caregroupId!)

        } else {
            trace.error("NO valid currentGroup")
        }
        
        trace.end()
    }
    
    func getMessageGroup(messageId: String) -> String? {
        
        trace.begin()
        trace.output("messageId = \(messageId)")
        trace.end()
        return cloudLayer.getMessageGroup(messageId: messageId)
        
    }
    
    func checkEmail(email: String) -> Bool {
        
        trace.begin()
        
        if(self.getUserID(email: email.lowercased()) == nil) {
            trace.output("Email: \(email) does NOT exist in Dynamo")
            return false
        }
        else {
            trace.output("Email: \(email) EXISTS in Dynamo")
            return true
        }
    }
    
    func clearCachedUserNames () {
        userNameCacheDict = [String:String] ()
    }
    
    func getCachedUserName (userid: String) -> String {
        var name = ""
        
        if userNameCacheDict.keys.contains(userid) {
            name = userNameCacheDict[userid]!
        }
        else {
            if let senderUser = self.getUser(userid: userid) {
                name = senderUser.userName
                userNameCacheDict[userid] = name
            } else {
                name = "???"
            }
        }
        
        return name
    }

   
    func getSupervisions(caregroupId: String) -> [Supervision] {
        trace.begin()
        
        var supervisions = [Supervision]()

        if let remoteGroupmembers = cloudLayer.getGroupmembers(caregroupId: caregroupId) {
            for groupmember in remoteGroupmembers {
                if let remoteSupervisions = cloudLayer.getSupervisions(groupmemberId: groupmember.groupmemberId) {
                    for supervision in remoteSupervisions {
                        supervisions.append(supervision)
                    }
                }
            }
        }
        
        trace.end()
        
        return supervisions
    }
    
    func getAllSupervisions() -> [String:[Supervision]] {
        
        trace.begin()
        
        var supervisionByGroup = [String:[Supervision]]()
        
        getCareGroups()
        
        for caregroup in self.caregroups! {
            var supervisions = [Supervision]()

            if caregroup.status == CaregroupStatusType.Active {
                
                if let remoteGroupmembers = cloudLayer.getGroupmembers(caregroupId: caregroup.caregroupId) {
                    for groupmember in remoteGroupmembers {
                        if let remoteSupervisions = cloudLayer.getSupervisions(groupmemberId: groupmember.groupmemberId) {
                            for supervision in remoteSupervisions {
                                supervisions.append(supervision)
                            }
                        }
                    }
                }
                
            }
            supervisionByGroup[caregroup.caregroupId] = supervisions
        }
        
        trace.end()
        
        return supervisionByGroup
    }
    
    func getCaregroupHistory(caregroupId: String) -> [Date:Any] {
        
        trace.begin()
        var history = [Date:Any]()
        
        if let remoteGroupmembers = cloudLayer.getGroupmembers(caregroupId: caregroupId) {
            for groupmember in remoteGroupmembers {
                // Add lifesigns for all members in the group to HISTORY
                if let remoteLifesigns = cloudLayer.getLifesigns(userid: groupmember.userid) {
                    for lifesign in remoteLifesigns {
                        history[lifesign.latestLifesign] = lifesign
                    }
                }

                // Add alarms for all supervision in the group to HISTORY
                if let remoteSupervisions = cloudLayer.getSupervisions(groupmemberId: groupmember.groupmemberId) {
                    for supervision in remoteSupervisions {
                        if let remoteAlarms = cloudLayer.getAlarms(supervisionid: supervision.supervisionId) {
                            for alarm in remoteAlarms {
                                history[alarm.date] = alarm
                            }
                        }
                    }
                }
            }
        }

        trace.end()
        return history
    }
    
    func addSupervision(supervision: Supervision, now: Date = Date()) -> String? {
        trace.begin()
        self.trace.output("Model - addSupervision")
        supervision.supervisionId = cloudLayer.addSupervision(supervision: supervision)!
        
        _ = self.updateCheckpoints(supervision: supervision, lifesign: false, now: now)
        trace.end()
        return supervision.supervisionId
    }
    
    func updateSupervision(supervision: Supervision) -> String? {
        trace.begin()
        self.trace.output("Model - updateSupervision")
        trace.end()
        let id = cloudLayer.updateSupervision(supervision: supervision)

        _ = self.updateCheckpoints(supervision: supervision, lifesign: false, now: Date())
        trace.end()
        return id
    }

    func deleteSupervision(supervision: Supervision) -> String? {
        trace.begin()
        self.trace.output("Model - deleteSupervision")
        trace.end()
        return cloudLayer.deleteSupervision(supervision: supervision)
    }

    func getSupervision(groupmemberId: String, supervisionId: String) -> Supervision? {
        trace.begin()
        self.trace.output("Model - getSupervision")
        trace.end()
        return cloudLayer.getSupervision(groupmemberId: groupmemberId, supervisionId: supervisionId)
    }
    
    func updateCheckpoints(supervision: Supervision, lifesign: Bool, now: Date) -> Date {
        let offset = self.getUser(userid: supervision.userId)?.userMinutesBeforeUTC ?? 0
        let deadlineTime = self.deadlineFrom(now: now, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let deadlineCheckpoint = Checkpoint(supervision: supervision, type: .Deadline, time: deadlineTime, notifiedCount: 0)
        _ = cloudLayer.addCheckpoint(checkpoint: deadlineCheckpoint)

        let countdownTime = self.countdownFrom(deadline: deadlineTime, supervision: supervision)
        let countdownCheckpoint = Checkpoint(supervision: supervision, type: .Countdown, time: countdownTime, notifiedCount: 0)
        _ = cloudLayer.addCheckpoint(checkpoint: countdownCheckpoint)
        
        return deadlineTime
    }

    func endCheckpoints(supervision: Supervision, now: Date) {
        _ = cloudLayer.deleteCheckpoints(supervisionid: supervision.supervisionId)

        let countdownCheckpoint = Checkpoint(supervision: supervision, type: .Ended, time: now, notifiedCount: 1)
        _ = cloudLayer.addCheckpoint(checkpoint: countdownCheckpoint)
    }

    func deadlineFrom(now: Date, supervision: Supervision, localUserOffsetMinutes: Int, lifesign: Bool) -> Date {
        trace.begin()
        var deadlineTime = now
        var nowcloseEnough = now
        trace.output("now = " + now.description);

        if (supervision.supervisionType == .Ongoing) {
            if (supervision.interval ?? -1 > -1) {
                deadlineTime.addTimeInterval(supervision.interval!)
                trace.output("Fixed interval, deadline = " + deadlineTime.description);
             } else {
                // FASTE TIDSPUNKTER
                trace.output("Fixed times, deadline1 = " + supervision.deadline1!.description)
                var deadlines = [TimeInterval]()
                deadlines.append(supervision.deadline1!)

                // closeEnoughMinutes er 1, 2 eller 3 timer for hhv. 3 tidspunkter, 2 eller 1
                var closeEnoughMinutes = 180

                if (supervision.deadline2 ?? -1 > -1) {
                    closeEnoughMinutes = 120;
                    deadlines.append(supervision.deadline2!)
                    deadlines = deadlines.sorted()
                    trace.output("Fixed times, deadline2 = " + supervision.deadline2!.description);
                }
                if (supervision.deadline3 ?? -1 > -1) {
                    closeEnoughMinutes = 60;
                    deadlines.append(supervision.deadline3!)
                    deadlines = deadlines.sorted()
                    trace.output("Fixed times, deadline3 = " + supervision.deadline3!.description);
                }

                if (!lifesign) {
                    closeEnoughMinutes = 0
                    // Vi lægger kun minutter til, hvis der er givet livstegn
                    // Således ingen tillæg, hvis man laver CreateSupervision eller UpdateSupervision
                } else {
                    nowcloseEnough.addTimeInterval(Double(60 * closeEnoughMinutes))
                }
                
                // Vi tilpasser deadlines, så det er UTC tidspunkter.
                var utcDeadlines = [TimeInterval]()
                for deadline in deadlines {
                    let diff : Double = Double(60 * (-localUserOffsetMinutes))
                    var utcDeadline : Double = deadline + diff
                    if (utcDeadline < 0) {
                        utcDeadline = utcDeadline + 86400
                    }
                    if (utcDeadline >= 86400) {
                        utcDeadline = utcDeadline - 86400
                    }
                    utcDeadlines.append(utcDeadline)
                    trace.output("Appended utcDeadline = " + utcDeadline.description);
                }
                utcDeadlines = utcDeadlines.sorted()
                
                
                var utcCalendar = Calendar(identifier: .gregorian)
                utcCalendar.timeZone = TimeZone(identifier: "UTC")!
                // Fremstil dates, passende til deadline Timeintervals
                var nextDeadline : Date?
                var utcDeadlineDates = [Date]()
                for utcDeadline in utcDeadlines {
                    let hour = Int(utcDeadline / 3600)
                    let secondsLeft = Double(utcDeadline) - Double(hour * 3600)
                    let minute = Int(secondsLeft / 60)
                    let date = utcCalendar.date(bySettingHour: hour, minute: minute, second: 0, of: now)!
                    utcDeadlineDates.append(date)
                    trace.output("Evaluaring appended utcDeadlineDate = " + date.description + " against " + nowcloseEnough.description)
                    if (date > nowcloseEnough) {
                        nextDeadline = date
                        trace.output("nextDeadline = " + nextDeadline!.description);
                        break
                    }
                }
                
                if (nextDeadline != nil) {
                    deadlineTime = nextDeadline!
                } else {
                    for utcDeadlineDate in utcDeadlineDates {
                        let oneDayLater = utcDeadlineDate.addingTimeInterval(86400)
                        trace.output("Evaluaring oneDayLater = " + oneDayLater.description + " against " + nowcloseEnough.description)
                        if (oneDayLater > nowcloseEnough) {
                            deadlineTime = oneDayLater
                            trace.output("deadlineTime = " + deadlineTime.description);
                            break
                        }
                    }
                }
                
                trace.output("Deadline (final) = " + deadlineTime.description);
            }
        } else {
            deadlineTime = supervision.periodEnd!

            trace.output("Temporary, deadline = " + deadlineTime.description);
        }
        trace.end()

        return deadlineTime
    }

    func countdownFrom(deadline: Date, supervision: Supervision) -> Date {
        trace.begin()
        let countdown = deadline.addingTimeInterval(-supervision.countdownTime)

        trace.output("Countdown = " + countdown.description);

        trace.end()
        return countdown
    }
    
    func addInvitation(fromUserId: String, toUserId: String = "", sameInvitation: String = "") -> String? {
        trace.begin()
        
        var invitationId : String? = nil
        
        if(self.currentGroup >= 0) {
            trace.output("OK")
            invitationId = cloudLayer.addGroupInvitation(fromUserId: fromUserId, toUserId: toUserId, caregroupId: self.caregroups![self.currentGroup].caregroupId, invitation: sameInvitation)
            //invitationId = cloudLayer.addGroupInvitation(userId: toUserId, caregroupId: self.caregroups![self.currentGroup].caregroupId, senderId: fromUserId, invitation: sameInvitation)
        } else {
            trace.error("NO valid currentGroup")
        }
        
        trace.end()
        return invitationId
    }
    
    func storeInvitationId(id: String){
        trace.begin()
        
        var invitations = self.deviceLayer.getInvitationIds()
        
        // Append invitationId if it is not already there...
        if (!(invitations?.contains(id))!) {
            invitations?.append(id)
            self.deviceLayer.saveInvitationIds(invitations: invitations!)
        }
        trace.end()
    }

    func listInvitationIds()->[String]? {
        trace.begin()
        
        return self.deviceLayer.getInvitationIds()
    }
    
    func deleteInvitationId(id: String){
        trace.begin()
        
        var invitations = self.deviceLayer.getInvitationIds()
        
        // Find index for det givne 'invitationId' i listen
        let index = invitations!.firstIndex(of: id)
        if (index != nil) {
            invitations?.remove(at: index!)
            self.deviceLayer.saveInvitationIds(invitations: invitations!)
        }
        trace.end()
    }
    
    func deleteAllInvitationIds(){
        trace.begin()
        
        var invitations = self.deviceLayer.getInvitationIds()
        invitations?.removeAll()
        self.deviceLayer.saveInvitationIds(invitations: invitations!)
        
        trace.end()
    }

    func haveInvitations(_userId: String = "") -> Bool {
        trace.begin()
        
        var userId = _userId
        if(userId == "") {
            userId = (self.user?.userId)!
            trace.output("userId initialized to self.user?.userId: \(userId)")
        }
        
        trace.end()
        return cloudLayer.haveGroupInvitations(userId: userId)
    }

    func getInvitations(_userId: String = "") -> [Invitation]? {
        
        trace.begin()
        
        var userId = _userId
        if(userId == "") {
            userId = (self.user?.userId)!
            trace.output("userId initialized to self.user?.userId: \(userId)")
        }
    

        // Invitations from existing Lifesign users (Invitation in DynamoDB)
        let invitations = cloudLayer.getGroupInvitations(userId: userId)
        var newlist = [Invitation]()
        
        for invitation in invitations! {
            
            // Add the name of the user sending the invitation
            let senderId = invitation.invitedByUserId
            let sender = self.getUser(userid: senderId!)
            invitation.senderName = sender!.userName
            
            // Add the name of the caregroup from the invitation
            let caregroupId = invitation.caregroupId
            let caregroup = self.getCareGroup(caregroupId: caregroupId!)
            invitation.caregroupId = caregroup.caregroupId
            invitation.caregroupName = caregroup.caregroupName
            
            newlist.append(invitation)
        }
        
        // Invitations received via link in SoMe (stored in Keychain)
        let invitationIdList = self.listInvitationIds()  // From universal links
        
        for invitationId in invitationIdList! {
            
            // Verify that the invitation exists before adding to the list
            let invitation = cloudLayer.getGroupInvitation(userId: "INVITATION", invitationId: invitationId)
            
            if(invitation!.invitationId != "" && invitation!.status?.description == InvitationStatusType.Sent.description) {
            
                // Add the name of the user sending the invitation
                let senderId = invitation!.invitedByUserId
                let sender = self.getUser(userid: senderId!)
                invitation!.senderName = sender!.userName
                
                // Add the name of the caregroup from the invitation
                let caregroupId = invitation!.caregroupId
                let caregroup = self.getCareGroup(caregroupId: caregroupId!)
                invitation!.caregroupId = caregroup.caregroupId
                invitation!.caregroupName = caregroup.caregroupName
                
                newlist.append(invitation!)
            }
        }
        
        trace.end()
        return newlist
    }
    
    func pushFeedback(_caregroupId: String = "")
    {
        trace.begin()
        var caregroupId = _caregroupId
        if(caregroupId == "") {
            caregroupId = ""  //(self.user?.userId)!
            trace.output("caregroupId initialized to : \(caregroupId)")
        }
        //self.user = self.getUser(userid: self.user!.userId)
        self.cloudLayer.pushFeedback(userId: self.user!.userId, caregroupId: caregroupId, fromUser: self.user!.userName)
    }
    
    func getInvitation(_userId: String = "", invitationId: String) -> Invitation? {
        
        trace.begin()
        
        var userId = _userId
        if(userId == "") {
            userId = (self.user?.userId)!
            trace.output("userId initialized to self.user?.userId: \(userId)")
        }
        
        let invitation = cloudLayer.getGroupInvitation(userId: userId, invitationId: invitationId)
        // If you want to modify the data??
        
        trace.end()
        return invitation!
        
    }
    
    func pushInvitation(userId: String, fromUser: String, toGroup: String, invitationId: String) -> String? {
        
        trace.begin()
        
        cloudLayer.pushGroupInvitation(userId: userId, invitationId: invitationId, fromUser: fromUser, toGroup: toGroup)
        
        trace.end()
        return invitationId
    }
    
    func acceptInvitation(userId: String, invitationId: String) {
        
        trace.begin()
        var invitation: Invitation?
        
        invitation = getInvitation(_userId: userId, invitationId: invitationId)
        
        let caregroupId = invitation!.caregroupId
        let groupmember = GroupMember()
        
        if(invitation?.invitationToUserId == "INVITATION") {
            _ = self.addGroupmember(_caregroupId: caregroupId!)
            self.deleteInvitationId(id: invitationId)
        } else {

        }
        
        groupmember.userid = userId
        groupmember.caregroupId = caregroupId!
        groupmember.status = GroupMemberStatusType.Active
        
        _ = cloudLayer.updateGroupmember(groupmember: groupmember)
        _ = cloudLayer.updateGroupInvitation(userId: userId,
                                         invitationId: invitationId,
                                         status: InvitationStatusType.Accepted)
        
        trace.end()
    }
    
    func rejectInvitation(userId: String, invitationId: String) {

        trace.begin()
        var invitation: Invitation?
        
        invitation = getInvitation(_userId: userId, invitationId: invitationId)
        
        if(invitation?.invitationToUserId == "INVITATION") {
            self.deleteInvitationId(id: invitationId)
        } else {
            
            let groupmember = GroupMember()
            groupmember.userid = userId
            groupmember.status = GroupMemberStatusType.Inactivated
            
            _ = cloudLayer.updateGroupmember(groupmember: groupmember)
        }
        
        _ = cloudLayer.updateGroupInvitation(userId: userId,
                                        invitationId: invitationId,
                                        status: InvitationStatusType.Rejected)

        
        trace.end()
    }
    
    /*
     **  Amazon Simple Notification Service (SNS)
     **  Setup push notifications
     */
    
    
    func setupPushNotifications(loggedIn: Bool, tokenReceived: Bool) {
        
        trace.begin()
        
        // First check that both 1) user is logged in and 2) that devicetoken exists - if not, then return
        if (loggedIn == false || tokenReceived == false) {
            trace.end()
            return
        }
        
        // First get the user identity based on the authenticated user profile
        let email : String? = deviceLayer.getEmail()
        let userID = getUserID(email: email!)

        var platformARN : String? = nil
        var endpointARN : String? = nil
        var topicARN : String? = nil
        
        if(userID != nil) {
            // Authenticate the user
            cloudLayer.snsAuthneticateUser(userId: userID!, email: email!)
            
            // Setup platform, endpoint, topic & subscription
            platformARN = cloudLayer.snsCreatePlatformARN(userId: userID!, email: email!)
        } else {
            let alertController = UIAlertController(title: "setupPushNotifications",
                                                    message: "userID = nil",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion:nil)
        }
        
        let token = deviceLayer.getDeviceToken()
        
        if((token != nil) && (platformARN != nil)) {
            endpointARN = cloudLayer.snsCreateEndpointARN(token: token!, platformARN: platformARN!)
        } else {
            if(token == nil) {
            let alertController = UIAlertController(title: "setupPushNotifications",
                                                    message: "token = nil",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion:nil)
            }

            if(platformARN == nil) {
                let alertController = UIAlertController(title: "setupPushNotifications",
                                                        message: "platformARN = nil",
                                                        preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion:nil)
            }

        }
        
        topicARN = cloudLayer.snsCreateTopicARN(userID: userID!)
        
        if((endpointARN != nil) && (topicARN != nil)) {
        _ = cloudLayer.snsCreateSubscriptionARN(endpointARN: endpointARN!, topicARN: topicARN!)
        } else {
            let alertController = UIAlertController(title: "setupPushNotifications",
                                                    message: "endpointARN or topicARN = nil",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion:nil)
        }
        
        trace.end()
    }
    
    func getCurrentWifiName() -> String {
        return deviceLayer.getCurrentWifiName()
    }
    
    /*
     Location services
     */

    let geoCoder = CLGeocoder()
    var unprocessedLocations = [Location]()
    private var lastSavedLocation : Location?
    var currentTracking : Tracking?
    var currentSupervision : Supervision?
    
    func startOrStopTracking(supervision: Supervision) {
        if (supervision.supervisionType == SupervisionType.Period)
            && (supervision.status == SupervisionStatusType.Active)
            && (supervision.endingType == SupervisionEndingType.Spot)
            && (supervision.periodStart! < Date())
            && (supervision.userId == self.user!.userId)
            && (!self.isTracking()) {
            
            let justTrackingId = Tracking()
            justTrackingId.trackingId = supervision.trackingId!
            _ = self.activateTracking(tracking: justTrackingId)
        } else if (self.isTracking()){
            self.deactiveTracking()
        }
    }

    func checkForTrackingReactivation (mainStatusList: [MainStatus]) {
        
        for status in mainStatusList {
            if (status.userId == self.user!.userId)
                && (status.supervisionType == .Period)
                && (status.trackingStatus == .Active) {

                let supervision = self.getSupervision(groupmemberId: status.groupmemberId, supervisionId: status.supervisionId)
                if (supervision != nil) {
                    self.startOrStopTracking(supervision: supervision!)
                }
            }
        }
        
        self.checkShouldBeTracking = true
    }

    func isTracking () -> Bool {
        var trackingActivate = false

        if (currentTracking != nil) {
            trackingActivate = true
        }
        
        return trackingActivate
    }
    
    func activateTracking (tracking: Tracking) -> Bool {
        var trackingActivated = false

        if (!self.isTracking()) {
            self.checkForLocationAutorization()

            unprocessedLocations = [Location]()
            currentTracking = self.cloudLayer.getTracking(trackingid: tracking.trackingId)
            currentSupervision = self.cloudLayer.getSupervision(groupmemberId: currentTracking!.groupmemberId, supervisionId: currentTracking!.supervisionId)
            currentTracking!.status = TrackingStatusType.Active
            _ = self.cloudLayer.updateTracking(tracking: currentTracking!)

            self.deviceLayer.getLocationManager().startUpdatingLocation()
            self.deviceLayer.getLocationManager().delegate = self
            
            // Uncomment following code to enable fake visits
            self.deviceLayer.getLocationManager().distanceFilter = 25 // 0
            if (self.deviceLayer is DeviceLayerImplementation) {
                self.deviceLayer.getLocationManager().allowsBackgroundLocationUpdates = true // 1
            }
            self.deviceLayer.getLocationManager().startUpdatingLocation()  // 2

            trackingActivated = true
        }
        
        return trackingActivated
    }
    
    func deactiveTracking () {
        self.deviceLayer.getLocationManager().stopUpdatingLocation()
        
        if (unprocessedLocations.count > 0) {
            for unprocessLocation in unprocessedLocations {
                _ = self.cloudLayer.addLocation(location: unprocessLocation) ?? ""
            }
            
            unprocessedLocations = [Location]()
        }

        self.currentTracking = nil
        self.lastSavedLocation = nil
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for newLocation in locations {
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else {
                continue
            }
            
            var trackingUpdated = false
            
            if currentTracking != nil {
                let thisLocationId = "LOC-" + UUID().uuidString
                
                if currentTracking!.leftSafe == false {
                    var distanceFromSafe : Double = 0
                    
                    if let supervisionSafeSpotCL = currentSupervision?.safeSpot {
                        distanceFromSafe = newLocation.distance(from: supervisionSafeSpotCL)
                    }

                    if distanceFromSafe > 100 {
                        currentTracking!.onSafe = false
                        currentTracking!.leftSafe = true
                        currentTracking!.backSafe = false
                        currentTracking!.latestLocationId = thisLocationId
                        _ = self.cloudLayer.updateTracking(tracking: currentTracking!)
                        trackingUpdated = true
                    } else if currentTracking!.onSafe == false {
                        currentTracking!.onSafe = true
                        currentTracking!.leftSafe = false
                        currentTracking!.backSafe = false
                        currentTracking!.latestLocationId = thisLocationId
                        _ = self.cloudLayer.updateTracking(tracking: currentTracking!)
                        trackingUpdated = true
                    }
                } else {
                    // leftSave = true
                    var distanceFromSafe : Double = 0

                    if let supervisionSafeSpotCL = currentSupervision?.safeSpot {
                        distanceFromSafe = newLocation.distance(from: supervisionSafeSpotCL)
                    }

                    if distanceFromSafe < 50 {
                        currentTracking!.onSafe = true
                        currentTracking!.backSafe = true
                        currentTracking!.status = .Ended
                        currentTracking!.latestLocationId = thisLocationId
                        _ = self.cloudLayer.updateTracking(tracking: currentTracking!)
                        trackingUpdated = true

                        _ = self.cloudLayer.addLifesign(userid: currentTracking!.userId, userMinutesBeforeUTC: self.user!.userMinutesBeforeUTC)
                        self.endCheckpoints(supervision: currentSupervision!, now: Date())
                    }
                }
                
                // Vi gemmer altid loction, hvis der er en statusændring. Det er der, hvis vi har opdateret Tracking.
                var saveLocation = trackingUpdated
                if (saveLocation == false && lastSavedLocation == nil) {
                    saveLocation = true
                }
                
                if (saveLocation == false && lastSavedLocation != nil) {
                    let howRecent = newLocation.timestamp.timeIntervalSince(lastSavedLocation!.time)
                    let lastCL = CLLocation.init(latitude: lastSavedLocation!.latitude, longitude: lastSavedLocation!.longitude)
                    let distance = newLocation.distance(from: lastCL)

                    if (abs(howRecent) > 60 && distance > 100) {
                        saveLocation = true
                    } else {
                        self.trace.output("Location dropped, howRecent = " + abs(howRecent).description + ", distance = " + distance.description)
                    }
                }
                
                if (saveLocation) {
                    self.getAddress(location: newLocation.coordinate) { address, error in
                        guard address != nil && error == nil else {
                            print("error = \(String(describing: error))")
                            return
                        }

                        let location = Location.init(newLocation.coordinate, time: newLocation.timestamp, address: address!, tracking: self.currentTracking!)
                        location.locationId = thisLocationId
                        self.lastSavedLocation = location

                        if let locationId = self.cloudLayer.addLocation(location: location) {
                            self.trace.output("Location added: " + locationId)
                        } else {
                            self.unprocessedLocations.append(location)
                        }
                    }
                }

            }
        }
        
        if currentTracking != nil {
            if currentTracking!.status == .Ended {
                let seconds = 1.0
                DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                    // Deaktivér, når den sidste
                    self.deactiveTracking()
                }
            }
        }
    }
    
    func getCoordinates(address: String) -> CLLocationCoordinate2D {
        self.checkForLocationAutorization()

        return deviceLayer.getCoordinates(address: address)
    }

    func getAddress(location: CLLocationCoordinate2D, completion: @escaping (String?, Error?) -> ()) {
        self.checkForLocationAutorization()
        
        let geoCoder = CLGeocoder()
        let clLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
        
        geoCoder.reverseGeocodeLocation(clLocation, preferredLocale: Locale.current) { placemarks, error in
            guard placemarks != nil && error == nil else {
                completion(nil, error)
                return
            }
            
            if let pm = placemarks!.first {
                var retAddress = ""
                let street : String = pm.thoroughfare ?? ""
                let streetNumber : String  = pm.subThoroughfare ?? ""
                let zip : String  = pm.postalCode ?? ""
                let iso : String  = pm.isoCountryCode ?? ""
                retAddress = street + " " + streetNumber + ", " + zip + ", " + iso
                
                print(retAddress)
                completion(retAddress, nil)
            } else {
                completion("", nil)
            }
        }
        
    }
    
    func locationTrackingStatusForUI() -> String {
        let uiString = currentTracking == nil ? "" : currentTracking!.status.description
        
        return uiString
    }
    
    func checkForLocationAutorization() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            self.deviceLayer.getLocationManager().requestWhenInUseAuthorization()
        }
    }
}



