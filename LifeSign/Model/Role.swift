//
//  Role.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 29/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation
/*
class Role : NSObject, Codable  {
    var roleId : String
    var roleType: RoleType
    var groupmemberId: String?
    
    
    enum CodingKeys: String, CodingKey
    {
        case roleId = "roleId"
        case roleType = "roletype"
        case groupmemberId = "groupmemberId"
    }
    
    override init() {
        self.roleId = ""
        self.roleType = RoleType.Owner
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.roleId = try container.decodeIfPresent(String.self, forKey: .roleId) ?? ""
        self.roleType = try container.decodeIfPresent(RoleType.self, forKey: .roleType) ?? RoleType.Owner
        self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? nil
    }
}

enum RoleType : Int, Codable {
    case Monitored = 0, Monitors, Payer, Owner
    
    static var count: Int { return RoleType.Owner.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Monitored : return "monitored"
        case .Monitors : return "monitors"
        case .Payer : return "payer"
        case .Owner : return "owner"
        }
    }
    
    static func enumFromString(string:String) -> RoleType? {
        var i = 0
        while let item = RoleType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }}
*/
