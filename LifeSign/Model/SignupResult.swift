//
//  SignupResult.swift
//  AWSAPIGateway
//
//  Created by Peter Rosendahl on 03/05/2019.
//

import Foundation

class SignupResult : NSObject {
    var error : NSError?
    var email : String?
    var user : String?
    var status : NewUserStatusType
    
    override init() {
        status = NewUserStatusType.Unknown
    }
}

enum NewUserStatusType : Int, Codable {
    case Unknown = 0, Confirmed, Unconfirmed
    
    static var count: Int { return NewUserStatusType.Unconfirmed.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Unknown : return "unknown"
        case .Confirmed: return "confirmed"
        case .Unconfirmed : return "unconfirmed"
        }
    }
    
    static func enumFromString(string:String) -> NewUserStatusType? {
        var i = 0
        while let item = NewUserStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}
