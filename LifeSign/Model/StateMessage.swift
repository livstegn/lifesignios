//
//  StateMessage.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 09/11/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation

class StateMessage : NSObject  {
    var monitoredUserName : String
    var userId : String
    var caregroupName : String
    var caregroupId : String
    var type: StateMessageType
    var severity: StateMessageSeverity
    var supervisionId: String

    override init() {
        self.monitoredUserName = ""
        self.userId = ""
        self.caregroupName = ""
        self.caregroupId = ""
        self.type = StateMessageType.NoDueLifesignYou
        self.severity = StateMessageSeverity.Info
        self.supervisionId = ""
    }
    
    init(_ monitoredUserName : String, _ userId: String, _ caregroupName : String, _ caregroupId : String, _ type: StateMessageType, _ severity: StateMessageSeverity, supervisionId: String)  {
        self.monitoredUserName = monitoredUserName
        self.userId = userId
        self.caregroupName = caregroupName
        self.caregroupId = caregroupId
        self.type = type
        self.severity = severity
        self.supervisionId = supervisionId
    }
    
    func text () -> String {
        var text = ""

        switch self.type {
        case StateMessageType.YouMonitor :
            text = NSLocalizedString("You monitor #1#", comment: "Stateview, message list: You monitor another member").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.YouAreMonitored :
            text = NSLocalizedString("You are monitored", comment: "Stateview, message list: You are monitored")
        case StateMessageType.NoDueLifesignYou :
            text = NSLocalizedString("No due lifesign from you", comment: "Stateview, message list: you dont lack any lifesigns")
        case StateMessageType.NoDueLifesignOther  :
            text = NSLocalizedString("No due lifesign from #1#", comment: "Stateview, message list: user #1# dont lack any lifesigns").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.SoonDueLifesignOther :
            text = NSLocalizedString("Soon due lifesign from #1#", comment: "Stateview, message list: a member is close to deadline").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.SoonDueLifesignYou :
            text = NSLocalizedString("Soon due lifesign from you", comment: "Stateview, message list: you are close to deadline")
        case StateMessageType.AfterDeadline :
            text = NSLocalizedString("#1#: Lifesign is given after deadline", comment: "Stateview, message list: a lifesign is received, but too late").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.DueLifesignOther :
            text = NSLocalizedString("Due lifesign from #1#", comment: "Stateview, message list: another member is close to deadline").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.DueLifesignYou :
            text = NSLocalizedString("Due lifesign from you", comment: "Stateview, message list: you are close to deadline")
        case StateMessageType.InformTheGroup :
            text = NSLocalizedString("You could info the group", comment: "Stateview, message list: give a relaxing message to the group, if you were late")
        case StateMessageType.GivenInTime :
            text = NSLocalizedString("Lifesign is given in time", comment: "Stateview, message list: a lifesign is received in time")
        case StateMessageType.EndedInTimeOther :
            text = NSLocalizedString("#1# ended ad hoc supervison in time", comment: "Stateview, message list: a member ended periodic supervision in time").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.EndedInTimeYou :
            text = NSLocalizedString("You ended ad hoc supervison in time", comment: "Stateview, message list: a member ended periodic supervision in time")
        case StateMessageType.UpcomingOther :
            text = NSLocalizedString("#1# will be supervised soon", comment: "Stateview, message list: a member has an upcoming periodic supervision").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.UpcomingYou :
            text = NSLocalizedString("You will be supervised soon", comment: "Stateview, message list: you have an upcoming periodic supervision in time")

            // SafespotUpcoming, SafespotActive, SafespotActiveLeftSafe, SafespotActiveOverdue, SafespotEndedBackSafe, SafespotEndedNeverLeftSafe, SafespotEndedNotOn
        case StateMessageType.SafespotUpcoming :
            text = NSLocalizedString("#1# will be tracked", comment: "Stateview, message list: a member has an upcoming periodic supervision with safe spot tracking").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.SafespotActive :
            text = NSLocalizedString("#1# is tracked now", comment: "Stateview, message list: a member has a supervision with an active safe spot tracking").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.SafespotActiveLeftSafe :
            text = NSLocalizedString("#1# has left safe spot", comment: "Stateview, message list: a member being tracked has left the safe spot").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.SafespotActiveOverdue :
            text = NSLocalizedString("#1# is not on safe spot in yet", comment: "Stateview, message list: a member being tracked is not on safe spot yet (not overdue)").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.SafespotEndedBackSafe :
            text = NSLocalizedString("#1# is back on safe spot", comment: "Stateview, message list: a member being tracked is back on safe spot in time").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.SafespotEndedNeverLeftSafe :
            text = NSLocalizedString("#1# never left safe spot", comment: "Stateview, message list: a member didn't leave the safe spot during the tracking periode").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        case StateMessageType.SafespotEndedTooLate :
            text = NSLocalizedString("#1# back on safe spot to late", comment: "Stateview, message list: a member being tracked is not on safe spot, and now overdue").replacingOccurrences(of: "#1#", with: self.monitoredUserName)
        }
        
        return text
    }
}

enum StateMessageType : Int, Codable {
    case YouMonitor = 0, YouAreMonitored, NoDueLifesignYou, NoDueLifesignOther, SoonDueLifesignOther, SoonDueLifesignYou, AfterDeadline, DueLifesignOther, DueLifesignYou, InformTheGroup, GivenInTime, EndedInTimeOther, EndedInTimeYou, UpcomingOther, UpcomingYou, SafespotUpcoming, SafespotActive, SafespotActiveLeftSafe, SafespotActiveOverdue, SafespotEndedBackSafe, SafespotEndedNeverLeftSafe, SafespotEndedTooLate
    
    static var count: Int { return StateMessageType.SafespotEndedTooLate.hashValue + 1 }
}

enum StateMessageSeverity : Int, Codable {
    case Info = 0, Warning = 1, Alarm = 2
    
    static var count: Int { return StateMessageSeverity.Alarm.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Info : return "Info"
        case .Warning : return "Warning"
        case .Alarm : return "Alarm"
        }
    }
}

