//
//  Status.swift
//  LifeSign
//
//  Created by Simon Jensen on 26/09/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class Status : NSObject, Codable  {
    var userId : String
    var status: AppStatusType
    var lastUpdate : Date?

    enum CodingKeys: String, CodingKey
    {
        case userId = "userId"
        case status = "status"
        case lastUpdate = "createdate"
    }
    
    override init() {
        userId = ""
        status = AppStatusType.NotRunning
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        status = try container.decodeIfPresent(AppStatusType.self, forKey: .status) ?? AppStatusType.NotRunning
        lastUpdate = try container.decodeIfPresent(Date.self, forKey: .lastUpdate) ?? nil
    }
    
    init(_ userId: String, _ status: AppStatusType, _ lastUpdate: Date) {
        self.userId = userId
        self.status = status
        self.lastUpdate = lastUpdate
    }

}

enum AppStatusType : Int, Codable {
    case NotRunning = 0, Inactive, Background, Active
    
    static var count: Int { return AppStatusType.Active.hashValue + 1 }
    
    var description: String {
        switch self {
        case .NotRunning: return "not running"
        case .Inactive : return "inactive"
        case .Background : return "background"
        case .Active : return "active"
        }
    }

    static func enumFromString(string:String) -> AppStatusType? {
        var i = 0
        while let item = AppStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}
