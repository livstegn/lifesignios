//
//  Supervision.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 16/06/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation
import CoreLocation

class Supervision : NSObject, Codable  {
    var groupmemberId : String
    var supervisionId : String
    var status: SupervisionStatusType
    var deadline1 : TimeInterval?
    var deadline2 : TimeInterval?
    var deadline3 : TimeInterval?
    var countdownTime : TimeInterval
    var interval : TimeInterval?
    var supervisionType: SupervisionType
    var periodStart : Date?
    var periodEnd : Date?
    var userId : String
    var caregroupId : String
    var endingType: SupervisionEndingType?
    var wifi : String?
    var locationInterval : Int?
    var locationLatitude : Double?
    var locationLongitude : Double?
    var trackingId : String?
    let trace = NoTraces()

    enum CodingKeys: String, CodingKey
    {
        case groupmemberId = "groupmemberId"
        case supervisionId = "supervisionId"
        case status = "status"
        case deadline1 = "deadline1"
        case deadline2 = "deadline2"
        case deadline3 = "deadline3"
        case countdownTime = "countdownTime"
        case interval = "interval"
        case supervisionType = "supervisionType"
        case periodStart = "periodStart"
        case periodEnd = "periodEnd"
        case userId = "userId"
        case caregroupId = "caregroupId"
        case endingType = "endingType"
        case wifi = "wifi"
        case locationInterval = "locationInterval"
        case locationLatitude = "locationLatitude"
        case locationLongitude = "locationLongitude"
        case trackingId = "trackingId"
    }
    
    override init() {
        self.groupmemberId = ""
        self.supervisionId = ""
        self.status = SupervisionStatusType.Inactive
        self.deadline1 = 0
        self.deadline2 = 0
        self.deadline3 = 0
        self.countdownTime = 0
        self.interval = nil
        self.supervisionType = SupervisionType.Ongoing
        self.userId = ""
        self.caregroupId = ""
        self.endingType = nil
        self.wifi = nil
        self.locationInterval = nil
        self.locationLatitude = nil
        self.locationLongitude = nil
        self.trackingId = nil
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? ""
        self.supervisionId = try container.decodeIfPresent(String.self, forKey: .supervisionId) ?? ""
        self.status = try container.decodeIfPresent(SupervisionStatusType.self, forKey: .status) ?? SupervisionStatusType.Active
        self.deadline1 = try container.decodeIfPresent(TimeInterval.self, forKey: .deadline1) ?? nil
        self.deadline2 = try container.decodeIfPresent(TimeInterval.self, forKey: .deadline2) ?? nil
        self.deadline3 = try container.decodeIfPresent(TimeInterval.self, forKey: .deadline3) ?? nil
        self.countdownTime = try container.decodeIfPresent(TimeInterval.self, forKey: .countdownTime) ?? 0
        self.interval = try container.decodeIfPresent(TimeInterval.self, forKey: .interval) ?? nil
        self.supervisionType = try container.decodeIfPresent(SupervisionType.self, forKey: .supervisionType) ?? SupervisionType.Ongoing
        self.periodStart = try container.decodeIfPresent(Date.self, forKey: .periodStart) ?? nil
        self.periodEnd = try container.decodeIfPresent(Date.self, forKey: .periodEnd) ?? nil
        self.userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        self.caregroupId = try container.decodeIfPresent(String.self, forKey: .caregroupId) ?? ""
        self.endingType = try container.decodeIfPresent(SupervisionEndingType.self, forKey: .endingType) ?? nil
        self.wifi = try container.decodeIfPresent(String.self, forKey: .wifi) ?? nil
        self.locationInterval = try container.decodeIfPresent(Int.self, forKey: .locationInterval) ?? nil
        self.locationLatitude = try container.decodeIfPresent(Double.self, forKey: .locationLatitude) ?? nil
        self.locationLongitude = try container.decodeIfPresent(Double.self, forKey: .locationLongitude) ?? nil
        self.trackingId = try container.decodeIfPresent(String.self, forKey: .trackingId) ?? nil
    }

    init(_ groupmemberId: String,_ supervisionId: String,_ status: SupervisionStatusType,_ countdownTime: TimeInterval,_ deadline1: TimeInterval,
         _ nextLifesignDue: TimeInterval?,_ supervisionType: SupervisionType,_ periodStart: Date?,_ periodEnd: Date?, _ userId: String, _ caregroupId: String)  {
        self.groupmemberId = groupmemberId
        self.supervisionId = supervisionId
        self.status = status
        self.countdownTime = countdownTime
        self.interval = nil
        self.deadline1 = deadline1
        self.deadline2 = nil
        self.deadline3 = nil
        self.supervisionType = supervisionType
        self.periodStart = periodStart
        self.periodEnd = periodEnd
        self.userId = userId
        self.caregroupId = caregroupId
        self.endingType = nil
        self.wifi = nil
        self.locationInterval = nil
        self.locationLatitude = nil
        self.locationLongitude = nil
        self.trackingId = nil
    }
    
    func deadlines () -> Int {
        var d = 0
        
        if (deadline1 ?? -1 > -1) {
            d = 1
        }
        if (deadline2 ?? -1 > -1) {
            d = 2
        }
        if (deadline3 ?? -1 > -1) {
            d = 3
        }
        
        return d
    }
    
    func repeatType () -> SupervisionRepeatType {
        var r = SupervisionRepeatType.FixedPointsInTime
        
        if (interval ?? -1 > -1) {
            r = SupervisionRepeatType.FixedInterval
        }
        
        return r
    }
    
    func isActive () -> Bool {
        var active = false
        
        switch self.supervisionType {
        case SupervisionType.Ongoing:
            if (self.status == SupervisionStatusType.Active) {
                active = true
            }
        default:
            if (self.status == SupervisionStatusType.Active) {
                if (self.periodStart! < Date() && self.periodEnd! > Date()) {
                    active = true
                } else if (self.periodStart! > Date()) {
                    trace.output("isActive false: Start ikke nået: " + self.periodStart!.description)
                } else if (self.periodEnd! < Date()) {
                    trace.output("isActive false: Forbi end: " + self.periodEnd!.description)
                }
            }
        }
        
        return active
    }

    func justEnded () -> Bool {
        var justEnded = false
        
        if self.supervisionType != SupervisionType.Ongoing {
            if (self.periodEnd!.addingTimeInterval(36*60*60) > Date() && self.periodEnd! < Date()) {
                justEnded = true
            } else {
                trace.output("justEnded false, periodEnd = " + self.periodEnd!.description)
            }
        }
        
        return justEnded
    }
    
    func upcoming () -> Bool {
        var upcoming = false
        
        if self.supervisionType != SupervisionType.Ongoing {
            if (self.periodStart!.addingTimeInterval(-12*60*60) < Date() && self.periodStart! > Date()) {
                upcoming = true
            } else {
                trace.output("upcoming false, periodStart = " + self.periodStart!.description)
            }
        }
        
        return upcoming
    }
    
    var safeSpot: CLLocation? {
        var safeSpot : CLLocation?
        if locationLatitude != nil {
            safeSpot = CLLocation.init(latitude: locationLatitude!, longitude: locationLongitude!)
        }
        return safeSpot
    }
}

enum SupervisionStatusType : Int, Codable {
    case Active, Inactive
    
    static var count: Int { return SupervisionStatusType.Inactive.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Active : return "active"
        case .Inactive : return "inactive"
        }
    }
    
    static func enumFromString(string:String) -> SupervisionStatusType? {
        var i = 0
        while let item = SupervisionStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
    
    func UIText () -> String {
        var text = ""
        
        switch self {
        case .Active :
            text = NSLocalizedString("Active", comment: "Status of supervision is: active")
        case .Inactive :
            text = NSLocalizedString("Inactive", comment: "Status of supervision is: inactive")
        }
        
        return text
    }
}

enum SupervisionType : Int, Codable {
    case Ongoing, Period
    
    static var count: Int { return SupervisionType.Period.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Ongoing : return "ongoing"
        case .Period : return "period"
        }
    }
    
    static func enumFromString(string:String) -> SupervisionType? {
        var i = 0
        while let item = SupervisionType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }

    func UIText () -> String {
        var text = ""
        
        switch self {
        case .Ongoing :
            text = NSLocalizedString("Recurring", comment: "Type of supervision is: recurring")
        case .Period :
            text = NSLocalizedString("Ad hoc", comment: "Type of supervision is: ad hoc (period)")
        }
        
        return text
    }
}

enum SupervisionEndingType : Int, Codable {
    case Manual, Wifi, Spot
    
    static var count: Int { return SupervisionEndingType.Spot.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Manual : return "manual"
        case .Wifi : return "wifi"
        case .Spot : return "spot"
        }
    }
    
    static func enumFromString(string:String) -> SupervisionEndingType? {
        var i = 0
        while let item = SupervisionEndingType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
    
    func UIText () -> String {
        var text = ""
        
        switch self {
        case .Manual :
            text = NSLocalizedString("Manual", comment: "Adhoc supervision ending is made: manual")
        case .Wifi :
            text = NSLocalizedString("Wifi", comment: "Adhoc supervision ending is made: by connection to Wifi")
        case .Spot :
            text = NSLocalizedString("Spot", comment: "Adhoc supervision ending is made: location spot")
        }
        
        return text
    }
}

enum SupervisionRepeatType : Int, Codable {
    case FixedInterval, FixedPointsInTime
    
    static var count: Int { return SupervisionRepeatType.FixedPointsInTime.hashValue + 1 }
    
    func UIText () -> String {
        var text = ""
        
        switch self {
        case .FixedInterval :
            text = NSLocalizedString("Fixed interval", comment: "Repeat mode of supervision is: Fixed interval")
        case .FixedPointsInTime :
            text = NSLocalizedString("Fixed times", comment: "Repeat mode of supervision is: Fixed times during the day")
        }
        
        return text
    }
}
