//
//  Traces.swift
//  LifeSign
//
//  Created by Simon Jensen on 17/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

// DESCRIPTION
// Wrapper for traces into the console when 'Build Settings' - 'Active Compilation Conditions' DEBUG is defined

import UIKit

class AllTraces: NSObject {
    
    public func begin(file: String = #file, function: String = #function) {
        
        #if DEBUG
        let filename = (file as NSString).lastPathComponent
        print("\nTRACE BEGIN >> \(filename)::\(function)")
        #endif
        
    }

    public func output(_ data: Any) {

        #if DEBUG
        debugPrint("TRACE DATA    \(data)")
        #endif
        
    }

    public func output(start: Date, text: String) {

        #if DEBUG
        let now = Date()
        let difference = Calendar.current.dateComponents([.nanosecond], from: start, to: now)
        let differenceString = String(format: " (%ld)", (difference.nanosecond! / 1000000))
        debugPrint( text + differenceString)
        #endif

    }

    func error(_ data: Any, file: String = #file, function: String = #function) {
        
        let filename = (file as NSString).lastPathComponent
        #if DEBUG
        print("TRACE ERROR :: [\(filename)::\(function)] \(data)")
        #endif
        DispatchQueue.main.async {
            NotificationBanner.show("TRACE ERROR :: [\(filename)::\(function)] \(data)", style: .error )
        }
    }
    
    public func end(file: String = #file, function: String = #function) {
        
        #if DEBUG
        let filename = (file as NSString).lastPathComponent
        print("TRACE END   << \(filename)::\(function)")
        #endif
        
    }
    
    public func user(_ data: Any?, file: String = #file, function: String = #function) {
        
        let filename = (file as NSString).lastPathComponent
        if (data == nil)
        {
            #if DEBUG
            print("TRACE USER :: USER == nil   << \(filename)::\(function)")
            #endif
            DispatchQueue.main.async {
                NotificationBanner.show("USER == nil :: [\(filename)::\(function)]", style: .error )
            }
        }
    }
}

class Traces: NSObject {
        
    public func begin(file: String = #file, function: String = #function) {
        
        #if DEBUG
        let filename = (file as NSString).lastPathComponent
        print("\nTRACE BEGIN >> \(filename)::\(function)")
        #endif
    }
    
    public func output(_ data: Any) {
    }

    public func output(start: Date, text: String) {
    }

    func error(_ data: Any, file: String = #file, function: String = #function) {
        
        let filename = (file as NSString).lastPathComponent
        #if DEBUG
        print("TRACE ERROR :: [\(filename)::\(function)] \(data)")
        #endif
        NotificationBanner.show("TRACE ERROR :: [\(filename)::\(function)] \(data)", style: .error )
    }
    
    public func end(file: String = #file, function: String = #function) {
        
        #if DEBUG
        let filename = (file as NSString).lastPathComponent
        print("TRACE END   << \(filename)::\(function)")
        #endif
        
    }
    
    public func user(_ data: Any?, file: String = #file, function: String = #function) {
        
        let filename = (file as NSString).lastPathComponent
        if (data == nil)
        {
            #if DEBUG
            print("TRACE USER :: USER == nil   << \(filename)::\(function)")
            #endif
            DispatchQueue.main.async {
                NotificationBanner.show("USER == nil :: [\(filename)::\(function)]", style: .error )
            }
        }
        
    }
}

class ErrorTraces: NSObject {
        
    public func begin(file: String = #file, function: String = #function) {
    }
    
    public func output(_ data: Any) {
    }

    public func output(start: Date, text: String) {
    }

    func error(_ data: Any, file: String = #file, function: String = #function) {
        
        let filename = (file as NSString).lastPathComponent
        #if DEBUG
        print("TRACE ERROR :: [\(filename)::\(function)] \(data)")
        #endif
        DispatchQueue.main.async {
            NotificationBanner.show("TRACE ERROR :: [\(filename)::\(function)] \(data)", style: .error )
        }
    }

    public func end(file: String = #file, function: String = #function) {
    }
    
    public func user(_ data: Any?, file: String = #file, function: String = #function) {
        
        let filename = (file as NSString).lastPathComponent
        if (data == nil)
        {
            #if DEBUG
            print("TRACE USER :: USER == nil   << \(filename)::\(function)")
            #endif
            DispatchQueue.main.async {
                NotificationBanner.show("USER == nil :: [\(filename)::\(function)]", style: .error )
            }
        }
        
    }
}

class NoTraces: NSObject {
    
    public func begin(file: String = #file, function: String = #function) {
    }
    
    public func output(_ data: Any) {
    }
    
    public func output(start: Date, text: String) {
    }

    func error(_ data: Any, file: String = #file, function: String = #function) {
    }
    
    public func end(file: String = #file, function: String = #function) {
    }
    
    public func user(_ data: Any?, file: String = #file, function: String = #function) {
    }
}

class PerformanceTraces: NSObject {
    var logger: Logger
    
    override init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.logger = appDelegate.logger
    }
    
    public func begin(file: String = #file, function: String = #function) {
        var unilineString = ""
        #if DEBUG
        let filename = (file as NSString).lastPathComponent
        unilineString = "BEGIN >> \(filename)::\(function)"
        #endif
        logger.addPerformanceLog(text: unilineString)
    }
    
        public func end(file: String = #file, function: String = #function) {
        var unilineString = ""
        #if DEBUG
        let filename = (file as NSString).lastPathComponent
        unilineString = "END >> \(filename)::\(function)"
        #endif
        logger.addPerformanceLog(text: unilineString)
    }

    public func output(_ data: Any) {
        let rawString = String(describing: data)
        let unilineString = rawString.components(separatedBy: CharacterSet.newlines)[0]
        //logger.addPerformanceLog(text: unilineString)
    }
    
    public func output(start: Date, text: String) {
        let now = Date()
        let difference = Calendar.current.dateComponents([.nanosecond], from: start, to: now)
        let differenceString = String(format: " (%ld ms)", (difference.nanosecond! / 1000000))
        logger.addPerformanceLog(text: text + differenceString)
    }
    
    func error(_ data: Any, file: String = #file, function: String = #function) {
    }
    
    public func user(_ data: Any?, file: String = #file, function: String = #function) {
    }
}
