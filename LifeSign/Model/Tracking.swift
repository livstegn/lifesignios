//
//  LocationCollection.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 08/03/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class Tracking : NSObject, Codable  {
    var trackingId : String
    var starttime : Date
    var status : TrackingStatusType
    var onSafe : Bool
    var leftSafe : Bool
    var backSafe : Bool
    var userId : String
    var caregroupId : String
    var groupmemberId : String
    var supervisionId : String
    var latestLocationId : String?

    enum CodingKeys: String, CodingKey
    {
        case trackingId = "trackingId"
        case starttime = "starttime"
        case status = "status"
        case onSafe = "onSafe"
        case leftSafe = "leftSafe"
        case backSafe = "backSafe"

        case userId = "userId"
        case caregroupId = "caregroupId"
        case groupmemberId = "groupmemberId"
        case supervisionId = "supervisionId"
        case latestLocationId = "latestLocationId"
    }
    
    override init() {
        self.trackingId = ""
        self.starttime = Date()
        self.status = TrackingStatusType.Upcoming
        self.onSafe = false
        self.leftSafe = false
        self.backSafe = false

        self.userId = ""
        self.caregroupId = ""
        self.groupmemberId = ""
        self.supervisionId = ""
        self.latestLocationId = nil
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.trackingId = try container.decodeIfPresent(String.self, forKey: .trackingId) ?? ""
        self.starttime = try container.decodeIfPresent(Date.self, forKey: .starttime) ?? Date()
        self.status = try container.decodeIfPresent(TrackingStatusType.self, forKey: .status) ?? TrackingStatusType.Upcoming
        self.onSafe = try container.decodeIfPresent(Bool.self, forKey: .onSafe) ?? false
        self.leftSafe = try container.decodeIfPresent(Bool.self, forKey: .leftSafe) ?? false
        self.backSafe = try container.decodeIfPresent(Bool.self, forKey: .backSafe) ?? false

        self.caregroupId = try container.decodeIfPresent(String.self, forKey: .caregroupId) ?? ""
        self.groupmemberId = try container.decodeIfPresent(String.self, forKey: .groupmemberId) ?? ""
        self.userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        self.supervisionId = try container.decodeIfPresent(String.self, forKey: .supervisionId) ?? ""
        self.latestLocationId = try container.decodeIfPresent(String.self, forKey: .latestLocationId) ?? nil
    }
    
    init (supervision: Supervision, starttime: Date, status : TrackingStatusType, onSafe: Bool,
          leftSafe: Bool, backSafe: Bool) {
        self.trackingId = supervision.trackingId!
        self.starttime = starttime
        self.status = status
        self.onSafe = onSafe
        self.leftSafe = leftSafe
        self.backSafe = backSafe

        self.supervisionId = supervision.supervisionId
        self.userId = supervision.userId
        self.caregroupId = supervision.caregroupId
        self.groupmemberId = supervision.groupmemberId
        self.latestLocationId = nil
    }
    
}

enum TrackingStatusType : Int, Codable {
    case Upcoming, Active, Ended
    
    static var count: Int { return TrackingStatusType.Ended.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Upcoming : return "upcoming"
        case .Active : return "active"
        case .Ended : return "ended"
        }
    }
    
    static func enumFromString(string:String) -> TrackingStatusType? {
        var i = 0
        while let item = TrackingStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}
