//
//  LifesignUser.swift
//  Lifesign-Model
//
//  Created by Peter Rosendahl on 18/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation

class User : NSObject, Codable  {
    
    let trace = NoTraces()
    
    var userId : String
    var userName: String
    var email : String
    var cellno : String
    var status: UserStatusType
    var deviceId : String
    var platform: PlatformType
    //var password : String
    var isMonitored : Bool
    var lastLifesign : Date?
    var userMinutesBeforeUTC : Int

    enum CodingKeys: String, CodingKey
    {
        case userId = "userId"
        case userName = "userName"
        case email = "email"
        case cellno = "cellno"
        case status = "status"
        case deviceId = "deviceId"
        case platform = "platform"
        //case password = "password"
        case isMonitored = "isMonitored"
        case lastLifesign = "lastLifesign"
        case userMinutesBeforeUTC = "userMinutesBeforeUTC"
    }
    
    override init() {
        
        trace.begin()
        userId = ""
        userName = "John Doe"
        email = ""
        cellno = ""
        status = UserStatusType.Creating
        deviceId = ""
        platform = PlatformType.Unknown
        //password = ""
        isMonitored = true
        userMinutesBeforeUTC = 0
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        userId = try container.decodeIfPresent(String.self, forKey: .userId) ?? ""
        userName = try container.decodeIfPresent(String.self, forKey: .userName) ?? ""
        email = try container.decodeIfPresent(String.self, forKey: .email) ?? ""
        cellno = try container.decodeIfPresent(String.self, forKey: .cellno) ?? ""
        status = try container.decodeIfPresent(UserStatusType.self, forKey: .status) ?? UserStatusType.Creating
        deviceId = try container.decodeIfPresent(String.self, forKey: .deviceId) ?? ""
        platform = try container.decodeIfPresent(PlatformType.self, forKey: .status) ?? PlatformType.iOS
        //password = try container.decodeIfPresent(String.self, forKey: .password) ?? ""
        isMonitored = try container.decodeIfPresent(Bool.self, forKey: .isMonitored) ?? true
        lastLifesign = try container.decodeIfPresent(Date.self, forKey: .lastLifesign) ?? nil
        userMinutesBeforeUTC = try container.decodeIfPresent(Int.self, forKey: .userMinutesBeforeUTC) ?? 0
    }
    
    init(_ userId: String, _ userName: String, _ email: String,_ cellno: String, _ status: UserStatusType,
         _ deviceId: String,_ platform: PlatformType, _ isMonitored: Bool, _ lastLifesign: Date, _ userMinutesBeforeUTC : Int) {
        self.userId = userId
        self.userName = userName
        self.email = email
        self.cellno = cellno
        self.status = status
        self.deviceId = deviceId
        self.platform = platform
        self.isMonitored = isMonitored
        self.lastLifesign = lastLifesign
        self.userMinutesBeforeUTC = userMinutesBeforeUTC
    }

}

enum UserStatusType : Int, Codable {
    case Invited = 0, Creating, Created, Active, Closing
    
    static var count: Int { return UserStatusType.Closing.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Invited: return "invited"
        case .Creating : return "creating"
        case .Created : return "created"
        case .Active : return "active"
        case .Closing : return "closing"
        }
    }

    static func enumFromString(string:String) -> UserStatusType? {
        var i = 0
        while let item = UserStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}

enum PlatformType : Int, Codable {
    case iOS = 0, Android, Browser, Unknown
    
    static var count: Int { return PlatformType.Browser.hashValue + 1 }
    
    var description: String {
        switch self {
        case .iOS: return "iOS"
        case .Android : return "android"
        case .Browser : return "browser"
        case .Unknown : return "unknown"
        }
    }
    
    static func enumFromString(string:String) -> PlatformType? {
        var i = 0
        while let item = PlatformType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}
