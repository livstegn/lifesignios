//
//  UserActivity.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 04/10/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class UserActivity : NSObject, Codable  {
    var time : Date
    var type : UserActivityType
    var status : UserActivityStatusType
    
    enum CodingKeys: String, CodingKey
    {
        case time = "time"
        case type = "type"
        case status = "status"
    }
    
    override init() {
        self.time = Date()
        self.type = .Unlocked
        self.status = .Detected
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.time = try container.decodeIfPresent(Date.self, forKey: .time) ?? Date()
        self.type = try container.decodeIfPresent(UserActivityType.self, forKey: .type) ?? UserActivityType.Unlocked
        self.status = try container.decodeIfPresent(UserActivityStatusType.self, forKey: .type) ?? UserActivityStatusType.Detected
    }

    init(time : Date, type : UserActivityType, detected : UserActivityStatusType = .Detected) {
        self.time = time
        self.type = type
        self.status = detected
    }
}

enum UserActivityType : Int, Codable {
    case Unlocked, StepsTaken, MovedOnWifi
    
    static var count: Int { return UserActivityType.MovedOnWifi.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Unlocked : return "unlocked"
        case .StepsTaken : return "stepstaken"
        case .MovedOnWifi : return "movedonwifi"
        }
    }
    
    static func enumFromString(string:String) -> UserActivityType? {
        var i = 0
        while let item = UserActivityType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}

enum UserActivityStatusType : Int, Codable {
    case Detected, Forwarded, Held
    
    static var count: Int { return UserActivityStatusType.Held.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Detected : return "detected"
        case .Forwarded : return "forwarded"
        case .Held : return "held"
        }
    }
    
    static func enumFromString(string:String) -> UserActivityStatusType? {
        var i = 0
        while let item = UserActivityStatusType(rawValue: i) {
            if item.description == string { return item }
            i += 1
        }
        return nil
    }
}
