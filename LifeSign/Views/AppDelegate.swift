//
//  AppDelegate.swift
//  LifeSign
//
//  Created by Simon Jensen on 24/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit
import UserNotifications

import AWSCore
import AWSSNS
import AWSMobileClient
import AWSCognitoIdentityProvider
import Network

import BackgroundTasks
import HealthKit


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    let logger = LoggerImplementation.init()
    var window: UIWindow?
    var signInViewController: SignInViewController?
    var navigationController: UINavigationController?
    var storyboard: UIStoryboard?
    lazy var deviceLayer = DeviceLayerImplementation.init()
    lazy var cloudLayer = CloudLayerImplementation.init()
    lazy var model = Model.init( deviceLayerImplementation: deviceLayer,
                                 cloudLayerImplementation: cloudLayer as CloudLayer,
                                 universalLinkLaunch: self.universalLinkLaunch,
                                 universalLinkData: self.universalLinkData)
    let notificationHandler = LSNotificationHandler()
    let monitor = NWPathMonitor()       // for iOS 12
    var isConnected2Network = false
    
    var registrationSuccess = false  // Debug
    var registrationFail = false  // Debug
    var notificationReceived = false  // Debug
    
    var universalLinkLaunch: Bool = false
    var universalLinkData: String?
    
    var healthStore: HKHealthStore? = nil
    var stepCount : Double = 0
    
    //var authenticationMethod = AuthenticationMethod.Unknown
    
    lazy var trace = AllTraces()
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions: [UIApplication.LaunchOptionsKey : Any]?) -> Bool  {
     
        trace.begin()
        trace.end()
        
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        trace.begin()
        
        // Override point for customization after application launch.
        _ = model.userSessionStatus // Forcér lazy init...
        
        notificationHandler.delegate = self
        
        // Assign handler for monitoring of network connection
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.trace.output("Internet status: CONNECTED")
                self.isConnected2Network = true
                DispatchQueue.main.async {
                    NotificationBanner.show("Dataforbindelse OK", style: .info )
                }
            } else {
                self.trace.output("Internet status: NO CONNECTION")
                self.isConnected2Network = false
                DispatchQueue.main.async {
                    NotificationBanner.show("Ingen dataforbindelse!", style: .warning )
                }
                self.trace.end()
                return
            }
        }
        let queue = DispatchQueue.global(qos: .background)
        monitor.start(queue: queue)
        
        trace.output("AppDelegate::startup")
        self.storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.trace.output( "Før AWSMobileClient.sharedInstance().initialize")
        
        // Create AWSMobileClient to connect with AWS
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            if let error = error {
                self.trace.output("Error initializing AWSMobileClient: \(error.localizedDescription)")
            } else if let userState = userState {
                self.trace.output("AWSMobileClient initialized. Current UserState: \(userState.rawValue)")
            }
        }
        self.trace.output( "Efter AWSMobileClient.sharedInstance().initialize")

        // DETECT LAUNCH BY NOTIFICATION
        // When the app launch after user tap on notification (originally was not running / not in background)
        if let remoteNotification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as?  [AnyHashable : Any] {

            /*
             *   STARTS HERE WHEN THE NOTIFICATION ARRIVE - ONLY IF APP IS NOT RUNNING
             */
            self.model.notificationLaunch = true  // Signal to the app that the app was launched because of a notification
        }
        
        // Check if it is the initial launch of the app, and delete the keychain if it is...
        let defaults = UserDefaults.standard
        
        if (!defaults.dictionaryRepresentation().keys.contains("firstAccessToApp")) {
            self.deviceLayer.removeAllKeys()
            let credentialProvider = AWSCognitoCredentialsProvider(regionType:.EUCentral1, identityPoolId: CognitoIdentityPoolId)
            credentialProvider.clearKeychain()
            
            trace.output("LIFESIGN - FIRST LAUNCH!!!")
            defaults.set(true, forKey: "firstAccessToApp")
            defaults.synchronize()
        }
    
        
        var directoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = URL(fileURLWithPath: "lifesign", relativeTo: directoryURL).appendingPathExtension("txt")
        
        // Create data to be saved
        let myString = "Lifesign✓"
        guard let data = myString.data(using: .utf8) else {
            trace.error("Unable to convert string to data")
            return false
        }
        
        do {
            try data.write(to: fileURL, options: .completeFileProtection)
            trace.output("File saved: \(fileURL.absoluteURL)")
        } catch {
         // Catch any errors
            trace.error(error.localizedDescription)
        }
        
        do {
            try FileManager.default.removeItem(at: fileURL)
            trace.output("File deleted: \(fileURL.absoluteURL)")
        } catch {
            // Catch any errors
            trace.error(error.localizedDescription)
        }
        /*
        // Enable background indication of App status
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "com.privat.LifeSign.check", using: nil) { task in
            self.handleAppRefresh(task: task as! BGProcessingTask)
        }
        */
        
        // Check settings bundle. If Activity update is enabled, then check for activity
        if(model.activityPreference()) {
            
            if HKHealthStore.isHealthDataAvailable() {   // Call the isHealthDataAvailable() method to confirm that HealthKit is available on the user's device.

                healthStore = HKHealthStore()
                if (healthStore == nil) {
                    // Handle the error here.
                }
            
                // Access Step Count
                let healthKitTypes = Set([ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)! ])
                 
                // Check for Authorization
                healthStore!.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (success, error) in
                    if success {
                        // Authorization Successful
                        self.getTodaysSteps { (result) in
                            DispatchQueue.main.async {
                                self.stepCount = Double(result)
                            }
                        }
                        // Handle the error here.
                    } else {
                        self.trace.error("HealthKit access not authorized")
                    }
                }
            }
        }
                
        trace.end()
        
        return true
    }
    
    
    func getTodaysSteps(completion: @escaping (Double) -> Void) {

        /*
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
         
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)

        let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { _, result, _ in
            guard let result = result, let sum = result.sumQuantity() else {
                completion(0.0)
                return
            }
            completion(sum.doubleValue(for: HKUnit.count()))
        }
        */
        
        let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.hour = 3
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                    intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
                var resultCount = 0.0
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in

                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    resultCount = sum.doubleValue(for: HKUnit.count())
                } // end if

                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in

            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }

        healthStore!.execute(query)
        
    }
    
    func scheduleAppRefresh() {
        
        trace.begin()
        
        let request = BGProcessingTaskRequest(identifier: "com.privat.LifeSign.check")
        
        request.requiresNetworkConnectivity = true
        request.requiresExternalPower = false
        request.earliestBeginDate = Date(timeIntervalSinceNow: 6*60*60)  // Seconds ~ 6 hours i.e. 4 times a day....
        
        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            trace.error(error)
        }
        
        trace.end()
    }
    
    func cancelAppRefresh() {
    
        trace.begin()
        
        BGTaskScheduler.shared.cancelAllTaskRequests()
        
        trace.end()
    }
    

    func handleAppRefresh(task: BGProcessingTask) {
        
        trace.begin()
        
        scheduleAppRefresh()
        
        task.expirationHandler = {
            task.setTaskCompleted(success: false)
        }
        
        switch(UIApplication.shared.applicationState) {
        case .inactive:
            _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Inactive)
        case .active:
            _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Active)
        case .background:
            _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Background)
        default:
            break
        }
            
        task.setTaskCompleted(success: true)
        
        trace.end()
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {

        trace.begin()
        
        // DETECT LAUNCH BY UNIVERSAL LINK
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let incomingURL = userActivity.webpageURL,
            let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true),
            let path = components.path,
            let host = components.host,
            let params = components.queryItems else {
                trace.error("Error?")
                trace.end()
                return false
        }
        
        trace.output("path = \(path)")
        trace.output("host = \(host)")
        
        // Example of invitation URL
        // http://www.livewebsite.dk/invites/?invite=I-a7bc7c20-ae38-11e9-b656-6b1571f64b3d
        
        model.universalLinkLaunch = true        // Called with "Universal link"
        
        if let invitation = params.first(where: { $0.name == "invite" } )?.value{
            
            trace.output("invitation = \(invitation)")
            universalLinkData = invitation
            universalLinkLaunch = true
            model.universalLinkLaunch = true
            model.universalLinkData = invitation
            
            model.storeInvitationId(id: invitation)  // Save invitation from universal link in memory
            
            self.gotoViewController(userInfo: self.model.notificationData)
            
            trace.end()
            return true
            
        } else {
            
            trace.output("Incorrect URL")
            trace.end()
            return false
        }
        
    }
    
    func applicationProtectedDataDidBecomeAvailable(_ application: UIApplication) {
        
        trace.begin()
        
        trace.output(">>> TELEFON LÅST OP... <<<")
        
        if(!model.startupPreference()) { // NORMAL OPSTART
            let now = Date()
            DispatchQueue.global(qos: .background).async {
                let userActivity = UserActivity(time: now, type: UserActivityType.Unlocked)
                self.model.newUserActivity(userActivity: userActivity)
                (UIApplication.shared.delegate as! AppDelegate).notificationHandler.activityText = self.model.getCollectedActivities()
            }
        } else { // SIMON's DEBUG OPSTART
            
        }
        
        trace.end()
    }
    
    func protectedDataWillBecomeUnavailableNotification(_ application: UIApplication) {
        
        trace.begin()
        trace.output(">>> TELEFON LÅST!! <<<")
        trace.end()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        trace.begin()

        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Inactive)  //Inactive

        trace.end()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        trace.begin()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //displayLocalNotification()
        
        _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Background)
        
        /*
        scheduleAppRefresh()
        */
 
        trace.end()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        trace.begin()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

        cancelAppRefresh()
        
        // fix for lost network connection - gives time to suspend
        var backgroundTask = 0
        backgroundTask = UIApplication.shared.beginBackgroundTask(withName: "BackgroundTask") { UIApplication.shared.endBackgroundTask(UIBackgroundTaskIdentifier(rawValue: backgroundTask))
        backgroundTask = UIBackgroundTaskIdentifier.invalid.rawValue }.rawValue
        
        if (model.user == nil &&
            model.cloudLayer.getUserSessionState() == UserSessionStatusType.LoggedIn) {
                model.restoreCurrentUser()
        }
    
        trace.end()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive.
        // If the application was previously in the background, optionally refresh the user interface.
        trace.begin()
        
        _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Active) //OK

        // Check if user is signed in to Cognito....
        if (model.cloudLayer.getUserSessionState() != UserSessionStatusType.LoggedIn) {
            trace.output("LOGGED OUT")
            trace.end()
            return
        }
        
        trace.end()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        trace.begin()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Background)  //Background
        
        trace.end()
    }
    
    func gotoMessageViewController(userInfo: [AnyHashable : Any]) {
        
        trace.begin()
        
        if let controller = UIStoryboard(name: "Messages", bundle: nil).instantiateViewController(withIdentifier: "Messages") as? MessagesViewController {
            if let window = self.window, let rootViewController = window.rootViewController {
                var currentController = rootViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                currentController.present(controller, animated: true, completion: nil)
            }
        }
        trace.end()
    }
    
    func gotoInvitationViewController() {
        
        trace.begin()

        if let controller = UIStoryboard(name: "UserInvitation", bundle: nil).instantiateViewController(withIdentifier: "invitationView") as? InvitationViewController {
            if let window = self.window, let rootViewController = window.rootViewController {
                var currentController = rootViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                currentController.present(controller, animated: true, completion: nil)
            }
        }
        trace.end()
    }
    
    func gotoStartupViewController() {
        
        trace.begin()
        let topViewController = UIApplication.topViewController()
        trace.output("topViewController = \(String(describing: topViewController))") //StatusViewController
        trace.output("topViewController.parent = \(String(describing: topViewController?.parent))")  // TabBarViewController
        
        let currentController = topViewController?.parent
        
        currentController?.performSegue(withIdentifier: "unwindTabVC2StartupVC", sender: currentController)
        
        trace.end()
    }
    
}

extension AppDelegate: LSNotificationHandlerDelegate {
    
    func gotoViewController(userInfo: [AnyHashable : Any]?) {
        
        trace.begin()

        if (model.universalLinkLaunch) {

            trace.output("Universal Link")
            gotoInvitationViewController()
        }
        else {  // Received notification data

            trace.output("Notification...")
            self.model.notificationLaunch = true  // Signal to the viewController that it was launched because of a notification
            if (userInfo != nil) {
                if let listData = userInfo!["aps"] as? [String: Any]  {
                    let category = listData["category"] as! String
                    
                    switch category {
                    
                    case "INVITATION":

                        trace.output("Invitation!")
                        if let controller = UIStoryboard(name: "UserInvitation", bundle: nil).instantiateViewController(withIdentifier: "invitationView") as? InvitationViewController {
                            if let window = self.window, let rootViewController = window.rootViewController {
                                var currentController = rootViewController
                                while let presentedController = currentController.presentedViewController {
                                    currentController = presentedController
                                }
                                currentController.present(controller, animated: true, completion: nil)
                            }
                        }
                        
                    case "MESSAGE":
                        
                        trace.output("Message!")
                        if let controller = UIStoryboard(name: "Messages", bundle: nil).instantiateViewController(withIdentifier: "Messages") as? MessagesViewController {
                            if let window = self.window, let rootViewController = window.rootViewController {
                                var currentController = rootViewController
                                while let presentedController = currentController.presentedViewController {
                                    currentController = presentedController
                                }
                                currentController.present(controller, animated: true, completion: nil)
                            }
                        }
                        
                    default:
                        break
                    
                        trace.output("Status!")
                        if let controller = UIStoryboard(name: "Status", bundle: nil).instantiateViewController(withIdentifier: "StatusVC") as? TabBarViewController {
                            if let window = self.window, let rootViewController = window.rootViewController {
                                var currentController = rootViewController
                                while let presentedController = currentController.presentedViewController {
                                    currentController = presentedController
                                }
                                currentController.present(controller, animated: true, completion: nil)
                            }
                        }
                        
                    }  // switch
                }  // if
            }  // if
        }  // if-else
        trace.end()
    }  // func
} // extension
