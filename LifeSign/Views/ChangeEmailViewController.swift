//
//  ChangeEmailViewController.swift
//  LifeSign
//
//  Created by Simon Jensen on 19/10/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class ChangeEmailViewController: UIViewController {
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace  = NoTraces()

    @IBOutlet weak var currentEmail: UITextField!
    @IBOutlet weak var newEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        
        self.currentEmail.text = model.getEmail()
        self.newEmail.text = nil
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func buttonChange(_ sender: Any) {
        
        trace.begin()
        
        guard let currentEmail = self.currentEmail.text, !currentEmail.isEmpty else {
            
            let alertController = UIAlertController(title: NSLocalizedString("Email Field Empty",
                                                                             comment: "Alert, title - user did not enter email"),
                                                    message: NSLocalizedString("Please enter your current email.",
                                                                               comment: "Alert, message - user did not enter email"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user did not enter email"), style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        guard let newEmail = self.newEmail.text, !newEmail.isEmpty else {
            
            let alertController = UIAlertController(title: NSLocalizedString("Email Field Empty",
                                                                             comment: "Alert, title - user did not enter email"),
                                                    message: NSLocalizedString("Please enter your new email.",
                                                                               comment: "Alert, message - user did not enter email"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user did not enter email"), style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        model.deviceLayer.saveNewEmail(email: newEmail)
        let error = model.changeEmail(email: currentEmail, newEmail: newEmail)
        
        if error != nil {
            let alertController = UIAlertController(title: error?.userInfo["__type"] as? String,
                                                    message: error?.userInfo["message"] as? String,
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - system error in save new email"),
                                                                  style: .default,
                                                                  handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
        } else {
            self.performSegue(withIdentifier: "confirmemail", sender: self)
        }
        
        trace.end()
        
    }
    
    @IBAction func buttonCancel(_ sender: Any) {
    
        self.performSegue(withIdentifier: "invitation2tabview", sender: self) 
    }
    
}
