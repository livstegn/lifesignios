//
//  ChangePWViewController.swift
//  LifeSign
//
//  Created by Simon Jensen on 28/09/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class ChangePWViewController: UIViewController {
    
    var isVisible: Bool?

    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace  = NoTraces()
    var email = String()
    
    @IBOutlet weak var currentPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    
    @IBOutlet weak var eyeCurrentPassword: UIButton!
    @IBOutlet weak var eyeNewPassword: UIButton!
    @IBOutlet weak var eyeRepeatPassword: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        isVisible = model.deviceLayer.getPasswordVisible() ?? false
        setVisibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func eyeCurrentPressed(_ sender: Any) {
        toggleVisibility()
    }
    @IBAction func eyeNewPressed(_ sender: Any) {
        toggleVisibility()
    }
    @IBAction func eyeRepeatPressed(_ sender: Any) {
        toggleVisibility()
    }
    
    func setVisibility(){
        
        currentPassword.isSecureTextEntry = !isVisible!
        newPassword.isSecureTextEntry = !isVisible!
        repeatPassword.isSecureTextEntry = !isVisible!
        
        if(isVisible!) {
            eyeCurrentPassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            eyeNewPassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            eyeRepeatPassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
        } else {
            eyeCurrentPassword.setImage(UIImage(systemName: "eye"), for: .normal)
            eyeNewPassword.setImage(UIImage(systemName: "eye"), for: .normal)
            eyeRepeatPassword.setImage(UIImage(systemName: "eye"), for: .normal)
        }
    }
    
    func toggleVisibility() {

        isVisible! = !isVisible!
        model.deviceLayer.savePasswordVisible(status: isVisible!)
        setVisibility()
    }
    
    @IBAction func changePressed(_ sender: Any) {
        
        trace.begin()
        
        if(self.newPassword.text != self.repeatPassword.text)
        {

            self.newPassword.text = ""
            self.repeatPassword.text = ""
            
            let alertController = UIAlertController(title: NSLocalizedString("Password Mismatch",
                                                                             comment: "Alert, title - repeat password is different from password"),
                                                    message: NSLocalizedString("Enter the same password in both fields.",
                                                                               comment: "Alert, message - repeat password is different from password"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user entered two different passwords"), style: .default, handler: { [self] _ in
                                                                    newPassword.becomeFirstResponder()
                                                                })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:  nil)

            return
            
        }
        
        guard let currentPassword = self.currentPassword.text, !currentPassword.isEmpty else {
            
            let alertController = UIAlertController(title: NSLocalizedString("Password Field Empty",
                                                                             comment: "Alert, title - user did not enter password"),
                                                    message: NSLocalizedString("Please enter your current password.",
                                                                               comment: "Alert, message - user did not enter password"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user did not enter password"), style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        guard let newPassword = self.newPassword.text, !newPassword.isEmpty else {
            let alertController = UIAlertController(title: "Password Field Empty",
                                                    message: "Please enter a new password of your own choice.",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        trace.output("Current password: \(currentPassword)")
        trace.output("New password: \(newPassword)")
        
        let email = model.getEmail()
        let error = model.changePassword(email: email!, password: self.currentPassword.text! , newPassword: self.newPassword.text!)
        
        if error != nil {
            let alertController = UIAlertController(title: error?.userInfo["__type"] as? String,
                                                    message: error?.userInfo["message"] as? String,
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - system error in change password"),
                                                                  style: .default,
                                                                  handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
        } else {
            self.performSegue(withIdentifier: "changepwtotabview", sender: self)
        }
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
    }
    
}
