//
//  ConfirmForgetPWViewController.swift
//  AwsTest
//
//  Created by Simon Jensen on 26/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class ConfirmForgetPWViewController: UIViewController {
    
    var isVisible: Bool?
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    var email = String()

    @IBOutlet weak var code: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    @IBOutlet weak var eyePassword: UIButton!
    @IBOutlet weak var eyeRepeatPassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isVisible = model.deviceLayer.getPasswordVisible() ?? false
        setVisibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func eyePasswordPressed(_ sender: Any) {
        toggleVisibility()
    }
    
    @IBAction func eyeRepeatPasswordPressed(_ sender: Any) {
        toggleVisibility()
    }
    
    func setVisibility(){
        
        password.isSecureTextEntry = !isVisible!
        
        if(isVisible!) {
            eyePassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
        } else {
            eyePassword.setImage(UIImage(systemName: "eye"), for: .normal)
        }
    }
    
    func toggleVisibility() {

        isVisible! = !isVisible!
        model.deviceLayer.savePasswordVisible(status: isVisible!)
        setVisibility()
    }
    
    // MARK: - IBActions
    
    @IBAction func confirmPressed(_ sender: Any) {
        
        if(self.password.text != self.repeatPassword.text)
        {

            self.password.text = ""
            self.repeatPassword.text = ""
            
            let alertController = UIAlertController(title: NSLocalizedString("Password Mismatch",
                                                                             comment: "Alert, title - repeat password is different from password"),
                                                    message: NSLocalizedString("Enter the same password in both fields.",
                                                                               comment: "Alert, message - repeat password is different from password"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user entered two different passwords"), style: .default, handler: { [self] _ in
                                                                    password.becomeFirstResponder()
                                                                })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:  nil)

            return
            
        }
    
        guard let confirmationCodeValue = self.code.text, !confirmationCodeValue.isEmpty else {
            
            let alertController = UIAlertController(title: NSLocalizedString("Password Empty",
                                                                             comment: "Alert, title - user did not enter password"),
                                                    message: NSLocalizedString("Please enter a password of your choice.",
                                                                               comment: "Alert, message - user did not enter password"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user did not enter password"), style: .default, handler: nil)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        let error = model.confirmForgotPassword(email: self.email, confirmationCodeValue: confirmationCodeValue, password: self.password.text!)

        if error != nil {
            let alertController = UIAlertController(title: error?.userInfo["__type"] as? String,
                                                    message: error?.userInfo["message"] as? String,
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - system error in confirm forgot password"),
                                                                  style: .default,
                                                                  handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
        } else {
            self.performSegue(withIdentifier: "returntologinSeague", sender: self)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
