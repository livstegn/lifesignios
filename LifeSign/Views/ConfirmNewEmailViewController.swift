//
//  ConfirmNewEmailViewController.swift
//  LifeSign
//
//  Created by Simon Jensen on 19/10/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class ConfirmNewEmailViewController: UIViewController {
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace  = NoTraces()

    @IBOutlet weak var verificationCode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func confirmPressed(_ sender: Any) {
        
        trace.begin()
        
        guard let verificationCode = self.verificationCode.text, !verificationCode.isEmpty else {
            
            let alertController = UIAlertController(title: NSLocalizedString("Code Field Empty",
                                                                             comment: "Alert, title - user did not enter verification code"),
                                                    message: NSLocalizedString("Please enter the verification code.",
                                                                               comment: "Alert, message - user did not enter verification code"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user did not enter verification code"), style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        let email = model.deviceLayer.getNewEmail()
        let error = model.confirmNewEmail(email: email!, code: verificationCode)
        
        if error != nil {
            let alertController = UIAlertController(title: error?.userInfo["__type"] as? String,
                                                    message: error?.userInfo["message"] as? String,
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - system error in confirm new email"),
                                                                  style: .default,
                                                                  handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
        } else {
            
            self.performSegue(withIdentifier: "verifyemailtotabview", sender: self)
        }
        
    trace.end()
    }
    
}
