//
//  ConfirmSignUpViewController.swift
//  AwsTest
//
//  Created by Simon Jensen on 26/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class ConfirmSignUpViewController: UIViewController {
    
    var user: String?
    var email: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func continuePressed(_ sender: Any) {
        
            //self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let signInViewController = segue.destination as? SignInViewController {
            signInViewController.emailFromCreation = self.email
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
