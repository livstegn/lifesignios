//
//  CreateGroupViewController.swift
//  LifeSign
//
//  Created by Simon Jensen on 09/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class CreateGroupViewController: UIViewController {
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblInformation: UILabel!
    @IBOutlet weak var groupname: UITextField!
    @IBOutlet weak var btnCreateGroup: UIButton!
    
    let trace = NoTraces()
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.navigationItem.title = NSLocalizedString("New group", comment: "Create group: View title text")
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func createGroupPressed(_ sender: Any) {
        
        // validate
        if self.groupname.text?.count == 0 {
            displayAlertMessage(messageToDisplay: NSLocalizedString("Group name cannot be empty", comment: "Create group: Empty name validation message"))
            return
        }
        
        // Add a new caregroup at cloudlayer
        let caregroup = Caregroup()
        
        caregroup.caregroupName = self.groupname.text ?? "unknown name"
        caregroup.status = CaregroupStatusType.Active
        
        let careGroupID = model.addCaregroup(userid: model.user?.userId ?? "", caregroup: caregroup)
        //if(careGroupID != nil)
        //    {NotificationBanner.show("New group created (\(careGroupID))", style: .success)}

        // Update model to include new caregroup
        model.getCareGroups(forceRefresh: true)

        // Select the just added caregroup as the active/current caregroup
        if(model.getCareGroup(caregroupId: careGroupID!)==nil) {
            NotificationBanner.show("New group not updated - returned nil (\(String(describing: careGroupID)))", style: .error)
        }
        
        model.setCurrentCaregroup(index: -1)
        for (index, caregroup) in self.model.caregroups!.enumerated() {
            if(caregroup.caregroupId == careGroupID ) {
                model.setCurrentCaregroup(index: index)
            }
        }
    }
    
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: NSLocalizedString("Alert", comment: "Create group: Alert message title"), message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Create group: OK alertaction button"), style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            self.trace.output("Ok button tapped");
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
