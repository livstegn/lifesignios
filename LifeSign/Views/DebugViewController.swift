//
//  TempViewController.swift
//  
//
//  Created by Simon Jensen on 09/06/2020.
//

import UIKit

class DebugViewController: UIViewController {

    @IBOutlet weak var txtInvitation: UITextView!
    @IBOutlet weak var txtTracking: UITextView!
    @IBOutlet weak var txtFeedback: UITextView!
    
    let notificationHandler = (UIApplication.shared.delegate as! AppDelegate).notificationHandler
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(animated)
        
        txtInvitation.text = notificationHandler.invitationText
        txtTracking.text = notificationHandler.trackingText
        txtFeedback.text = notificationHandler.activityText

    }
    
    @IBAction func BtnExit(_ sender: Any) {
        exit(0)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
