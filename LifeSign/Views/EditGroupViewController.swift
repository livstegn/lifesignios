//
//  EditGroupViewController.swift
//  LifeSign
//
//  Created by Bo Dalberg on 24/07/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class EditGroupViewController: UITableViewController {
    
    @IBOutlet weak var txtGroupName: UITextField!
    @IBOutlet weak var swActive: UISwitch!
    @IBOutlet weak var btnDeleteGroup: UIButton!
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace  = NoTraces()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(self.btnCancel(sender:)))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.btnDone(sender:)))

        //self.navigationItem.title = NSLocalizedString("Properties", comment: "Edit group: View title text")

        txtGroupName.text = model.getCurrentCaregroup()!.caregroupName
        swActive.isOn = false
        btnDeleteGroup.setTitle(NSLocalizedString("Delete Caregroup", comment: "Edit group: Delete group button text"), for: .normal)
        //print(model.caregroups![model.currentGroup].status)
        //print(model.caregroups![model.currentGroup].status.description)
        if(model.getCurrentCaregroup()!.status.description == "active") {
            swActive.isOn = true
        }
        
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    @objc func btnCancel(sender: UIBarButtonItem) {
        // Function body goes here
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func btnDone(sender: UIBarButtonItem) {
        // Function body goes here
        
        // validate
        if txtGroupName.text?.count == 0 {
            displayAlertMessage(messageToDisplay: NSLocalizedString("Group name cannot be empty", comment: "Edit group properties: Empty name validation message"))
            return
        }
        // TODO: Kald (nyt) API UpdateCaregroup(...)
        let caregroup = Caregroup()
        
        caregroup.caregroupId = model.getCurrentCaregroup()!.caregroupId
        caregroup.caregroupName = txtGroupName.text ?? ""
        caregroup.status = swActive.isOn ? .Active : .Inactive
        
        _ = model.updateCaregroup(userid: model.user?.userId ?? "", caregroup: caregroup)
        
        // Update model to include new caregroup
        //model.getCareGroups()
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: NSLocalizedString("Alert", comment: "Edit group properties: Alert message title"), message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Edit group properties: OK alertaction button"), style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            self.trace.output("Ok button tapped");
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    @IBAction func DeleteBtn(sender: AnyObject) {
        confirmDelete(groupName: txtGroupName.text ?? "<missing name>")
    }
    
    func confirmDelete(groupName: String) {

        let alert = UIAlertController(title: NSLocalizedString("Delete Caregroup", comment: "Edit group properties: Delete alert title"), message: NSLocalizedString("Are you sure you want to permanently delete #1# including references to members?", comment: "List of groups: Delete alert message").replacingOccurrences(of: "#1#", with: groupName), preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: NSLocalizedString("Delete", comment: "Edit group properties: Delete alert button"), style: .destructive, handler: handleDelete)
        let CancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Edit group properties: Cancel alert button"), style: .cancel, handler: cancelDelete)
        
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleDelete(alertAction: UIAlertAction!) -> Void {
        // call delete API
        trace.begin()
        self.trace.output("GroupList - delete group")
        
        let deletegroupid = model.getCurrentCaregroup()!.caregroupId
        let rc = self.model.deleteCaregroup(caregroupid: deletegroupid)
        trace.output(rc as Any)
        trace.end()

        // close view - and pop 2 levels back
        performSegue(withIdentifier: "UnwindToGroupListSegueID", sender: self)
        
    }
    
    func cancelDelete(alertAction: UIAlertAction!) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
