//
//  EditSupervisionViewController.swift
//  LifeSign
//
//  Created by Bo Dalberg on 26/09/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class EditSupervisionViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var supervisionTypePicker: UIPickerView!
    @IBOutlet weak var deadlineTimePicker: UIDatePicker!
    @IBOutlet weak var reminderTimePicker: UIDatePicker!
    @IBOutlet weak var startTimePicker: UIDatePicker!
    @IBOutlet weak var EndTimePicker: UIDatePicker!
    @IBOutlet weak var swActive: UISwitch!
    
    var groupmember = GroupMember()
    var supervisions = [Supervision]()
    var supervision = Supervision()
    var fromSupervisions = false
    var detailSupervision : Supervision?
    
    let supervisionTypePickerValues = ["Ongoing", "Time limited", "Spot"]
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model

    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return supervisionTypePickerValues.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return supervisionTypePickerValues[row]
    }
    
    /*
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        return NSAttributedString(string: supervisionTypePickerValues[row], attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
    }*/
    
/*
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        trace.begin()
        
        self.supervisionTypePicker.dataSource = self
        self.supervisionTypePicker.delegate = self
        
        if (!fromSupervisions) {
            groupmember = model.groupmembers![model.currentMember]
            trace.output(groupmember.userid)
            trace.output(groupmember.groupmemberId)
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }

        startTimePicker.timeZone = NSTimeZone.local
        EndTimePicker.timeZone = NSTimeZone.local

        readAndPresentData()
        showTraces()

        // disable non supported functionality
        supervisionTypePicker.isUserInteractionEnabled = false
        startTimePicker.isEnabled = false
        EndTimePicker.isEnabled = false

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        trace.end()

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        saveChanges()
    }

    func saveChanges() {
        trace.output("Saving supervision")

        switch supervisionTypePicker.selectedRow(inComponent: 0) {
        case 0:
            supervision.supervisionType = .Ongoing
        case 1:
            supervision.supervisionType = .Period
        default:
            supervision.supervisionType = .Ongoing
        }
        
        supervision.deadline1 = deadlineTimePicker.countDownDuration
        supervision.countdownTime = reminderTimePicker.countDownDuration
        supervision.status = swActive.isOn ? .Active : .Inactive
        supervision.periodStart = startTimePicker.date
        supervision.periodEnd = EndTimePicker.date
        
        showTraces()
        
        if(supervision.supervisionId == "") {
            trace.output("Add new supervision")
            supervision.groupmemberId = groupmember.groupmemberId
            _ = model.addSupervision(supervision: supervision)
        } else {
            trace.output("Update existing supervision")
            _ = model.updateSupervision(supervision: supervision)
        }
    }
    
    func showTraces() {
        trace.output(supervision.supervisionId)
        trace.output(supervision.supervisionType)
        trace.output(Helper.simpleTimeFromInterval(second: supervision.countdownTime) as Any)
        trace.output(supervision.status)
        trace.output(supervision.periodStart as Any)
        trace.output(supervision.periodEnd as Any)
    }
    
    func readAndPresentData() {
        if (!fromSupervisions) {
            supervisions = model.cloudLayer.getSupervisions(groupmemberId: groupmember.groupmemberId)!
            if(supervisions.count > 0) {
                supervision = supervisions[0]   // always only use the first one...
            }
        } else {
            supervision = detailSupervision!
        }
        
        let currentdate = Date()
        var tmpdate = Date()
        var calendar = Calendar.current
        trace.output(calendar.timeZone)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
        
        tmpdate = dateFormatter.date(from: Helper.simpleTimeFromInterval(second: supervision.deadline1 ?? 0)!)!
        deadlineTimePicker.date = tmpdate
        tmpdate = dateFormatter.date(from: Helper.simpleTimeFromInterval(second: supervision.countdownTime)!)!
        reminderTimePicker.date = tmpdate
        
        var pickerIndex : Int = 0
        switch supervision.supervisionType {
        case .Ongoing:
            pickerIndex = 0
        case .Period:
            pickerIndex = 1
        default:
            pickerIndex = 0
        }
        supervisionTypePicker.selectRow(pickerIndex, inComponent: 0, animated: true)

        swActive.isOn = (supervision.status == .Active )
        startTimePicker.setDate(supervision.periodStart ?? currentdate, animated: true)
        EndTimePicker.setDate(supervision.periodEnd ?? currentdate, animated: true)
    }

    // MARK: - Table view data source
/*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
