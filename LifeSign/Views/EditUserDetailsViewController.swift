//
//  EditUserDetailsViewController.swift
//  LifeSign
//
//  Created by Bo Dalberg on 25/08/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class EditUserDetailsViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var btnCancel: UIBarButtonItem!
    @IBOutlet weak var btnEdit: UIBarButtonItem!
    @IBOutlet weak var UserName: UITextField!
    @IBOutlet weak var UserEmail: UITextField!
    @IBOutlet weak var UserCellNo: UITextField!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblMonitor: UILabel!
    @IBOutlet weak var lblMonitored: UILabel!
    @IBOutlet weak var lblOwner: UILabel!
    @IBOutlet weak var swActive: UISwitch!
    @IBOutlet weak var swMonitor: UISwitch!
    @IBOutlet weak var swMonitored: UISwitch!
    @IBOutlet weak var swOwner: UISwitch!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var monitoredCell: UITableViewCell!
    @IBOutlet weak var btnLogOff: UIButton!
    
    var oldUserName: String = ""
    var oldUserEmail: String = ""
    var oldUserCellNo: String = ""
    var editUser = User()
    var segueName: String = ""
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model

    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        trace.begin()
        trace.output("EditUserDetailsViewController::viewDidLoad")
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        self.navigationItem.leftBarButtonItem?.title = ""
        self.navigationItem.leftBarButtonItem?.tintColor = .clear

        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.edit, target: self, action: #selector(self.btnEdit(sender:)))
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false

        self.tableView.delegate = self
        self.tableView.dataSource = self
        //self.tableView.rowHeight = UITableView.automaticDimension
        //self.tableView.estimatedRowHeight = 52.0
        
        self.UserName.delegate = self
        self.UserEmail.delegate = self
        self.UserCellNo.delegate = self
        
        // Add a shadow under the logout button
        self.btnLogOff.layer.shadowColor = UIColor.lightGray.cgColor
        self.btnLogOff.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.btnLogOff.layer.shadowOpacity = 1.0
        self.btnLogOff.layer.shadowRadius = 0.0
        
        trace.end()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        trace.begin()
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.hidesBackButton = false
        btnDelete.isHidden = true
        
        //hideFields(hideState: true)
        
        if (segueName == "group2MemberDetailsSeague") {
            // coming from caregroupview
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            btnDelete.setTitle(NSLocalizedString("Remove from group", comment: "Groupmember - remove from group?"), for: .normal)

            btnLogOff.isEnabled = false
            btnLogOff.isHidden = true

            editUser = model.getUser(userid: model.groupmembers![model.currentMember].userid)!
            
            swActive.isOn = (model.groupmembers![model.currentMember].status.description == "active")
            swMonitored.isOn = (model.groupmembers![model.currentMember].isMonitored == true)
            swMonitor.isOn = (model.groupmembers![model.currentMember].isMonitor == true)
            swOwner.isOn = (model.groupmembers![model.currentMember].isOwner == true)
            
            monitoredCell.tintColor = UIColor.black

            if(model.groupmembers![model.currentMember].status == .Invited || model.groupmembers![model.currentMember].userid == model.user?.userId) {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }

            hideFields(hideState: true) // set to false to show fields - fields temporaly hidden
        }
        else {
            // coming from main tab bar
            model.currentMember = -1
            btnDelete.setTitle(NSLocalizedString("Delete user profile", comment: "Selection - Delete user profile?"), for: .normal)
            editUser.userName = model.getUserName()
            editUser.email = model.user?.email ?? ""
            editUser.cellno = model.user?.cellno ?? ""
            if(editUser.cellno == " ") {
                editUser.cellno = ""
            }
            editUser.userId = model.user!.userId
            hideFields(hideState: true)
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        trace.output(editUser.userId)
        
        UserName.isEnabled = false
        UserEmail.isEnabled = false
        UserCellNo.isEnabled = false
        
        UserName.text = editUser.userName
        UserEmail.text = editUser.email
        UserCellNo.text = editUser.cellno
        
        swActive.isEnabled = false
        swOwner.isEnabled = false
        swMonitor.isEnabled = false
        swMonitored.isEnabled = false
        
        trace.end()
         
    }

    override func viewDidAppear(_ animated: Bool) {
        trace.begin()
        
        
        trace.end()
    }
    
    func hideFields(hideState: Bool) {
        trace.output("show/hide fiels")
        swActive.isHidden = hideState
        lblStatus.isHidden = hideState
        lblMonitor.isHidden = hideState
        swMonitor.isHidden = hideState
        lblMonitored.isHidden = hideState
        swMonitored.isHidden = hideState
        lblOwner.isHidden = hideState
        swOwner.isHidden = hideState
    }
    
    @objc func btnEdit(sender: UIBarButtonItem) {
        // Function body goes here
        oldUserName = UserName.text!
        oldUserEmail = UserEmail.text!
        oldUserCellNo = UserCellNo.text!
        
        setEditing(true, animated: false)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.okEditing(sender:) ))
    }
    
    @IBAction func switchIsChanged(_ sender: UISwitch) {
        if sender.isOn {
            monitoredCell.accessoryType = .detailButton
        } else {
            monitoredCell.accessoryType = .none
        }
        trace.output(sender.isOn)
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        guard let headerView = view as? UITableViewHeaderFooterView else { return }

        headerView.contentView.backgroundColor = UIColor(named: "lsTextVIewBackground")
        headerView.textLabel?.textColor = UIColor(named: "lsTextFontStandard")
        headerView.textLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)
        headerView.textLabel?.adjustsFontForContentSizeCategory = true

    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var hideSections: Bool = true // was false - temporaly hidden
        
        if (segueName == "") {
            hideSections = true
        }
        if ((section == 1 || section == 2) && hideSections) {
            //header height for selected section
            return 0.1
        } else {
            //keeps all other Headers unaltered
            return super.tableView(tableView, heightForHeaderInSection: section)
        }
    }
    
    /*override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }*/
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var hideSections: Bool = true
        
        if (segueName == "") {
            hideSections = true
        }
        
        switch section {
        case 0:
            return super.tableView(tableView, titleForHeaderInSection: section)
        case 1:
            return (hideSections ? "" : super.tableView(tableView, titleForHeaderInSection: section))
        case 2:
            return (hideSections ? "" : super.tableView(tableView, titleForHeaderInSection: section))
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    /*
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if(!self.isEditing) {
            self.performSegue(withIdentifier: "profile2supervisionSettings", sender: self)
        }
    }*/
    /*
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var hideSections: Bool = false
        
        if (segueName == "") {
            hideSections = true
        }
        switch section {
        case 0:
            return 3
        case 1:
            return (hideSections ? 0 : 1)
        case 2:
            return (hideSections ? 0 : 3)
        case 3:
            return 4
        default:
            return 0
        }
    }*/
    
    @IBAction func cancelEditing(sender: AnyObject) {

        UserName.text = oldUserName
        UserEmail.text = oldUserEmail
        UserCellNo.text = oldUserCellNo
        
        setEditing(false, animated: false)
    }
    
    @IBAction func okEditing(sender: AnyObject) {
        trace.begin()
        // validate efter editing
        if(self.isEditing)
        {
            trace.output("Validate")
            // validate
            if UserName.text?.count == 0 {
                displayAlertMessage(messageToDisplay: NSLocalizedString("Username cannot be empty", comment: "Validation - username"))
                return
            }
            if !isValidEmail(emailStr: UserEmail.text!) {
                displayAlertMessage(messageToDisplay: NSLocalizedString("Email address is not valid", comment: "Validation - email") )
                return
            }
            let formattedString = UserCellNo.text?.replacingOccurrences(of: " ", with: "")
            UserCellNo.text = formattedString
            if UserCellNo.text!.count>0
            {
                if !isValidCellNo(cellNoStr: UserCellNo.text!) {
                    displayAlertMessage(messageToDisplay: NSLocalizedString("Mobile no is not valid", comment: "Validation - mobile no") )
                    return
                }
            }
            else {
                // field is empty  insert 1 space - to be able to update in DB...
                UserCellNo.text = " "
            }
            
            editUser.userName = UserName.text! // was: model.user?
            editUser.email = UserEmail.text!
            editUser.cellno = UserCellNo.text!

            if (model.user?.userId == editUser.userId) {
                trace.output("Update own profile data in model")
                model.user?.userName = editUser.userName
                model.user?.email = editUser.email
                model.user?.cellno = editUser.cellno
            }
            
            if (segueName == "") {
                //update user - only for direct navigation = logged on user
                trace.output("UpdateUserDetails")
                self.model.updateUserDetails(userId: editUser.userId)
            }
            else
            {
                trace.output("UpdateGroupMemberUserDetails")
                self.model.updateGroupMemberUserDetails(userid: editUser.userId, gmuser: editUser )
                
                model.groupmembers![model.currentMember].status = swActive.isOn ? .Active : .Inactivated
                model.groupmembers![model.currentMember].isMonitored = swMonitored.isOn ? true : false
                model.groupmembers![model.currentMember].isMonitor = swMonitor.isOn ? true : false
                model.groupmembers![model.currentMember].isOwner = swOwner.isOn ? true : false
                
                // todo: update groupmember details by API
                model.updateGroupMemberDetails(groupMember: model.groupmembers![model.currentMember])
                
            }
            setEditing(false, animated: false)
        }
        else {
            trace.output("Editing mode")
            setEditing(true, animated: false)
            oldUserName = UserName.text!
            oldUserEmail = UserEmail.text!
            oldUserCellNo = UserCellNo.text!
        }
        trace.end()
    }
    
    override func setEditing (_ isEditing:Bool, animated:Bool)
    {
        super.setEditing(isEditing,animated:animated)
        if(self.isEditing)
        {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.okEditing(sender:) ))
            self.navigationItem.leftBarButtonItem?.title = "Cancel"
            self.navigationItem.leftBarButtonItem?.tintColor = .darkGray
            self.navigationItem.leftBarButtonItem?.isEnabled = true
        }else
        {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.edit, target: self, action: #selector(self.btnEdit(sender:)))
            self.navigationItem.leftBarButtonItem?.title = ""
            self.navigationItem.leftBarButtonItem?.tintColor = .clear
            self.navigationItem.leftBarButtonItem?.isEnabled = false
            
        }
        btnDelete.isHidden = !isEditing
        if (segueName != "group2MemberDetailsSeague") {
            btnLogOff.isHidden = isEditing
        }
        UserName.isEnabled = isEditing
        UserEmail.isEnabled = isEditing
        UserCellNo.isEnabled = isEditing
        swActive.isEnabled = isEditing
        swOwner.isEnabled = isEditing
        swMonitor.isEnabled = isEditing
        swMonitored.isEnabled = isEditing
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        //displayAlertMessage(messageToDisplay: "test")
    }

    override func viewWillDisappear(_ animated: Bool) {
        setEditing(false, animated: false)
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func isValidCellNo(cellNoStr:String) -> Bool {
        let cellNoRegEx = "^(00|\\+)?[0-9]{8,15}"
        let cellNoTest = NSPredicate(format:"SELF MATCHES %@", cellNoRegEx)
        return cellNoTest.evaluate(with: cellNoStr)
    }
    
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            self.trace.output("Ok button tapped");
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    @IBAction func DeleteBtn(sender: AnyObject) {
        confirmDelete(userName: editUser.userName)
    }
    
    func confirmDelete(userName: String) {
        var alertTitle = ""
        var alertMessage = ""
        
        if (segueName == "") {
            alertTitle = NSLocalizedString("Delete user profile", comment: "Selection - Delete user profile?")
            let format = NSLocalizedString("Are you sure you want to permanently delete %@ from this app?", comment: "Information - Delete user profile?")
            alertMessage = String.localizedStringWithFormat(format, userName)
        } else {
            alertTitle = NSLocalizedString("Remove Groupmember", comment: "Selection - Remove Groupmember?")
            let format = NSLocalizedString("Are you sure you want to permanently remove %@ from this group?", comment: "Information - Remove Groupmember?")
            alertMessage = String.localizedStringWithFormat(format, userName)
        }
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .actionSheet)
        var actionStr: String = ""
        if (segueName == "") {
            actionStr = NSLocalizedString("Delete", comment: "Title - Remove Groupmember?")
        } else {
            actionStr = NSLocalizedString("Remove", comment: "Title - Remove Groupmember?")
        }
        
        let DeleteAction = UIAlertAction(title: actionStr, style: .destructive, handler: handleDelete)
        alert.addAction(DeleteAction)

        let CancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Selection - Cancel action"),
                                         style: .cancel, handler: cancelDelete)
        alert.addAction(CancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleDelete(alertAction: UIAlertAction!) -> Void {
        trace.begin()

        if (segueName == "") {
            // delete user using cloudlayer
            trace.output("Delete user")
            let rc = model.cloudLayer.deleteUser(userid: editUser.userId)
            trace.output(rc as Any)

            let error_rc = model.cloudLayer.deleteCognitoUser()
            trace.output(error_rc as Any)

            model.signOut()
                        
            self.parent?.navigationController?.popToRootViewController(animated: true)
        } else {
            // remove user from CG using Cloudlayer
            trace.output("Remove user from CG")
            trace.output(editUser.userId)
            trace.output(model.getCurrentCaregroup()?.caregroupId as Any)
            trace.output(model.groupmembers?[model.currentMember].groupmemberId as Any)

            let rc = model.cloudLayer.removeGroupmember(userId: editUser.userId, caregroupId: (model.getCurrentCaregroup()?.caregroupId)!, memberId: (model.groupmembers?[model.currentMember].groupmemberId)!)
            trace.output(rc as Any)
            
            model.groupmembers?.remove(at: model.currentMember)
            model.currentMember = -1
            
            self.navigationController?.popViewController(animated: true)
        }
        trace.end()
    }
    
    func cancelDelete(alertAction: UIAlertAction!) {
    }
    
    @IBAction func signoutPressed(_ sender: Any) {
        trace.output("UserProfileViewController::signoutPressed")
        trace.output("UserProfileViewController::signoutPressed::self.user?.signOut()")
        
        model.signOut()
        self.parent?.navigationController?.popToRootViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
