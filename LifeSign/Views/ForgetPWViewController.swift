//
//  ForgotPWViewController.swift
//  AwsTest
//
//  Created by Simon Jensen on 25/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class ForgetPWViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    var email = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.username.text = self.model.getEmail()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let confirmForgetPWViewController = segue.destination as? ConfirmForgetPWViewController  {
            confirmForgetPWViewController.email = self.email
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func sendPressed(_ sender: Any) {
        
        guard let emailValue = self.username.text, !emailValue.isEmpty else {
            
            let alertController = UIAlertController(title: NSLocalizedString("Missing email",
                                                                             comment: "Alert, title - user did not enter email"),
                                                    message: NSLocalizedString("Please enter a valid email.",
                                                                               comment: "Alert, message - user did not enter email"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user did not enter email"), style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        let error = model.forgotPassword(email: emailValue)

        if error != nil {
            let alertController = UIAlertController(title: error?.userInfo["__type"] as? String,
                                                    message: error?.userInfo["message"] as? String,
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - system error in forgot password"),
                                                                  style: .default,
                                                                  handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
        } else {
            self.email = emailValue
            self.performSegue(withIdentifier: "confirmForgotPWSegue", sender: sender)
        }
        
    }
}
