//
//  GroupHistoryTableViewCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 20/10/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class GroupHistoryListViewCell: UITableViewCell {
    @IBOutlet weak var lblGroupName: UILabel!
    
    var caregroup: Caregroup?
}
