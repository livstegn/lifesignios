//
//  GroupTableViewCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 05/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class GroupListViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!

    var caregroup: Caregroup?
}
