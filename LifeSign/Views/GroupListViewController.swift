//
//  ViewController.swift
//  UITableView
//
//  Created by Simon Jensen on 27/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit
import AMPopTip // https://github.com/andreamazz/AMPopTip



class GroupListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var activityPopup: UIView!
    @IBOutlet weak var btnNewGroup: UIButton!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    var refreshControl = UIRefreshControl()
    var deleteRowIndexPath: IndexPath? = nil

    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let caregroup = Caregroup()
    var isCareGroupLoaded : Bool = false
    var isViewReady : Bool = false
    
    // Crate PopTip and vars
    let popTip = PopTip()
    var direction = PopTipDirection.up
    var topRightDirection = PopTipDirection.down
    var timer: Timer? = nil
    var autolayoutView: UIView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        trace.begin()
        trace.output("GroupListViewController::viewDidLoad")

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //self.lblTitle.font = Helper.weightedPreferredFont(font: self.lblTitle.font, weight: .semibold)
        //lblTitle.text = NSLocalizedString("Your groups", comment: "List of groups: Title")
        
        //btnNewGroup.setTitle(NSLocalizedString("Create a new group", comment: "List of groups: New group button text"), for: .normal)

        self.refreshControl.addTarget(self, action: #selector(refreshAfterDrag), for: .valueChanged)
        if #available(iOS 10, *){
            self.tableView.refreshControl = self.refreshControl
        }else{
            self.tableView.addSubview(self.refreshControl)
        }
        
        // Prepare PopTip
        popTip.font = UIFont.systemFont (ofSize: 16) //UIFont(name: "Avenir-Medium", size: 12)!
        popTip.textColor = .white
        popTip.bubbleColor = UIColor(named: "lsBigButtonBackgroundMain") ?? UIColor(red: 0.95, green: 0.65, blue: 0.21, alpha: 1)
        popTip.textAlignment = .left
        popTip.shouldDismissOnTap = true
        popTip.shouldDismissOnTapOutside = true
        popTip.shouldDismissOnSwipeOutside = true
        popTip.cornerRadius = 15
        popTip.arrowSize = CGSize(width: 16, height: 16)
        popTip.edgeMargin = 5
        popTip.offset = 2
        popTip.bubbleOffset = 0
        popTip.edgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        popTip.maskColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        popTip.shouldShowMask = true

    }
    
   override func viewWillAppear(_ animated: Bool) {

        trace.begin()
        super.viewWillAppear(animated)
        self.loadCareGroups()
        popTip.hide()
        trace.end()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.presentPopTip()
        }
    }
    
    func presentPopTip() {
        // Present PopTip
        let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: popTip.font, NSAttributedString.Key.foregroundColor: popTip.textColor]
        let bold: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: popTip.textColor]

        if model.caregroups!.count < 1 {
            if let frame = btnNewGroup.superview?.convert(btnNewGroup.frame, to: nil) {
                let attributedText = NSMutableAttributedString(string: "Create care group\n", attributes: bold)
                attributedText.append(NSAttributedString(string: "Tap here to add caregroups and invite people\n\n", attributes: attributes))
                attributedText.append(NSAttributedString(string: "Got it", attributes: bold))
                popTip.show(attributedText: attributedText, direction: .autoVertical, maxWidth: 200, in: self.view, from: frame)
             }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        isViewReady = true
    }
    
    @objc func refreshAfterDrag() {
        self.loadCareGroups()
    }
    
    override func updateViewConstraints() {
        
    /*  Note! This Constraint update is very view specific!
        The purpose is to adapt the table height to the content,
        but also limit the height to assure that the button is
        always displayed */

        trace.begin()

        if(isViewReady) {
            let safeAreaGuide = self.view.safeAreaLayoutGuide
            let safeAreaHeight = safeAreaGuide.layoutFrame.size.height
            let buttonHeight  = btnNewGroup.frame.height + btnNewGroup.contentEdgeInsets.top + btnNewGroup.contentEdgeInsets.bottom
            let tabBarHeight = self.tabBarController?.tabBar.bounds.height ?? 0
            let tableStart = tableView.frame.origin.y
            
            if(isCareGroupLoaded) {
                var tableHeight = tableView.contentSize.height
                let tableHeightMax = safeAreaHeight - tableStart - tabBarHeight - buttonHeight

                if (tableHeight > tableHeightMax) {
                    tableHeight = tableHeightMax
                }
                self.tableHeight.constant = tableHeight
            }
        }
        
        super.updateViewConstraints()
        trace.end()
    }
        
    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        trace.begin()
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            deleteRowIndexPath = indexPath
            let deletename = model.caregroups![indexPath.row].caregroupName
            confirmDelete(groupName: deletename)
        }
        trace.end()
    }
    
    @IBAction func unwindToGroupListViewController(segue: UIStoryboardSegue) {
        // Update model to include new caregroup
        trace.begin()
        //model.getCareGroups()
        trace.end()
    }
    
    func confirmDelete(groupName: String) {
        
        trace.begin()
        
        let alert = UIAlertController(title: NSLocalizedString("Delete Caregroup", comment: "List of groups: Delete alert title"), message: NSLocalizedString("Are you sure you want to permanently delete #1# including references to members?", comment: "List of groups: Delete alert message").replacingOccurrences(of: "#1#", with: groupName), preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: NSLocalizedString("Delete", comment: "List of groups: Delete alert button"), style: .destructive, handler: handleDeleteRow)
        let CancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "List of groups: Cancel alert button"), style: .cancel, handler: cancelDeleteRow)
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        
        self.present(alert, animated: true, completion: nil)
        trace.end()
    }
    
    func handleDeleteRow(alertAction: UIAlertAction!) -> Void {
        
        trace.begin()
        if let indexPath = deleteRowIndexPath {
 
            trace.output("handleDeleteRow")

            trace.begin()
            self.trace.output("GroupList - delete group")
            trace.end()
            
            let careGroupToDelete = model.caregroups![indexPath.row]
            let deletegroupid = careGroupToDelete.caregroupId
            let rc = self.model.deleteCaregroup(caregroupid: deletegroupid)
            
            model.setCurrentCaregroup(index: -1)

            //var rc_str = String(rc ?? 0)
            //NotificationBanner.show("\(rc)", style: .info )
            
            deleteRowIndexPath = nil
            
            loadCareGroups(forceModelRefresh : true)
            
            /*
             tableView.beginUpdates()
             
             xxx.removeAtIndex(indexPath.row)
             
             // Note that indexPath is wrapped in an array:  [indexPath]
             tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
             
             deleteRowIndexPath = nil
             
             tableView.endUpdates()
             */
        }
        trace.end()
    }
    
    func cancelDeleteRow(alertAction: UIAlertAction!) {
        
        trace.begin()
        deleteRowIndexPath = nil
        trace.end()
    }

    func loadCareGroups(forceModelRefresh: Bool = false) {
        trace.begin()
        isCareGroupLoaded = false
        
        if refreshControl.isRefreshing == false {
            self.spinner.startAnimating()
            self.activityPopup.isHidden = false
        }
        
        DispatchQueue.global(qos: .background).async {
            self.model.getCareGroups(forceRefresh : forceModelRefresh)
            DispatchQueue.main.async {
                self.careGroupsLoaded()
            }
        }
        trace.end()
        
        //let str = "Caregroups loaded: \(model.caregroups?.count ?? -1)"
        //NotificationBanner.show(str, style: .info )
    }
    
    func careGroupsLoaded() {
        trace.begin()
        if refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        } else {
            self.spinner.stopAnimating()
            self.activityPopup.isHidden = true
        }
        self.isCareGroupLoaded = true
        self.tableView.reloadData()
        trace.end()
    }
        
    // number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        trace.begin()
        
        if(isCareGroupLoaded) {
            trace.output("CareGroup IS Loaded!!")
            if (model.caregroups?.count == 0) {
                trace.output("tabelView.height (numberOfRowsInSection [0]) = \(tableView.contentSize.height)")
            }
            let rows = model.caregroups?.count ?? 0
            trace.output("rows = \(rows)")
            trace.end()
            return rows
        }
        else {
            trace.output("CareGroup NOT Loaded!!")
            trace.end()
            return 0
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        trace.begin()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellGroup", for: indexPath) as! GroupListViewCell
        cell.caregroup = model.caregroups![indexPath.row]
        
        cell.lblName.text = cell.caregroup?.caregroupName
        cell.lblStatus.text = cell.caregroup?.status.description
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        // Set the height of the table to match the content
        updateViewConstraints()
        
        trace.end()

        return cell
    }

    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        trace.output("You tapped cell number \(indexPath.row).")
        
        self.model.setCurrentCaregroup(index: indexPath.row)
        self.performSegue(withIdentifier: "groupCreate2MemberSeague", sender: self)
        trace.end()
    }

    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let memberListViewController = segue.destination as! MemberListViewController
        
        if let cell = sender as? GroupTableViewCell, let _ = tableView.indexPath(for: cell) {
            memberListViewController.caregroup = cell.caregroup!
        }
    }
 */
        
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
}


