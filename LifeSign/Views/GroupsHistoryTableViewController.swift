//
//  GroupsHistoryTableViewController.swift
//  
//
//  Created by Peter Rosendahl on 20/10/2019.
//

import UIKit

class GroupsHistoryTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    override func viewDidLoad() {
        super.viewDidLoad()
        trace.output("GroupsHistoryTableViewController entry")
        
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "lsViewBackground")
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "lsHeadlineText")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor(named: "lsHeadlineText")

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 54.0


        model.getCareGroups()

        // Do any additional setup after loading the view.
        trace.output("No of groups: \(model.caregroups?.count ?? 0)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let title = NSLocalizedString("Back", comment: "Back button label in list of historic events")

        if let moreList = self.tabBarController?.moreNavigationController.viewControllers.first(where: { String(describing: type(of: $0)) == "UIMoreListController" }) {
          moreList.navigationItem.backBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.caregroups?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellGroupHistory", for: indexPath) as! GroupHistoryListViewCell
        
        cell.lblGroupName.text = model.caregroups![indexPath.row].caregroupName
        cell.caregroup = model.caregroups![indexPath.row]
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator

        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let historyListViewController = segue.destination as! HistoryListViewController

        //.viewControllers

        if let cell = sender as? GroupHistoryListViewCell, let _ = tableView.indexPath(for: cell) {
            //historyListViewController.caregroup = cell.caregroup!
        }
    }

}
