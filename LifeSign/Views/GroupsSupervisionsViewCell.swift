//
//  GroupsSupervisionsViewCell.swift
//  
//
//  Created by Simon Jensen on 19/04/2020.
//

import UIKit

class GroupsSupervisionsViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
