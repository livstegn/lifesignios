//
//  GroupsSupervisionsViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 22/02/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class GroupsSupervisionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityPopup: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let caregroup = Caregroup()
    var isCareGroupLoaded : Bool = false
    
    let trace = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        trace.output("GroupsSupervisions viewDidLoad")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

        // Do any additional setup after loading the view.
        trace.output("No of groups: \(model.caregroups?.count ?? 0)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        trace.output("GroupsSupervisions viewWillAppear")
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        // deselect rows
        tableView.indexPathsForSelectedRows?.forEach({ tableView.deselectRow(at: $0, animated: false) })
        
        self.isCareGroupLoaded = false
        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        DispatchQueue.global(qos: .background).async {
            self.model.getCareGroups()
            DispatchQueue.main.async {
                self.careGroupsLoaded()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(!isCareGroupLoaded) {
            return 0
        } else {
            return model.caregroups?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSuper", for: indexPath) as! GroupsSupervisionsViewCell
        cell.lblName.text = model.caregroups![indexPath.row].caregroupName
        //cell.textLabel?.font = UIFont.systemFont(ofSize: 20.0)
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let supervisionsTableViewController = segue.destination as! SupervisionsTableViewController

        if let cell = sender as? UITableViewCell, let ipath = tableView.indexPath(for: cell) {
            //supervisionsTableViewController.selectedCaregroup = model.caregroups![ipath.row]
        }
        self.hidesBottomBarWhenPushed = false
    }
    
    func careGroupsLoaded() {
        trace.begin()
        self.isCareGroupLoaded = true
        self.spinner.stopAnimating()
        self.activityPopup.isHidden = true
        self.tableView.reloadData()
    }
}
