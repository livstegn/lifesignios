//
//  HistoryListViewCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 20/10/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class HistoryListViewCell: UITableViewCell {
    @IBOutlet weak var lblEventTime: UILabel!
    @IBOutlet weak var lblEventUser: UILabel!
    @IBOutlet weak var lblEventText: UILabel!
    @IBOutlet weak var imgHistory: UIImageView!
    
}
