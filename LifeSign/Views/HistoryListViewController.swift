//
//  HistoryListViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 20/10/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu


class HistoryListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityPopup: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var historyDict : [Date:Any] = [:]
    var historyKeys : [Date] = []
    var textStyleHeader : UIFont.TextStyle?

    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        trace.output("HistoryListViewController viewDidLoad")

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if(self.model.getCurrentCaregroup() == nil) {
            self.model.setCurrentCaregroup(index: 0)
        }
        
        let menuView = Helper.groupMenuView(navigationController: self.tabBarController?.moreNavigationController, model: self.model)
        self.navigationItem.titleView = menuView

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trace.output("HistoryListViewController viewWillAppear")

        trace.begin()

        self.showNavigationBar()

        self.reloadMenuItems()
        
        let menuView = self.navigationItem.titleView as! BTNavigationDropdownMenu
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
            self.model.setCurrentCaregroup(index: indexPath)
            self.loadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
        self.showNavigationBar()
    }
    
    func showNavigationBar () {
        self.tabBarController?.moreNavigationController.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.moreNavigationController.navigationItem.hidesBackButton = false
        self.tabBarController?.tabBar.isHidden = false
    }
 
    func reloadMenuItems () {
        self.model.getCareGroups()
        var items = [String]()
        let NoOfCaregroups = model.caregroups?.count ?? 0
        for index in 0..<NoOfCaregroups {
            items.append(model.caregroups?[index].caregroupName ?? "NA")
        }
        let menuView = self.navigationItem.titleView as! BTNavigationDropdownMenu
        menuView.updateItems(items)
    }
    
    func loadData () {
        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        DispatchQueue.global(qos: .default).async {
            self.model.getCareGroups()
            let cg = self.model.getCurrentCaregroup()
            if cg != nil {
                self.historyDict = self.model.getCaregroupHistory(caregroupId: cg!.caregroupId)
                self.historyKeys = self.historyDict.keys.sorted(by: >)
                self.model.clearCachedUserNames()
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                if self.model.user != nil {
                    self.spinner.stopAnimating()
                    self.activityPopup.isHidden = true
                }
            }
        }

        trace.output("No of groups: \(self.historyDict.count)")
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHistoryList", for: indexPath) as! HistoryListViewCell
        let key = historyKeys[indexPath.row]
        let history = self.historyDict[key]
 
        if history is Alarm {
            let alarm = history as! Alarm
            
            cell.imgHistory.image = UIImage.init(named: "redbell.png")
            cell.lblEventTime.text = Helper.uiTime(date: alarm.date)
            cell.lblEventText.text = NSLocalizedString("Alarm: missing lifesign", comment: "List of historic events, description of an alarm event")
            cell.lblEventUser.text = model.getCachedUserName(userid: alarm.surveiledUserId)
        }
        
        if history is LifeSign {
            cell.imgHistory.image = UIImage.init(named: "greenheart.png")
            let lifesign = history as! LifeSign
            cell.lblEventTime.text = Helper.uiTime(date: lifesign.latestLifesign)
            cell.lblEventText.text = NSLocalizedString("Lifesign made", comment: "List of historic events, description of a made lifesign")
            cell.lblEventUser.text = model.getCachedUserName(userid: lifesign.userId)
        }

        if textStyleHeader == nil {
            textStyleHeader = (cell.lblEventTime.font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.textStyle) as! UIFont.TextStyle)
        }
        
        cell.setNeedsLayout()

        return cell
    }    
}
