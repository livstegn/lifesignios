//
//  InvitationViewController.swift
//  LifeSign
//
//  Created by Simon Jensen on 24/07/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class InvitationViewController: UIViewController {

    @IBOutlet weak var lblInvitationId: UILabel!
    @IBOutlet weak var txtInvitation: UITextView!
    @IBOutlet weak var lblInviteIndex: UILabel!
    @IBOutlet weak var lblInviteTotal: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!

    @IBOutlet weak var invitationID: UIStackView!
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    
    var invitations = [Invitation]()
    var index = 0
    var invitationId: String = ""
    var universalLinkIsValid: Bool = false
    
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        trace.begin()
        
        invitations = model.getInvitations()!  // All invitations (notification & link)
        
        // If the invitation view is displayed due to an invitation notification, then display that invitation first
        if(model.notificationLaunch) {
            var _index = 0
            let invitationId = model.notificationData!["invite_id"] as! String
            for invitation in invitations {
                if (invitation.invitationId == invitationId) {
                    index = _index
                    break
                }
                _index += 1
            }
            self.model.notificationLaunch = false
        }
        
        // If the invitation view is displayed due to an universal link, then display that invitation first
        if(model.universalLinkLaunch) {
            var _index = 0
            for invitation in invitations {
                if (invitation.invitationId == model.universalLinkData) {
                    universalLinkIsValid = true  // The share invitation also exist in on the server
                    index = _index
                    break
                }
                _index += 1
            }
            //model.universalLinkLaunch = false
        }

        invitationID.isHidden = true    // #DEBUG: Set to 'false' to display the invitation ID at the bottom of the screen
        showInvitation(index: index)
        
        trace.end()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        trace.begin()
        trace.end()
    }
    
    func updateInvitations() {
        
        invitations = model.getInvitations()!

        if (index >= invitations.count) {
            index = 0
        }
        
        self.showInvitation(index: index)
    }
    
    func showInvitation(index: Int) {
        
        trace.begin()
        
        if(invitations.count > 0) {
            lblInviteIndex.text = String(describing: index+1)
            lblInviteTotal.text = String(invitations.count)
        } else {
            lblInviteIndex.text = "-"
            lblInviteTotal.text = "-"
            txtInvitation.text = ""
        }
            
        // Launched via iOS share universalLink, but invitation no longer exist on server (too old)
        if (model.universalLinkLaunch && !universalLinkIsValid) {
            trace.error("Share invitation is no longer available")
            
            let alertController = UIAlertController(title: NSLocalizedString("Invalid invitation",
                                                                             comment: "Alert, title - Invitation is no longer valid"),
                                                    message: NSLocalizedString("The received invitation is no longer valid, it was probably to old?",
                                                                               comment: "Alert, message - Invitation is no longer valid"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - Invitation is no longer valid"), style: .default) { (action:UIAlertAction!) in
                
                self.model.universalLinkLaunch = false
                self.model.universalLinkData = ""
                self.showInvitation(index: index)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:  nil)
        }
        
        if(invitations.count > 0) {
            let invitation : Invitation = invitations[index]
            self.invitationId = invitation.invitationId!
            lblInvitationId.text = self.invitationId
            
            txtInvitation.text = NSLocalizedString("#1 has invited you to the group \"#2\"", comment: "Invitation from user #1 for group #2").replacingOccurrences(of: "#1", with: invitation.senderName!).replacingOccurrences(of: "#2", with: invitation.caregroupName!)
            
            // If the caregroup is no longer there, then display an error
            if (invitation.caregroupId == "") {
                trace.error("Missing caregroupId = group is deleted!")
                
                let alertController = UIAlertController(title: NSLocalizedString("Group deleted",
                                                                                 comment: "Alert, title - Caregroup no longer exist"),
                                                        message: NSLocalizedString("The invitation is for a group that no longer exists",
                                                                                   comment: "Alert, message - Caregroup no longer exist"),
                                                        preferredStyle: .alert)
                let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                      comment: "Alert, button 'Ok' - Caregroup no longer exist"), style: .default) { (action:UIAlertAction!) in
                    
                    self.trace.output("Group deleted - delete invitation")
                    
                    self.model.rejectInvitation(userId: self.invitations[index].invitationToUserId!,
                                                invitationId: self.invitations[index].invitationId!)
                    self.updateInvitations()
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion:  nil)
                
                trace.end()
                return
            }
            
        }
        else {
            trace.output("No more invitations")

            txtInvitation.text = NSLocalizedString("NO INVITATIONS", comment: "Message to user that there are no invitations")
            
            let alertController = UIAlertController(title: NSLocalizedString("No invitations",
                                                                             comment: "Alert, title - No invitations left"),
                                                    message: NSLocalizedString("There are no invitations left",
                                                                               comment: "Alert, message - No invitations left"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - No invitations left"), style: .default) { (action:UIAlertAction!) in
                
                self.trace.output("Close popup window")
                self.btnClose(self)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:  nil)
        }
        
        // Present next button ONLY if there is more than one invitation
        if (invitations.count > 1) {
            self.btnNext.isHidden = false
        } else {
            self.btnNext.isHidden = true
        }

        // Disable and grayout Accept/Reject buttons if there is no invitation
        if (invitations.count == 0) {
            self.btnAccept.isEnabled = false
            self.btnReject.isEnabled = false
            self.btnAccept.alpha = 0.5
            self.btnReject.alpha = 0.5

        } else {
            self.btnAccept.isEnabled = true
            self.btnReject.isEnabled = true
            self.btnAccept.alpha = 1.0
            self.btnReject.alpha = 1.0
        }
                
        trace.end()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func btnAccept(_ sender: Any) {
        
        model.acceptInvitation(userId: invitations[index].invitationToUserId!,
                               invitationId: invitations[index].invitationId!)
        updateInvitations()
        
        model.universalLinkLaunch = false
    }

    @IBAction func btnReject(_ sender: Any) {
        
        model.rejectInvitation(userId: invitations[index].invitationToUserId!,
                               invitationId: invitations[index].invitationId!)
        updateInvitations()
        
        model.universalLinkLaunch = false
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        let inviCount = invitations.count
    
        if (inviCount > 1) {
            btnNext.isEnabled = true
        } else {
            btnNext.isEnabled = false
            return
        }
        
        index+=1
        if index >= inviCount {
            index = 0
        }
        
        showInvitation(index: index)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        
        self.model.ignoreInvitation = true
        model.universalLinkLaunch = false
        
        if(isModal) {
            //self.performSegue(withIdentifier: "invitation2tabview", sender: self)
            self.dismiss(animated: true)
            //self.dismiss(animated: true, completion: nil)
        } else {
            // Returnerer tilbage til forrige view
            (UIApplication.shared.delegate as! AppDelegate).notificationHandler.pushNotification = nil
            self.navigationController!.popViewController(animated: true)
        }
        
    }
}

extension UIViewController {
    var isModal: Bool {
        if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else if navigationController?.presentingViewController?.presentedViewController == navigationController {
            return true
        } else if tabBarController?.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
    }
}
