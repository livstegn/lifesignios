//
//  LSNotificationHandler.swift
//  LifeSign
//
//  Created by Simon Jensen on 11/08/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

/*
import AWSCore
import AWSSNS
import AWSCognitoIdentityProvider
*/

import UserNotifications

protocol LSNotificationHandlerDelegate: class {
    func gotoViewController(userInfo: [AnyHashable : Any]?)
}

class LSNotificationHandler: NSObject, UNUserNotificationCenterDelegate {
    

    var model: Model? = nil     //Model is initilized when module has intiated
    
    var tokenCopy : String?
    var deviceTokenCopy : Data?
    var errorMessage : String?
    
    var registrationStatus = false
    var inviteAccept = false
    
    var universalLink: String?
    var pushNotification: String?
    
    var loggedIn = false
    var tokenReceived = false
    
    weak var delegate: LSNotificationHandlerDelegate?
    
    var invitationText : String? = "(no data)"
    var trackingText : String? = "(no data)"
    var activityText : String? = "(no data)"
    
    //var successCounter = 0
    
    
    let trace = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    
    override init() {
    
        trace.begin()
        
        super.init()
        
        registerForPushNotifications()
        
        trace.end()
    }

    // Request user to grant permissions for the app to use notifications
    func registerForPushNotifications() {
        
        trace.begin()
        
        if registrationStatus == true {
            return
        }
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            self.trace.output("Permission granted: \(granted)")
            // 1. Check if permission granted
            guard granted else {
                self.trace.output("RETURN")
                return
            }
            
            // 2. Attempt registration for remote notifications on the main thread - or you’ll receive a runtime warning.
            DispatchQueue.main.async {
                self.trace.output("REMOTE")
                UIApplication.shared.registerForRemoteNotifications()
                // Registration reply via delegates 1) didRegisterForRemoteNotificationsWithDeviceToken or 2) didFailToRegisterForRemoteNotificationsWithError
            }
        }
        trace.end()
    }

    
    func registerActionsAndNotificationTypes() {
        
        trace.begin()
        // Declaration Your Custom Actions and Notification Types
        
        // -------------------------------------------------------------------
        // Define custom actions types to be associated with .
        // -------------------------------------------------------------------
        
        let acceptAction = UNNotificationAction(identifier: "ACCEPT_ACTION",
                                                title: "Accept",
                                                options: UNNotificationActionOptions(rawValue: 0))
        let declineAction = UNNotificationAction(identifier: "DECLINE_ACTION",
                                                 title: "Decline",
                                                 options: UNNotificationActionOptions(rawValue: 0))

        // -------------------------------------------------------------------
        // Associate the custom actions to the notification type (you want to give actions)
        // and register the associated actions to the notificationCenter
        // -------------------------------------------------------------------

        // Associate the custom actions to the notification type
        let groupInviteCategory =
            UNNotificationCategory(identifier: "INVITATION",
                                   actions: [acceptAction, declineAction],
                                   intentIdentifiers: [],
                                   hiddenPreviewsBodyPlaceholder: "",
                                   options: .customDismissAction)
        
        // Register the actions for the notification type.
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.setNotificationCategories([groupInviteCategory])

        
        // Associate the custom actions to the notification type
        let lifesignReminderCategory =
            UNNotificationCategory(identifier: "COUNTDOWN",
                                   actions: [acceptAction],
                                   intentIdentifiers: [],
                                   hiddenPreviewsBodyPlaceholder: "",
                                   options: .customDismissAction)
        
        // Register the actions for the notification type.
        notificationCenter.setNotificationCategories([lifesignReminderCategory])
        trace.end()

        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler:
        @escaping () -> Void) {
        
        trace.begin()
        
        /*
         *  GOES HERE WHEN USER TAPPED THE DISPLAYED NOTIFICATION
         *  (Objective: Handle any user action associated with the displayed notification, e.g. goto the appropriate view)
         */
        
        /* Generally used to redirect the user to a particular screen of the app after user taps on the notification.
         */
        
        /* Asks the delegate to process the user's response to a delivered notification.
         *
         * Use this method to process the user's response to a notification.
         * If the user selected one of your app's custom actions, the response parameter contains the identifier for that action.
         * (The response can also indicate that the user dismissed the notification interface, or launched your app, without selecting a custom action.)
         * At the end of your implementation, call the completionHandler block to let the system know that you are done processing the user's response.
         * If you do not implement this method, your app never responds to custom actions.
         * You specify your app’s notification types at app launch using UNNotificationCategory objects,
         * and you specify the custom actions for each type using UNNotificationAction objects.
         */
        
        
        // App started by Notification
        // Is the app in foreground or background?
        let application = UIApplication.shared

        if(application.applicationState == .active){  // Foreground
            trace.output("Notification tapped while app is in foreground")
        }
        if(application.applicationState == .inactive)
        {
            trace.output("Notification tapped while app is in background")
        }
        
        let request = response.notification.request
        let userInfo = request.content.userInfo
        let category = request.content.categoryIdentifier
        
        switch category {
        
        case "INVITATION":
            trace.output("category = INVITATION")

            let inviteId = userInfo["invite_id"] as! String
    
            // Perform the task associated with the action.
            switch response.actionIdentifier {
                
            case "ACCEPT_ACTION":
                trace.output("ACCEPT_ACTION")
                (UIApplication.shared.delegate as! AppDelegate).notificationHandler.inviteAccept = true  // Debug
                if(model != nil) {
                    model!.acceptInvitation(userId: model!.user!.userId, invitationId: inviteId)
                }
            
            case "DECLINE_ACTION":
                trace.output("DECLINE_ACTION")
                (UIApplication.shared.delegate as! AppDelegate).notificationHandler.inviteAccept = false  // Debug
                if(model != nil) {
                    model!.rejectInvitation(userId: model!.user!.userId, invitationId: inviteId)
                }

            case UNNotificationDefaultActionIdentifier:
                trace.output("INVITATION:  case UNNotificationDefaultActionIdentifier")
                // The user just tapped the notification( to open the app)
                trace.output("invitationId = \(String(describing: pushNotification))")
                self.delegate!.gotoViewController(userInfo: userInfo)

            case UNNotificationDismissActionIdentifier:
                trace.output("INVITATION:  case UNNotificationDismissActionIdentifier")
               break
                
            default:
                trace.error("INVITATION:  case default")
            }
        
        case "MESSAGE":
            trace.output("category = MESSAGE")
    
            // Perform the task associated with the action.
            switch response.actionIdentifier {
                
            case UNNotificationDefaultActionIdentifier:
                trace.output("MESSAGE:  case UNNotificationDefaultActionIdentifier")
                // The user just tapped the notification( to open the app)
                trace.output("messageId = \(String(describing: pushNotification))")
                self.delegate!.gotoViewController(userInfo: userInfo)
                
            case UNNotificationDismissActionIdentifier:
                trace.output("MESSAGE:  case UNNotificationDismissActionIdentifier")
                break

            default:
                trace.error("MESSAGE:  case default")
                break
            }
            
        
        case "COUNTDOWN":
            trace.output("category = COUNTDOWN")
            
            // Perform the task associated with the action.
            switch response.actionIdentifier {
                    
            case "ACCEPT_ACTION":
                if(model != nil) {
                    DispatchQueue.global(qos: .background).async {
                        self.model!.giveLifesignNew(lifesignTime: Date())
                    }
                }
                
            case UNNotificationDefaultActionIdentifier:
                trace.output("COUNTDOWN:  case UNNotificationDefaultActionIdentifier")
                // The user just tapped the notification( to open the app)
                trace.output("countdownId = \(String(describing: pushNotification))")
                self.delegate!.gotoViewController(userInfo: userInfo)
                
            case UNNotificationDismissActionIdentifier:
                trace.output("COUNTDOWN:  case UNNotificationDismissActionIdentifier")
                break
                
            default:
                trace.error("COUNTDOWN:  case default")
            }
            
        default:
            trace.error("Switch CATEGORY:  case default")
        
        }

        // Always call the completion handler when done.
        completionHandler()
        
        trace.end()
    }
  
    
    // This function will be called when the app is in FOREGROUND and receive notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        /*
         *  CALLS HERE WHEN THE NOTIFICATION ARRIVE - BUT ONLY IF APP IS IN FOREGROUND
         *  (Objective: handle/update the active view if it is associated with the notification, e.g. receiving a message)
         *  Calls application(_didReceiveRemoteNotification _userInfo) to process notification data afterwards
         */
        
        /* Generally used to decide what to do when user is already inside the app and a notification arrives.
         * You could possibly trigger a remote notification inside the app.
         * After he taps on the remote notification, method 2 (didReceive response) gets called.
         */
        
        /* Asks the delegate how to handle a notification that arrived while the app was running in the foreground.
         *
         * If your app is in the foreground when a notification arrives, the shared user notification center calls this method to deliver
         * the notification directly to your app. If you implement this method, you can take whatever actions are necessary to process
         * the notification and update your app.
         * When you finish, call the completionHandler block and specify how you want the system to alert the user, if at all.
         */
        
        trace.begin()

        let notificationType = notification.request.content.categoryIdentifier
        let topViewController = UIApplication.topViewController()
        var rightViewIsPresented = false
        
        switch notificationType {
            
        case "INVITATION":
            trace.output("[INVITATION]")
            // Vi har modtaget en push notifikation med en invitation. Vis invitationen i invitationviewcontroller
            if(topViewController is InvitationViewController) {
                rightViewIsPresented = true
                break
            }
            
        case "MESSAGE":
            trace.output("[MESSAGE]")
            // Vi har modtaget en push notifikation med en besked. Vis beskeden i messageviewcontroller
            if(topViewController is MessagesViewController) {
                rightViewIsPresented = true
                break
            }
            
        case "FEEDBACK":
            // Vi har modtaget en push notifikation med feedback på et livstegn.
            trace.output("[FEEDBACK]")
            
        case "STATUSCHECK":
            // Vi har modtaget en push notifikation for check af app status (aktiv / inaktiv).
            trace.output("[STATUSCHECK]")
            
        case "DEADLINE":
            // Vi har modtaget en push notifikation med en deadline.
            trace.output("[DEADLINE]")
            
        case "COUNTDOWN":
            // Vi har modtaget en push notifikation med en countdown.
            trace.output("[COUNTDOWN]")

        case "TRACKING":
            // Vi har modtaget en push notifikation med en tracking.
            trace.output("[TRACKING]")

        default:
            trace.output("default")
            
        }

        if(rightViewIsPresented) {
            completionHandler([]) //keep it quiet
        } else {
            completionHandler([.alert, .sound, .badge]) //required to show notification when in foreground
        }
        
        trace.end()
    }
    
    func userIsSignedIn() {
        trace.begin()
        loggedIn = true
        if(model != nil) {
            model!.setupPushNotifications(loggedIn: loggedIn, tokenReceived: tokenReceived)
        }
        trace.end()
    }
    
    func deviceTokenReceived() {
        trace.begin()
        tokenReceived = true
        if(model != nil) {
            model!.setupPushNotifications(loggedIn: loggedIn, tokenReceived: tokenReceived)
        }
        trace.end()
    }
    
    func registrationFail() {
        trace.begin()
        registrationStatus = false
        trace.end()
    }
    
    func registrationSuccess(deviceToken: Data) {

        trace.begin()
        registrationStatus = true
        
        deviceTokenCopy = deviceToken
        trace.output("deviceTokenCopy = \(String(describing: deviceTokenCopy))")
        
        let token : String = deviceTokenCopy!.reduce("", {$0 + String(format: "%02x", $1)})
        tokenCopy = String(describing: token.lowercased())
        
        model = (UIApplication.shared.delegate as! AppDelegate).model
        model!.deviceLayer.saveDeviceToken(token: tokenCopy!)
        
        trace.output("tokenCopy = \(String(describing: tokenCopy))")
        trace.output("Device Token: \(token)")
        
        deviceTokenReceived()
        
        registerActionsAndNotificationTypes()
        
        trace.end()
    }
    
    func displayLocalNotification() {
        
        // Create content
        let content = UNMutableNotificationContent()
        
        content.title = "Dette er en test"
        content.subtitle = "Går notifikationen igennem?"
        content.body = "Hvis du kan læse denne besked, så er det fordi det er lykkede at vise en lokal notifikation!"
        content.badge = 1
        
        // Create Trigger  (5 sek is similar to 'now')
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let requestIdentifier = "localNotification"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: nil)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
    // -------------------
    
    private func displayAlert(message: String) {
        
        let alertController = UIAlertController(title: "Alert",
                                                message: message,
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion:nil)
        
        //self.present(alertController, animated: true, completion:  nil)
        
    }
    
}

extension AppDelegate {
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        registrationFail = true
        
        // Response to func registerForRemoteNotifications() if registration fails
        trace.begin()
        var string = ""
        string.append(error.localizedDescription)
        // nedenstående lavet af Bo, for at slippe for denne besked, der ALTID kommer i simulator...
        if string.contains("remote notifications are not supported in the simulator") {
            trace.output("remote notifications are not supported in the simulator")
        } else {
            trace.error(error.localizedDescription)
        }
        notificationHandler.errorMessage = error.localizedDescription
        notificationHandler.registrationFail()
        trace.end()
    }
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        registrationSuccess = true
        
        // Response to func registerForRemoteNotifications() if registration succeed
        trace.begin()
        notificationHandler.registrationSuccess(deviceToken: deviceToken)
        trace.end()
        
    }
    
  
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        /*
         *   GOES HERE TO PROCESS NOTIFICATION - REGARDLESS OF APP STATUS
         */
        
        /* Tells the delegate that a background notification has arrived.
         * Implement this method to process incoming background notifications.
         * The system launches your app or wakes it from the suspended state when the notification arrives,
         * and your app receives a brief amount of time to run in the background.
         */
        
        trace.begin()
        
        self.model.notificationData = userInfo
        
        let snsCategory = userInfo["category"] as? String ?? "NO CATEGORY"
        if let apsData = userInfo["aps"] as? [String: Any] {
        
            //let snsCategory = apsData["category"] as? String ?? nil
            
            self.model.notificationType = snsCategory
            
            switch(snsCategory) {
                
                case "MESSAGE":

                    var messageID : String? = nil
                    messageID = userInfo["message_id"] as? String ?? nil
                    
                    let listDataJSON = try! JSONSerialization.data(withJSONObject: apsData, options: .prettyPrinted)

                    if let jsonString = String(data: listDataJSON, encoding: .utf8) {
                        (UIApplication.shared.delegate as! AppDelegate).notificationHandler.invitationText = "MESSAGE:\n" + jsonString + "\n" + messageID!
                    }
                    
                    break;
            
                case "INVITATION":
                    
                    var snsInviteID : String? = nil
                    snsInviteID = userInfo["invite_id"] as? String ?? nil
                    
                    let listDataJSON = try! JSONSerialization.data(withJSONObject: apsData, options: .prettyPrinted)

                    if let jsonString = String(data: listDataJSON, encoding: .utf8) {
                        (UIApplication.shared.delegate as! AppDelegate).notificationHandler.invitationText = "INVITATION:\n" + jsonString + "\n" + snsInviteID!
                    }
                    break;
                    
                    
                case "STATUSCHECK":
                    
                    trace.user(model.user)      // Temporary check to verify model.user != nil
                    
                    switch(UIApplication.shared.applicationState) {

                        case .inactive:
                            _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Inactive)
                            (UIApplication.shared.delegate as! AppDelegate).notificationHandler.activityText = "STATUSCHECK:\nAppStatusType: .inactive"
                        case .active:
                            _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Active)
                            (UIApplication.shared.delegate as! AppDelegate).notificationHandler.activityText = "STATUSCHECK:\nAppStatusType: .active"
                        case .background:
                            _ = model.setAppStatus(userid: deviceLayer.getUserId(), status: AppStatusType.Background)
                            (UIApplication.shared.delegate as! AppDelegate).notificationHandler.activityText = "STATUSCHECK:\nAppStatusType: .background"
                        default:
                            (UIApplication.shared.delegate as! AppDelegate).notificationHandler.activityText = "STATUSCHECK:\nAppStatusType: default?"
                    }
                    
                    break;
                
                case "FEEDBACK":
                    
                    break;

                case "TRACKING":
                    
                    return

                    // Vi har modtaget en push notifikation med en tracking instruks. Få Model til at håndtere instruksen...
                    trace.output("[TRACKING]")
                    /*
                     {
                         "aps": {
                            "content-available": 1,
                            "category": "TRACKING"
                          },
                          "livstegn": {
                            "mode" : "START",
                            "tracking" : {
                                "supervisionid" : "S-e17f2ca0-a8b0-11ea-b91d-f1aaf3bbf6bd",
                                "trackingid" : "TR-33331784-F22E-480E-879B-95A33C2F85CE"
                            }
                          }
                      }
                    */
                    if let livstegnData = userInfo["livstegn"] as? [String: Any] {
                        if let mode = livstegnData["mode"] as? String {
                            switch mode {
                            case "START":
                                if let trackingData = livstegnData["tracking"] as? [String: Any] {
                                    let tracking = Tracking()
                                    tracking.trackingId = trackingData["trackingid"] as? String ?? ""

                                    model.activateTracking(tracking: tracking)

                                    let listData = userInfo["aps"] as? [String: Any]
                                    let listDataJSON = try! JSONSerialization.data(withJSONObject: listData, options: .prettyPrinted)
                                    let livstegnDataJSON = try! JSONSerialization.data(withJSONObject: livstegnData, options: .prettyPrinted)

                                    if let jsonString1 = String(data: listDataJSON, encoding: .utf8),
                                       let jsonString2 = String(data: livstegnDataJSON, encoding: .utf8) {
                                        (UIApplication.shared.delegate as! AppDelegate).notificationHandler.trackingText = "TRACKING: (Activate)\naps:\n" + jsonString1 + "\nLivstegn:\n" + jsonString2
                                    }
                                }
                            default:
                                (UIApplication.shared.delegate as! AppDelegate).notificationHandler.trackingText = "TRACKING: (Deactivate)"
                                model.deactiveTracking()
                            }
                        }
                    }
                    
                    break;
                    
                default:
                    break;
                
            }
            completionHandler(.newData)
            
        } else {
            
            (UIApplication.shared.delegate as! AppDelegate).notificationHandler.errorMessage = "Failing the if...:"
            completionHandler(.failed)
        }
        trace.end()

    }
}

extension UIApplication {
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        return viewController
    }
}




