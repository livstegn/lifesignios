//
//  MemberTableViewCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 05/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class MemberListViewCell: UITableViewCell {
    // MARK: Properties
    // force commit...
    
    @IBOutlet weak var lblMemberUserName: UILabel!
    @IBOutlet weak var lblMemberRole: UILabel!
    
    var groupMember: GroupMember?
    var memberUser: User?
}
