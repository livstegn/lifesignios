//
//  ViewController.swift
//  UITableView
//
//  Created by Simon Jensen on 27/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit
import MessageUI

class MemberListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var activityPopup: UIView!
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    var caregroup = Caregroup()
    var members = [GroupMember]()
    var deleteRowIndexPath: IndexPath? = nil
    
    var areMembersLoaded : Bool = false


    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        trace.begin()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.edit, target: self, action: #selector(self.btnEdit(sender:)))
        self.navigationItem.rightBarButtonItem?.isEnabled = true

        /*
        self.navigationItem.title = NSLocalizedString("Group members", comment: "Group member list: View title text")

        lblGroupName.font = Helper.weightedPreferredFont(font: .preferredFont(forTextStyle: .title3), weight: .semibold)
        btnInvite.setTitle(NSLocalizedString("Invite another member", comment: "Group member list: Invite member button text"), for: .normal)
        */
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsMultipleSelection = false

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        trace.begin()
   
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")

        // Update model to include new caregroup
        model.getCareGroups()
        
        self.caregroup = model.getCurrentCaregroup()!
        self.lblGroupName.text =  self.caregroup.caregroupName
        
        loadGroupMembers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        trace.begin()
        if self.isMovingFromParent {
            popToTabBarViewController()
        }
        trace.end()
    }
    
    func popToTabBarViewController() {
        if let viewControllers = self.navigationController?.viewControllers {
            for viewController in viewControllers {
                if viewController.isKind(of: TabBarViewController.self) {
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return;
                }
            }
        }
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        trace.begin()
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil) { (_) in
            // Updates to your UI...
            self.tableView.reloadData()
        }
        trace.end()
    }
    
    override func updateViewConstraints() {
        
    /*  Note! This Constraint update is very view specific!
        The purpose is to adapt the table height to the content,
        but also limit the height to assure that the button is
        always displayed */
        
        trace.begin()
        
        let safeAreaGuide = view.safeAreaLayoutGuide
        let safeAreaHeight = safeAreaGuide.layoutFrame.size.height
        let buttonHeight  = btnInvite.frame.height + btnInvite.contentEdgeInsets.top + btnInvite.contentEdgeInsets.bottom
        let tabBarHeight = self.tabBarController?.tabBar.bounds.height ?? 0
        let tableStart = tableView.frame.origin.y
        
        trace.output("safeAreaHeight = \(safeAreaGuide.layoutFrame.size.height)\nbuttonHeight  = \(btnInvite.frame.height + btnInvite.contentEdgeInsets.top + btnInvite.contentEdgeInsets.bottom)\ntabBarHeight = \(self.tabBarController?.tabBar.bounds.height ?? 0)\ntableStart = \(tableView.frame.origin.y)")

        if(areMembersLoaded) {
            var tableHeight = tableView.contentSize.height
            let tableHeightMax = safeAreaHeight - tableStart - tabBarHeight - buttonHeight
            if (tableHeight > tableHeightMax) {
                tableHeight = tableHeightMax
            }
            self.tableHeight.constant = tableHeight
        }
        super.updateViewConstraints()
        trace.end()
    }
    
    func loadGroupMembers() {
        trace.begin()
        areMembersLoaded = false
        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        DispatchQueue.global(qos: .background).async {
            self.model.getGroupMembers()
            DispatchQueue.main.async {
                self.membersLoaded()
            }
        }
        trace.end()
    }
    
    
    func membersLoaded() {
        trace.begin()
        self.areMembersLoaded = true
        self.spinner.stopAnimating()
        self.activityPopup.isHidden = true
        self.members = self.model.groupmembers!
        self.tableView.reloadData()
        trace.end()
    }
    
    
    // MARK: - Table view data source
    
    @objc func btnEdit(sender: UIBarButtonItem) {
        // Function body goes here
        self.performSegue(withIdentifier: "group2GroupDetailsSeague", sender: self)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            deleteRowIndexPath = indexPath
            let groupMemberToDelete = members[indexPath.row]
            let deletename = (model.getUser(userid: groupMemberToDelete.userid)?.userName)!
            confirmDelete(groupMember: deletename)
        }
    }
    
    func confirmDelete(groupMember: String) {
        let alert = UIAlertController(title: NSLocalizedString("Delete Groupmember", comment: "Group member list: Delete alert title"), message: NSLocalizedString("Are you sure you want to permanently delete #1# from this group?", comment: "Group member list: Delete alert message").replacingOccurrences(of: "#1#", with: groupMember), preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: NSLocalizedString("Delete", comment: "Group member list: Delete alert button"), style: .destructive, handler: handleDeleteRow)
        let CancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Group member list: Cancel alert button"), style: .cancel, handler: cancelDeleteRow)
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleDeleteRow(alertAction: UIAlertAction!) -> Void {
        if let indexPath = deleteRowIndexPath {
            
            trace.output("Remove user from CG")
            trace.output(members[(indexPath.row)].userid)
            trace.output(caregroup.caregroupId)
            trace.output(members[(indexPath.row)].groupmemberId)
            
            let rc = model.cloudLayer.removeGroupmember(userId: members[(indexPath.row)].userid, caregroupId: caregroup.caregroupId, memberId: members[(indexPath.row)].groupmemberId)
            trace.output(rc as Any)
            
            self.tableView.beginUpdates()
            members.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
            
            deleteRowIndexPath = nil
        }
    }
    
    func cancelDeleteRow(alertAction: UIAlertAction!) {
        deleteRowIndexPath = nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "group2MemberDetailsSeague"
        {
            //
            if let destinationVC = segue.destination as? EditUserDetailsViewController {
                destinationVC.segueName = segue.identifier ?? "group2MemberDetailsSeague"
            }
        }
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        trace.begin()

        trace.output("You tapped cell number \(indexPath.row).")
        
        self.model.currentMember = indexPath.row
        self.performSegue(withIdentifier: "group2MemberDetailsSeague", sender: self)
    }
    
    // number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        trace.begin()
        trace.output(self.members.count)
        return self.members.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        trace.begin()
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMember", for: indexPath) as! MemberListViewCell
        cell.groupMember = members[indexPath.row]
        cell.memberUser = model.getUser(userid: cell.groupMember!.userid)
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
       
        cell.lblMemberUserName.text = cell.memberUser?.userName

        if cell.groupMember?.status == GroupMemberStatusType.Invited {
            cell.lblMemberUserName?.textColor = UIColor(named: "lsTextFontWeak")
            cell.lblMemberRole.textColor = UIColor(named: "lsTextFontWeak")
            trace.output(cell.lblMemberUserName?.text! as Any)
        } else {
            cell.lblMemberUserName?.textColor = UIColor(named: "lsTextFontStandard")
            cell.lblMemberRole.textColor = UIColor(named: "lsTextFontStandard")
            trace.output(cell.lblMemberUserName?.text! as Any)
        }
        cell.lblMemberRole.text?.removeAll(keepingCapacity: false)
        
        // Set the height of the table to match the content
        updateViewConstraints()
        
        return cell
    }

}

