//
//  MessageViewCell.swift
//  LifeSign
//
//  Created by Bo Dalberg on 11/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit


class MessageViewCell: UITableViewCell {
   
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var lblMessage: UITextView!
    
    let txtSender = UILabel()
    let txtMessage = UILabel()
    let bubbleBackgroundview = UIView()
    
    let myMessagesColor = UIColor(red: 14.0/255, green: 122.0/255, blue: 254.0/255, alpha: 1)
    let incommingMessagesColor = UIColor(white: 0.9, alpha: 1)
    let systemGeneratedMessagesColor = UIColor(named: "lsBigButtonBackgroundMain")

    var leadingConstraint: NSLayoutConstraint!
    var trailingConstraint: NSLayoutConstraint!
    var isGenerated: Bool = false
    
    var isIncoming: Bool! {
        didSet {
            bubbleBackgroundview.backgroundColor = isIncoming ? incommingMessagesColor : myMessagesColor
            bubbleBackgroundview.backgroundColor = isGenerated ? systemGeneratedMessagesColor : bubbleBackgroundview.backgroundColor
            
            txtMessage.textColor = isIncoming ? .black : UIColor.white
            txtMessage.textColor = isGenerated ? .white : txtMessage.textColor
            
            //txtSender.isHidden = isIncoming ? false : true
            if isIncoming {
                leadingConstraint.isActive = true
                trailingConstraint.isActive = false
            } else {
                leadingConstraint.isActive = false
                trailingConstraint.isActive = true
            }
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        // sender name
        txtSender.translatesAutoresizingMaskIntoConstraints = false
        txtSender.font = UIFont(name: "Futura", size: 12)
        txtSender.textColor = UIColor.lightGray
        addSubview(txtSender)
        
        //bubble background
        bubbleBackgroundview.layer.cornerRadius = 15
        bubbleBackgroundview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bubbleBackgroundview)

        // message cells
        txtMessage.numberOfLines = 0
        txtMessage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(txtMessage)
        
        // constrains
        let constraints = [
            txtSender.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            txtMessage.topAnchor.constraint(equalTo: txtSender.topAnchor, constant: 28), // 16
            txtMessage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -32),
            txtMessage.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
            bubbleBackgroundview.topAnchor.constraint(equalTo: txtMessage.topAnchor, constant: -8),
            bubbleBackgroundview.leadingAnchor.constraint(equalTo: txtMessage.leadingAnchor, constant: -12),
            bubbleBackgroundview.bottomAnchor.constraint(equalTo: txtMessage.bottomAnchor, constant: 8),
            bubbleBackgroundview.trailingAnchor.constraint(equalTo: txtMessage.trailingAnchor, constant: 12),
            txtSender.leadingAnchor.constraint(equalTo: txtMessage.leadingAnchor, constant: -6),
            txtSender.trailingAnchor.constraint(equalTo: txtMessage.trailingAnchor, constant: 6)
        ]
                
        NSLayoutConstraint.activate(constraints)

        leadingConstraint = txtMessage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16)
        trailingConstraint = txtMessage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
