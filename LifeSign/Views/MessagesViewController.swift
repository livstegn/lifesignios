//
//  MessagesViewController.swift
//  LifeSign
//
//  Created by Bo Dalberg on 10/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu

class MessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityPopup: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    var groupedMessages = [Message]();
    var isShowingKeyboard = false

    var activeField: UITextField?
    
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    override func viewDidLoad() {
        super.viewDidLoad()
        
        trace.begin()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.register(MessageViewCell.self, forCellReuseIdentifier: "cellMessage")         // register cell name
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.allowsSelection = false

        txtMessage.delegate = self
        
        // If the message view is displayed due to an message notification, then set currentGroup to the group associated with the message(id)
        // TODO: nedenstående skal justeres ift. nu dropdown title løsning
        if(self.model.notificationLaunch && self.model.notificationType == "MESSAGE") {
            trace.output("NOTIFICATION")
            trace.output("notificationLaunch = \(self.model.notificationLaunch)")
            trace.output("notificationData = \(self.model.notificationData)")
            
            let messageId = self.model.notificationData!["message_id"] as! String
            trace.output("messageId = \(messageId)")
            let caregroupId = self.model.getMessageGroup(messageId: messageId)
            trace.output("caregroupId = \(caregroupId ?? "no caregroupid set")")
            self.model.getGroupMessages(_caregroupId: caregroupId!)
            //self.title = self.model.getCareGroup(caregroupId: caregroupId!).caregroupName
            self.model.notificationLaunch = false
            
        } else {
            if(self.model.getCurrentCaregroup() == nil) {
                self.model.setCurrentCaregroup(index: 0)
            }
            //self.title = self.model.getCurrentCaregroup()?.caregroupName
            self.model.getGroupMessages()
        }
           
        self.model.clearCachedUserNames()

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        
        let menuView = Helper.groupMenuView(navigationController: self.navigationController, model: self.model)
        self.tabBarController?.navigationItem.titleView = menuView

        initializeHideKeyboard()

        trace.end()
    }

    override func loadView() {
        super.loadView()
    }
    
    override func viewWillAppear(_ animated:Bool) {
            super.viewWillAppear(animated)

        self.reloadMenuItems()

        let menuView = self.tabBarController?.navigationItem.titleView as! BTNavigationDropdownMenu
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
            self.model.setCurrentCaregroup(index: indexPath)
            self.loadData()
        }
        
        self.showNavigationBar()
        self.loadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

    }
        
    override func viewDidAppear(_ animated: Bool) {

        self.showNavigationBar()
    }
    
    func showNavigationBar () {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
        
            self.navigationController?.setNavigationBarHidden(true, animated: false)

            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // MARK: - Table view data source
    
    func reloadMenuItems () {
        self.model.getCareGroups()
        var items = [String]()
        let NoOfCaregroups = model.caregroups?.count ?? 0
        for index in 0..<NoOfCaregroups {
            items.append(model.caregroups?[index].caregroupName ?? "NA")
        }
        let menuView = self.tabBarController?.navigationItem.titleView as! BTNavigationDropdownMenu
        menuView.updateItems(items)
    }
    
    func loadData () {

        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        DispatchQueue.global(qos: .background).async {
            self.model.getCareGroups()
            self.model.getGroupMessages(_caregroupId: self.model.getCurrentCaregroup()!.caregroupId)
            self.buildDateGroupedMessages()

            DispatchQueue.main.async {
                self.tableView.reloadData()
                if self.groupedMessages.count>0
                {
                    let indexPath = NSIndexPath(item: 0, section: (self.groupedMessages.count-1) )
                    self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: false)
                }
                self.spinner.stopAnimating()
                self.activityPopup.isHidden = true
            }
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if isShowingKeyboard{
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                let tabBarHeight = self.tabBarController?.tabBar.frame.height ?? 49.0
                self.view.frame.origin.y -= (keyboardSize.height - tabBarHeight)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if isShowingKeyboard{
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.view.frame.origin.y = 0
            }
        }
    }
   
    @objc func keyboardWillChangeFrame(_ notification: Notification) {
        if isShowingKeyboard{
            if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardHeight = keyboardFrame.cgRectValue.size.height
                // reset frame as ShowKeyboard will recalculate
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func initializeHideKeyboard(){
        //Declare a Tap Gesture Recognizer which will trigger our dismissMyKeyboard() function
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissMyKeyboard))
        
        //Add this tap gesture recognizer to the parent view
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissMyKeyboard(){
        //endEditing causes the view (or one of its embedded text fields) to resign the first responder status.
        //In short- Dismiss the active keyboard.
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        trace.begin()
        activeField = textField
        isShowingKeyboard = true
        trace.end()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        trace.begin()
        activeField = nil
        isShowingKeyboard = false
        trace.end()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        trace.begin()
        btnSend.sendActions(for: .touchUpInside)
//        textField.resignFirstResponder()
        trace.end()
        return true
    }
    
    fileprivate func buildDateGroupedMessages() {
        trace.begin()
        
        groupedMessages.removeAll(keepingCapacity: false)

        var sectionHeaderDatesDict = Dictionary<Date, [Message]> ()
//        sectionHeaderDatesDict.removeAll()
        sectionHeaderDatesDict = Dictionary(grouping: model.groupMessages!, by: { $0.createddate })

        let sortedKeysMessages = sectionHeaderDatesDict.keys.sorted()
        var lastvalue = Date()
        
        sortedKeysMessages.forEach { (key) in
            let values = sectionHeaderDatesDict[key]
            groupedMessages.append(values![0])
            lastvalue = values![0].createddate
        }
    }
    
    @IBAction func CreateNewMessage(_ sender: UIButton) {
        trace.begin()
        if txtMessage.text?.isEmpty == false {
            
            let caregroupID = model.getCurrentCaregroup()?.caregroupId ?? "unknown groupid"
            
            // Add a new message at cloudlayer
            let messageID = model.cloudLayer.addGroupMessage(userid: model.user?.userId ?? "", caregroupId: caregroupID, messagetext: txtMessage.text!)
            
            // Update model to include the message
            self.model.getGroupMessages()
            buildDateGroupedMessages()

            txtMessage.text = ""
            tableView.reloadData()
            
            // scroll to bottom
            let indexPath = NSIndexPath(item: 0, section: (groupedMessages.count-1) )
            self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
            
            txtMessage.resignFirstResponder()
        }
    }
    
    @IBAction func NewMessage(_ sender: Any) {
        trace.begin()
    }
    
    // number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return groupedMessages.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let title = self.tableView(tableView, titleForHeaderInSection: section)
        if (title == nil) {
            return 0.0
        }
        return 20.0
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let groupedMessagesinSection = groupedMessages.filter { $0.createddate == groupedMessages[section].createddate  }
        //print(groupedMessagesinSection.count)
        return groupedMessagesinSection.count
    }
    
    /*
    // Bo: following not implemented - and not going to be for now...
 
    // prepare for swipe actions
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        return nil
    }

    // prepare for swipe action - delete messages
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            //YOUR_CODE_HERE
        }
        deleteAction.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    */
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let dateString = Helper.simpleTimeMessengerStyle(date: groupedMessages[section].createddate)
        
        if section > 0 {
            let dateString1 = Helper.simpleTimeMessengerStyle(date: groupedMessages[section-1].createddate)

            if dateString1.uppercased() == dateString.uppercased() {
                return nil
            }
            else {
                return dateString
            }
        } else {
            return dateString
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Futura", size: 12)!
        header.textLabel?.textColor = UIColor.lightGray
        header.textLabel?.textAlignment = NSTextAlignment.center
        header.contentView.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMessage", for: indexPath) as! MessageViewCell
        
        // Configure the cell...
        let groupedMessagesinCurrentSection = groupedMessages.filter { $0.createddate == groupedMessages[indexPath.section].createddate  }

        cell.isGenerated = groupedMessagesinCurrentSection[indexPath.row].systemgenerated
        
        if (groupedMessagesinCurrentSection[indexPath.row].senderid != model.user?.userId)
        {
            cell.isIncoming = true
        } else {
            cell.isIncoming = false
        }
        
        if (groupedMessagesinCurrentSection[indexPath.row].systemgenerated == true) {
            cell.txtMessage.text = NSLocalizedString(groupedMessagesinCurrentSection[indexPath.row].messagetext, comment: "")
            cell.txtSender.text = "Your Lifesign app-team"
            //print(groupedMessagesinCurrentSection[indexPath.row].messagetext)
            //print(messagetext)
        } else {
            cell.txtMessage.text = groupedMessagesinCurrentSection[indexPath.row].messagetext
            cell.txtSender.text = model.getCachedUserName(userid: groupedMessagesinCurrentSection[indexPath.row].senderid)
        }
        
        return cell
    }
 
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
