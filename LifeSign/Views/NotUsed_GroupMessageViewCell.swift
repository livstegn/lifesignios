//
//  GroupMessageViewCell.swift
//  LifeSign
//
//  Created by Simon Jensen on 18/04/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import UIKit

class GroupMessageViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
