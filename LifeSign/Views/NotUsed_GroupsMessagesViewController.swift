//
//  GroupsMessagesViewController.swift
//  LifeSign
//
//  Created by Bo Dalberg on 10/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class GroupsMessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

 
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var activityPopup: UIView!
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let caregroup = Caregroup()
    var isCareGroupLoaded : Bool = false
    
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()

        trace.output("GroupsMessages entry")
        
//        navigationItem.title = "Groups"
//        navigationController?.navigationBar.prefersLargeTitles = true
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //self.lblTitle.font = Helper.weightedPreferredFont(font: self.lblTitle.font, weight: .semibold)
        //lblTitle.text = NSLocalizedString("Your groups", comment: "List of groups: Title")

        // Do any additional setup after loading the view.
        trace.output("No of groups: \(model.caregroups?.count ?? 0)")
                
 /*      for caregroup in model.caregroups! {
            trace.output(caregroup.caregroupName)
        }*/
        
        //NotificationBanner.show("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", style: .error )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /*
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "lsViewBackground")
        self.navigationController?.navigationBar.tintColor = UIColor.darkGray
        self.navigationController?.navigationBar.titleTextAttributes  = [NSAttributedString.Key.foregroundColor : UIColor.darkGray]
         */
        
        isCareGroupLoaded = false

        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        DispatchQueue.global(qos: .background).async {
            self.model.getCareGroups()
            DispatchQueue.main.async {
                self.careGroupsLoaded()
            }
        }
        
        // deselect rows
        tableView.indexPathsForSelectedRows?.forEach({ tableView.deselectRow(at: $0, animated: false) })
    }
    
    func careGroupsLoaded() {
        trace.begin()
        self.isCareGroupLoaded = true
        self.spinner.stopAnimating()
        self.activityPopup.isHidden = true
        self.tableView.reloadData()
    }
    
    // number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(isCareGroupLoaded) {
            return model.caregroups?.count ?? 0
        }
        else {
            return 0
        }
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMessage", for: indexPath) as! GroupMessageViewCell
        cell.lblName.text = model.caregroups![indexPath.row].caregroupName
        //cell.textLabel?.font = UIFont.systemFont(ofSize: 20.0)
        //cell.textLabel?.textColor = UIColor.white
        //cell.textLabel?.textColor = UIColor.init(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator

        return cell
    }

    // method to run when table view cell is tapped
    //override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        trace.output("You tapped cell number \(indexPath.row).")
        
        //self.model.currentGroup = indexPath.row
        //self.performSegue(withIdentifier: "Groups2MessagesSeague", sender: self)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
