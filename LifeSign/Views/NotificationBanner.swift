//
//  NotificationBanner.swift
//  TopNotificationRecipe
//
//  Created by Dushyant Bansal on 11/02/18.
//  Copyright © 2018 db42.in. All rights reserved.
//

import Foundation
import UIKit

public enum BannerStyle: Int {
    case error
    case info
    case success
    case warning
}

struct Queueelement {
    var style: BannerStyle
    var text: String
}

class QueueSingletonClass: NSObject {
    static let sharedInstance = QueueSingletonClass()

    private var bannerQueue = [Queueelement]()
    
    private override init() {
        print("Singleton init")
    }

    func addBanner(_ text: String, _ style: BannerStyle) {
        bannerQueue.append(Queueelement.init(style: style, text: text))
        //print("addBanner", bannerQueue.count, text)
    }
    
    func removeBanner() -> Queueelement {
        /*let element: Queueelement = bannerQueue.first!
        print("removeBanner", bannerQueue.count, element)*/
        return bannerQueue.remove(at: 0)
    }
    
    func count() -> Int {
        return bannerQueue.count
    }

}


class NotificationBanner: UIViewController {
    
    //var NBviewController: UIViewController!
    //weak var delegate: NotificationBannerDelegate?
        
    static var labelTopMargin = CGFloat(24)
    static let labelLeftMarging = CGFloat(16)
    static let animateDuration = 0.5
    static let bannerAppearanceDuration: TimeInterval = 3
    static var bannerView: UIView!
    static var superView: UIView!
    static var bannerTopConstraint: NSLayoutConstraint!
    static var isActiveBanner = false
    static var isFirstRun = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationBannerQueue = QueueSingletonClass.sharedInstance
      
    }

    
    static func show(_ text: String, style: BannerStyle, actionDelegate: UIViewController? = nil) {
        QueueSingletonClass.sharedInstance.addBanner(text, style)
        if(isActiveBanner == false) {
            isActiveBanner = true
            
            if(isFirstRun == true) {
                // delay to fix renderingissue caused by too early rendering
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    showFromQueue()
                    isFirstRun = false
                })
            }
            else {
                showFromQueue()
            }
        }
    }
    
    static func showFromQueue() {

        let element = QueueSingletonClass.sharedInstance.removeBanner()

        // Notch check
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        if (bottom > 0 && isFirstRun==true) {
            // has notch
            NotificationBanner.labelTopMargin = NotificationBanner.labelTopMargin + 20
        }
        superView = UIApplication.shared.keyWindow!.rootViewController!.view!
        
        let height = CGFloat(64)
        let width = superView.bounds.size.width
        
        bannerView = UIView(frame: CGRect(x: 0, y: 0-height, width: width, height: height))
        bannerView.layer.opacity = 1
        bannerView.translatesAutoresizingMaskIntoConstraints = false

        let label = UILabel(frame: CGRect.zero)
        label.textColor = UIColor.white
        label.numberOfLines = 0
        label.text = element.text
        label.translatesAutoresizingMaskIntoConstraints = false
        
        switch element.style {
        case .info:
            bannerView.backgroundColor = UIColor(named: "lsMessageBackgroundInfo")
        case .success:
            bannerView.backgroundColor = UIColor(named: "lsMessageBackgroundSuccess")
            label.textColor = UIColor.black
        case .warning:
            bannerView.backgroundColor = UIColor(named: "lsMessageBackgroundWarning")
        default: //error
            bannerView.backgroundColor = UIColor(named: "lsMessageBackgroundError")
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureRecognizer (_:) ))
        
        bannerView.addGestureRecognizer(tapGestureRecognizer)
        bannerView.addSubview(label)
        superView.addSubview(bannerView)

        let labelCenterYContstraint = NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: bannerView, attribute: .centerY, multiplier: 1, constant: 0)
        let labelCenterXConstraint = NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: bannerView, attribute: .centerX, multiplier: 1, constant: 0)
        let labelWidthConstraint = NSLayoutConstraint(item: label, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width - NotificationBanner.labelLeftMarging*2)
        let labelTopConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: bannerView, attribute: .top, multiplier: 1, constant: NotificationBanner.labelTopMargin)
        
        let bannerWidthConstraint = NSLayoutConstraint(item: bannerView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width)
        let bannerCenterXConstraint = NSLayoutConstraint(item: bannerView, attribute: .leading, relatedBy: .equal, toItem: superView, attribute: .leading, multiplier: 1, constant: 0)
        bannerTopConstraint = NSLayoutConstraint(item: bannerView, attribute: .top, relatedBy: .equal, toItem: superView, attribute: .top, multiplier: 1, constant: 0-height)
        
        NSLayoutConstraint.activate([labelCenterYContstraint, labelCenterXConstraint, labelWidthConstraint, labelTopConstraint, bannerWidthConstraint, bannerCenterXConstraint, bannerTopConstraint])
        
        UIView.animate(withDuration: NotificationBanner.animateDuration) {
            bannerTopConstraint.constant = 0
            superView.layoutIfNeeded()
        }
        /*
        superView.setNeedsDisplay()
        superView.bringSubviewToFront(bannerView)
        bannerView.setNeedsDisplay()
        */
        
        if(element.style == .error)
        {
            // will be handled by gesturerecognizer
        }
        else
        {
            //remove subview after time
            dismissView(hidedelay: NotificationBanner.bannerAppearanceDuration)
        }
        
    }
    
    @objc static func onTapGestureRecognizer(_ sender:UITapGestureRecognizer) {
        //print("Tap")
        dismissView(hidedelay: 0)
    }
    
    static func dismissView(hidedelay:Double? = 0) {
        UIView.animate(withDuration: NotificationBanner.animateDuration, delay: hidedelay ?? 0, options: [], animations: {
            bannerTopConstraint.constant = 0 - bannerView.frame.height
            superView.layoutIfNeeded()

            }, completion: { finished in
                
                if finished {
                    bannerView.removeFromSuperview()
                    isActiveBanner = false
                    
                    if(QueueSingletonClass.sharedInstance.count() > 0 ) {
                        showFromQueue()
                    }
                }
            }
        )
    }
    
}


