//
//  ReloaderDelegate.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 27/03/2021.
//  Copyright © 2021 Peter Rosendahl. All rights reserved.
//

import Foundation

protocol ReloaderDelegate {
    func reload()
}
