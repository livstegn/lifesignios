//
//  SafeSpotViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 07/07/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import UIKit
import MapKit

class SafeSpotViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet private var mapView: MKMapView!
    @IBOutlet weak var tfSelectedSafeSpot: UITextField!
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    var selectedLocation : CLLocationCoordinate2D?
    var annotation : MKPointAnnotation?
    
    // Segue variable
    var initialLocation : CLLocation?
    var supervisionEditTableViewController : SupervisionEditTableViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false

        let rightItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(useAddress))
        rightItem.title = NSLocalizedString("Use", comment: "Safe spot selektor for supervision: Use-button")
        navigationItem.rightBarButtonItem = rightItem
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "lsHeadlineText")

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))

        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Set initial location in Olesvej, Virum, Denmark
        self.model.checkForLocationAutorization()

        mapView.centerToLocation(initialLocation!)
        self.annotation = MKPointAnnotation()
        annotation!.coordinate = initialLocation!.coordinate
        mapView.addAnnotation(annotation!)

        self.model.getAddress(location: initialLocation!.coordinate) { address, error in
            guard address != nil && error == nil else {
                print("error = \(error)")
                return
            }

            self.tfSelectedSafeSpot.text = address!
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {

        let location = sender.location(in: mapView)
        selectedLocation = mapView.convert(location, toCoordinateFrom: mapView)

        // Add annotation:
        mapView.removeAnnotations(mapView.annotations)
        self.annotation = MKPointAnnotation()
        annotation!.coordinate = selectedLocation!
        mapView.addAnnotation(annotation!)
        
        self.model.getAddress(location: selectedLocation!) { address, error in
            guard address != nil && error == nil else {
                print("error = \(error)")
                return
            }

            self.tfSelectedSafeSpot.text = address!
        }
    }

    @objc func useAddress() {
        self.supervisionEditTableViewController!.supervision.locationLatitude = selectedLocation?.latitude
        self.supervisionEditTableViewController!.supervision.locationLongitude = selectedLocation?.longitude
        self.supervisionEditTableViewController!.lblSafeSpotAddress.text = self.tfSelectedSafeSpot.text

        self.navigationController?.popViewController(animated: true)
    }
}

private extension MKMapView {
  func centerToLocation(_ location: CLLocation, regionRadius: CLLocationDistance = 1000) {
    let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}
