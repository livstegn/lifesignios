//
//  ShareListViewCell.swift
//  LifeSign
//
//  Created by Simon Jensen on 24/02/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import UIKit

class ShareListViewCell: UITableViewCell {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var imgAlreadyMember: UIImageView!
    
    /*
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 */

}
