//
//  ShareListViewController.swift
//  LifeSign
//
//  Created by Simon Jensen on 24/02/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import UIKit

class ShareListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIActivityItemSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnInvite: UIButton!

    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var activityPopup: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    let inviteSubject = "Lifesign✓"
    let inviteImage = UIImage(named: "LifesignAd")
    let exchangeMessage = "Exchange heartbeats with your loved ones"
    let appstoreMessage = "Join us in the Lifesign app and start exchanging heartbeats"
    var htmlMessage = "<html><body>Empty</body></html>"
    
    let inviteURL = LifesignDeepLink
    var myWebsite: URL = URL(string: "www.google.com")!
    
    var contacts = [LSContact]()
    var myGroupMembers = [GroupMember]()
    var thisGroupMembers = [GroupMember]()
    var selected = 0
    
    var areMembersFiltered : Bool = false
    
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        UITableViewCell.appearance().tintColor = UIColor(named: "lsBigButtonBackgroundMain")
        
        // Configure buttons
        btnInvite.setTitle(NSLocalizedString("Add new contacts", comment: "Invite button text: invite new people from contacts"), for: .normal)
        btnInvite.setTitle(NSLocalizedString("Add new contacts", comment: "Invite button text: invite new people from contacts"), for: .disabled)
        
        btnInvite.backgroundColor = UIColor(named: "lsBigButtonBackgroundOther")
        btnInvite.isEnabled = false
        
        loadKnownMembers()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        trace.begin()
        trace.end()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        trace.begin()
        if self.isMovingFromParent {
            popToTabBarViewController()
        }
        trace.end()
    }
    
    func popToTabBarViewController() {
        if let viewControllers = self.navigationController?.viewControllers {
            for viewController in viewControllers {
                if viewController.isKind(of: TabBarViewController.self) {
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return;
                }
            }
        }
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        trace.begin()
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil) { (_) in
            // Updates to your UI...
            self.tableView.reloadData()
        }
        trace.end()
    }
    
    override func updateViewConstraints() {
        
    /*  Note! This Constraint update is very view specific!
        The purpose is to adapt the table height to the content,
        but also limit the height to assure that the button is
        always displayed */
        
        trace.begin()
        
        let safeAreaGuide = view.safeAreaLayoutGuide
        let safeAreaHeight = safeAreaGuide.layoutFrame.size.height
        let buttonHeight  = btnInvite.frame.height + btnInvite.contentEdgeInsets.top + btnInvite.contentEdgeInsets.bottom
        let tabBarHeight = self.tabBarController?.tabBar.bounds.height ?? 0
        let tableStart = tableView.frame.origin.y

        if(areMembersFiltered) {
            var tableHeight = tableView.contentSize.height
            let tableHeightMax = safeAreaHeight - tableStart - tabBarHeight - buttonHeight
            if (tableHeight > tableHeightMax) {
                tableHeight = tableHeightMax
            }
            self.tableHeight.constant = tableHeight
        }
        super.updateViewConstraints()
        trace.end()
    }
    
    func filterCurrentMembers() {
        
        trace.begin()
        
        model.getCareGroups()
        
        var allMembers = [GroupMember]()
        for group in model.caregroups! {
            // Members in all groups
            allMembers.append(contentsOf: model.getGroupMembers(caregroupId: group.caregroupId))
            // Members in this group
            if(group.caregroupId == model.getCurrentCaregroup()!.caregroupId) {
                thisGroupMembers = model.getGroupMembers(caregroupId: group.caregroupId)
            }
        }
        
        // Remove duplicates
        myGroupMembers = Array(Set(allMembers))
        
        // Delete myself
        myGroupMembers.removeAll(where: { $0.userid == model.user?.userId })
        thisGroupMembers.removeAll(where: { $0.userid == model.user?.userId })
        
        // Delete all users that are not .Active
        myGroupMembers.removeAll(where: { $0.status != GroupMemberStatusType.Active })
        thisGroupMembers.removeAll(where: { $0.status != GroupMemberStatusType.Active })
        
        // Build a list of contacts to display
        var tempContacts = [LSContact]()
        for member in myGroupMembers {

            let contact = LSContact()
            let user = model.getUser(userid: member.userid)
            if(user != nil) {
                contact.user = user
                contact.name = user?.userName
                contact.email = user?.email
            } else {
                break   // Fejlsituation: User tilknyttet gruppe findes ikke - tilføjes ikke
            }
            
            // Indicate contacts who are already member of the group
            if thisGroupMembers.contains(where: {$0.userid == member.userid}) {
                contact.isMember = true
                //break  // ...alternatively delete contacts who are already member of the group (by not appending contact below))
            }
            
            // Add to list if it is not already there...
            if (!contact.isMember && !tempContacts.contains(where: {$0.user?.userId == contact.user?.userId})){
                tempContacts.append(contact)
            }
        }
        
        // Sort the contact list by 'name'
        if(tempContacts.count>0) {
            contacts = tempContacts.sorted(by: { $0.name!.lowercased() < $1.name!.lowercased() })
        }
        
        trace.end()
    }
    
    func loadKnownMembers() {
        trace.begin()
        areMembersFiltered = false
        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        DispatchQueue.global(qos: .background).async {
            self.filterCurrentMembers()
            DispatchQueue.main.async {
                self.membersFiltered()
            }
        }
        trace.end()
    }
    
    
    func membersFiltered() {
        trace.begin()
        self.areMembersFiltered = true
        self.spinner.stopAnimating()
        self.activityPopup.isHidden = true
        btnInvite.backgroundColor = UIColor(named: "lsBigButtonBackgroundMain")
        btnInvite.isEnabled = true
        self.tableView.reloadData()

        trace.end()
    }
    
    // number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellContact", for: indexPath) as! ShareListViewCell
        
        cell.lblUserName.text = contacts[indexPath.row].name
        cell.lblUserEmail.text = contacts[indexPath.row].email
        
        cell.imgAlreadyMember.isHidden = !contacts[indexPath.row].isMember
        
        if (contacts[indexPath.row].selected) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        //cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        updateViewConstraints()
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        trace.begin()
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        tableView.beginUpdates()
        
        let row = indexPath.row
        
        // Toggle the 'selected' flag
        contacts[row].selected = !contacts[row].selected
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if (contacts[row].selected) {
                cell.accessoryType = .checkmark
                selected += 1
            } else {
                cell.accessoryType = .none
                selected -= 1
            }
        }
        
        tableView.endUpdates()
        
        if (selected > 0) {
            btnInvite.setTitle(NSLocalizedString("Add people from network", comment: "Invite button text: invite known people from groups"), for: .normal)
        } else {
            btnInvite.setTitle(NSLocalizedString("Add new contacts", comment: "Invite button text: invite new people from contacts"), for: .normal)
        }
        
        //selectedCellIndex = tableView.indexPathForSelectedRow!
    }
    
    @IBAction func invitePressed(_ sender: Any) {
        
        trace.begin()
        
        if(selected > 0) {  // Invite new member from existing grops
            
            let currentGroup : Caregroup = self.model.getCurrentCaregroup()!
            let groupName : String = currentGroup.caregroupName
            
            // Get list of selected users in
            for contact in contacts {
                
                if contact.selected {
                    trace.output("SELECTED!!")
                    
                    var groupmemberId : String = ""
                    var invitationId : String = ""
                    
                    trace.output("Email: \(String(describing: contact.email))")
                    groupmemberId = model.addGroupmember(userId: (contact.user?.userId)!, status: GroupMemberStatusType.Invited, memberId: groupmemberId)!
                    invitationId = model.addInvitation(fromUserId: (model.user?.userId)!, toUserId: (contact.user?.userId)!, sameInvitation: invitationId)!
                    
                    _ = model.pushInvitation(userId: (contact.user?.userId)!, fromUser: (model.user?.userName)!, toGroup: groupName, invitationId: invitationId)
             
                }
            }
            NotificationBanner.show("Invitation sent...", style: .success )
            self.popToTabBarViewController()
        
        } else {  // Invite new member from contacts
            
            let invitationId = model.addInvitation(fromUserId: (model.user?.userId)!)!
            trace.output("invitation ID: \(String(describing: invitationId))")
            let stringURL = inviteURL+invitationId
            myWebsite = URL(string: stringURL)!
            
            htmlMessage = "<html><body>Join us in the Lifesign app and start exchanging heartbeats<br><a href=\"\(myWebsite)\"><img src=\"https://www.livewebsite.dk/wp-content/uploads/2020/03/LifesignIconSmall.png\" width=\"256\" height=\"256\"></a><br><a href=\"\(myWebsite)\">https://www.livewebsite.dk/</a></body></html>"
            
            let activityVC = UIActivityViewController(activityItems: [self], applicationActivities: nil)
            
            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.assignToContact,
                                                UIActivity.ActivityType.addToReadingList,
                                                UIActivity.ActivityType.copyToPasteboard,
                                                UIActivity.ActivityType.markupAsPDF,
                                                UIActivity.ActivityType.openInIBooks,
                                                UIActivity.ActivityType.postToFlickr,
                                                UIActivity.ActivityType.postToTencentWeibo,
                                                UIActivity.ActivityType.postToVimeo,
                                                UIActivity.ActivityType.postToWeibo,
                                                UIActivity.ActivityType.print,
                                                UIActivity.ActivityType.saveToCameraRoll,
                                                UIActivity.ActivityType(rawValue:"com.apple.mobilenotes.SharingExtension"),
                                                UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension")]
            
            let presentingVC = topViewController()
            presentingVC!.present(activityVC, animated: true, completion: nil)
            
            //Completion handler
            activityVC.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed:
                Bool, arrayReturnedItems: [Any]?, error: Error?) in
                if completed {
                    self.trace.output("share completed")
                    NotificationBanner.show("Invitation sent...", style: .success )
                    self.popToTabBarViewController()
                    return
                } else {
                    self.trace.output("cancel")
                }
                if let shareError = error {
                    self.trace.error("error while sharing: \(shareError.localizedDescription)")
                }
            }
        }
    
        trace.end()
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        
        trace.begin()
        return self.inviteSubject
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        
        trace.begin()
        return self.inviteSubject
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        
        trace.begin()
        if activityType == UIActivity.ActivityType.message {
            return self.myWebsite
        } else if activityType == UIActivity.ActivityType.mail {
            return "\(htmlMessage)"
        } else if activityType == UIActivity.ActivityType.postToTwitter {
            return "\(inviteSubject)\n\(exchangeMessage)\n\(myWebsite)"
        } else if activityType == UIActivity.ActivityType.postToFacebook {
            return "\(inviteSubject)\n\(exchangeMessage)\n\(myWebsite)"
        }
        
        return "\(inviteSubject)\n\(exchangeMessage)\n\(myWebsite)"
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, thumbnailImageForActivityType activityType: UIActivity.ActivityType?) -> UIImage? {
        
        trace.begin()
        if activityType == UIActivity.ActivityType.message {
            return nil
        } else {
            return UIImage(named: "LifesignAd")
        }
    }
        
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func topViewController() -> UIViewController? {
        
        var topViewController = self.view.window?.rootViewController
        
        while (topViewController?.presentedViewController != nil) {
            topViewController = topViewController?.presentedViewController
        }
        
        return topViewController
    }

}
