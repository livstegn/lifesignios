//
//  SignInViewController.swift
//  AwsTest
//
//  Created by Simon Jensen on 25/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit
import LocalAuthentication

class SignInViewController: UIViewController {
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var eyePassword: UIButton!
    
    var isVisible: Bool?
    
    var usernameText: String?
    var emailFromCreation: String?

    override func viewDidLoad() {
        
        trace.begin()
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.signInViewController = self
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.password.leftView = paddingView
    
        isVisible = model.deviceLayer.getPasswordVisible() ?? false
        setVisibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.

        self.email.textContentType = .emailAddress
        self.password.textContentType = .password
        
        self.email.text = model.getEmail()
        self.password.text = nil
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    @IBAction func eyePasswordPressed(_ sender: Any) {
        toggleVisibility()
    }
    
    func setVisibility(){
        
        password.isSecureTextEntry = !isVisible!
        
        if(isVisible!) {
            eyePassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
        } else {
            eyePassword.setImage(UIImage(systemName: "eye"), for: .normal)
        }
    }
    
    func toggleVisibility() {

        isVisible! = !isVisible!
        model.deviceLayer.savePasswordVisible(status: isVisible!)
        setVisibility()
    }
    
    @IBAction func signinPressed(_ sender: Any) {

        trace.begin()

        // First check to see if the user has enterer email and password...
        if (self.email.text == nil || self.password.text == nil) {
            trace.error("Missing email or password: \(self.email.text!) or \(self.password.text!)")
            let alertController = UIAlertController(title: NSLocalizedString("Missing information",
                                                                             comment: "Alert: Title - user did not enter email or password"),
                                                    message: NSLocalizedString("Please enter a valid email and password",
                                                                               comment: "Alert: Asks user to enter email or password"),
                                                    preferredStyle: .alert)
            let retryAction = UIAlertAction(title: NSLocalizedString("Retry",
                                                                     comment: "Alert, button 'Retry' - user did not enter email or password"), style: .default, handler: nil)
            alertController.addAction(retryAction)
            self.present(alertController, animated: true, completion:  nil)
            
            trace.end()
            return
        }
        
        // Then try to signin to Cognito with the provided email/password set
        let signinError = model.signinPressed(email: self.email.text!, password: self.password.text!)
        
        // If user is unable to signin to Cognito - then show the error message from Cognito...
        if (signinError != nil) {
            trace.error("Unable to login to Cognito with email: \(self.email.text!) and password: \(self.password.text!)")
            let alertController = UIAlertController(title: signinError!.userInfo["__type"] as? String,
                                                    message: signinError!.userInfo["message"] as? String,
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - system error in signin"), style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:  nil)
            
            // Reset the email/password
            self.password.text = nil
            self.email.text = ""
            
            trace.end()
            return
        }
            
        // User is able to signin to Cognito - save the email and password in Keychain for later reference
        model.deviceLayer.saveEmail(email: self.email.text!)
        model.deviceLayer.savePassword(password: self.password.text!)
        model.deviceLayer.saveAuthMethod(method: AuthenticationMethod.User)
        
        // First check to see if this user exist in Dynamo - otherwise create a new user
        if(!self.model.checkEmail(email: self.email.text!)) {
            trace.output("Login by a new user...Create a profile in Dynamo")
            self.model.createNewUser(email: self.email.text!)
            self.model.user?.status = UserStatusType.Active
            self.model.updateUserDetails()
        }
        
        // By now it should be safe to load userprofile from Dynamo
        trace.output("Restore userprofile from Dynamo")
        self.model.restoreCurrentUser()
        
        // Check to see if there are any invitations?
        if(model.haveInvitations()) {
            trace.output("INVITATIONER EFTER LOGIN")
        } else {
            trace.output("INGEN INVITATIONER")
        }
        
        self.performSegue(withIdentifier: "justsignedinSeague", sender: self)
    
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

