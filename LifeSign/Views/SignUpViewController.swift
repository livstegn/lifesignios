//
//  SignUpViewController.swift
//  AwsTest
//
//  Created by Simon Jensen on 25/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!  // This is the actual name of the user
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    
    @IBOutlet weak var eyePassword: UIButton!
    @IBOutlet weak var eyeRepeatPassword: UIButton!
    
    var isVisible: Bool?

    let model = (UIApplication.shared.delegate as! AppDelegate).model
    
    var emailToConfirm: String?
    var userToConfirm: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        isVisible = model.deviceLayer.getPasswordVisible() ?? false
        setVisibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        let email = self.model.getEmail()
        if(email != nil) {
            self.email.text = email
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let confirmationViewController = segue.destination as? ConfirmSignUpViewController {
            confirmationViewController.email = self.emailToConfirm
            //confirmationViewController.user = self.userToConfirm
        }
    }
    
    @IBAction func eyePassword(_ sender: Any) {
        toggleVisibility()
    }
    
    @IBAction func eyeRepeatPassword(_ sender: Any) {
        toggleVisibility()
    }
    
    func setVisibility(){
        
        password.isSecureTextEntry = !isVisible!
        repeatPassword.isSecureTextEntry = !isVisible!
        
        if(isVisible!) {
            eyePassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            eyeRepeatPassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
        } else {
            eyePassword.setImage(UIImage(systemName: "eye"), for: .normal)
            eyeRepeatPassword.setImage(UIImage(systemName: "eye"), for: .normal)
        }
    }
    
    func toggleVisibility() {

        isVisible! = !isVisible!
        model.deviceLayer.savePasswordVisible(status: isVisible!)
        setVisibility()
    }
    
    @IBAction func signupPressed(_ sender: Any) {
        
        if(self.password.text != self.repeatPassword.text)
        {

            self.password.text = ""
            self.repeatPassword.text = ""
            
            let alertController = UIAlertController(title: NSLocalizedString("Password Mismatch",
                                                                             comment: "Alert, title - repeat password is different from password"),
                                                    message: NSLocalizedString("Enter the same password in both fields.",
                                                                               comment: "Alert, message - repeat password is different from password"),
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                  comment: "Alert, button 'Ok' - user entered two different passwords"), style: .default, handler: { [self] _ in
                                                                    password.becomeFirstResponder()
                                                                })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:  nil)

            return
            
        }
        
        guard let userNameValue = self.username.text, !userNameValue.isEmpty,
            let emailValue = self.email.text, !emailValue.isEmpty,
            let passwordValue = self.password.text, !passwordValue.isEmpty else {

                let alertController = UIAlertController(title: NSLocalizedString("Missing Required Fields",
                                                                                 comment: "Alert, title - user did not enter required info"),
                                                        message: NSLocalizedString("Username / Email / Password are required for registration.",
                                                                                   comment: "Alert, message - user did not enter required info"),
                                                        preferredStyle: .alert)
                let okAction = UIAlertAction(title: NSLocalizedString("Ok",
                                                                      comment: "Alert, button 'Ok' - user did not enter password"), style: .default, handler: nil)
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion:  nil)
                return
        }
        
        let result : SignupResult = model.signupPressed(userName: userNameValue,
                                                        email: emailValue,
                                                        password: passwordValue)

        if result.error != nil {
            let alertController = UIAlertController(title: result.error!.userInfo["__type"] as? String,
                                                    message: result.error!.userInfo["message"] as? String,
                                                    preferredStyle: .alert)
            let retryAction = UIAlertAction(title: NSLocalizedString("Retry",
                                                                  comment: "Alert, button 'Retry' - system error in sign up"),
                                                                  style: .default,
                                                                  handler: nil)
            alertController.addAction(retryAction)
            
            self.present(alertController, animated: true, completion:  nil)
        } else {
            // handle the case where user has to confirm his identity via email / SMS
            if (result.status != NewUserStatusType.Confirmed) {
                self.emailToConfirm = result.email!
                self.userToConfirm = result.user!

                model.deviceLayer.saveEmail(email: emailValue)
                model.deviceLayer.savePassword(password: passwordValue)
                model.deviceLayer.saveUserName(name: userNameValue)
                
                self.performSegue(withIdentifier: "confirmSignUpSegue", sender:sender)
            } else {
                let _ = self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

