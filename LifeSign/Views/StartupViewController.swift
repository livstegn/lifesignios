//
//  StartupViewController.swift
//  LifeSign
//
//  Created by Simon Jensen on 01/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit
import LocalAuthentication

class StartupViewController: UIViewController {
    
    //let logger = (UIApplication.shared.delegate as! AppDelegate).logger
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace = AllTraces()
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var launchImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        trace.begin()
        self.trace.output( "StartupViewController.viewDidLoad()")
        
        trace.output("launchImage: \(String(describing: launchImage))")
        
        let myLaunchImage = UIImage.launchImage
        trace.output("myLaunchImage: \(myLaunchImage.debugDescription)")
        
        let name = launchImageName()
        launchImage.image = UIImage(named: name)
        
        trace.end()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        trace.begin()
        
        // https://developers.facebook.com/docs/swift/login/
        // https://dawnthemes.com/generating-never-expiring-facebook-page-access-token/
        
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // Check if user is signed in to Cognito....
        if (model.cloudLayer.getUserSessionState() != UserSessionStatusType.LoggedIn) {
            trace.output("LOGGED OUT")
            registerButton.isHidden = false
            loginButton.isHidden = false
            trace.end()
            return
        }
        
        // User IS authenticated to Cognito...
        trace.output("LOGGED IN")
        registerButton.isHidden = true
        loginButton.isHidden = true
    
        //_ = model.cloudLayer.setupCredentials(authMethod: AuthenticationMethod.User)
            
        // All is GOOD - user IS signed in AND known in Dynamo - we continue...
        if(!self.model.signinKnownUser()){  // signin using the keychain values: email & password...
            
            trace.output("FORCE LOGOUT - user not known in Cognito")
            self.model.signOut()
            registerButton.isHidden = false
            loginButton.isHidden = false
                
            trace.end()
            return

        }
        
        // First restore userdata of current user
        self.model.restoreCurrentUser()
            
        // Then make transition to appropriate viewcontroller
        self.performSegue(withIdentifier: "issignedinsegue", sender: self)
        
        trace.end()
    }
    
    
    @IBAction func unwind2StartupVCAction( _ seg: UIStoryboardSegue) {
    }
    
    
    @IBAction func registerPressed(_ sender: Any) {

        trace.begin()

        _ = model.cloudLayer.setupCredentials(authMethod: AuthenticationMethod.User)
        
        trace.end()
        
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        
        trace.begin()
        
        self.performSegue(withIdentifier: "signinSeague", sender: self)
        
        trace.end()
    }
    
    override func viewDidAppear(_ animated: Bool) {

        trace.begin()
        
        trace.end()
    }
    
    func launchImageName() -> String {
    
        trace.begin()
        
        switch (UIDevice.current.userInterfaceIdiom, UIScreen.main.scale, UIScreen.main.bounds.size.height) {
        
        case (.phone, _, 480):      //
            trace.output("480")
            return "LaunchImage-700@2x.png"
        
        case (.phone, _, 568):// iPhone SE
            trace.output("568")
            return "LaunchImage-700-568h@2x.png"
            
        case (.phone, _, 667):// iPhone 6, 7, 8
            trace.output("667")
            return "LaunchImage-800-667h@2x.png"
        
        case (.phone, _, 736):// iPhone 7, 8 Plus
            trace.output("736")
            return "LaunchImage-800-Portrait-736h@3x.png"

        case (.phone, _, 812):// iPhone X, Xs
            trace.output("812")
            return "LaunchImage-1100-Portrait-2436h@3x.png"
            
        case (.phone, _, 896):// iPhone Xr, Xs MAX
            trace.output("812, scale: \(UIScreen.main.scale)")
            if (UIScreen.main.scale == 2.0) {
                trace.output("iPhone Xr")
                return "LaunchImage-1200-Portrait-1792h@2x.png" // iPhone Xr
            } else {
                trace.output("iPhone Xs MAX")
                return "LaunchImage-1200-Portrait-2688h@3x.png" // iPhone Xs MAX
            }

        default:
            trace.output("default")
            return "LaunchImage"

        }
        trace.end()
    }

}

extension UIImage {

    static var launchImage: UIImage? {
        let pngs = Bundle.main.paths(forResourcesOfType: "png", inDirectory: nil)
        return pngs
            .filter({$0.contains("LaunchImage")})
            .compactMap({UIImage(named: $0)})
            .filter({$0.size == UIScreen.main.bounds.size})
            .first
    }

}
