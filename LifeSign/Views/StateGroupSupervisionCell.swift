//
//  StateGroupSupervisionCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 01/03/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation
import UIKit

class StateGroupSupervisionCell: UITableViewCell {
    @IBOutlet weak var vwStatus: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    var supervision : Supervision?
}
