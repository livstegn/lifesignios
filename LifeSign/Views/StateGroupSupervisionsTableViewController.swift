//
//  StateGroupSupervisionsTableViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 01/03/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import UIKit

class StateGroupSupervisionsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var stateMessages = [StateMessage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateMessages.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellGroupMessages") as! StateGroupSupervisionCell
        
        switch stateMessages[indexPath.row].severity {
        case StateMessageSeverity.Alarm:
            cell.vwStatus.backgroundColor = .red
        case StateMessageSeverity.Info:
            cell.vwStatus.backgroundColor = .orange
        default:
            cell.vwStatus.backgroundColor = .green
        }
        cell.lblMessage.text = stateMessages[indexPath.row].text()
        
        return cell
    }
}
