//
//  StateGroupTableViewCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 01/03/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import UIKit

class StateGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var imgGroupSmiley: UIImageView!
    @IBOutlet weak var tvGroupSupervisions: UITableView!
    var stateMessages = [StateMessage]()
}
