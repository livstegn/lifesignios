//
//  StateTableViewCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 18/08/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

class StateTableViewCell: UITableViewCell {

    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblGroup: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    
}
