//
//  StatusGroupsTableView.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 01/03/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class StatusGroupsTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
    
}
