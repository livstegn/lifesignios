//
//  StatusViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 08/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit
import AMPopTip // https://github.com/andreamazz/AMPopTip

class StatusViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ReloaderDelegate {
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    var dueLifesigns = [String:Supervision]()

    let trace = ErrorTraces() //
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    @IBOutlet weak var lblStatusTitle: UILabel!
    @IBOutlet weak var lblStatusInstruction: UILabel!
    @IBOutlet weak var activityPopup: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var lblLastLifesign: UILabel!
    @IBOutlet weak var lblLifesignDate: UILabel!
    @IBOutlet weak var lblNextLifesign: UILabel!
    @IBOutlet weak var lblNextLifesignDate: UILabel!
    @IBOutlet weak var tvGroups: UITableView!
    @IBOutlet weak var btnLifesign: UIButton!
    
    var refreshControl = UIRefreshControl()
    var caregroups = [Caregroup]()
    //var messagesByGroupId = [String:[StateMessage]]()
    var textStyleHeader : UIFont.TextStyle?
    var subCellHeight = 0
    
    var mainStatusList = [MainStatus]()
    var mainStatusByGroupId = [String:[StateMessage]]()
    
    // Crate PopTip and vars
    let popTip = PopTip()
    var direction = PopTipDirection.up
    var topRightDirection = PopTipDirection.down
    var timer: Timer? = nil
    var autolayoutView: UIView?

    override func viewDidLoad() {
        trace.begin()
        
        super.viewDidLoad()
        self.model.statusViewController = self
        self.tvGroups.tableFooterView = UIView()  // Skjuler tomme linier...
        
        self.hideLifesignControls()
        self.lblStatusInstruction.sizeToFit()
        
        // Add a shadow under the logout button
        self.btnLifesign.layer.shadowColor = UIColor.lightGray.cgColor
        self.btnLifesign.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.btnLifesign.layer.shadowOpacity = 1.0
        self.btnLifesign.layer.shadowRadius = 0.0
        
        self.refreshControl.addTarget(self, action: #selector(refreshAfterDrag), for: .valueChanged)
        
        if #available(iOS 10, *){
            self.tvGroups.refreshControl = self.refreshControl
        }else{
            self.tvGroups.addSubview(self.refreshControl)
        }
        self.pendingTimestamps()
        
        /*NotificationCenter.default.addObserver(self, selector: #selector(viewDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)*/
        
        // Prepare PopTip
        popTip.font = UIFont.systemFont (ofSize: 16) //UIFont(name: "Avenir-Medium", size: 12)!
        popTip.textColor = .white
        popTip.bubbleColor = UIColor(named: "lsBigButtonBackgroundMain") ?? UIColor(red: 0.95, green: 0.65, blue: 0.21, alpha: 1)
        popTip.textAlignment = .left
        popTip.shouldDismissOnTap = true
        popTip.shouldDismissOnTapOutside = true
        popTip.shouldDismissOnSwipeOutside = true
        popTip.cornerRadius = 15
        popTip.arrowSize = CGSize(width: 16, height: 16)
        popTip.edgeMargin = 5
        popTip.offset = 2
        popTip.bubbleOffset = 0
        popTip.edgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        popTip.maskColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        popTip.shouldShowMask = true
        
        trace.end()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        trace.begin()

        let subCellFont = UIFont.preferredFont(forTextStyle: .subheadline)
        subCellHeight = Int(subCellFont.lineHeight * 1.25) + 5

        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        NotificationCenter.default.addObserver(
          self,
          selector: #selector(viewWillEnterForeground),
          name: UIApplication.willEnterForegroundNotification,
          object: nil)
        
        self.loadData()
        popTip.hide()

        trace.end()
    }
        
    @objc func viewWillEnterForeground() {
        trace.begin()
        self.loadData()
        trace.end()
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        trace.begin()
        NotificationCenter.default.removeObserver(self)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        popTip.hide()
        trace.end()
    }
    
    @objc func refreshAfterDrag() {
        trace.begin()
        self.loadData()
        trace.end()
    }
    
    func reload() {
        trace.begin()
        // Sørg for, at delegate-drevet reload
        self.loadData()
        trace.end()
    }
    
    func loadData() {
        trace.begin()
        self.hideLifesignControls()
        
        if refreshControl.isRefreshing == false {
            self.spinner.startAnimating()
            self.activityPopup.isHidden = false
        }

        DispatchQueue.global(qos: .background).async {
            self.trace.user(self.model.user)  // Temporary check to verify model.user != nil
            self.mainStatusList = self.model.getLifesignStateNew()
            
            self.trace.output( "StatusViewController: efter model.getLifesignState()")

            DispatchQueue.main.async {
                self.trace.user(self.model.user)    // Temporary check to verify model.user != nil

                self.updateState()
                
                if self.trace.isKind(of: PerformanceTraces.self)  {
                    (UIApplication.shared.delegate as! AppDelegate).logger.printPerformanceLog()
                    (UIApplication.shared.delegate as! AppDelegate).logger.clearPerformanceLog ()
                }
                    
                if !self.model.checkShouldBeTracking {
                    self.model.checkForTrackingReactivation(mainStatusList: self.mainStatusList)
                }
                
            }

        }
        
        trace.end()
    }

    fileprivate func pendingTimestamps() {
        lblLifesignDate.text = NSLocalizedString("(loading...)", comment: "Stateview, temporary content while loading from AWS")
        lblNextLifesignDate.text = NSLocalizedString("(loading...)", comment: "Stateview, temporary content while loading from AWS")
    }
    
    func hideLifesignControls () {
        lblLastLifesign.isHidden = true
        lblLifesignDate.isHidden = true
        lblNextLifesign.isHidden = true
        lblNextLifesignDate.isHidden = true
        self.btnLifesign.isHidden = true
    }

    func unhideLifesignControls () {
        lblLastLifesign.isHidden = false
        lblLifesignDate.isHidden = false
        lblNextLifesign.isHidden = false
        lblNextLifesignDate.isHidden = false
        self.btnLifesign.isHidden = false
    }

    fileprivate func updateState() {
    
        trace.begin()
        
        if (model.stateLastLifesign != nil) {
            lblLifesignDate.text = Helper.simpleUITime(date: model.stateLastLifesign!)
        } else {
            lblLifesignDate.text = ""
        }
        
        if (model.stateNextLifesign != nil) {
            lblNextLifesignDate.text = Helper.simpleUITime(date: model.stateNextLifesign!)
        } else {
            lblNextLifesignDate.text = ""
        }
        
        if refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        } else {
            self.spinner.stopAnimating()
            self.activityPopup.isHidden = true
        }

        var userMonitored = false
        
        let sortedMainStatus = self.mainStatusList.sorted(by: { $0.supervisionId > $1.supervisionId })
        mainStatusByGroupId = [String:[StateMessage]]()
        for ms in sortedMainStatus {
            let message = ms.getStateMessage(appUserid: self.model.user!.userId, now: Date())
            
            var groupMessages = mainStatusByGroupId[message.caregroupId]
            if groupMessages == nil {
                var messages = [StateMessage]()
                messages.append(message)
                mainStatusByGroupId[message.caregroupId] = messages
            } else {
                groupMessages!.append(message)
                mainStatusByGroupId[message.caregroupId] = groupMessages
            }
        
            if (message.userId == self.model.user?.userId) {
                userMonitored = true
            }
        }

        
        if (userMonitored) {
            unhideLifesignControls()
        } else {
            hideLifesignControls()
        }
        tvGroups.reloadData()
        //self.view.setNeedsLayout()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.presentPopTip()
        }
        
        trace.end()
    }
    
    func presentPopTip() {
        let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: popTip.font, NSAttributedString.Key.foregroundColor: popTip.textColor]
        let bold: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: popTip.textColor]

        if self.model.caregroups?.count ?? 0 < 1 {
            if let destinationIndex = self.tabBarController?.viewControllers?.firstIndex(where: { $0 is GroupListViewController }) {
                let frame = Helper.frameForTabAtIndex(tabBarVC: tabBarController as! TabBarViewController, toview: view, index: destinationIndex)
                let attributedText = NSMutableAttributedString(string: "Create a caregroup\n", attributes: bold)
                attributedText.append(NSAttributedString(string: "Start by adding a caregroup and invite people to the group\n\n", attributes: attributes))
                attributedText.append(NSAttributedString(string: "Got it", attributes: bold))
                popTip.show(attributedText: attributedText, direction: .autoVertical, maxWidth: 200, in: self.view, from: frame)
            }
        } else if mainStatusByGroupId.keys.count < 1 {
            if let destinationIndex = self.tabBarController?.viewControllers?.firstIndex(where: { $0 is SupervisionsTableViewController }) {
                let frame = Helper.frameForTabAtIndex(tabBarVC: tabBarController as! TabBarViewController, toview: view, index: destinationIndex)
                let attributedText = NSMutableAttributedString(string: "Create a supervision\n", attributes: bold)
                attributedText.append(NSAttributedString(string: "Create a supervision to start monitoring within the group\n\n", attributes: attributes))
                attributedText.append(NSAttributedString(string: "Got it", attributes: bold))
                popTip.show(attributedText: attributedText, direction: .autoVertical, maxWidth: 200, in: self.view, from: frame)
            }
        }
    }
    
    @IBAction func btnGiveLifesign(_ sender: Any) {

        trace.begin()
        
        (UIApplication.shared.delegate as! AppDelegate).logger.clearPerformanceLog()
        self.pendingTimestamps()

        self.spinner.startAnimating()
        self.activityPopup.isHidden = false

        DispatchQueue.global(qos: .background).async {
            self.model.getCareGroups()
            DispatchQueue.main.async {
                self.trace.user(self.model.user)    // Temporary check to verify model.user != nil
                    
                self.model.giveLifesignNew(lifesignTime: Date())
                self.mainStatusList = self.model.getLifesignStateNew()
                
                self.updateState()

                if self.trace.isKind(of: PerformanceTraces.self)  {
                    (UIApplication.shared.delegate as! AppDelegate).logger.printPerformanceLog()
                }
                self.trace.end()
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return mainStatusByGroupId.keys.count // mainStatusByGroupId.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let sortedKeys = mainStatusByGroupId.keys.sorted()
        let numbersInSection = mainStatusByGroupId[sortedKeys[section]]?.count ?? 0
        
        return numbersInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cellFor(tableView: tableView, cellForRowAt: indexPath)

        self.trace.output ("cellForRowAt: " + cell.lblMessage.text! + " = " + cell.lblMessage.frame.size.height.description)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = cellFor(tableView: tableView, cellForRowAt: indexPath)

        self.trace.output ("Height: tableView.frame.width = " + tableView.frame.width.description + ", cell.lblMessage.frame.size.width = " + cell.lblMessage.frame.size.width.description)
        var height = cell.lblMessage.frame.size.height * 1.2
        if ((tableView.frame.width - cell.lblMessage.frame.size.width) < 89) {
            height = cell.lblMessage.frame.size.height * 2.2
        }
        self.trace.output ("Height: " + cell.lblMessage.text! + " = " + height.description)

        return height
    }
    
    func cellFor (tableView: UITableView, cellForRowAt indexPath: IndexPath) -> StateGroupSupervisionCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StateGroupSupervisionCell") as! StateGroupSupervisionCell
        
        let caregroupId = mainStatusByGroupId.keys.sorted()[indexPath.section]
        let messages : [StateMessage] = mainStatusByGroupId[caregroupId]!
        let message = messages[indexPath.row]

        switch message.severity {
        case StateMessageSeverity.Alarm:
            cell.vwStatus.backgroundColor = UIColor(named: "lsBullitAlarm")
        case StateMessageSeverity.Warning:
            cell.vwStatus.backgroundColor = UIColor(named: "lsBullitWarning")
        default:
            cell.vwStatus.backgroundColor = UIColor(named: "lsBullitInfo")
        }
        cell.lblMessage.text = message.text()
        cell.lblMessage.sizeToFit()
        cell.sizeToFit()

        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: tableView.frame.width,
                                              height: 50))
        let gruppeLabel = UILabel.init(frame: CGRect(x: 64                                                     ,
                                                     y: 0,
                                                     width: tableView.frame.width - 64,
                                                     height: 28))
        let groupImage = UIImageView.init(frame: CGRect(x: 0,
                                                        y: 0,
                                                        width: 57,
                                                        height: 57))

        let caregroupId = mainStatusByGroupId.keys.sorted()[section]
        let messages = mainStatusByGroupId[caregroupId]
        let caregroupName = messages![0].caregroupName

        var worstSeverity : StateMessageSeverity = StateMessageSeverity.Info
        for i in 0...(messages!.count - 1) {
            if worstSeverity.rawValue < messages![i].severity.rawValue {
                worstSeverity = messages![i].severity
            }
        }
        switch worstSeverity {
        case StateMessageSeverity.Alarm:
            groupImage.image = UIImage.init(named: "StateRed.png")
        case StateMessageSeverity.Warning:
            groupImage.image = UIImage.init(named: "StateYellow.png")
        case StateMessageSeverity.Info:
            groupImage.image = UIImage.init(named: "StateGreen.png")
        }
        
        gruppeLabel.text = caregroupName
        gruppeLabel.contentMode = .scaleToFill
        gruppeLabel.numberOfLines = 0
        gruppeLabel.font = UIFont.preferredFont(forTextStyle: .body)
        gruppeLabel.backgroundColor = .clear
        headerView.addSubview(gruppeLabel)
        headerView.addSubview(groupImage)
        headerView.backgroundColor = .clear

        return headerView
    }

    
    
    @IBAction func btnMessages(_ sender: Any) {
        
        let test : [AnyHashable : Any] = ["label": "værdi"]
        
        (UIApplication.shared.delegate as! AppDelegate).gotoMessageViewController(userInfo: test)
    }
    
    @IBAction func btnUserActivity(_ sender: Any) {
        let ua = UserActivity(time: Date(), type: .Unlocked, detected: .Detected)
        self.model.newUserActivity(userActivity: ua, now: Date())
    }

}

