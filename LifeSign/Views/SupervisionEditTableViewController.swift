//
//  SupervisionEditTableViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 02/01/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import UIKit
import CoreLocation

class SupervisionEditTableViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var lblGroupLeft: UILabel!
    @IBOutlet weak var lblMonitoredLeft: UILabel!
    @IBOutlet weak var lblTypeLeft: UILabel!
    @IBOutlet weak var lblRepeatLeft: UILabel!
    @IBOutlet weak var lblIntervalLeft: UILabel!
    @IBOutlet weak var lblCountLEft: UILabel!
    @IBOutlet weak var lblTime1Left: UILabel!
    @IBOutlet weak var lblTime2Left: UILabel!
    @IBOutlet weak var lblTime3Left: UILabel!
    @IBOutlet weak var lblReminderLeft: UILabel!
    @IBOutlet weak var lblAlarmLeft: UILabel!
    @IBOutlet weak var lblReminderTimeLeft: UILabel!
    @IBOutlet weak var lblGpsLeft: UILabel!
    @IBOutlet weak var lblEndingLeft: UILabel!
    @IBOutlet weak var lblWifiLeft: UILabel!
    @IBOutlet weak var lblActiveLeft: UILabel!
    
    
    @IBOutlet weak var lblGroupname: UILabel!
    @IBOutlet weak var pwGroupname: UIPickerView!
    @IBOutlet weak var lblMonitoredUser: UILabel!
    @IBOutlet weak var pwMonitoredUser: UIPickerView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var scType: UISegmentedControl!
    @IBOutlet weak var lblRepeatMode: UILabel!
    @IBOutlet weak var scRepeatMode: UISegmentedControl!
    @IBOutlet weak var lblIntervalTime: UILabel!
    @IBOutlet weak var pwIntervalTime: UIPickerView!
    @IBOutlet weak var lblTidspunkter: UILabel!
    @IBOutlet weak var scTidspunkter: UISegmentedControl!
    @IBOutlet weak var lblTime1: UILabel!
    @IBOutlet weak var dpTime1: UIDatePicker!
    //
    @IBOutlet weak var lblTime2: UILabel!
    @IBOutlet weak var dpTime2: UIDatePicker!
    @IBOutlet weak var lblTime3: UILabel!
    @IBOutlet weak var dpTime3: UIDatePicker!
    @IBOutlet weak var lblCountdown: UILabel!
    @IBOutlet weak var dpCountdown: UIDatePicker!
    @IBOutlet weak var cellPart3Delimiter: UITableViewCell!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var dpEndTime: UIDatePicker!
    @IBOutlet weak var lblAlarmTIme: UILabel!
    @IBOutlet weak var dpAlarmTime: UIDatePicker!
    @IBOutlet weak var lblGPSTrack: UILabel!
    @IBOutlet weak var scGPSTrack: UISegmentedControl!
    @IBOutlet weak var lblEndMode: UILabel!
    @IBOutlet weak var scEndMode: UISegmentedControl!
    @IBOutlet weak var lblSafeSpotAddress: UILabel!
    @IBOutlet weak var swActive: UISwitch!
    @IBOutlet weak var navItem: UINavigationItem!

    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let intervalHours = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
    let trace = NoTraces()
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    var caregroup = Caregroup()
    var supervision = Supervision()
    var mode = SupervisionEditViewControllerMode.None
    var caregroups = [Caregroup]()
    var groupMembers = [String:[GroupMember]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false

        let backItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelChange))
        backItem.title = NSLocalizedString("Cancel", comment: "Parameterliste for supervisions: Cancel-button")
        backItem.tintColor = UIColor(named: "lsHeadlineText")
        navigationItem.backBarButtonItem = backItem

        let rightItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveChanges))
        rightItem.title = NSLocalizedString("Save", comment: "Parameterliste for supervisions: Save-button")
        navigationItem.rightBarButtonItem = rightItem
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "lsHeadlineText")
        
        pwIntervalTime.dataSource = self
        pwIntervalTime.delegate = self

        caregroups.append(self.caregroup)
            
        var members = [GroupMember]()
        for member in model.getGroupMembers(caregroupId: caregroup.caregroupId) {
            members.append(member)
        }
        groupMembers[caregroup.caregroupId] = members
    
        initCells()
        setVisibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let titleLabel = UILabel()
        titleLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        titleLabel.textColor = UIColor(named: "lsHeadlineText")
        titleLabel.text = self.title
        titleLabel.backgroundColor = .clear
        self.navigationItem.title = nil // clear the default title
        navigationItem.titleView = titleLabel
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false

    }

    @objc func cancelChange () {
        trace.output("SupervisionEditViewController - exit without saving...")

        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func saveChanges() {
        trace.output("Saving supervision")
        
        if (supervision.supervisionType == SupervisionType.Period) && (supervision.status == SupervisionStatusType.Active) {
            if (supervision.endingType == SupervisionEndingType.Spot) {
                if (supervision.trackingId == nil) {
                    supervision.trackingId = "TR-" + UUID().uuidString
                }
            }
        }

        // Gem supervision
        switch mode {
        case SupervisionEditViewControllerMode.Add:
            trace.output("Add new supervision")

            if let groupMember = model.getGroupmember(userId: supervision.userId, caregroupId: supervision.caregroupId) {
                if groupMember.isMonitored == false {
                    groupMember.isMonitored = true
                    model.updateGroupMember(groupMember: groupMember)
                }
            }
            
            // Sørg for, at andre members i gruppen er Monitor, hvis der er en monitored...
            for member in groupMembers[caregroup.caregroupId]! {
                if member.isMonitor == false {
                    if let awsMember = model.getGroupmember(userId: member.userid, caregroupId: member.caregroupId) {
                        awsMember.isMonitor = true
                        model.updateGroupMember(groupMember: awsMember)
                    }
                }
            }
            
            supervision.supervisionId = model.addSupervision(supervision: supervision) ?? ""
            
        default: // Update
            trace.output("Update existing supervision")
            _ = model.updateSupervision(supervision: supervision)
        }
        
        model.startOrStopTracking(supervision: supervision)

        self.navigationController?.popViewController(animated: true)
    }
    
    func initCells() {
        
        lblGroupname.text = caregroup.caregroupName
        lblMonitoredUser.text = model.getCachedUserName(userid: supervision.userId)
        
        lblType.text = supervision.supervisionType.UIText()
        scType.selectedSegmentIndex = supervision.supervisionType.rawValue

        if (supervision.supervisionType == SupervisionType.Ongoing) {
            lblRepeatMode.text = supervision.repeatType().UIText()

            if (supervision.repeatType() == SupervisionRepeatType.FixedInterval) {
                scRepeatMode.selectedSegmentIndex = 1

                let interval = Int((supervision.interval ?? 3600) / 3600)
                lblIntervalTime.text = interval.description + NSLocalizedString(" h", comment: "Interval parameter for supervisions: h abbreviation for hours")
                pwIntervalTime.selectRow(interval - 1, inComponent: 0, animated: false)

                lblTidspunkter.text = ""
                scTidspunkter.selectedSegmentIndex = 0
            } else {
                scRepeatMode.selectedSegmentIndex = 0

                switch supervision.deadlines() {
                case 1:
                    lblTidspunkter.text = "1"
                    scTidspunkter.selectedSegmentIndex = 0
                case 2:
                    lblTidspunkter.text = "2"
                    scTidspunkter.selectedSegmentIndex = 1
                case 3:
                    lblTidspunkter.text = "3"
                    scTidspunkter.selectedSegmentIndex = 2
                default:
                    lblTidspunkter.text = ""
                    scTidspunkter.selectedSegmentIndex = 0
                }
                
                if (supervision.deadline1 != nil) {
                    lblTime1.text = Helper.hhColonMm(timeofday: supervision.deadline1!)
                    let deadline = Date.init(timeInterval: supervision.deadline1!, since: getBaseDate())
                    dpTime1.setDate(deadline, animated: true)
                    dpTime1.locale = NSLocale.current
                }

                if (supervision.deadline2 != nil) {
                    lblTime2.text = Helper.hhColonMm(timeofday: supervision.deadline2!)
                    let deadline = Date.init(timeInterval: supervision.deadline2!, since: getBaseDate())
                    dpTime2.setDate(deadline, animated: true)
                    dpTime2.locale = NSLocale.current
                }
                
                if (supervision.deadline3 != nil) {
                    lblTime3.text = Helper.hhColonMm(timeofday: supervision.deadline3!)
                    let deadline = Date.init(timeInterval: supervision.deadline3!, since: getBaseDate())
                    dpTime3.setDate(deadline, animated: true)
                    dpTime3.locale = NSLocale.current
                }
            }

            lblCountdown.text = Helper.hhColonMm(timeofday: supervision.countdownTime)
            let countdown = Date.init(timeInterval: supervision.countdownTime, since: getBaseDate())
            dpCountdown.setDate(countdown, animated: true)
            dpCountdown.locale = NSLocale.current
        } else {
            lblEndTime.text = Helper.simpleUITime(date: supervision.periodEnd!)
            dpEndTime.setDate(supervision.periodEnd!, animated: true)
            dpEndTime.locale = NSLocale.current

            let alarmtime = Date.init(timeInterval: -supervision.countdownTime, since: supervision.periodEnd!)
            lblAlarmTIme.text = Helper.simpleUITime(date: alarmtime)
            dpAlarmTime.setDate(alarmtime, animated: true)
            dpAlarmTime.locale = NSLocale.current

            lblGPSTrack.text = scGPSTrack.titleForSegment(at: 0)
            scGPSTrack.selectedSegmentIndex = 0
            supervision.locationInterval = nil

            if (supervision.endingType == nil) {
                supervision.endingType = SupervisionEndingType.Manual
            }
            
            switch supervision.endingType! {
            case SupervisionEndingType.Manual:
                scEndMode.selectedSegmentIndex = 0
            case SupervisionEndingType.Wifi:
                scEndMode.selectedSegmentIndex = 0
            case SupervisionEndingType.Spot:
                scEndMode.selectedSegmentIndex = 1
            }
            
            lblEndMode.text = scEndMode.titleForSegment(at: scEndMode.selectedSegmentIndex)
            
            let location = CLLocationCoordinate2D.init(latitude: supervision.locationLatitude!, longitude: supervision.locationLongitude!)
            
            self.model.getAddress(location: location) { address, error in
                guard address != nil && error == nil else {
                    print("error = \(String(describing: error))")
                    return
                }

                self.lblSafeSpotAddress.text = address!
            }
        }
        
        swActive.isOn = supervision.status == SupervisionStatusType.Active ? true : false
    }
    
    func getBaseDate() -> Date {
        let date = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
        
        return date
    }
    
    func hideFieldsByType() {
        if (scType.selectedSegmentIndex == 0) {  // 0 = Tilbagevendende
            lblRepeatMode.isHidden = false

            if (supervision.repeatType() == SupervisionRepeatType.FixedInterval) {
                lblIntervalTime.isHidden = false
                lblTidspunkter.isHidden = true
                lblTime1.isHidden = true
                lblTime2.isHidden = true
                lblTime3.isHidden = true
            } else {
                lblIntervalTime.isHidden = true
                lblTidspunkter.isHidden = false
                lblTime1.isHidden = false
                lblTime2.isHidden = true
                lblTime3.isHidden = true

                if (supervision.deadlines() > 1) {
                    lblTime2.isHidden = false
                }
                if (supervision.deadlines() > 2) {
                    lblTime3.isHidden = false
                }
            }
            
            lblCountdown.isHidden = false
            
            cellPart3Delimiter.isHidden = true
            lblEndTime.isHidden = true
            lblAlarmTIme.isHidden = true
            lblGPSTrack.isHidden = true
            lblEndMode.isHidden = true
            lblSafeSpotAddress.isHidden = true
        } else { // Tidsbegrænset
            lblIntervalTime.isHidden = true
            lblRepeatMode.isHidden = true
            lblTidspunkter.isHidden = true
            lblTime1.isHidden = true
            lblTime2.isHidden = true
            lblTime3.isHidden = true
            
            lblCountdown.isHidden = true
            
            cellPart3Delimiter.isHidden = true
            lblEndTime.isHidden = false
            lblAlarmTIme.isHidden = false
            lblGPSTrack.isHidden = true
            lblEndMode.isHidden = false
            switch supervision.endingType! {
            case SupervisionEndingType.Manual:
                lblSafeSpotAddress.isHidden = true
            case SupervisionEndingType.Wifi:
                lblSafeSpotAddress.isHidden = false
            case SupervisionEndingType.Spot:
                lblSafeSpotAddress.isHidden = false
            }
        }
    }
    
    func setVisibility ()
    {
        hideFieldsByType()
        
        lblGroupname.isHidden = false
        lblMonitoredUser.isHidden = false
        lblType.isHidden = false
        swActive.isHidden = false

        // Initially hidden
        pwGroupname.isHidden = true
        pwMonitoredUser.isHidden = true
        scType.isHidden = true
        scRepeatMode.isHidden = true
        pwIntervalTime.isHidden = true
        scTidspunkter.isHidden = true
        dpTime1.isHidden = true
        dpTime2.isHidden = true
        dpTime3.isHidden = true
        dpCountdown.isHidden = true
        dpEndTime.isHidden = true
        dpAlarmTime.isHidden = true
        scGPSTrack.isHidden = true
        scEndMode.isHidden = true
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let superHeight : CGFloat = 38
        let delimiterHeight : CGFloat = 28
        var height:CGFloat = -1
        
        switch indexPath.row {
        case RowNumber.Part1Delimiter.rawValue:
            height = delimiterHeight
        case RowNumber.GroupSelector.rawValue:
            height = pwGroupname.isHidden ? 0.0 : 216
        case RowNumber.MonitoredNameSelector.rawValue:
            height = pwMonitoredUser.isHidden ? 0.0 : 216
        case RowNumber.Supervision_Type.rawValue:
            height = lblType.isHidden ? 0.0 : superHeight
        case RowNumber.TypeSwitch.rawValue:
            height = scType.isHidden ? 0.0 : superHeight
        case RowNumber.Part2Delimiter.rawValue:
            height = delimiterHeight
        case RowNumber.RepeatMode.rawValue:
            height = lblRepeatMode.isHidden ? 0.0 : superHeight
        case RowNumber.RepeatSwitch.rawValue:
            height = scRepeatMode.isHidden ? 0.0 : superHeight
        case RowNumber.Interval.rawValue:
            height = lblIntervalTime.isHidden ? 0.0 : superHeight
        case RowNumber.IntervalPicker.rawValue:
            height = pwIntervalTime.isHidden ? 0.0 : 216
        case RowNumber.Tidspunkter.rawValue:
            height = lblTidspunkter.isHidden ? 0.0 : superHeight
        case RowNumber.TidspunkterSwitch.rawValue:
            height = scTidspunkter.isHidden ? 0.0 : superHeight
        case RowNumber.Time1.rawValue:
            height = lblTime1.isHidden ? 0.0 : superHeight
        case RowNumber.Time1Picker.rawValue:
            height = dpTime1.isHidden ? 0.0 : 216
        case RowNumber.Time2.rawValue:
            height = lblTime2.isHidden ? 0.0 : superHeight
        case RowNumber.Time2Picker.rawValue:
            height = dpTime2.isHidden ? 0.0 : 216
        case RowNumber.Time3.rawValue:
            height = lblTime3.isHidden ? 0.0 : superHeight
        case RowNumber.Time3Picker.rawValue:
            height = dpTime3.isHidden ? 0.0 : 216
        case RowNumber.Countdown.rawValue:
            height = lblCountdown.isHidden ? 0.0 : superHeight
        case RowNumber.CountdownPicker.rawValue:
            height = dpCountdown.isHidden ? 0.0 : 216
        case RowNumber.Part3Delimiter.rawValue:
            height = cellPart3Delimiter.isHidden ? 0.0 : delimiterHeight
        case RowNumber.EndTime.rawValue:
            height = lblEndTime.isHidden ? 0.0 : superHeight
        case RowNumber.EndTimePicker.rawValue:
            height = dpEndTime.isHidden ? 0.0 : 216
        case RowNumber.AlarmTime.rawValue:
            height = lblAlarmTIme.isHidden ? 0.0 : superHeight
        case RowNumber.AlarmtimePicker.rawValue:
            height = dpAlarmTime.isHidden ? 0.0 : 216
        case RowNumber.GPSTrack.rawValue:
            height = lblGPSTrack.isHidden ? 0.0 : superHeight
        case RowNumber.GPSTrackSelector.rawValue:
            height = scGPSTrack.isHidden ? 0.0 : superHeight
        case RowNumber.Part4Delimiter.rawValue:
            height = delimiterHeight
        case RowNumber.EndMode.rawValue:
            height = lblEndMode.isHidden ? 0.0 : superHeight
        case RowNumber.EndModeSwitch.rawValue:
            height = scEndMode.isHidden ? 0.0 : superHeight
        case RowNumber.SafeSpotAddress.rawValue:
            height = lblSafeSpotAddress.isHidden ? 0.0 : superHeight
        case RowNumber.Active.rawValue:
            height = swActive.isHidden ? 0.0 : superHeight

        default:
            height = superHeight
        }
        
        return height
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var updateNeeded : Bool
        
        switch Int(indexPath.row) {
        case RowNumber.GroupName.rawValue:
            pwGroupname.isHidden = !pwGroupname.isHidden
            updateNeeded = true
        case RowNumber.MonitoredName.rawValue:
            pwMonitoredUser.isHidden = !pwMonitoredUser.isHidden
            updateNeeded = true
        case RowNumber.Supervision_Type.rawValue:
            scType.isHidden = !scType.isHidden
            updateNeeded = true
        case RowNumber.RepeatMode.rawValue:
            scRepeatMode.isHidden = !scRepeatMode.isHidden
            updateNeeded = true
        case RowNumber.Interval.rawValue:
            pwIntervalTime.isHidden = !pwIntervalTime.isHidden
            updateNeeded = true
        case RowNumber.Tidspunkter.rawValue:
            scTidspunkter.isHidden = !scTidspunkter.isHidden
            updateNeeded = true
        case RowNumber.Time1.rawValue:
            dpTime1.isHidden = !dpTime1.isHidden
            updateNeeded = true
        case RowNumber.Time2.rawValue:
            dpTime2.isHidden = !dpTime2.isHidden
            updateNeeded = true
        case RowNumber.Time3.rawValue:
            dpTime3.isHidden = !dpTime3.isHidden
            updateNeeded = true
        case RowNumber.Countdown.rawValue:
            dpCountdown.isHidden = !dpCountdown.isHidden
            updateNeeded = true
        case RowNumber.EndTime.rawValue:
            dpEndTime.isHidden = !dpEndTime.isHidden
            updateNeeded = true
        case RowNumber.AlarmTime.rawValue:
            dpAlarmTime.isHidden = !dpAlarmTime.isHidden
            updateNeeded = true
        case RowNumber.GPSTrack.rawValue:
            scGPSTrack.isHidden = !scGPSTrack.isHidden
            updateNeeded = true
        case RowNumber.EndMode.rawValue:
            scEndMode.isHidden = !scEndMode.isHidden
            updateNeeded = true
        case RowNumber.SafeSpotAddress.rawValue:
            self.performSegue(withIdentifier: "ShowSafeSpotSelector", sender: self)
            updateNeeded = false
        default:
            updateNeeded = false
        }

        if (updateNeeded) {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.tableView.beginUpdates()
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.tableView.endUpdates()
            })
        }
    }
    
    @IBAction func typeChanged(_ sender: Any) {
        guard let sc = sender as? UISegmentedControl else {
            return
        }
   
        if supervision.supervisionType.hashValue != sc.selectedSegmentIndex {
            supervision.supervisionType = sc.selectedSegmentIndex == 0 ? SupervisionType.Ongoing : SupervisionType.Period
            lblType.text = supervision.supervisionType.UIText()
            
            if (supervision.supervisionType == SupervisionType.Period) {
                supervision.interval = nil
                supervision.deadline1 = nil
                supervision.deadline2 = nil
                supervision.deadline3 = nil
                supervision.periodStart = Date()

                let calendar = Calendar.current
                supervision.periodEnd = calendar.date(byAdding: .hour, value: 3, to: supervision.periodStart!)
                lblEndTime.text = Helper.simpleUITime(date: supervision.periodEnd!)
                dpEndTime.date = supervision.periodEnd!
                
                supervision.countdownTime = 30*60 // 00:30
                dpAlarmTime.date = supervision.periodEnd!.addingTimeInterval(-supervision.countdownTime)
                lblAlarmTIme.text = Helper.simpleUITime(date: dpAlarmTime.date)
                
                safeSpotDefaultValues()
                
                supervision.locationInterval = nil
                lblGPSTrack.text = scGPSTrack.titleForSegment(at: 0)
                
                supervision.wifi = model.getCurrentWifiName()
                lblSafeSpotAddress.text = supervision.wifi!
            }

        }
        
        scType.isHidden = true
        hideFieldsByType()
        redrawRows()
    }
        
    @IBAction func repeatModeChanged(_ sender: Any) {
        guard let sc = sender as? UISegmentedControl else {
            return
        }
        
        switch sc.selectedSegmentIndex {
        case 1:  // Faste intervaller
            lblRepeatMode.text = sc.titleForSegment(at: 1)
            supervision.interval = 24 * 3600
            pwIntervalTime.selectRow(23, inComponent: 0, animated: false)
        default: // Faste tidspunkter
            lblRepeatMode.text = sc.titleForSegment(at: 0)
            supervision.interval = nil
            
            lblTidspunkter.text = "1"
            
            supervision.deadline1 = 9*60*60
            lblTime1.text = Helper.hhColonMm(timeofday: supervision.deadline1!)
            
            let deadline = Date.init(timeInterval: supervision.deadline1!, since: getBaseDate())
            dpTime1.isHidden = true
            dpTime1.setDate(deadline, animated: true)
            dpTime1.locale = NSLocale.current

            lblTime2.isHidden = true
            dpTime2.isHidden = true
            supervision.deadline2 = nil

            lblTime3.isHidden = true
            dpTime3.isHidden = true
            supervision.deadline3 = nil
        }
        
        scRepeatMode.isHidden = true

        hideFieldsByType()
        redrawRows()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var c = 0
        
        if pickerView == self.pwGroupname {
            c = self.caregroups.count
            trace.output("pickerView pwGroupname numberOfRowsInComponent, c = " + String(c))
        } else if pickerView == self.pwMonitoredUser {
            let cg = self.caregroups[self.pwGroupname.selectedRow(inComponent: 0)]
            c = self.groupMembers[cg.caregroupId]!.count
            trace.output("pickerView pwMonitoredUser numberOfRowsInComponent, c = " + String(c))
        } else if pickerView == self.pwIntervalTime {
            c = intervalHours.count
            trace.output("pickerView pwMonitoredUser numberOfRowsInComponent, c = " + String(c))
        }
        
        return c
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var str = ""
        
        if pickerView == self.pwGroupname {
            str = self.caregroups[row].caregroupName
            trace.output("pickerView pwGroupname titleForRow, str = " + str)
        } else if pickerView == self.pwMonitoredUser {
            let cg = self.caregroups[self.pwGroupname.selectedRow(inComponent: 0)]
            let members = self.groupMembers[cg.caregroupId]
            let member = members?[row]
            str = (member == nil) ? "" : model.getCachedUserName(userid: member!.userid)
            trace.output("pickerView pwMonitoredUser titleForRow, str = " + str)
        } else if pickerView == self.pwIntervalTime {
            str = intervalHours[row].description
            trace.output("pickerView pwIntervalTime titleForRow, str = " + str)
        }

        return str
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.pwGroupname {
            lblGroupname.text = self.caregroups[row].caregroupName
            supervision.caregroupId = self.caregroups[row].caregroupId

            self.pwMonitoredUser.reloadAllComponents()
            self.pwMonitoredUser.selectRow(0, inComponent: 0, animated: false)
            let member = self.groupMembers[supervision.caregroupId]![0]
            lblMonitoredUser.text = model.getCachedUserName(userid: member.userid)
            supervision.userId = member.userid
            supervision.groupmemberId = member.groupmemberId
            pwGroupname.isHidden = true
            trace.output("pickerView pwGroupname didSelectRow, group = " + lblGroupname.text!)
        } else if pickerView == self.pwMonitoredUser {
            let cg = self.caregroups[self.pwGroupname.selectedRow(inComponent: 0)]
            let members = self.groupMembers[cg.caregroupId]
            let member = members?[row]

            lblMonitoredUser.text = (member == nil) ? "" : model.getCachedUserName(userid: member!.userid)
            supervision.userId = member!.userid
            supervision.groupmemberId = member!.groupmemberId
            pwMonitoredUser.isHidden = true
            trace.output("pickerView pwMonitoredUser didSelectRow, member = " + lblMonitoredUser.text!)
        } else {
            let newInterval = row + 1
            supervision.interval = Double(newInterval * 3600)
            lblIntervalTime.text = newInterval.description + NSLocalizedString(" h", comment: "Interval parameter for supervisions: h abbreviation for hours")
            trace.output("pickerView pwMonitoredUser didSelectRow, time = " + lblIntervalTime.text!)
        }
        
        redrawRows()
    }
    
    @IBAction func tidspunkterChanged(_ sender: Any) {
        guard let sc = sender as? UISegmentedControl else {
            return
        }
        
        let newValue = sc.selectedSegmentIndex + 1
        
        if (newValue != supervision.deadlines()) {
            lblTime1.isHidden = false
            switch newValue {
            case 2:
                lblTime2.isHidden = false
                if (supervision.deadlines() < 2) {
                    supervision.deadline2 = 15*60*60
                    lblTime2.text = Helper.hhColonMm(timeofday: supervision.deadline2!)

                    let deadline = Date.init(timeInterval: supervision.deadline2!, since: getBaseDate())
                    dpTime2.isHidden = true
                    dpTime2.setDate(deadline, animated: true)
                    dpTime2.locale = NSLocale.current
                }
                lblTime3.isHidden = true
                dpTime3.isHidden = true
                supervision.deadline3 = nil
            case 3:
                lblTime2.isHidden = false

                if (supervision.deadlines() < 2) {
                    supervision.deadline2 = 15*60*60
                    lblTime2.text = Helper.hhColonMm(timeofday: supervision.deadline2!)

                    let deadline = Date.init(timeInterval: supervision.deadline2!, since: getBaseDate())
                    dpTime2.isHidden = true
                    dpTime2.setDate(deadline, animated: true)
                    dpTime2.locale = NSLocale.current
                }

                lblTime3.isHidden = false
                dpTime3.isHidden = true
                supervision.deadline3 = 22*60*60
                lblTime3.text = Helper.hhColonMm(timeofday: supervision.deadline3!)

                let deadline3 = Date.init(timeInterval: supervision.deadline3!, since: getBaseDate())
                dpTime3.isHidden = true
                dpTime3.setDate(deadline3, animated: true)
                dpTime3.locale = NSLocale.current
            default:
                lblTime2.isHidden = true
                dpTime2.isHidden = true
                supervision.deadline2 = nil
                lblTime3.isHidden = true
                dpTime3.isHidden = true
                supervision.deadline3 = nil
            }
            
            scTidspunkter.isHidden = true
            
            lblTidspunkter.text = supervision.deadlines().description
            redrawRows()
        }
    }
    
    @IBAction func time1Changed(_ sender: Any) {
        guard let dp = sender as? UIDatePicker else {
            return
        }
        
        supervision.deadline1 = dp.date.timeIntervalSince(getBaseDate())
        lblTime1.text = Helper.hhColonMm(timeofday: supervision.deadline1!)
        redrawRows()
    }

    @IBAction func time2Changed(_ sender: Any) {
        guard let dp = sender as? UIDatePicker else {
            return
        }
        
        supervision.deadline2 = dp.date.timeIntervalSince(getBaseDate())
        lblTime2.text = Helper.hhColonMm(timeofday: supervision.deadline2!)
        redrawRows()
    }
    
    @IBAction func time3Changed(_ sender: Any) {
        guard let dp = sender as? UIDatePicker else {
            return
        }
        
        supervision.deadline3 = dp.date.timeIntervalSince(getBaseDate())
        lblTime3.text = Helper.hhColonMm(timeofday: supervision.deadline3!)
        redrawRows()
    }

    @IBAction func countdownChanged(_ sender: Any) {
        guard let dp = sender as? UIDatePicker else {
            return
        }
        
        supervision.countdownTime = dp.date.timeIntervalSince(getBaseDate())
        lblCountdown.text = Helper.hhColonMm(timeofday: supervision.countdownTime)
        redrawRows()
    }

    @IBAction func endTimeChanged(_ sender: Any) {
        guard let dp = sender as? UIDatePicker else {
            return
        }
        
        let alarmtimeBefore = Date.init(timeInterval: -supervision.countdownTime, since: supervision.periodEnd!)

        supervision.periodEnd = dp.date
        lblEndTime.text = Helper.simpleUITime(date: supervision.periodEnd!)
        
        let alarmtimeAfter = Date.init(timeInterval: -supervision.countdownTime, since: supervision.periodEnd!)
        if (alarmtimeBefore > supervision.periodEnd!) {
            lblAlarmTIme.text = Helper.simpleUITime(date: alarmtimeAfter)
        } else {
            supervision.countdownTime = supervision.periodEnd!.timeIntervalSince(alarmtimeBefore)
            lblAlarmTIme.text = Helper.simpleUITime(date: alarmtimeBefore)
        }
        
        redrawRows()
    }
    
    @IBAction func alarmTimeChanged(_ sender: Any) {
        guard let dp = sender as? UIDatePicker else {
            return
        }
        
        supervision.countdownTime = supervision.periodEnd!.timeIntervalSince(dp.date)
        lblAlarmTIme.text = Helper.simpleUITime(date: dp.date)
        redrawRows()
    }

    @IBAction func gpsTrackChanged(_ sender: Any) {
    }

    @IBAction func endModeChanged(_ sender: Any) {
        guard let sc = sender as? UISegmentedControl else {
            return
        }
        
        switch sc.selectedSegmentIndex {
        case 1:  // Spot
            safeSpotDefaultValues()
        default: // Manuel
            lblEndMode.text = sc.titleForSegment(at: 0)
            supervision.endingType = SupervisionEndingType.Manual
            lblSafeSpotAddress.isHidden = true
        }
        
        scEndMode.isHidden = true
        
        redrawRows()
    }
    
    @IBAction func activeChanged(_ sender: UISwitch) {
        supervision.status = sender.isOn ? SupervisionStatusType.Active : SupervisionStatusType.Inactive
    }
    
    fileprivate func safeSpotDefaultValues() {
        supervision.endingType = SupervisionEndingType.Spot
        lblEndMode.text = scEndMode.titleForSegment(at: 1)
        
        lblSafeSpotAddress.text = ""
        var location = CLLocationCoordinate2D.init(latitude: 55.7968950, longitude: 12.4696750) // Default Safespot
        if (supervision.locationLatitude != nil && supervision.locationLongitude != nil) {
            location = CLLocationCoordinate2D.init(latitude: supervision.locationLatitude!, longitude: supervision.locationLongitude!)
        } else {
            supervision.locationLatitude = location.latitude
            supervision.locationLongitude = location.longitude
        }
        
        self.model.getAddress(location: location) { address, error in
            guard address != nil && error == nil else {
                print("error = \(String(describing: error))")
                return
            }

            self.lblSafeSpotAddress.text = address!
        }
        
        lblSafeSpotAddress.isHidden = false
    }
    
    func redrawRows() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        })
    }
    
    @IBAction func btnNavigationRight(_ sender: Any) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let safeSpotViewController = segue.destination as! SafeSpotViewController
        
        if (supervision.locationLatitude != nil) {
            safeSpotViewController.initialLocation = CLLocation(latitude: supervision.locationLatitude!, longitude: supervision.locationLongitude!)
        } else {
            safeSpotViewController.initialLocation = CLLocation(latitude: 55.7968950, longitude: 12.4696750)
        }
        safeSpotViewController.supervisionEditTableViewController = self
        
        // self.hidesBottomBarWhenPushed = false
    }

}

enum SupervisionEditViewControllerMode : Int {
    case Update = 0, Add = 1, None = 2
}

enum RowNumber : Int {
    case Part1Delimiter = 0, GroupName, GroupSelector, MonitoredName, MonitoredNameSelector,
    Supervision_Type, TypeSwitch, Part2Delimiter, RepeatMode, RepeatSwitch,
    // Nu kommer 10:
    Interval, IntervalPicker, Tidspunkter, TidspunkterSwitch,
    Time1, Time1Picker, Time2, Time2Picker, Time3, Time3Picker,
    // Nu kommer 20:
    Countdown, CountdownPicker, Part3Delimiter, EndTime, EndTimePicker,
    AlarmTime, AlarmtimePicker,
    // Nu kommer 27:
    GPSTrack, GPSTrackSelector, Part4Delimiter, EndMode, EndModeSwitch,
    SafeSpotAddress, Active
}
