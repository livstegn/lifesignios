//
//  SupervisionListViewCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 28/12/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation
import UIKit

class SupervisionListViewCell: UITableViewCell {
    @IBOutlet weak var imgSupervisionType: UIImageView!
    @IBOutlet weak var lblMonitoredUser: UILabel!
    @IBOutlet weak var lblSupervisionSpecifications: UILabel!
    @IBOutlet weak var swSupervisionActive: UISwitch!
    
    @IBAction func switchActive(_ sender: UISwitch) {
        if (model != nil && supervision != nil) {
            self.supervision!.status = sender.isOn ? SupervisionStatusType.Active : SupervisionStatusType.Inactive
            
            model!.startOrStopTracking(supervision: supervision!)
            _ = model!.updateSupervision(supervision: supervision!)
        }
    }

    var supervision: Supervision?
    var model: Model?
}
