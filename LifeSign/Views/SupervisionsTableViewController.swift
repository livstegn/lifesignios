//
//  SupervisionsTableViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 28/12/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation
import UIKit
import BTNavigationDropdownMenu


class SupervisionsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityPopup: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    var supervisions = [Supervision]()
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    var deleteRowIndexPath: IndexPath? = nil
    var textStyleHeader : UIFont.TextStyle?
    
    let trace = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        trace.output("SupervisionsTableViewController viewDidLoad")
        //self.navigationItem.rightBarButtonItem = addButton
        //self.navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "lsHeadlineText")
        //self.navigationItem.rightBarButtonItem!.isEnabled = true
        self.tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        self.tabBarController?.navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "lsHeadlineText")

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 56.0
        
        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        let menuView = Helper.groupMenuView(navigationController: self.navigationController, model: self.model)
        self.tabBarController?.navigationItem.titleView = menuView

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trace.output("SupervisionsTableViewController viewWillAppear")
        
        trace.begin()
        
        self.reloadMenuItems()

        let menuView = self.tabBarController?.navigationItem.titleView as! BTNavigationDropdownMenu
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
            self.model.setCurrentCaregroup(index: indexPath)
            self.loadData()
        }
        self.showNavigationBar()
        self.loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showNavigationBar()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    func showNavigationBar () {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.tabBarController?.navigationItem.rightBarButtonItem?.isEnabled = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func reloadMenuItems () {
        self.model.getCareGroups()
        var items = [String]()
        let NoOfCaregroups = model.caregroups?.count ?? 0
        for index in 0..<NoOfCaregroups {
            items.append(model.caregroups?[index].caregroupName ?? "NA")
        }
        let menuView = self.tabBarController?.navigationItem.titleView as! BTNavigationDropdownMenu
        menuView.updateItems(items)
    }
    
    func loadData () {
        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        DispatchQueue.global(qos: .background).async {
            let currentCaregroup = self.model.getCurrentCaregroup()
            if (currentCaregroup == nil) {
                return
            }
            self.supervisions = self.model.getSupervisions(caregroupId: currentCaregroup!.caregroupId)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.spinner.stopAnimating()
                self.activityPopup.isHidden = true
                
                //self.trace.logger.printPerformanceLog()
            }
        }

        trace.output("No of groups: \(self.supervisions.count)")
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.supervisions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSupervisionList", for: indexPath) as! SupervisionListViewCell
        
        cell.supervision = self.supervisions[indexPath.row]
        cell.model = self.model

        if textStyleHeader == nil {
            textStyleHeader = (cell.lblMonitoredUser.font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.textStyle) as! UIFont.TextStyle)
        }
        cell.lblMonitoredUser.text = self.model.getCachedUserName(userid: cell.supervision!.userId)

        if (cell.supervision!.status == SupervisionStatusType.Active) {
            cell.swSupervisionActive.isOn = true
        } else {
            cell.swSupervisionActive.isOn = false
        }
        
        if (cell.supervision!.supervisionType == SupervisionType.Ongoing) {
            cell.imgSupervisionType.image = UIImage.init(named: "Ongoing.png")
            
            if (cell.supervision!.interval ?? -1 > -1) {
                let uiText = NSLocalizedString("Ongoing, fixed interval: #1# h.", comment: "List of supervisions, parameters for fixed interval supervision")
                let interval = Int((cell.supervision!.interval!) / 3600)
                cell.lblSupervisionSpecifications.text = uiText.replacingOccurrences(of: "#1#", with: interval.description)
            } else {
                if (cell.supervision!.deadlines() == 1) {
                    let uiText = NSLocalizedString("Daily #1#", comment: "List of supervisions, parameters for supervision with one daily deadline")
                    cell.lblSupervisionSpecifications.text = uiText.replacingOccurrences(of: "#1#", with: Helper.hhColonMm(timeofday: cell.supervision!.deadline1!))
                }
                else if (cell.supervision!.deadlines() == 2) {
                    var uiText = NSLocalizedString("Daily #1# and #2#", comment: "List of supervisions, parameters for supervision with two daily deadlines")
                    uiText = uiText.replacingOccurrences(of: "#1#", with: Helper.hhColonMm(timeofday: cell.supervision!.deadline1!))
                    uiText = uiText.replacingOccurrences(of: "#2#", with: Helper.hhColonMm(timeofday: cell.supervision!.deadline2!))
                    cell.lblSupervisionSpecifications.text = uiText
                }
                else if (cell.supervision!.deadlines() == 3) {
                    var uiText = NSLocalizedString("Daily #1#, #2# and #3#", comment: "List of supervisions, parameters for supervision with three daily deadlines")
                    uiText = uiText.replacingOccurrences(of: "#1#", with: Helper.hhColonMm(timeofday: cell.supervision!.deadline1!))
                    uiText = uiText.replacingOccurrences(of: "#2#", with: Helper.hhColonMm(timeofday: cell.supervision!.deadline2!))
                    uiText = uiText.replacingOccurrences(of: "#3#", with: Helper.hhColonMm(timeofday: cell.supervision!.deadline3!))
                    cell.lblSupervisionSpecifications.text = uiText
                }

            }
        } else {
            cell.imgSupervisionType.image = UIImage.init(named: "Temporary.png")
            
            if (cell.supervision!.endingType == SupervisionEndingType.Manual) {
                let uiText = NSLocalizedString("#1#, Lifesign by button", comment: "List of supervisions, parameters for temporary supervision with manual stop")
                cell.lblSupervisionSpecifications.text = uiText.replacingOccurrences(of: "#1#", with: Helper.simpleUITime(date: cell.supervision!.periodEnd!))
            } else if (cell.supervision!.endingType == SupervisionEndingType.Wifi) {
                let uiText = NSLocalizedString("#1#, Wifi: #2#", comment: "List of supervisions, parameters for temporary supervision with wifi stop")
                cell.lblSupervisionSpecifications.text = uiText.replacingOccurrences(of: "#1#", with: Helper.simpleUITime(date: cell.supervision!.periodEnd!))
            } else if (cell.supervision!.endingType == SupervisionEndingType.Spot) {
                let uiText = NSLocalizedString("#1#, Lifesign by location", comment: "List of supervisions, parameters for temporary supervision with location based stop")
                cell.lblSupervisionSpecifications.text = uiText.replacingOccurrences(of: "#1#", with: Helper.simpleUITime(date: cell.supervision!.periodEnd!))
            }
        }
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        cell.setNeedsLayout()

        return cell
    }
    
    // DELETE
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            deleteRowIndexPath = indexPath
            confirmDelete()
        }
    }
    
    func confirmDelete() {
        let loTitle = NSLocalizedString("Delete Supervision", comment: "Popup before deleting a supervision: Title")
        let loMessage = NSLocalizedString("Are you sure you want to permanently delete this supervision, and all related checkpoints and alarms?", comment: "Popup before deleting a supervision: Message")
        let loButtonDelete = NSLocalizedString("Delete", comment: "Popup before deleting - button label")
        let loButtonCancel = NSLocalizedString("Cancel", comment: "Popup before deleting - button label")
        let alert = UIAlertController(title: loTitle, message: loMessage, preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: loButtonDelete, style: .destructive, handler: handleDeleteRow)
        let CancelAction = UIAlertAction(title: loButtonCancel, style: .cancel, handler: cancelDeleteRow)
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleDeleteRow(alertAction: UIAlertAction!) -> Void {
        if let indexPath = deleteRowIndexPath {
            let supervisionToDelete = self.supervisions[indexPath.row]

            trace.output("Delete supervision from groupmember")
            trace.output(supervisionToDelete.userId)
            trace.output(supervisionToDelete.groupmemberId)
            trace.output(supervisionToDelete.supervisionId)

            let rc = model.deleteSupervision(supervision: supervisionToDelete)
            trace.output(rc!)
            
            supervisions.remove(at: indexPath.row)
            self.loadData()
            
            deleteRowIndexPath = nil
        }
    }
    
    func cancelDeleteRow(alertAction: UIAlertAction!) {
        deleteRowIndexPath = nil
    }

    @IBAction func btnAdd(_ sender: Any) {
        performSegue(withIdentifier: "addSupervision", sender: self)
    }
  
    @objc func addTapped(sender: AnyObject) {
        performSegue(withIdentifier: "addSupervision", sender: self)
    }
    
    // SEQUE's
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let supervisionEditTableViewController = segue.destination as! SupervisionEditTableViewController
        
        let backItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: nil)
        backItem.title = NSLocalizedString("Cancel", comment: "Back button label in list of supervision parameters")
        navigationItem.backBarButtonItem = backItem
        
        switch segue.identifier! {
        case "updateSupervision":
            supervisionEditTableViewController.mode = SupervisionEditViewControllerMode.Update
            if let cell = sender as? SupervisionListViewCell {
                supervisionEditTableViewController.supervision = cell.supervision!
                supervisionEditTableViewController.caregroup = self.model.getCurrentCaregroup()!
            }
        case "addSupervision":
            supervisionEditTableViewController.caregroup = self.model.getCurrentCaregroup()!
            supervisionEditTableViewController.mode = SupervisionEditViewControllerMode.Add
            supervisionEditTableViewController.supervision = Supervision.init()
            supervisionEditTableViewController.supervision.userId = model.user!.userId
            supervisionEditTableViewController.supervision.supervisionType = SupervisionType.Ongoing
            supervisionEditTableViewController.supervision.countdownTime = 30*60  // 00:30
            supervisionEditTableViewController.supervision.deadline1 = 9*60*60 // 09:00
            supervisionEditTableViewController.supervision.deadline2 = nil
            supervisionEditTableViewController.supervision.deadline3 = nil
            supervisionEditTableViewController.supervision.interval = nil
            supervisionEditTableViewController.supervision.status = SupervisionStatusType.Active
            supervisionEditTableViewController.supervision.caregroupId = supervisionEditTableViewController.caregroup.caregroupId

            for member in model.getGroupMembers(caregroupId: supervisionEditTableViewController.caregroup.caregroupId) {
                if (member.userid == supervisionEditTableViewController.supervision.userId && member.status == GroupMemberStatusType.Active) {
                    supervisionEditTableViewController.supervision.groupmemberId = member.groupmemberId
                }
            }
        default:
            break
        }
        
        self.hidesBottomBarWhenPushed = false
    }
}
