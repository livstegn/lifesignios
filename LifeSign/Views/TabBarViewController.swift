//
//  TabBarViewController.swift
//  LifeSign
//
//  Created by Simon Jensen on 17/05/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import UIKit

import AWSCore
import AWSSNS
import AWSCognitoIdentityProvider


class TabBarViewController: UITabBarController {
    let logger = (UIApplication.shared.delegate as! AppDelegate).logger
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let notificationHandler = (UIApplication.shared.delegate as! AppDelegate).notificationHandler
    
    let trace = AllTraces()
    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled

    override func viewDidLoad() {
        super.viewDidLoad()
        
        trace.begin()

        self.trace.output( "TabBarViewController viewDidLoad() start")
        // Do any additional setup after loading the view.
        
        self.notificationHandler.userIsSignedIn()
        self.trace.output( "TabBarViewController efter: notificationHandler.userIsSignedIn()")

        if (self.model.ignoreInvitation == false && self.model.haveInvitations()) {
            self.trace.output( "TabBarViewController efter: model.haveInvitations()")
            trace.output("[self.model.getInvitations()].count = \([self.model.getInvitations()].count)")
            let actionsheet = UIAlertController(title: "Invitationer", message: "Du har invitioner som venter på dig?", preferredStyle: UIAlertController.Style.actionSheet)
            
            actionsheet.addAction(UIAlertAction(title: NSLocalizedString("Review now",
            comment: "Selection - Review invitation now?"), style: UIAlertAction.Style.default, handler: { (action) -> Void in
                self.performSegue(withIdentifier: "tabview2invitation", sender: self)
            }))
            actionsheet.addAction(UIAlertAction(title: NSLocalizedString("Later",
            comment: "Selection - Review invitation later?"), style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
                self.model.ignoreInvitation = true
            }))
            
            self.present(actionsheet, animated: true)
        }
        
        /*
        Timer.scheduledTimer(timeInterval: 15.0,
                             target: self,
                             selector: #selector(self.checkUserEvents),
                             userInfo: nil,
                             repeats: true)
        */
        
        self.trace.output( "TabBarViewController viewDidLoad() slut")

        trace.end()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        trace.begin()
        
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        moreNavigationController.view.tintColor = UIColor(named: "lsBigButtonBackgroundMain")
        moreNavigationController.navigationBar.setValue(true, forKey: "hidesShadow")
        moreNavigationController.navigationBar.barTintColor = UIColor(named: "lsViewBackground")
        moreNavigationController.navigationBar.tintColor = UIColor(named: "lsTextFontStandard")
        moreNavigationController.tabBarController?.customizableViewControllers = []
       
        // Check settings bundle. If Activity update is enabled, then check for activity
        if(!model.debugPreference()) {
            let tabs = self.viewControllers
            for index in 0..<tabs!.count {
                let tab = tabs![index]
                trace.output(tab.title ?? "intet navn")
                if tab.isKind(of: DebugViewController.self){
                    self.viewControllers?.remove(at: index)
                }
            }
        }

        //nedenstående linje SKAL komme efter ovenstående remove viewcontroller kode...
        moreNavigationController.tabBarController?.customizableViewControllers = []

        trace.end()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        trace.begin()
        
        super.viewDidAppear(animated)

        trace.end()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        trace.begin()
        
        super.viewWillDisappear(animated)
        
        trace.end()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        trace.begin()
        
        super.viewDidDisappear(animated)
        
        trace.end()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    /*
    @objc func checkUserEvents() {
        
        trace.begin()
        //trace.error("TIMER!")
        trace.end()
    }
    */

}
