//
//  TrackingViewCell.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 20/09/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class TrackingViewCell: UITableViewCell {
    @IBOutlet weak var lblPeriodStartTime: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblEventUser: UILabel!
    @IBOutlet weak var lblEventText: UILabel!

    var supervision = Supervision()
    var tracking = Tracking()
}
