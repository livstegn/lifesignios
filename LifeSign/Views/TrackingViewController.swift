//
//  TrackingViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 12/09/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import UIKit
import MapKit

class TrackingViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet private var mapView: MKMapView!
    let model = (UIApplication.shared.delegate as! AppDelegate).model
    var supervision : Supervision?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self

        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        // Find lokations
        var locations = model.getLocations(supervision: supervision!)
        locations.sort { $0.time < $1.time }

        var annotations = [MKPointAnnotation]()

        var locationCoordinates = [CLLocationCoordinate2D]()
        for location in locations {
            let loc2d = CLLocationCoordinate2DMake(location.latitude, location.longitude)
            locationCoordinates.append(loc2d)
            
            var annotation = MKPointAnnotation()
            annotation.coordinate = loc2d
            annotation.title = Helper.uiHoursMinutes(date: location.time)
            annotations.append(annotation)
        }
        
        if locationCoordinates.count > 0 {
            var minLat : Double = locations.first!.latitude
            var maxLat : Double = locations.first!.latitude
            var minLong : Double = locations.first!.longitude
            var maxLong : Double = locations.first!.longitude

            for location in locationCoordinates {
                if (location.latitude < minLat) {
                    minLat = location.latitude
                } else if (location.latitude > maxLat) {
                    maxLat = location.latitude
                }
                
                if (location.longitude < minLong) {
                    minLong = location.longitude
                } else if (location.longitude > maxLong) {
                    maxLong = location.longitude
                }
            }
            let deltaLat = maxLat - minLat
            let deltaLong = maxLong - minLong
            let middleLat = minLat + deltaLat / 2
            let middleLong = minLong + deltaLong / 2
            let mapCenter = CLLocationCoordinate2DMake(middleLat, middleLong)

            mapView.centerCoordinate = mapCenter
            // Think of a span as a tv size, measure from one corner to another
            let span = MKCoordinateSpan(latitudeDelta: deltaLat * 1.4, longitudeDelta: deltaLong * 1.4)
            let region = MKCoordinateRegion(center: mapCenter, span: span)

            mapView.region = region
            mapView.addOverlay(MKPolyline(coordinates: locationCoordinates, count: locationCoordinates.count))
            mapView.addAnnotations(annotations)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
      if overlay is MKPolyline {
        let lineView = MKPolylineRenderer(overlay: overlay)
        lineView.strokeColor = .blue
        lineView.lineWidth = 2
        return lineView
      }

      return MKOverlayRenderer()
    }


}


