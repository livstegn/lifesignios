//
//  TrackingsTableViewController.swift
//  LifeSign
//
//  Created by Peter Rosendahl on 20/09/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation
import UIKit

class TrackingsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityPopup: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var trackings : [Date:Tracking] = [:]
    var lastLocations : [Date:Location] = [:]
    var supervisions : [String:Supervision] = [:]
    var trackingsKeys : [Date] = []
    var textStyleHeader : UIFont.TextStyle?

    let model = (UIApplication.shared.delegate as! AppDelegate).model
    let trace  = NoTraces()    // Wrapper for traces when DEBUG is defined. Traces() = trace enabled / NoTraces() = traces disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        trace.output("TrackingsTableViewController viewDidLoad")

        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trace.output("TrackingsTableViewController viewWillAppear")
        trace.begin()

        self.showNavigationBar()

        let menuView = Helper.groupMenuView(navigationController: self.tabBarController?.moreNavigationController, model: self.model)
        
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
            self.model.setCurrentCaregroup(index: indexPath)
            self.loadData()
        }
        self.navigationItem.titleView = menuView
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
        self.showNavigationBar()
    }
    
    func showNavigationBar () {
        self.tabBarController?.moreNavigationController.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.moreNavigationController.navigationItem.hidesBackButton = false
        self.tabBarController?.tabBar.isHidden = false
    }
 
    func loadData () {
        self.spinner.startAnimating()
        self.activityPopup.isHidden = false
        
        DispatchQueue.global(qos: .default).async {
            self.model.getCareGroups()
            self.trace.user(self.model.user)    // Temporary check to verify model.user != nil

            let cg = self.model.getCurrentCaregroup()

            if (cg != nil) {
                let allSupervisions = self.model.getSupervisions(caregroupId: cg!.caregroupId)
                for supervision in allSupervisions {
                    if let trackingid = supervision.trackingId {
                        if let tracking = self.model.getTracking(trackingId: trackingid) {
                            self.trackings[tracking.starttime] = tracking
                            self.supervisions[supervision.supervisionId] = supervision
                            
                            if tracking.latestLocationId != nil {
                                self.lastLocations[tracking.starttime] = self.model.getLocation(trackingId: trackingid, locationsId: tracking.latestLocationId!)
                            }
                        }
                    }
                }

                self.trackingsKeys = self.trackings.keys.sorted(by: >)
                self.model.clearCachedUserNames()
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.trace.user(self.model.user)    // Temporary check to verify model.user != nil
                self.spinner.stopAnimating()
                self.activityPopup.isHidden = true
            }
        }
        
        trace.output("No of groups: \(self.trackingsKeys.count)")
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.trackingsKeys.count
    }

    func cellFor (tableView: UITableView, cellForRowAt indexPath: IndexPath) -> TrackingViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellTracking") as! TrackingViewCell
        let key = trackingsKeys[indexPath.row]
        cell.tracking = self.trackings[key]!
        cell.supervision = supervisions[cell.tracking.supervisionId]!
        let lastLocation : Location? = lastLocations[key]

        /*
         Tracking      Cell
         Status        Icon      Message
         ------------  -----------   -------   ---------------------------------
         Upcoming      Clock     Upcoming tracking
         Active        Location  Tracking active
         Ended         Check     Ended at dd/mm hh:mm:ss

         */

        switch cell.tracking.status {
        case .Upcoming:
            cell.imgStatus.image = UIImage.init(systemName: "timer")
            cell.lblEventText.text = NSLocalizedString("Upcoming tracking", comment: "List of trackings, showing status for an upcoming tracking")
        case .Active:
            cell.imgStatus.image = UIImage(systemName: "location.fill")
            cell.lblEventText.text = NSLocalizedString("Tracking active", comment: "List of trackings, showing status for an active tracking")
        default:
            cell.imgStatus.image = UIImage(systemName: "checkmark.circle.fill")
            if lastLocation == nil {
                cell.lblEventText.text = NSLocalizedString("Tracking ended", comment: "List of trackings, showing status for an ended tracking")
            }
            else {
                let uiText = NSLocalizedString("Tracking ended #1#", comment: "List of trackings, showing status and end time for an ended tracking")
                cell.lblEventText.text = uiText.replacingOccurrences(of: "#1#", with: Helper.uiTime(date: lastLocation!.time))
            }
        }

        cell.lblEventUser.text = model.getCachedUserName(userid: cell.supervision.userId)
        
        if ((tableView.frame.width - cell.lblEventText.frame.size.width) < 120) {
            cell.lblEventText.frame.size.height = cell.lblEventText.frame.size.height * 2.2
        }

        cell.setNeedsLayout()

        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cellFor(tableView: tableView, cellForRowAt: indexPath)

        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let trackingViewController = segue.destination as! TrackingViewController
        
        if let cell = sender as? TrackingViewCell {
            trackingViewController.supervision = cell.supervision
        }
        
    }
}
