//
//  UITextFieldWithPadding.swift
//  LifeSign
//
//  Created by Simon Jensen on 26/12/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class UITextFieldWithPadding: UITextField {
    var textPadding = UIEdgeInsets(
        top: 7,
        left: 8,
        bottom: 7,
        right: 8
    )

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
}
