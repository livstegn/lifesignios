//
//  CloudMuck
//  LifeSign
//
//  Created by Peter Rosendahl on 29/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation
@testable import LifeSign

class CloudMuck: CloudLayer {
    func deleteCheckpoints(supervisionid: String) -> String? {
        return nil
    }
    
    func getMainStatusList(userId: String, caregroupId: String) -> [MainStatus] {
        return [MainStatus]()
    }
    
    func addCheckpoint(checkpoint: Checkpoint) -> String {
        return ""
    }
    
    func pushFeedback(userId: String, caregroupId: String, fromUser: String) {
    }
    
    func getAppStatus(userid: String) -> Status? {
        return nil
    }
    
    func setAppStatus(userid: String, status: AppStatusType) -> Status? {
        return nil
    }
    
    func getLocation(trackingId: String, locationsId: String) -> Location? {
        return nil
    }
    
    func addLifesign(userid: String, userMinutesBeforeUTC: Int) -> String {
        return ""
    }
    
    func getSupervision(groupmemberId: String, supervisionId: String) -> Supervision? {
        return nil
    }
    
    func updateTracking(tracking: Tracking) -> String? {
        return nil
    }
    
    func getTracking(trackingid: String) -> Tracking? {
        return nil
    }
    
    func addTracking(tracking: Tracking) -> String? {
        return nil
    }
    
    func getTrackings(supervision: Supervision) -> [Tracking] {
        return [Tracking]()
    }
    
    func setupCredentials(authMethod: AuthenticationMethod) -> Bool {
        return true
    }
    
    func setAuthorizationToken(token: String?) {
    }
    
    func updateCaregroup(userid: String, caregroup: Caregroup) -> String? {
        return ""
    }
    
    func updateGroupmember(groupmember: GroupMember) -> GroupMember? {
        return groupmember
    }
    
    func deleteSupervision(supervision: Supervision) -> String? {
        return supervision.supervisionId
    }
    
    func getLocations(supervision: Supervision) -> [Location] {
        return [Location]()
    }
    
    func addLocation(location: Location) -> String? {
        return ""
    }
    
    func getGroupInvitation(userId: String, invitationId: String) -> Invitation? {
        return nil
    }
    
    func getGroupInvitations(userId: String) -> [Invitation]? {
        return nil
    }
    
    func newsignup(userName: String, email: String, password: String) -> SignupResult {
        return SignupResult()
    }
    
    func deleteCognitoUser() -> NSError? {
        return nil
    }
    
    func createUser(user: User) -> User {
        return User()
    }
    
    func updateUser(userid: String, user: User) -> User {
        return User()
    }
    
    func getUser(userid: String) -> User? {
        return User()
    }
    
    func deleteUser(userid: String) -> String? {
        return ""
    }
    
    func addCaregroup(userid: String, caregroup: Caregroup) -> String? {
        return ""
    }
    
    func deleteCaregroup(caregroupid: String) -> String? {
        return ""
    }
    
    func addLifesign(userid: String, groupmemberid: String, supervisionid: String) -> String {
        return ""
    }
    
    func getCaregroups(userid: String) -> [Caregroup]? {
        return nil
    }
    
    func getCaregroup(caregroupid: String) -> Caregroup? {
        return nil
    }
    
    func addGroupmember(userId: String, status: GroupMemberStatusType, caregroupId: String, memberId: String, isMonitor: Bool, isMonitored: Bool, isOwner: Bool) -> String? {
        return nil
    }
    
    func removeGroupmember(userId: String, caregroupId: String, memberId: String) -> String? {
        return nil
    }
    
    func updateGroupmember(userId: String, caregroupId: String, status: GroupMemberStatusType) -> GroupMember? {
        return nil
    }
    
    func getGroupmember(userId: String, caregroupId: String) -> GroupMember? {
        return nil
    }
    
    func getGroupmembers(caregroupId: String) -> [GroupMember]? {
        return nil
    }
    
    func addGroupMessage(userid: String, caregroupId: String, messagetext: String) -> String? {
        return nil
    }
    
    func getGroupMessages(caregroupId: String) -> [Message]? {
        return nil
    }
    
    func getMessageGroup(messageId: String) -> String? {
        return nil
    }
    
    func addSupervision(supervision: Supervision) -> String? {
        return nil
    }
    
    func updateSupervision(supervision: Supervision) -> String? {
        return nil
    }
    
    func getSupervisions(groupmemberId: String) -> [Supervision]? {
        return nil
    }
    
    func getCheckpoints(supervisionid: String) -> [Checkpoint]? {
        return nil
    }
    
    func addGroupInvitation(fromUserId userId: String, toUserId caregroupId: String, caregroupId senderId: String, invitation: String) -> String? {
        return nil
    }
    
    func getLifesigns(userid: String) -> [LifeSign]? {
        return nil
    }
    
    func getAlarms(supervisionid: String) -> [Alarm]? {
        return nil
    }
    
    func updateGroupInvitation(userId: String, invitationId: String, status: InvitationStatusType) -> Invitation {
        return Invitation()
    }
    
    func pushGroupInvitation(userId: String, invitationId: String, fromUser: String, toGroup: String) {
    }
    
    func haveGroupInvitations(userId: String) -> Bool {
        return false
    }
    
    func getGroupInvitation(invitationId: String) -> Invitation? {
        return nil
    }
    /*
    func getGroupInvitations(userId: String) -> [Invitation]? {
        return nil
    }
    */
    func signup(userName: String, email: String, password: String) -> SignupResult {
        return SignupResult()
    }
    
    func forgotPassword(email: String) -> NSError? {
        return nil
    }
    
    func confirmForgotPassword(email: String, confirmationCodeValue: String, password: String) -> NSError? {
        return nil
    }
    
    func changeEmail(email: String, newEmail: String) -> NSError? {
        return nil
    }
    
    func confirmNewEmail(email: String, code: String) -> NSError? {
        return nil
    }
    
    func changePassword(email: String, password: String, newPassword: String) -> NSError? {
        return nil
    }
    
    func isCloudSessionSignedIn() -> Bool {
        return false
    }
    
    func snsAuthneticateUser(userId: String, email: String) {
    }
    
    func snsCreatePlatformARN(userId: String, email: String) -> String? {
        return nil
    }
    
    func snsCreateEndpointARN(token: String, platformARN: String) -> String? {
        return nil
    }
    
    func snsCreateTopicARN(userID: String) -> String? {
        return nil
    }
    
    func snsCreateSubscriptionARN(endpointARN: String, topicARN: String) -> String? {
        return nil
    }
    
    var user : User?
    func signIn(email: String, password: String) -> NSError? {
        return nil
    }

    func signOut() {
    }
    
    func getUserSessionState() -> UserSessionStatusType {
        return UserSessionStatusType.Unknown
    }

    func getUserID(email: String) -> String? {
        return nil
    }

    func getUsers() -> [User] {
        return[User]()
    }

    func getCloudSessionIdToken() -> String? {
        return nil
    }

    func getCloudSessionUserName() -> String? {
        return nil
    }
}
