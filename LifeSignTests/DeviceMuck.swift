//
//  DeviceMuck.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 29/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation
import CoreLocation
@testable import LifeSign

class DeviceMuck: DeviceLayer {
    func savePasswordVisible(status: Bool) { }
    
    func getPasswordVisible() -> Bool? {
        return false
    }
    
    func getCoordinates(address: String) -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D.init()
    }
    
    func getAddress(location: CLLocationCoordinate2D) -> String {
        return ""
    }
    
    func debugPreference() -> Bool {
        return false
    }
    
    func activityPreference() -> Bool {
        return false
    }
    
    func startupPreference() -> Bool {
        return false
    }
    
    var user : User?
    
    func savelocalUser(user: User) -> Bool {
        self.user = user
        
        return true
    }
    
    func loadlocalUser() -> User? {
        return user
    }
    
    func getNewEmail() -> String? {return ""}
    func getEmail() -> String? {return ""}
    func getPassword() -> String? {return ""}
    func getDeviceToken() -> String? {return ""}
    func getUserName() -> String? {return ""}

    func saveNewEmail(email: String) {}
    func saveEmail(email: String) {}
    func savePassword(password: String) {}
    func saveUserName(name: String) {}
    func saveDeviceToken(token: String) {}
    
    
    func getInvitationIds() -> [String]? {return [String]()}
    func getAuthMethod() -> AuthenticationMethod {return AuthenticationMethod.Unknown}
    func saveAuthMethod(method: AuthenticationMethod) {}
    func saveInvitationIds(invitations: [String]) {}
    func getCurrentWifiName() -> String {return ""}
    func getLocationManager() -> CLLocationManager { return CLLocationManager()}
}


