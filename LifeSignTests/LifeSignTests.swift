//
//  LifeSignTests.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 29/04/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import XCTest
@testable import LifeSign

class LifeSignTests: XCTestCase {

    func testHelperIntervalFromTimer_10_40() {
        let i = Helper.intervalFromTime(time: "10:40")
        XCTAssertEqual(i, 38400)
    }

    func testHelperIntervalFromTimer_10_61() {
        let i = Helper.intervalFromTime(time: "10:61")
        XCTAssertEqual(i, -1)
    }

    func testHelperIntervalFromTimer_24_00() {
        let i = Helper.intervalFromTime(time: "24:00")
        XCTAssertEqual(i, 86400)
    }
    
    func testHelperIntervalFromTimer_blank() {
        let i = Helper.intervalFromTime(time: "")
        XCTAssertEqual(i, -1)
    }

    func testHelperIntervalFromTimer_XX_40() {
        let i = Helper.intervalFromTime(time: "XX:40")
        XCTAssertEqual(i, -1)
    }

    func testHelperdateFromUTC() {
        let date = Helper.dateFromUTC(utc: "2020-09-12T17:38:39.043Z")
        let dateStr = Helper.uiTime(date: date!)// dd/MM HH:mm
        
        XCTAssertEqual(dateStr, "12/09 19:38")
    }
}
