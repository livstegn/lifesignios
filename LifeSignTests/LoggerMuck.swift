//
//  LoggerMuck.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 05/11/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import Foundation
@testable import LifeSign

class LoggerMuck: Logger {
    func clearPerformanceLog() {

    }
    
    func addPerformanceLog(text: String) {
    }
    
    func printPerformanceLog() {
    }
    
    func addCloudAPILog(start: Date, text: String) {
    }
    
    func printCloudAPILog() {
    }
}

