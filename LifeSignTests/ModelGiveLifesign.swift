//
//  ModelGiveLifesign.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 22/03/2021.
//  Copyright © 2021 Simon Jensen. All rights reserved.
//

import XCTest
import CoreLocation
@testable import LifeSign

class ModelGiveLifesign: XCTestCase {
    let calendar = Calendar.current
    private let support = SetupGiveLifesign()

    func testSimon1Ongoing() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDate = calendar.date(byAdding: .hour, value: -8, to: now)
        let nextLifesignDate = calendar.date(byAdding: .hour, value: 25, to: lifesignDate!)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)

        support.simonMonitored1Ongoing (model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)
        let preLifesignCount = clm.getLifesigns(userid: model.user!.userId)?.count

        // Act
        model.giveLifesignNew(lifesignTime: now)
        let postLifesignCount = clm.getLifesigns(userid: model.user!.userId)?.count

        // Assert
        XCTAssertEqual(model.stateLastLifesign, now)
        XCTAssertEqual(clm.remoteFeedbacks.count, 1)
        XCTAssertEqual(preLifesignCount!, (postLifesignCount! - 1))

        let feedback = clm.remoteFeedbacks.first
        
        XCTAssertEqual(feedback!.fromUser, "Simon")
        XCTAssertEqual(feedback!.userId, "U-1-1")
        XCTAssertEqual(feedback!.groupId, "CG-1")
    }
    
    func testSimon2Ongoing() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDate = calendar.date(byAdding: .hour, value: -8, to: now)
        let nextLifesignDate = calendar.date(byAdding: .hour, value: 25, to: lifesignDate!)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)

        support.simonMonitored2Ongoing (model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)
        let preLifesignCount = clm.getLifesigns(userid: model.user!.userId)?.count

        // Act
        model.giveLifesignNew(lifesignTime: now)
        let postLifesignCount = clm.getLifesigns(userid: model.user!.userId)?.count

        // Assert
        XCTAssertEqual(model.stateLastLifesign, now)
        XCTAssertEqual(clm.remoteFeedbacks.count, 2)
        XCTAssertEqual(preLifesignCount!, (postLifesignCount! - 1))

        var cg1Count = 0
        var cg2Count = 0

        for feedback in clm.remoteFeedbacks {
            XCTAssertEqual(feedback.fromUser, "Simon")
            XCTAssertEqual(feedback.userId, "U-1-1")
            
            if (feedback.groupId == "CG-1") {
                cg1Count = cg1Count + 1
            }
            if (feedback.groupId == "CG-2") {
                cg2Count = cg2Count + 1
            }

        }
        
        XCTAssertEqual(cg1Count, 1)
        XCTAssertEqual(cg2Count, 1)
    }

    func testSimon2Ongoing1LatePeriod() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let endDate = calendar.date(byAdding: .hour, value: 5, to: startDate!)
        let lifesignDate = calendar.date(byAdding: .hour, value: -8, to: now)
        let nextLifesignDate = calendar.date(byAdding: .hour, value: 25, to: lifesignDate!)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)

        support.simonMonitored2Ongoing1Period(model: model, clm: clm, startDate: startDate, endDate: endDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)

        let preLifesignCount = clm.getLifesigns(userid: model.user!.userId)?.count
        
        // Act
        model.giveLifesignNew(lifesignTime: now)
        let postLifesignCount = clm.getLifesigns(userid: model.user!.userId)?.count

        // Assert
        XCTAssertEqual(model.stateLastLifesign, now)
        XCTAssertEqual(clm.remoteFeedbacks.count, 2)
        XCTAssertEqual(preLifesignCount!, (postLifesignCount! - 1))
        
        var cg1Count = 0
        var cg2Count = 0

        for feedback in clm.remoteFeedbacks {
            XCTAssertEqual(feedback.fromUser, model.user!.userName)
            XCTAssertEqual(feedback.userId, model.user!.userId)
            
            if (feedback.groupId == "CG-1") {
                cg1Count = cg1Count + 1
            }
            if (feedback.groupId == "CG-2") {
                cg2Count = cg2Count + 1
            }

        }
        
        XCTAssertEqual(cg1Count, 1)
        XCTAssertEqual(cg2Count, 1)
    }
}

private class SetupGiveLifesign : NSObject {
    
    func simonMonitored1Ongoing (model: Model, clm: clmModelFunctions, startDate: Date?, lifesignDate: Date?, countdownDate: Date?, nextLifesignDate: Date?) {

        let offset = 120
        let simon = clm.createUser(user: User("U-1-1","Simon","simonjensen@privat.dk","555-555-555", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        model.user = simon
        let bo = clm.createUser(user: User("U-1-2","Bo","bdp@mail.dk","555-555-555", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))

        print("nextLifesignDate = \(String(describing: nextLifesignDate?.description))")
        print("countdownDate = \(String(describing: countdownDate?.description))")
        let uiNext = Helper.simpleUITimeNoDate(date: nextLifesignDate!)
        let countdownTime = nextLifesignDate!.timeIntervalSince(countdownDate!)
        var deadlineTime = Helper.intervalFromTime(time: uiNext) + Double(offset * 60)
        if (deadlineTime > 86400) {
            deadlineTime = deadlineTime - 86400
        }

        let gnGroup = Caregroup("CG-1", "Bo, Peter og Simon", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: simon.userId, caregroup: gnGroup)
        let simonGMid = model.addGroupmember(userId: simon.userId, status: GroupMemberStatusType.Active, caregroupId: gnGroup.caregroupId, memberId: "GM-1-1", isMonitor: false, isMonitored: true, isOwner: true)
        _ = model.addGroupmember(userId: bo.userId, status: GroupMemberStatusType.Active, caregroupId: gnGroup.caregroupId, memberId: "GM-1-2", isMonitor: true, isMonitored: false, isOwner: false)
        let simonSupervision = Supervision(simonGMid!, "S-1-1", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, simon.userId, gnGroup.caregroupId)
        _ = model.addSupervision(supervision: simonSupervision)

        let otteGroup = Caregroup("CG-2", "Ottetallet", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: simon.userId, caregroup: otteGroup)
        _ = model.addGroupmember(userId: simon.userId, status: GroupMemberStatusType.Active, caregroupId: otteGroup.caregroupId, memberId: "GM-2-1", isMonitor: true, isMonitored: false, isOwner: true)
                                 

        clm.clearCheckpoints()
        _ = model.updateCheckpoints(supervision: simonSupervision, lifesign: true, now: lifesignDate!)
        clm.addLifeSign(lifesign: LifeSign(simon.userId, "L-1-2-1", lifesignDate!, "", offset))
    }

    func simonMonitored2Ongoing (model: Model, clm: clmModelFunctions, startDate: Date?, lifesignDate: Date?, countdownDate: Date?, nextLifesignDate: Date?) {

        let offset = 120
        let simon = clm.createUser(user: User("U-1-1","Simon","simonjensen@privat.dk","555-555-551", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        model.user = simon
        let bo = clm.createUser(user: User("U-1-2","Bo","bdp@mail.dk","555-555-552", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let peter = clm.createUser(user: User("U-1-3","Peter","peter@olesvej20.dk","555-555-553", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let ac = clm.createUser(user: User("U-2-1","Ann Cathrine","ac@ottetallet.dk","555-555-554", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))

        print("nextLifesignDate = \(String(describing: nextLifesignDate?.description))")
        print("countdownDate = \(String(describing: countdownDate?.description))")
        let uiNext = Helper.simpleUITimeNoDate(date: nextLifesignDate!)
        let countdownTime = nextLifesignDate!.timeIntervalSince(countdownDate!)
        var deadlineTime = Helper.intervalFromTime(time: uiNext) + Double(offset * 60)
        if (deadlineTime > 86400) {
            deadlineTime = deadlineTime - 86400
        }

        let gnGroup = Caregroup("CG-1", "Bo, Peter og Simon", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: simon.userId, caregroup: gnGroup)
        let simonGMid = model.addGroupmember(userId: simon.userId, status: GroupMemberStatusType.Active, caregroupId: gnGroup.caregroupId, memberId: "GM-1-1", isMonitor: false, isMonitored: true, isOwner: true)
        _ = model.addGroupmember(userId: bo.userId, status: GroupMemberStatusType.Active, caregroupId: gnGroup.caregroupId, memberId: "GM-1-2", isMonitor: true, isMonitored: false, isOwner: false)
        _ = model.addGroupmember(userId: peter.userId, status: GroupMemberStatusType.Active, caregroupId: gnGroup.caregroupId, memberId: "GM-1-3", isMonitor: true, isMonitored: false, isOwner: false)
        let simonSupervisionGN = Supervision(simonGMid!, "S-1-1", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, simon.userId, gnGroup.caregroupId)
        _ = model.addSupervision(supervision: simonSupervisionGN)

        let otteGroup = Caregroup("CG-2", "Ottetallet", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: simon.userId, caregroup: otteGroup)
        _ = model.addGroupmember(userId: simon.userId, status: GroupMemberStatusType.Active, caregroupId: otteGroup.caregroupId, memberId: "GM-2-1", isMonitor: false, isMonitored: true, isOwner: true)
        _ = model.addGroupmember(userId: ac.userId, status: GroupMemberStatusType.Active, caregroupId: otteGroup.caregroupId, memberId: "GM-2-2", isMonitor: true, isMonitored: false, isOwner: true)
        let simonSupervision8tal = Supervision(simonGMid!, "S-2-1", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, simon.userId, otteGroup.caregroupId)
        _ = model.addSupervision(supervision: simonSupervision8tal)

        clm.clearCheckpoints()
        _ = model.updateCheckpoints(supervision: simonSupervisionGN, lifesign: true, now: lifesignDate!)
        _ = model.updateCheckpoints(supervision: simonSupervision8tal, lifesign: true, now: lifesignDate!)
        clm.addLifeSign(lifesign: LifeSign(simon.userId, "L-1-2-1", lifesignDate!, "", offset))
    }

    func simonMonitored2Ongoing1Period (model: Model, clm: clmModelFunctions, startDate: Date?, endDate: Date?, lifesignDate: Date?, countdownDate: Date?, nextLifesignDate: Date?) {

        let offset = 120
        let simon = clm.createUser(user: User("U-1-1","Simon","simonjensen@privat.dk","555-555-551", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        model.user = simon
        let bo = clm.createUser(user: User("U-1-2","Bo","bdp@mail.dk","555-555-552", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let peter = clm.createUser(user: User("U-1-3","Peter","peter@olesvej20.dk","555-555-553", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let ac = clm.createUser(user: User("U-2-1","Ann Cathrine","ac@ottetallet.dk","555-555-554", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))

        print("nextLifesignDate = \(String(describing: nextLifesignDate?.description))")
        print("countdownDate = \(String(describing: countdownDate?.description))")
        let uiNext = Helper.simpleUITimeNoDate(date: nextLifesignDate!)
        let countdownTime = nextLifesignDate!.timeIntervalSince(countdownDate!)
        var deadlineTime = Helper.intervalFromTime(time: uiNext) + Double(offset * 60)
        if (deadlineTime > 86400) {
            deadlineTime = deadlineTime - 86400
        }

        let gnGroup = Caregroup("CG-1", "Bo, Peter og Simon", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: simon.userId, caregroup: gnGroup)
        let simonGMid = model.addGroupmember(userId: simon.userId, status: GroupMemberStatusType.Active, caregroupId: gnGroup.caregroupId, memberId: "GM-1-1", isMonitor: false, isMonitored: true, isOwner: true)
        _ = model.addGroupmember(userId: bo.userId, status: GroupMemberStatusType.Active, caregroupId: gnGroup.caregroupId, memberId: "GM-1-2", isMonitor: true, isMonitored: false, isOwner: false)
        _ = model.addGroupmember(userId: peter.userId, status: GroupMemberStatusType.Active, caregroupId: gnGroup.caregroupId, memberId: "GM-1-3", isMonitor: true, isMonitored: false, isOwner: false)
        let simonSupervisionGNOngoing = Supervision(simonGMid!, "S-1-1", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, simon.userId, gnGroup.caregroupId)
        _ = model.addSupervision(supervision: simonSupervisionGNOngoing)
        let simonSupervisionGNPeriod = Supervision(simonGMid!, "S-1-2", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Period, startDate, endDate, simon.userId, gnGroup.caregroupId)
        _ = model.addSupervision(supervision: simonSupervisionGNPeriod, now: startDate!)

        let otteGroup = Caregroup("CG-2", "Ottetallet", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: simon.userId, caregroup: otteGroup)
        _ = model.addGroupmember(userId: simon.userId, status: GroupMemberStatusType.Active, caregroupId: otteGroup.caregroupId, memberId: "GM-2-1", isMonitor: false, isMonitored: true, isOwner: true)
        _ = model.addGroupmember(userId: ac.userId, status: GroupMemberStatusType.Active, caregroupId: otteGroup.caregroupId, memberId: "GM-2-2", isMonitor: true, isMonitored: false, isOwner: true)
        let simonSupervision8tal = Supervision(simonGMid!, "S-2-1", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, simon.userId, otteGroup.caregroupId)
        _ = model.addSupervision(supervision: simonSupervision8tal)

        clm.clearCheckpoints()
        _ = model.updateCheckpoints(supervision: simonSupervisionGNOngoing, lifesign: true, now: lifesignDate!)
        _ = model.updateCheckpoints(supervision: simonSupervision8tal, lifesign: true, now: lifesignDate!)
        clm.addLifeSign(lifesign: LifeSign(simon.userId, "L-1-2-1", lifesignDate!, "", offset))
    }}


