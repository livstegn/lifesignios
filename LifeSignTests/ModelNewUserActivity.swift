//
//  ModelNewUserActivity.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 04/10/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import XCTest
@testable import LifeSign

class ModelNewUserActivity: XCTestCase {
    let calendar = Calendar.current
    let support = SetupSupport()

    func test_Ongoing_BeforeWindow_OneActivity () {
        /* Tjekker, at når der ikke er bufferet nogen aktivitet, så kommer et livstegn igennem */

        // Arrange
        let model = setupNo1()
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let actTime1 = calendar.date(byAdding: .second, value: 1, to: now)!

        // Act
        let lifesign1 = model.getLatestLifesign(userId: model.user!.userId)
        let newActivity = UserActivity.init(time: actTime1, type: .Unlocked)
        model.newUserActivity(userActivity: newActivity, now: actTime1)
        let lifesign2 = model.getLatestLifesign(userId: model.user!.userId)
        
        // Assert
        let difference = lifesign2!.timeIntervalSince(lifesign1!)
        XCTAssertGreaterThan(difference, 0)
    }
    
    func test_Ongoing_BeforeWindow_savedActivities () {
        /* Tjekker, at når der er bufferet en aktivitet, så kommer der ikke et livstegn igennem
         inden for tidsgrænsen (2 sek. i unittesten)  */

        // Arrange
        let model = setupNo1()
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let actTime2 = calendar.date(byAdding: .second, value: 1, to: now)!

        // Act
        let lifesign1 = model.getLatestLifesign(userId: model.user!.userId)
        let newActivity1 = UserActivity.init(time: now, type: .Unlocked)
        model.newUserActivity(userActivity: newActivity1, now: now)
        
        let lifesign2 = model.getLatestLifesign(userId: model.user!.userId)
        let newActivity2 = UserActivity.init(time: actTime2, type: .Unlocked)
        model.newUserActivity(userActivity: newActivity2, now: actTime2)
        
        let lifesign3 = model.getLatestLifesign(userId: model.user!.userId)

        // Assert
        let difference2 = lifesign2!.timeIntervalSince(lifesign1!)
        let difference3 = lifesign3!.timeIntervalSince(lifesign2!)

        XCTAssertGreaterThan(difference2, 0)
        XCTAssertEqual(difference3, 0)
    }

    func test_OngoingOne_AfterWindow_savedActivities () {
        /* Tjekker, at når der er bufferet en aktivitet (1), så kommer der først et livstegn igennem
         på aktivitet (3) efter tidsgrænsen (2 sek. i unittesten). Aktivitet (2) opsamles.  */

        // Arrange
        let model = setupNo1()
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let actTime2 = calendar.date(byAdding: .second, value: 1, to: now)!
        let actTime3 = calendar.date(byAdding: .second, value: 3, to: now)!

        // Act
        let lifesign1 = model.getLatestLifesign(userId: model.user!.userId)
        let newActivity1 = UserActivity.init(time: now, type: .Unlocked)
        model.newUserActivity(userActivity: newActivity1, now: now)
        
        let lifesign2 = model.getLatestLifesign(userId: model.user!.userId)
        
        let newActivity2 = UserActivity.init(time: actTime2, type: .Unlocked)
        model.newUserActivity(userActivity: newActivity2, now: actTime2)
        let lifesign3 = model.getLatestLifesign(userId: model.user!.userId)
        let cua2 = model.collectedUserActivites.count

        let newActivity3 = UserActivity.init(time: actTime3, type: .Unlocked)
        model.newUserActivity(userActivity: newActivity3, now: actTime3)
        let cua3 = model.collectedUserActivites.count

        let lifesign4 = model.getLatestLifesign(userId: model.user!.userId)

        // Assert
        let difference2 = lifesign2!.timeIntervalSince(lifesign1!)
        let difference3 = lifesign3!.timeIntervalSince(lifesign2!)
        let difference4 = lifesign4!.timeIntervalSince(lifesign2!)

        XCTAssertGreaterThan(difference2, 0)
        XCTAssertEqual(cua2, 2)
        XCTAssertEqual(difference3, 0)
        XCTAssertEqual(cua3, 1) // Fordi der er givet livstegn er bufferen clearet
        XCTAssertGreaterThan(difference4, 0)
    }

    func test_OngoingOne_AfterBufferAge_savedActivities () {
        /* Tjekker, at når der er bufferet en aktivitet (1), og næste aktivitet kommer efter
         bufferens aldersudløb (10 sek i unittesten) så kommer der et livstegn igennem
         på aktivitet (3).  */

        // Arrange
        let model = setupNo1()
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let actTime2 = calendar.date(byAdding: .second, value: 15, to: now)!

        // Act
        let lifesign1 = model.getLatestLifesign(userId: model.user!.userId)
        let newActivity1 = UserActivity.init(time: now, type: .Unlocked)
        model.newUserActivity(userActivity: newActivity1, now: now)
        
        let lifesign2 = model.getLatestLifesign(userId: model.user!.userId)
        let cua2 = model.collectedUserActivites.count
        
        let newActivity2 = UserActivity.init(time: actTime2, type: .Unlocked)
        model.newUserActivity(userActivity: newActivity2, now: actTime2)
        let lifesign3 = model.getLatestLifesign(userId: model.user!.userId)
        let cua3 = model.collectedUserActivites.count

        // Assert
        let difference2 = lifesign2!.timeIntervalSince(lifesign1!)
        let difference3 = lifesign3!.timeIntervalSince(lifesign2!)

        XCTAssertGreaterThan(difference2, 0)
        XCTAssertEqual(cua2, 1)
        XCTAssertGreaterThan(difference3, 0)
        XCTAssertEqual(cua3, 1)
    }

    func setupNo1 () -> Model {
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        model.overrideLimitsUserActivities(holdSeconds: 2, bufferAge: 10)
        
        let startDate = calendar.date(byAdding: .day, value: -5, to: Date())
        let lifesignDate = calendar.date(byAdding: .hour, value: -3, to: Date())
        let nextLifesignDate = calendar.date(byAdding: .hour, value: 25, to: lifesignDate!)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)

        support.hansOgMorIenGruppeHansMonitoredOngoing(model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)

        return model
    }
}
