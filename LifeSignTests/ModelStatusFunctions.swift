//
//  ModelStatusFunctions.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 05/11/2019.
//  Copyright © 2019 Simon Jensen. All rights reserved.
//

import XCTest
import CoreLocation
@testable import LifeSign

class ModelStatusFunctions: XCTestCase {
    let calendar = Calendar.current
    let support = SetupSupport()

    func testUserNotMonitoredNoOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDate = calendar.date(byAdding: .hour, value: -3, to: now)
        let nextLifesignDate = calendar.date(byAdding: .hour, value: 25, to: lifesignDate!)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)

        support.hansOgMorIenGruppeMorMonitoredOngoing(model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)

        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, nil)
        XCTAssertEqual(statusList.count, 1)
        
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            
            if (status.userName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignOther)
            }
        }
    }
    
    func testUserNotMonitoredReminder() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDate = calendar.date(byAdding: .hour, value: -24, to: now)
        let nextLifesignDate = calendar.date(byAdding: .minute, value: 10, to: now)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)

        support.hansOgMorIenGruppeMorMonitoredOngoing(model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)

        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, nil)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Warning)
                XCTAssertEqual(message.type, StateMessageType.SoonDueLifesignOther)
            }
        }
        
    }
    
    func testUserNotMonitoredAlarm() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDate = calendar.date(byAdding: .hour, value: -26, to: now)
        let nextLifesignDate = calendar.date(byAdding: .minute, value: -30, to: now)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)

        support.hansOgMorIenGruppeMorMonitoredOngoing(model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)

        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, nil)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Alarm)
                XCTAssertEqual(message.type, StateMessageType.DueLifesignOther)
            }
        }
    }
    
    func testUserMonitoredNoOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDate = calendar.date(byAdding: .hour, value: -3, to: now)
        let nextLifesignDate = calendar.date(byAdding: .hour, value: 25, to: lifesignDate!)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)
        
        support.hansOgMorIenGruppeHansMonitoredOngoing(model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign!, lifesignDate!)
        XCTAssertEqual(model.stateNextLifesign!, nextLifesignDate!)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignOther)
            }
        }
    }

    func testUserMonitoredReminded() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDate = calendar.date(byAdding: .hour, value: -24, to: now)
        let nextLifesignDate = calendar.date(byAdding: .minute, value: 10, to: now)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)
        
        support.hansOgMorIenGruppeHansMonitoredOngoing(model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)

        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign!, lifesignDate!)
        XCTAssertEqual(model.stateNextLifesign!, nextLifesignDate!)
        XCTAssertEqual(statusList.count, 1)
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Warning)
                XCTAssertEqual(message.type, StateMessageType.SoonDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignOther)
            }
        }
    }

    func testUserMonitoredOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDate = calendar.date(byAdding: .hour, value: -26, to: now)
        let nextLifesignDate = calendar.date(byAdding: .minute, value: -30, to: now)
        let countdownDate = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDate!)
        
        support.hansOgMorIenGruppeHansMonitoredOngoing(model: model, clm: clm, startDate: startDate, lifesignDate: lifesignDate, countdownDate: countdownDate, nextLifesignDate: nextLifesignDate)

        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign!, lifesignDate!)
        XCTAssertEqual(model.stateNextLifesign!, nextLifesignDate!)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Alarm)
                XCTAssertEqual(message.type, StateMessageType.DueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignOther)
            }
        }
    }
    
    func testBeggeMonitoredNoOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDateHans = calendar.date(byAdding: .hour, value: -3, to: now)
        let nextLifesignDateHans = calendar.date(byAdding: .hour, value: 25, to: lifesignDateHans!)
        let countdownDateHans = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDateHans!)
        let lifesignDateMor = calendar.date(byAdding: .hour, value: -3, to: now)
        let nextLifesignDateMor = calendar.date(byAdding: .hour, value: 25, to: lifesignDateMor!)
        let countdownDateMor = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDateMor!)

        support.hansOgMorIenGruppeBeggeMonitoredOngoing(model: model, clm: clm, startDate: startDate,
                                         lifesignDateHans: lifesignDateHans, countdownDateHans: countdownDateHans, nextLifesignDateHans: nextLifesignDateHans,
                                         lifesignDateMor: lifesignDateMor, countdownDateMor: countdownDateMor, nextLifesignDateMor: nextLifesignDateMor)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign!, lifesignDateHans!)
        XCTAssertEqual(model.stateNextLifesign!, nextLifesignDateHans!)
        XCTAssertEqual(statusList.count, 2)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignOther)
            }
        }
    }
    
    func testBeggeMonitoredRemindedHans() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDateHans = calendar.date(byAdding: .hour, value: -24, to: now)
        let nextLifesignDateHans = calendar.date(byAdding: .minute, value: 10, to: now)
        let countdownDateHans = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDateHans!)
        let lifesignDateMor = calendar.date(byAdding: .hour, value: -3, to: now)
        let nextLifesignDateMor = calendar.date(byAdding: .hour, value: 25, to: lifesignDateMor!)
        let countdownDateMor = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDateMor!)
        
        support.hansOgMorIenGruppeBeggeMonitoredOngoing(model: model, clm: clm, startDate: startDate,
                                         lifesignDateHans: lifesignDateHans, countdownDateHans: countdownDateHans, nextLifesignDateHans: nextLifesignDateHans,
                                         lifesignDateMor: lifesignDateMor, countdownDateMor: countdownDateMor, nextLifesignDateMor: nextLifesignDateMor)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign!, lifesignDateHans!)
        XCTAssertEqual(model.stateNextLifesign!, nextLifesignDateHans!)
        XCTAssertEqual(statusList.count, 2)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Warning)
                XCTAssertEqual(message.type, StateMessageType.SoonDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignOther)
            }
        }
    }

    func testBeggeMonitoredRemindedMor() {
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm,  universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDateHans = calendar.date(byAdding: .hour, value: -3, to: now)
        let nextLifesignDateHans = calendar.date(byAdding: .hour, value: 25, to: lifesignDateHans!)
        let countdownDateHans = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDateHans!)
        let lifesignDateMor = calendar.date(byAdding: .hour, value: -24, to: now)
        let nextLifesignDateMor = calendar.date(byAdding: .minute, value: 10, to: now)
        let countdownDateMor = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDateMor!)

        support.hansOgMorIenGruppeBeggeMonitoredOngoing(model: model, clm: clm, startDate: startDate,
                                         lifesignDateHans: lifesignDateHans, countdownDateHans: countdownDateHans, nextLifesignDateHans: nextLifesignDateHans,
                                         lifesignDateMor: lifesignDateMor, countdownDateMor: countdownDateMor, nextLifesignDateMor: nextLifesignDateMor)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign!, lifesignDateHans!)
        XCTAssertEqual(model.stateNextLifesign!, nextLifesignDateHans!)
        XCTAssertEqual(statusList.count, 2)
        
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Warning)
                XCTAssertEqual(message.type, StateMessageType.SoonDueLifesignOther)
            }
        }
    }

    func testBeggeMonitoredAlarmHans() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm,  universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDateHans = calendar.date(byAdding: .hour, value: -24, to: now)
        let nextLifesignDateHans = calendar.date(byAdding: .minute, value: -10, to: now)
        let countdownDateHans = calendar.date(byAdding: .minute, value: -40, to: nextLifesignDateHans!)
        let lifesignDateMor = calendar.date(byAdding: .hour, value: -3, to: now)
        let nextLifesignDateMor = calendar.date(byAdding: .hour, value: 25, to: lifesignDateMor!)
        let countdownDateMor = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDateMor!)
        
        support.hansOgMorIenGruppeBeggeMonitoredOngoing(model: model, clm: clm, startDate: startDate,
                                         lifesignDateHans: lifesignDateHans, countdownDateHans: countdownDateHans, nextLifesignDateHans: nextLifesignDateHans,
                                         lifesignDateMor: lifesignDateMor, countdownDateMor: countdownDateMor, nextLifesignDateMor: nextLifesignDateMor)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign!, lifesignDateHans!)
        XCTAssertEqual(model.stateNextLifesign!, nextLifesignDateHans!)
        XCTAssertEqual(statusList.count, 2)
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Alarm)
                XCTAssertEqual(message.type, StateMessageType.DueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignOther)
            }
        }
    }

    func testBeggeMonitoredAlarmMor() {
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm,  universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let lifesignDateHans = calendar.date(byAdding: .hour, value: -3, to: now)
        let nextLifesignDateHans = calendar.date(byAdding: .hour, value: 25, to: lifesignDateHans!)
        let countdownDateHans = calendar.date(byAdding: .minute, value: -20, to: nextLifesignDateHans!)
        let lifesignDateMor = calendar.date(byAdding: .hour, value: -24, to: now)
        let nextLifesignDateMor = calendar.date(byAdding: .minute, value: -10, to: now)
        let countdownDateMor = calendar.date(byAdding: .minute, value: -40, to: nextLifesignDateMor!)
        
        support.hansOgMorIenGruppeBeggeMonitoredOngoing(model: model, clm: clm, startDate: startDate,
                                         lifesignDateHans: lifesignDateHans, countdownDateHans: countdownDateHans, nextLifesignDateHans: nextLifesignDateHans,
                                         lifesignDateMor: lifesignDateMor, countdownDateMor: countdownDateMor, nextLifesignDateMor: nextLifesignDateMor)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign!, lifesignDateHans!)
        XCTAssertEqual(model.stateNextLifesign!, nextLifesignDateHans!)
        XCTAssertEqual(statusList.count, 2)
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignYou)
            } else {
                XCTAssertEqual(message.severity, StateMessageSeverity.Alarm)
                XCTAssertEqual(message.type, StateMessageType.DueLifesignOther)
            }
        }
    }
    
    func testUserPeriodMonitoredNoOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = now
        let periodEnd = calendar.date(byAdding: .minute, value: 180, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: periodEnd)

        support.hansOgMorIenGruppeHansMonitoredPeriod(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.NoDueLifesignYou)
            }
        }
    }

    func testUserPeriodMonitoredReminded() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = calendar.date(byAdding: .hour, value: -2, to: now)!
        let periodEnd = calendar.date(byAdding: .hour, value: 3, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -10, to: now)!
        
        support.hansOgMorIenGruppeHansMonitoredPeriod(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)
        
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Warning)
                XCTAssertEqual(message.type, StateMessageType.SoonDueLifesignYou)
            }
        }
    }
    
    func testUserPeriodMonitoredOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = calendar.date(byAdding: .hour, value: -4, to: now)!
        let periodEnd = calendar.date(byAdding: .hour, value: 3, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: periodEnd)!
        
        support.hansOgMorIenGruppeHansMonitoredPeriod(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)
        
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Alarm)
                XCTAssertEqual(message.type, StateMessageType.DueLifesignYou)
            }
        }
    }
   
    func testUserPeriodMonitoredEndedInTime() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = calendar.date(byAdding: .hour, value: -4, to: now)!
        let periodEnd = calendar.date(byAdding: .hour, value: 5, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: periodEnd)!
        let endedDate = calendar.date(byAdding: .hour, value: -1, to: periodEnd)!
        
        support.hansOgMorIenGruppeHansMonitoredPeriod(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: endedDate)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, nil)
        XCTAssertEqual(statusList.count, 1)
        
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.EndedInTimeYou)
            }
        }
    }
    
    func testUserPeriodMonitoredEndedOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = calendar.date(byAdding: .hour, value: -4, to: now)!
        let periodEnd = calendar.date(byAdding: .hour, value: 3, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: periodEnd)!
        let endedDate = calendar.date(byAdding: .hour, value: 1, to: periodEnd)!
        
        support.hansOgMorIenGruppeHansMonitoredPeriod(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: endedDate)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, nil)
        XCTAssertEqual(statusList.count, 1)
        
        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Warning)
                XCTAssertEqual(message.type, StateMessageType.AfterDeadline)
            }
        }
    }
    
    func testUserSafeSpotMonitoredUpcomingNotOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = calendar.date(byAdding: .minute, value: 5, to: now)!
        let periodEnd = calendar.date(byAdding: .minute, value: 180, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: periodEnd)

        _ = support.hansOgMorIenGruppeHansMonitoredSafeSpot(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.SafespotUpcoming)
            }
        }
    }

    func testUserSafeSpotMonitoredActiveNotOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = now
        let periodEnd = calendar.date(byAdding: .minute, value: 180, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: periodEnd)

        let supervision = support.hansOgMorIenGruppeHansMonitoredSafeSpot(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        _ = model.activateTracking(tracking: tracking!)
        
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.SafespotActive)
            }
        }
    }

    func testUserSafeSpotMonitoredActiveLeftSafe() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = now
        let periodEnd = calendar.date(byAdding: .minute, value: 180, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: periodEnd)

        let supervision = support.hansOgMorIenGruppeHansMonitoredSafeSpot(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        _ = model.activateTracking(tracking: tracking!)
        var locations = [CLLocation]()
        let hellerup1 = CLLocation(latitude: 55.7331440, longitude: 12.5754760)
        let hellerup2 = CLLocation(latitude: 55.7362850, longitude: 12.5750660)
        locations.append(hellerup1)
        locations.append(hellerup2)
        model.locationManager(dm.getLocationManager(), didUpdateLocations: locations)
        
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.SafespotActiveLeftSafe)
            }
        }
    }

    func testUserSafeSpotMonitoredSoonDue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = calendar.date(byAdding: .minute, value: -150, to: now)!
        let periodEnd = calendar.date(byAdding: .minute, value: 180, to: periodStart)!
        let countdownDate = calendar.date(byAdding: .minute, value: -10, to: now)

        let supervision = support.hansOgMorIenGruppeHansMonitoredSafeSpot(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        _ = model.activateTracking(tracking: tracking!)
        var locations = [CLLocation]()
        let hellerup1 = CLLocation(latitude: 55.7331440, longitude: 12.5754760)
        let hellerup2 = CLLocation(latitude: 55.7362850, longitude: 12.5750660)
        locations.append(hellerup1)
        locations.append(hellerup2)
        model.locationManager(dm.getLocationManager(), didUpdateLocations: locations)
        
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Warning)
                XCTAssertEqual(message.type, StateMessageType.SoonDueLifesignYou)
            }
        }
    }

    func testUserSafeSpotMonitoredActiveOverdue() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = calendar.date(byAdding: .minute, value: -150, to: now)!
        let periodEnd = calendar.date(byAdding: .minute, value: -10, to: now)!
        let countdownDate = calendar.date(byAdding: .minute, value: -40, to: now)!

        let supervision = support.hansOgMorIenGruppeHansMonitoredSafeSpot(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        _ = model.activateTracking(tracking: tracking!)
        var locations = [CLLocation]()
        let hellerup1 = CLLocation(latitude: 55.7331440, longitude: 12.5754760)
        let hellerup2 = CLLocation(latitude: 55.7362850, longitude: 12.5750660)
        locations.append(hellerup1)
        locations.append(hellerup2)
        model.locationManager(dm.getLocationManager(), didUpdateLocations: locations)
        
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Alarm)
                XCTAssertEqual(message.type, StateMessageType.SafespotActiveOverdue)
            }
        }
    }

    func testUserSafeSpotMonitoredEndedBackSafe() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: now)
        let periodStart = calendar.date(byAdding: .minute, value: -150, to: now)!
        let periodEnd = calendar.date(byAdding: .minute, value: 30, to: now)!
        let countdownDate = calendar.date(byAdding: .minute, value: 10, to: now)!

        let supervision = support.hansOgMorIenGruppeHansMonitoredSafeSpot(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        let beforeTracking = Date()
        let tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        _ = model.activateTracking(tracking: tracking!)
        var locations = [CLLocation]()
        let hellerup1 = CLLocation(latitude: 55.7331440, longitude: 12.5754760)
        let hellerup2 = CLLocation(latitude: 55.7362850, longitude: 12.5750660)
        let virum = CLLocation(latitude: 55.7968950, longitude: 12.4696750)  // Safespot
        locations.append(hellerup1)
        locations.append(hellerup2)
        locations.append(virum)
        model.locationManager(dm.getLocationManager(), didUpdateLocations: locations)
        
        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertGreaterThan(model.stateLastLifesign!, beforeTracking)
        XCTAssertEqual(model.stateNextLifesign, periodEnd)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.SafespotEndedBackSafe)
            }
        }
    }

    func testUserSafeSpotMonitoredEndedNeverLeftSafe() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: Date())
        let periodStart = calendar.date(byAdding: .minute, value: -150, to: Date())!
        let periodEnd = calendar.date(byAdding: .second, value: 2, to: Date())!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: Date())!

        let supervision = support.hansOgMorIenGruppeHansMonitoredSafeSpot(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        var tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        _ = model.activateTracking(tracking: tracking!)
        var locations = [CLLocation]()
        locations.append(CLLocation(latitude: 55.7968950, longitude: 12.4696750))  // Safespot
        model.locationManager(dm.getLocationManager(), didUpdateLocations: locations)

        do { sleep(3) }
        // Simulér AWS: tracking skifter til ended, og Ended-checkpoint oprettes (og de to andre slettes)
        tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        tracking?.status = .Ended
        tracking?.onSafe = true
        _ = model.cloudLayer.updateTracking(tracking: tracking!)
        
        _ = clm.endCheckpoints(supervision: supervision)
        
        XCTAssertLessThan(periodEnd, Date())

        let statusList = model.getLifesignStateNew()
            
        // Assert
        XCTAssertEqual(model.stateLastLifesign, nil)
        XCTAssertEqual(model.stateNextLifesign, nil)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Info)
                XCTAssertEqual(message.type, StateMessageType.SafespotEndedNeverLeftSafe)
            }
        }
    }

    func testUserSafeSpotMonitoredEndedTooLate() {
        // Arrange
        let dm = DeviceMuck.init()
        let clm = clmModelFunctions.init()
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: clm, universalLinkLaunch: false, universalLinkData: nil)
        
        let now = calendar.date(bySetting: .second, value: 0, of: Date())!
        let startDate = calendar.date(byAdding: .day, value: -5, to: Date())
        let periodStart = calendar.date(byAdding: .minute, value: -150, to: Date())!
        let periodEnd = calendar.date(byAdding: .second, value: 2, to: Date())!
        let countdownDate = calendar.date(byAdding: .minute, value: -30, to: Date())!

        let supervision = support.hansOgMorIenGruppeHansMonitoredSafeSpot(model: model, clm: clm, startDate: startDate, countdownDate: countdownDate, periodStart: periodStart, periodEnd: periodEnd, endedDate: nil)
        
        // Act
        var tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        _ = model.activateTracking(tracking: tracking!)
        
        var locations1 = [CLLocation]()
        let hellerup1 = CLLocation(latitude: 55.7331440, longitude: 12.5754760)
        locations1.append(hellerup1)
        model.locationManager(dm.getLocationManager(), didUpdateLocations: locations1)

        do { sleep(3) }
        XCTAssertLessThan(periodEnd, Date())

        var locations2 = [CLLocation]()
        locations2.append(CLLocation(latitude: 55.7968950, longitude: 12.4696750))  // Safespot
        model.locationManager(dm.getLocationManager(), didUpdateLocations: locations2)

        // Simulér AWS: tracking skifter til ended, og Ended-checkpoint oprettes (og de to andre slettes)
        tracking = model.cloudLayer.getTracking(trackingid: supervision.trackingId!)
        tracking?.status = .Ended
        _ = model.cloudLayer.updateTracking(tracking: tracking!)
        _ = clm.endCheckpoints(supervision: supervision)

        let statusList = model.getLifesignStateNew()
        
        // Assert
        XCTAssertGreaterThan(model.stateLastLifesign!, periodEnd)
        XCTAssertEqual(model.stateNextLifesign, nil)
        XCTAssertEqual(statusList.count, 1)

        for status in statusList {
            let message = status.getStateMessage(appUserid: model.user!.userId, now: now)
            if (message.monitoredUserName == "Hans") {
                XCTAssertEqual(message.severity, StateMessageSeverity.Warning)
                XCTAssertEqual(message.type, StateMessageType.SafespotEndedTooLate)
            }
        }
    }

}

class dlmModelStatusFunctions : DeviceMuck {
    private var locManager = CLLocationManager()

    override func getLocationManager() -> CLLocationManager {
        return locManager
    }
}

