//
//  SetupSupport.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 04/10/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class SetupSupport: NSObject {

    func hansOgMorIenGruppeMorMonitoredOngoing (model: Model, clm: clmModelFunctions, startDate: Date?, lifesignDate: Date?, countdownDate: Date?, nextLifesignDate: Date?) {
        let offset = 120
        let hans = clm.createUser(user: User("U-1-1","Hans","m@olesvej20.dk","555-555-555", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let mor = clm.createUser(user: User("U-1-2","Grethe","g@olesvej20.dk","555-555-551", UserStatusType.Active, "12", PlatformType.iOS, false, startDate!, offset))
        model.user = hans
        print("nextLifesignDate = \(nextLifesignDate?.description)")
        print("countdownDate = \(countdownDate?.description)")
        let uiNext = Helper.simpleUITimeNoDate(date: nextLifesignDate!)
        let countdownTime = nextLifesignDate!.timeIntervalSince(countdownDate!)
        var deadlineTime = Helper.intervalFromTime(time: uiNext) + Double(offset * 60)
        if (deadlineTime > 86400) {
            deadlineTime = deadlineTime - 86400
        }

        print("deadlineTime = \(deadlineTime.description)")
        print("countdownTime = \(countdownTime.description)")

        let hansParens = Caregroup("CG-1", "Hans og mor", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: hans.userId, caregroup: hansParens)
        _ = model.addGroupmember(userId: hans.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-1", isMonitor: true, isMonitored: false, isOwner: true)
        let morGMid = model.addGroupmember(userId: mor.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-2", isMonitor: false, isMonitored: true, isOwner: false)
        let morSupervision = Supervision(morGMid!, "S-1-2", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, mor.userId, hansParens.caregroupId)
        _ = model.addSupervision(supervision: morSupervision)

        clm.clearCheckpoints()
        model.updateCheckpoints(supervision: morSupervision, lifesign: true, now: lifesignDate!)
        _ = clm.addLifeSign(lifesign: LifeSign(mor.userId, "L-1-2-1", lifesignDate!, "", offset))
    }

    func hansOgMorIenGruppeHansMonitoredOngoing (model: Model, clm: clmModelFunctions, startDate: Date?, lifesignDate: Date?, countdownDate: Date?, nextLifesignDate: Date?) {
        let offset = 120
        let hans = clm.createUser(user: User("U-1-1","Hans","m@olesvej20.dk","555-555-555", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let mor = clm.createUser(user: User("U-1-2","Grethe","g@olesvej20.dk","555-555-551", UserStatusType.Active, "12", PlatformType.iOS, false, startDate!, offset))
        model.user = hans
        var deadlineTime = Helper.intervalFromTime(time: Helper.simpleUITimeNoDate(date: nextLifesignDate!)) + Double(offset * 60)
        if (deadlineTime > 86400) {
            deadlineTime = deadlineTime - 86400
        }
        let countdownTime = nextLifesignDate!.timeIntervalSince(countdownDate!)

        let hansParens = Caregroup("CG-1", "Hans og mor", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: hans.userId, caregroup: hansParens)
        let hansGMid = model.addGroupmember(userId: hans.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-1", isMonitor: false, isMonitored: true, isOwner: true)
        _ = model.addGroupmember(userId: mor.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-2", isMonitor: true, isMonitored: false, isOwner: false)
        let hansSupervision = Supervision(hansGMid!, "S-1-2", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, hans.userId, hansParens.caregroupId)
        _ = model.addSupervision(supervision: hansSupervision)

        clm.clearCheckpoints()
        model.updateCheckpoints(supervision: hansSupervision, lifesign: true, now: lifesignDate!)
        _ = clm.addLifeSign(lifesign: LifeSign(hans.userId, "L-1-2-1", lifesignDate!, "", offset))
    }

    func hansOgMorIenGruppeBeggeMonitoredOngoing (model: Model, clm: clmModelFunctions, startDate: Date?, lifesignDateHans: Date?, countdownDateHans: Date?, nextLifesignDateHans: Date?,
        lifesignDateMor: Date?, countdownDateMor: Date?, nextLifesignDateMor: Date?) {
        let offset = 120
        let hans = clm.createUser(user: User("U-1-1","Hans","m@olesvej20.dk","555-555-555", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let mor = clm.createUser(user: User("U-1-2","Grethe","g@olesvej20.dk","555-555-551", UserStatusType.Active, "12", PlatformType.iOS, false, startDate!, offset))
        model.user = hans
        var deadlineTime = Helper.intervalFromTime(time: Helper.simpleUITimeNoDate(date: nextLifesignDateHans!)) + Double(offset * 60)
        if (deadlineTime > 86400) {
            deadlineTime = deadlineTime - 86400
        }
        var countdownTime = nextLifesignDateHans!.timeIntervalSince(countdownDateHans!)
        print("Hans deadlineTime = \(deadlineTime.description)")
        print("Hans countdownTime = \(countdownTime.description)")

        let hansParens = Caregroup("CG-1", "Hans og mor", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: hans.userId, caregroup: hansParens)
        let hansGMid = model.addGroupmember(userId: hans.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-1", isMonitor: true, isMonitored: true, isOwner: true)
        let morGMid = model.addGroupmember(userId: mor.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-2", isMonitor: true, isMonitored: true, isOwner: false)
        let hansSupervision = Supervision(hansGMid!, "S-1-2", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, hans.userId, hansParens.caregroupId)
        _ = model.addSupervision(supervision: hansSupervision)

        deadlineTime = Helper.intervalFromTime(time: Helper.simpleUITimeNoDate(date: nextLifesignDateMor!)) + Double(offset * 60)
        if (deadlineTime > 86400) {
            deadlineTime = deadlineTime - 86400
        }
        countdownTime = nextLifesignDateMor!.timeIntervalSince(countdownDateMor!)
        print("Mor deadlineTime = \(deadlineTime.description)")
        print("Mor countdownTime = \(countdownTime.description)")

        let morSupervision = Supervision(morGMid!, "S-1-3", SupervisionStatusType.Active, countdownTime, deadlineTime, 0,SupervisionType.Ongoing, nil, nil, mor.userId, hansParens.caregroupId)
        _ = model.addSupervision(supervision: morSupervision)

        clm.clearCheckpoints()
        model.updateCheckpoints(supervision: hansSupervision, lifesign: true, now: lifesignDateHans!)
        model.updateCheckpoints(supervision: morSupervision, lifesign: true, now: lifesignDateMor!)
        _ = clm.addLifeSign(lifesign: LifeSign(hans.userId, "L-1-3-1", lifesignDateHans!, "", offset))
        _ = clm.addLifeSign(lifesign: LifeSign(mor.userId, "L-1-3-2", lifesignDateMor!, "", offset))
    }

    func hansOgMorIenGruppeHansMonitoredPeriod (model: Model, clm: clmModelFunctions, startDate: Date?, countdownDate: Date?, periodStart: Date, periodEnd: Date, endedDate: Date?) {
        let offset = 120
        let hans = clm.createUser(user: User("U-1-1","Hans","m@olesvej20.dk","555-555-555", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let mor = clm.createUser(user: User("U-1-2","Grethe","g@olesvej20.dk","555-555-551", UserStatusType.Active, "12", PlatformType.iOS, false, startDate!, offset))
        model.user = hans
        
        let countdownTime = periodEnd.timeIntervalSince(countdownDate!)
        print("countdownTime = \(countdownTime.description)")

        let hansParens = Caregroup("CG-1", "Hans og mor", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: hans.userId, caregroup: hansParens)
        let hansGMid = model.addGroupmember(userId: hans.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-1", isMonitor: false, isMonitored: true, isOwner: true)
        _ = model.addGroupmember(userId: mor.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-2", isMonitor: true, isMonitored: false, isOwner: false)
        let hansSupervision = Supervision(hansGMid!, "S-1-2", SupervisionStatusType.Active, countdownTime, -1, -1,SupervisionType.Period, periodStart, periodEnd, hans.userId, hansParens.caregroupId)
        _ = model.addSupervision(supervision: hansSupervision)

        if (endedDate != nil) {
            clm.clearCheckpoints()
            _ = clm.addCheckpoint(checkpoint: Checkpoint(supervision: hansSupervision, type: .Ended, time: endedDate!, notifiedCount: 0))
        } else {
            clm.clearCheckpoints()
            _ = clm.addCheckpoint(checkpoint: Checkpoint(supervision: hansSupervision, type: .Countdown, time: countdownDate!, notifiedCount: 0))
            _ = clm.addCheckpoint(checkpoint: Checkpoint(supervision: hansSupervision, type: .Deadline, time: periodEnd, notifiedCount: 0))
        }
    }

    func hansOgMorIenGruppeHansMonitoredSafeSpot (model: Model, clm: clmModelFunctions, startDate: Date?, countdownDate: Date?, periodStart: Date, periodEnd: Date, endedDate: Date?) -> Supervision {
        let offset = 120
        let hans = clm.createUser(user: User("U-1-1","Hans","m@olesvej20.dk","555-555-555", UserStatusType.Active, "11", PlatformType.iOS, false, startDate!, offset))
        let mor = clm.createUser(user: User("U-1-2","Grethe","g@olesvej20.dk","555-555-551", UserStatusType.Active, "12", PlatformType.iOS, false, startDate!, offset))
        model.user = hans
        
        let countdownTime = periodEnd.timeIntervalSince(countdownDate!)
        print("countdownTime = \(countdownTime.description)")

        let hansParens = Caregroup("CG-1", "Hans og mor", startDate! ,CaregroupStatusType.Active)
        _ = model.addCaregroup(userid: hans.userId, caregroup: hansParens)
        let hansGMid = model.addGroupmember(userId: hans.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-1", isMonitor: false, isMonitored: true, isOwner: true)
        _ = model.addGroupmember(userId: mor.userId, status: GroupMemberStatusType.Active, caregroupId: hansParens.caregroupId, memberId: "GM-1-2", isMonitor: true, isMonitored: false, isOwner: false)

        let supervision = Supervision(hansGMid!, "S-1-2", SupervisionStatusType.Active, countdownTime, -1, -1,SupervisionType.Period, periodStart, periodEnd, hans.userId, hansParens.caregroupId)
        supervision.trackingId = "TR-" + UUID().uuidString
        supervision.locationLatitude = 55.7968950
        supervision.locationLongitude = 12.4696750
        _ = model.addSupervision(supervision: supervision)
        
        _ = model.addTracking(tracking: Tracking(supervision: supervision, starttime: Date(), status: TrackingStatusType.Upcoming, onSafe: false, leftSafe: false, backSafe: false))
        
        if (endedDate != nil) {
            clm.clearCheckpoints()
            _ = clm.addCheckpoint(checkpoint: Checkpoint(supervision: supervision, type: .Ended, time: endedDate!, notifiedCount: 0))
        }
        
        return supervision
    }
}
