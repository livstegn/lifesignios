//
//  checkpointTimes.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 21/11/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import XCTest
@testable import LifeSign

class CheckpointTimes: XCTestCase {
    let calendar = Calendar.current
    let support = SetupSupport()
    let dm = DeviceMuck.init()
    let clm = clmModelFunctions.init()
    lazy var model = Model.init(deviceLayerImplementation: self.dm, cloudLayerImplementation: self.clm, universalLinkLaunch: false, universalLinkData: nil)
    
    func testIntervalBeforeMidnight() {
        // Arrange
        let offset = 60 // minutes before UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.interval = 11 * 60 * 60  // 11 timer
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T11:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T22:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T21:30:00.000Z")
    }

    func testIntervalAtMidnight() {
        // Arrange
        let offset = 60 // minutes before UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.interval = 13 * 60 * 60  // timer
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T11:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T00:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T23:30:00.000Z")
    }

    func testIntervalAfterMidnight() {
        // Arrange
        let offset = 60 // minutes before UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.interval = 14 * 60 * 60  // timer
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T11:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T01:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-02T00:30:00.000Z")
    }

    func testIntervalNegativeOffset() {
        // Arrange
        let offset = -180 // minutes before UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.interval = 24 * 60 * 60  // 24 timer
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T11:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T11:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-02T10:30:00.000Z")
    }

    func testOngoing1100_Next2300_offset_plus180() {
        // Arrange
        let offset = 180 // minutes before UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 23 * 60 * 60  // 23:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T11:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T20:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T19:30:00.000Z")
    }

    func testOngoing2000_Next0900_offset_plus120() {
        // Arrange
        let offset = 120 // minutes before UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T20:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T07:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-02T06:30:00.000Z")
    }

    func testOngoing2200_Next0900_offset_plus120() {
        // Arrange
        let offset = 120 // minutes before UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T22:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T07:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-02T06:30:00.000Z")
    }

    func testOngoing0701_Next0900_offset_plus120() {
        // Arrange
        let offset = 120 // minutes before UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T07:01:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T07:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-02T06:30:00.000Z")
    }

    func testOngoing2300_Next1800_offset_minus360() {
        // Arrange
        let offset = -360 // minus: minutes after UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T23:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T00:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T23:30:00.000Z")
    }

    func testOngoing0100_Next0900_offset_minus360() {
        // Arrange
        let offset = -360 // minus: minutes after UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T01:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T15:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T14:30:00.000Z")
    }

    func testOngoing0300_Next0900_offset_minus360() {
        // Arrange
        let offset = -360 // minus: minutes after UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 13 * 60 * 60  // 13:00
        supervision.deadline3 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T03:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T15:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T14:30:00.000Z")
    }

    func testOngoing1500_Next1300_offset_minus360() {
        // Arrange
        let offset = -360 // minus: minutes after UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 13 * 60 * 60  // 13:00
        supervision.deadline3 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T15:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T19:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T18:30:00.000Z")
    }

    func testOngoing2200_Next1800_offset_minus360() {
        // Arrange
        let offset = -360 // minus: minutes after UTC
        let lifesign = false
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 13 * 60 * 60  // 13:00
        supervision.deadline3 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T22:00:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T00:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T23:30:00.000Z")
    }

    // Tests, hvor der gives livstegn

    func testLifesign0701_Next0900_offset_plus60() {
        // Arrange
        let offset = 60 // plus: minutes before UTC
        let lifesign = true
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 13 * 60 * 60  // 13:00
        supervision.deadline3 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T06:01:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T08:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T07:30:00.000Z")
    }

    func testLifesign0801_Next0900_offset_plus60() {
        // Arrange
        let offset = 60 // plus: minutes before UTC
        let lifesign = true
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 13 * 60 * 60  // 13:00
        supervision.deadline3 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T07:01:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T12:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T11:30:00.000Z")
    }

    func testLifesign1601_Next0900_offset_plus60() {
        // Arrange
        let offset = 60 // plus: minutes before UTC
        let lifesign = true
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 13 * 60 * 60  // 13:00
        supervision.deadline3 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T15:01:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T17:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T16:30:00.000Z")
    }

    func testLifesign2101_Next0900_offset_plus60() {
        // Arrange
        let offset = 60 // plus: minutes before UTC
        let lifesign = true
        let supervision = basicSupervision()
        supervision.deadline1 = 9 * 60 * 60  // 09:00
        supervision.deadline2 = 13 * 60 * 60  // 13:00
        supervision.deadline3 = 18 * 60 * 60  // 18:00
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T20:01:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-02T08:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-02T07:30:00.000Z")
    }

    func testPeriod1901_End2300_offset_plus60() {
        // Arrange
        let offset = 60 // plus: minutes before UTC
        let lifesign = true
        let supervision = basicSupervision()
        supervision.supervisionType = .Period
        supervision.periodEnd = Helper.dateFromUTC(utc: "2020-12-01T22:00:00.000Z")!
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T18:01:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T22:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T21:30:00.000Z")
    }

    func testPeriod1901_End2300_offset_minus600() {
        // Arrange
        let offset = -600 // minus: minutes after UTC
        let lifesign = true
        let supervision = basicSupervision()
        supervision.supervisionType = .Period
        supervision.periodEnd = Helper.dateFromUTC(utc: "2020-12-01T13:00:00.000Z")!
        let baseline = Helper.dateFromUTC(utc: "2020-12-01T09:01:00.000Z")! // yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ

        // Act
        let deadline = model.deadlineFrom(now: baseline, supervision: supervision, localUserOffsetMinutes: offset, lifesign: lifesign)
        let countdown = model.countdownFrom(deadline: deadline, supervision: supervision)
        
        // Assert
        XCTAssertEqual(Helper.utcTime(date: deadline), "2020-12-01T13:00:00.000Z")
        XCTAssertEqual(Helper.utcTime(date: countdown), "2020-12-01T12:30:00.000Z")
    }

    // Support

    func basicSupervision() -> Supervision {
        let s = Supervision()
        s.status = SupervisionStatusType.Active
        s.groupmemberId = "G1"
        s.supervisionId = "S1"
        s.deadline1 = nil
        s.deadline2 = nil
        s.deadline3 = nil
        s.countdownTime = 30 * 60  // 30 minutter
        s.interval = nil
        s.supervisionType = SupervisionType.Ongoing
        s.userId = "U1"
        s.caregroupId = "CG1"
        s.endingType = nil
        s.wifi = nil
        s.locationInterval = nil
        s.locationLatitude = nil
        s.locationLongitude = nil
        s.trackingId = nil

        return s
    }
}
