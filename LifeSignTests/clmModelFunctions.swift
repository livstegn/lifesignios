//
//  CloudModelFunctions.swift
//  LifeSignTests
//
//  Created by Peter Rosendahl on 04/10/2020.
//  Copyright © 2020 Simon Jensen. All rights reserved.
//

import Foundation

class clmModelFunctions : CloudMuck {
    private var remoteCaregroups = [Caregroup]()
    private var remoteGroupMembers = [GroupMember]()
    private var remoteSupervisions = [Supervision]()
    private var remoteCheckpoints = [Checkpoint]()
    private var remoteLifesigns = [LifeSign]()
    private var remoteUsers = [User]()
    private var remoteTrackings = [Tracking]()
    private var remoteLocations = [Location]()
    var remoteFeedbacks = [TestFeedback]()

    override func addCaregroup(userid: String, caregroup: Caregroup) -> String? {
        remoteCaregroups.append(caregroup)
        
        return caregroup.caregroupId
    }

    override func addGroupmember(userId: String, status: GroupMemberStatusType, caregroupId: String, memberId: String, isMonitor: Bool, isMonitored: Bool, isOwner: Bool) -> String? {
        let gm = GroupMember(memberId, caregroupId, status, userId, isMonitored, isMonitor, isOwner)
        remoteGroupMembers.append(gm)
        
        return memberId
    }
    
    override func addSupervision(supervision: Supervision) -> String? {
        remoteSupervisions.append(supervision)
        
        return supervision.supervisionId
    }
    
    override func addCheckpoint(checkpoint: Checkpoint) -> String {
        var fundetCheckpoint : Int = -1
        
        var i = 0
        for cp in remoteCheckpoints {
            if (cp.supervisionId == checkpoint.supervisionId && cp.type == checkpoint.type) {
                fundetCheckpoint = i
            }
            i = i + 1
        }
        
        if (fundetCheckpoint > -1) {
            remoteCheckpoints[fundetCheckpoint] = checkpoint
        }
        else {
            remoteCheckpoints.append(checkpoint)
        }
        
        return ""
    }

    func addLifeSign(lifesign: LifeSign) {
        remoteLifesigns.append(lifesign)
    }

    override func addLifesign(userid: String, userMinutesBeforeUTC: Int) -> String {
        let lifesignId = "L-" + UUID().uuidString
        let lifesign = LifeSign.init(userid, lifesignId, Date(), "", userMinutesBeforeUTC)
        remoteLifesigns.append(lifesign)
        return lifesignId
    }

    override func addTracking(tracking: Tracking) -> String? {
        remoteTrackings.append(tracking)
        
        return tracking.trackingId
    }

    override func addLocation(location: Location) -> String? {
        remoteLocations.append(location)
        
        return location.locationId
    }
    
    override func createUser(user: User) -> User {
        remoteUsers.append(user)
        
        return user
    }
    
    override func getMainStatusList(userId: String, caregroupId: String) -> [MainStatus] {
        var statusList = [MainStatus]()

        for supervision in remoteSupervisions {
            if (caregroupId == supervision.caregroupId) {
                let ms = MainStatus.init(supervision: supervision)
                ms.caregroupName = getCaregroup(caregroupid: caregroupId)?.caregroupName ?? ""
                ms.userName = getUser(userid: supervision.userId)?.userName ?? ""
                
                if (supervision.trackingId != nil) {
                    let t = getTracking(trackingid: supervision.trackingId!)!
                    ms.trackingStatus = t.status
                    ms.backSafe = t.backSafe
                    ms.onSafe = t.onSafe
                    ms.leftSafe = t.leftSafe
                }
                
                let cpList = getCheckpoints(supervisionid: supervision.supervisionId) ?? [Checkpoint]()
                for cp in cpList {
                    if (cp.type == .Countdown) {
                        ms.countdownTime = cp.time
                    }
                    if (cp.type == .Deadline) {
                        ms.deadlineTime = cp.time
                    }
                    if (cp.type == .Ended) {
                        ms.endedTime = cp.time
                    }
                }
                
                statusList.append(ms)
            }
        }
        
        return statusList
    }
    
    override func getCaregroups(userid: String) -> [Caregroup]? {
        var caregroups = [Caregroup]()
        for groupmember in remoteGroupMembers {
            if (groupmember.userid == userid) {
                for caregroup in remoteCaregroups {
                    if (caregroup.caregroupId == groupmember.caregroupId) {
                        caregroups.append(caregroup)
                    }
                }
            }
        }

        return caregroups
    }
    
    override func getGroupmembers(caregroupId: String) -> [GroupMember]? {
        var groupMembers = [GroupMember]()

        for groupmember in remoteGroupMembers {
            if (caregroupId == groupmember.caregroupId) {
                groupMembers.append(groupmember)
            }
        }
        
        return groupMembers
    }
    
    override func getSupervisions(groupmemberId: String) -> [Supervision]? {
        var supervisions = [Supervision]()

        for supervision in remoteSupervisions {
            if (groupmemberId == supervision.groupmemberId) {
                supervisions.append(supervision)
            }
        }
        
        return supervisions
    }
    
    override func getCheckpoints(supervisionid: String) -> [Checkpoint]? {
        var checkpoints = [Checkpoint]()

        for checkpoint in remoteCheckpoints {
            if (supervisionid == checkpoint.supervisionId) {
                checkpoints.append(checkpoint)
            }
        }
        
        return checkpoints
    }
    
    override func getLifesigns(userid: String) -> [LifeSign]? {
        var lifesigns = [LifeSign]()
        
        for lifesign in remoteLifesigns {
            if (userid == lifesign.userId) {
                lifesigns.append(lifesign)
            }
        }
        
        return lifesigns
    }
 
    override func getUser(userid: String) -> User? {
        var user : User?
        
        for remoteUser in remoteUsers {
            if remoteUser.userId == userid {
                user = remoteUser
                break
            }
        }
        
        return user
    }
    
    override func getSupervision(groupmemberId: String, supervisionId: String) -> Supervision? {
        var supervision : Supervision?
        
        for remoteSupervision in remoteSupervisions {
            if remoteSupervision.supervisionId == supervisionId {
                supervision = remoteSupervision
                break
            }
        }

        return supervision
    }
    
    override func getTracking(trackingid: String) -> Tracking? {
        var tracking : Tracking?
        
        for remoteTracking in remoteTrackings {
            if remoteTracking.trackingId == trackingid {
                tracking = remoteTracking
                break
            }
        }

        return tracking
    }
    
    override func updateTracking(tracking: Tracking) -> String? {
        var i = -1

        for remoteTracking in remoteTrackings {
            if (i<0) {
                i = 0
            }
            if remoteTracking.trackingId == tracking.trackingId {
                break
            }
            i = i + 1
        }
        
        if (i>0) {
            remoteTrackings.remove(at: i)
            remoteTrackings.append(tracking)
        }
        
        return tracking.trackingId
    }
    
    func endCheckpoints(supervision: Supervision) -> Checkpoint {
        let endedCheckpoint = Checkpoint(supervision: supervision, type: CheckpointType.Ended, time: Date(), notifiedCount: 0)
        remoteCheckpoints = [Checkpoint]()
        
        remoteCheckpoints.append(endedCheckpoint)
        
        return endedCheckpoint
    }
    
    func clearCheckpoints() {
        remoteCheckpoints = [Checkpoint]()
    }
    
    
    override func pushFeedback(userId: String, caregroupId: String, fromUser: String) {
        let feedback = TestFeedback(userId: userId, fromUser: fromUser, groupId: caregroupId)
        
        remoteFeedbacks.append(feedback)
    }

}

struct TestFeedback {
    var userId : String
    var fromUser : String
    var groupId : String
}
