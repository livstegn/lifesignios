/*
 Copyright 2010-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */


import AWSCore
import AWSAPIGateway

public class AWSLifesignClient: AWSAPIGatewayClient {

	static let AWSInfoClientKey = "AWSLifesignClient"

	private static let _serviceClients = AWSSynchronizedMutableDictionary()
	private static let _defaultClient:AWSLifesignClient = {
		var serviceConfiguration: AWSServiceConfiguration? = nil
        let serviceInfo = AWSInfo.default().defaultServiceInfo(AWSInfoClientKey)
        if let serviceInfo = serviceInfo {
            serviceConfiguration = AWSServiceConfiguration(region: serviceInfo.region, credentialsProvider: serviceInfo.cognitoCredentialsProvider)
        } else if (AWSServiceManager.default().defaultServiceConfiguration != nil) {
            serviceConfiguration = AWSServiceManager.default().defaultServiceConfiguration
        } else {
            serviceConfiguration = AWSServiceConfiguration(region: .Unknown, credentialsProvider: nil)
        }
        
        return AWSLifesignClient(configuration: serviceConfiguration!)
	}()
    
	/**
	 Returns the singleton service client. If the singleton object does not exist, the SDK instantiates the default service client with `defaultServiceConfiguration` from `AWSServiceManager.defaultServiceManager()`. The reference to this object is maintained by the SDK, and you do not need to retain it manually.
	
	 If you want to enable AWS Signature, set the default service configuration in `func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?)`
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
	        AWSServiceManager.default().defaultServiceConfiguration = configuration
	 
	        return true
	     }
	
	 Then call the following to get the default service client:
	
	     let serviceClient = AWSLifesignClient.default()

     Alternatively, this configuration could also be set in the `info.plist` file of your app under `AWS` dictionary with a configuration dictionary by name `AWSLifesignClient`.
	
	 @return The default service client.
	 */ 
	 
	public class func `default`() -> AWSLifesignClient{
		return _defaultClient
	}

	/**
	 Creates a service client with the given service configuration and registers it for the key.
	
	 If you want to enable AWS Signature, set the default service configuration in `func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)`
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	         let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	         let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
	         AWSLifesignClient.registerClient(withConfiguration: configuration, forKey: "USWest2AWSLifesignClient")
	
	         return true
	     }
	
	 Then call the following to get the service client:
	
	
	     let serviceClient = AWSLifesignClient.client(forKey: "USWest2AWSLifesignClient")
	
	 @warning After calling this method, do not modify the configuration object. It may cause unspecified behaviors.
	
	 @param configuration A service configuration object.
	 @param key           A string to identify the service client.
	 */
	
	public class func registerClient(withConfiguration configuration: AWSServiceConfiguration, forKey key: String){
		_serviceClients.setObject(AWSLifesignClient(configuration: configuration), forKey: key  as NSString);
	}

	/**
	 Retrieves the service client associated with the key. You need to call `registerClient(withConfiguration:configuration, forKey:)` before invoking this method or alternatively, set the configuration in your application's `info.plist` file. If `registerClientWithConfiguration(configuration, forKey:)` has not been called in advance or if a configuration is not present in the `info.plist` file of the app, this method returns `nil`.
	
	 For example, set the default service configuration in `func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) `
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	         let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	         let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
	         AWSLifesignClient.registerClient(withConfiguration: configuration, forKey: "USWest2AWSLifesignClient")
	
	         return true
	     }
	
	 Then call the following to get the service client:
	 
	 	let serviceClient = AWSLifesignClient.client(forKey: "USWest2AWSLifesignClient")
	 
	 @param key A string to identify the service client.
	 @return An instance of the service client.
	 */
	public class func client(forKey key: String) -> AWSLifesignClient {
		objc_sync_enter(self)
		if let client: AWSLifesignClient = _serviceClients.object(forKey: key) as? AWSLifesignClient {
			objc_sync_exit(self)
		    return client
		}

		let serviceInfo = AWSInfo.default().defaultServiceInfo(AWSInfoClientKey)
		if let serviceInfo = serviceInfo {
			let serviceConfiguration = AWSServiceConfiguration(region: serviceInfo.region, credentialsProvider: serviceInfo.cognitoCredentialsProvider)
			AWSLifesignClient.registerClient(withConfiguration: serviceConfiguration!, forKey: key)
		}
		objc_sync_exit(self)
		return _serviceClients.object(forKey: key) as! AWSLifesignClient;
	}

	/**
	 Removes the service client associated with the key and release it.
	 
	 @warning Before calling this method, make sure no method is running on this client.
	 
	 @param key A string to identify the service client.
	 */
	public class func removeClient(forKey key: String) -> Void{
		_serviceClients.remove(key)
	}
	
	init(configuration: AWSServiceConfiguration) {
	    super.init()
	
	    self.configuration = configuration.copy() as! AWSServiceConfiguration
	    var URLString: String = "https://y15h3ayew7.execute-api.eu-central-1.amazonaws.com/test"
	    if URLString.hasSuffix("/") {
	        URLString = URLString.substring(to: URLString.index(before: URLString.endIndex))
	    }
	    self.configuration.endpoint = AWSEndpoint(region: configuration.regionType, service: .APIGateway, url: URL(string: URLString))
	    let signer: AWSSignatureV4Signer = AWSSignatureV4Signer(credentialsProvider: configuration.credentialsProvider, endpoint: self.configuration.endpoint)
	    if let endpoint = self.configuration.endpoint {
	    	self.configuration.baseURL = endpoint.url
	    }
	    self.configuration.requestInterceptors = [AWSNetworkingRequestInterceptor(), signer]
	}

	
    /*
     
     
     
     return type: Empty
     */
    public func eventPut() -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    let pathParameters:[String:Any] = [:]
	    
	    return self.invokeHTTPRequest("PUT", urlString: "/event", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param email 
     
     return type: AWSUser
     */
    public func usersGet(email: String?) -> AWSTask<AWSUser> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["email"] = email
	    
	    let pathParameters:[String:Any] = [:]
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self) as! AWSTask<AWSUser>
	}

	
    /*
     
     
     @param email 
     @param cellno 
     @param status 
     @param deviceid 
     @param platform 
     @param name 
     
     return type: AWSUser
     */
    public func usersPost(email: String, cellno: String?, status: String?, deviceid: String?, platform: String?, name: String?) -> AWSTask<AWSUser> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["cellno"] = cellno
	    queryParameters["status"] = status
	    queryParameters["email"] = email
	    queryParameters["deviceid"] = deviceid
	    queryParameters["platform"] = platform
	    queryParameters["name"] = name
	    
	    let pathParameters:[String:Any] = [:]
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self) as! AWSTask<AWSUser>
	}

	
    /*
     
     
     @param userid 
     
     return type: AWSUser
     */
    public func usersUseridGet(userid: String) -> AWSTask<AWSUser> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self) as! AWSTask<AWSUser>
	}

	
    /*
     
     
     @param userid 
     
     return type: Empty
     */
    public func usersUseridDelete(userid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("DELETE", urlString: "/users/{userid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     @param email 
     @param name 
     @param cellno 
     @param status 
     @param deviceid 
     @param platform 
     
     return type: AWSUser
     */
    public func usersUseridPatch(userid: String, email: String?, name: String?, cellno: String?, status: String?, deviceid: String?, platform: String?) -> AWSTask<AWSUser> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["email"] = email
	    queryParameters["name"] = name
	    queryParameters["cellno"] = cellno
	    queryParameters["status"] = status
	    queryParameters["deviceid"] = deviceid
	    queryParameters["platform"] = platform
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("PATCH", urlString: "/users/{userid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSUser.self) as! AWSTask<AWSUser>
	}

	
    /*
     
     
     @param userid 
     
     return type: AWSCaregroup
     */
    public func usersUseridCaregroupsGet(userid: String) -> AWSTask<AWSCaregroup> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self) as! AWSTask<AWSCaregroup>
	}

	
    /*
     
     
     @param userid 
     @param caregroupname 
     @param status 
     
     return type: AWSCaregroup
     */
    public func usersUseridCaregroupsPost(userid: String, caregroupname: String, status: String?) -> AWSTask<AWSCaregroup> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["caregroupname"] = caregroupname
	    queryParameters["status"] = status
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self) as! AWSTask<AWSCaregroup>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: AWSCaregroup
     */
    public func usersUseridCaregroupsCaregroupidGet(userid: String, caregroupid: String) -> AWSTask<AWSCaregroup> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self) as! AWSTask<AWSCaregroup>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidDelete(userid: String, caregroupid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/caregroups/{caregroupid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     @param caregroupname 
     @param status 
     
     return type: AWSCaregroup
     */
    public func usersUseridCaregroupsCaregroupidPatch(userid: String, caregroupid: String, caregroupname: String?, status: String?) -> AWSTask<AWSCaregroup> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["caregroupname"] = caregroupname
	    queryParameters["status"] = status
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/caregroups/{caregroupid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCaregroup.self) as! AWSTask<AWSCaregroup>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: AWSInvitation
     */
    public func usersUseridCaregroupsCaregroupidInvitesPost(userid: String, caregroupid: String) -> AWSTask<AWSInvitation> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/invites", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self) as! AWSTask<AWSInvitation>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: AWSMainStatus
     */
    public func usersUseridCaregroupsCaregroupidMainstatusGet(userid: String, caregroupid: String) -> AWSTask<AWSMainStatus> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/mainstatus", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSMainStatus.self) as! AWSTask<AWSMainStatus>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: AWSGroupMember
     */
    public func usersUseridCaregroupsCaregroupidMembersGet(userid: String, caregroupid: String) -> AWSTask<AWSGroupMember> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self) as! AWSTask<AWSGroupMember>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     @param status 
     
     return type: AWSGroupMember
     */
    public func usersUseridCaregroupsCaregroupidMembersPost(userid: String, caregroupid: String, status: String?) -> AWSTask<AWSGroupMember> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["status"] = status
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/members", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self) as! AWSTask<AWSGroupMember>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param caregroupid 
     
     return type: AWSGroupMember
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidGet(memberid: String, userid: String, caregroupid: String) -> AWSTask<AWSGroupMember> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self) as! AWSTask<AWSGroupMember>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param caregroupid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidDelete(memberid: String, userid: String, caregroupid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param memberid 
     @param ismonitor 
     @param userid 
     @param status 
     @param caregroupid 
     @param ismonitored 
     @param isowner 
     
     return type: AWSGroupMember
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidPatch(memberid: String, ismonitor: String, userid: String, status: String, caregroupid: String, ismonitored: String, isowner: String) -> AWSTask<AWSGroupMember> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["ismonitor"] = ismonitor
	    queryParameters["status"] = status
	    queryParameters["ismonitored"] = ismonitored
	    queryParameters["isowner"] = isowner
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self) as! AWSTask<AWSGroupMember>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param caregroupid 
     
     return type: AWSSupervision
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsGet(memberid: String, userid: String, caregroupid: String) -> AWSTask<AWSSupervision> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self) as! AWSTask<AWSSupervision>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param caregroupid 
     @param locationinterval 
     @param periodstart 
     @param supervisiontype 
     @param locationlongitude 
     @param periodend 
     @param locationlatitude 
     @param countdowntime 
     @param status 
     @param interval 
     @param endingtype 
     @param deadline1 
     @param deadline2 
     @param deadline3 
     @param wifi 
     
     return type: AWSSupervision
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsPost(memberid: String, userid: String, caregroupid: String, locationinterval: String?, periodstart: String?, supervisiontype: String?, locationlongitude: String?, periodend: String?, locationlatitude: String?, countdowntime: String?, status: String?, interval: String?, endingtype: String?, deadline1: String?, deadline2: String?, deadline3: String?, wifi: String?) -> AWSTask<AWSSupervision> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["locationinterval"] = locationinterval
	    queryParameters["periodstart"] = periodstart
	    queryParameters["supervisiontype"] = supervisiontype
	    queryParameters["locationlongitude"] = locationlongitude
	    queryParameters["periodend"] = periodend
	    queryParameters["locationlatitude"] = locationlatitude
	    queryParameters["countdowntime"] = countdowntime
	    queryParameters["status"] = status
	    queryParameters["interval"] = interval
	    queryParameters["endingtype"] = endingtype
	    queryParameters["deadline1"] = deadline1
	    queryParameters["deadline2"] = deadline2
	    queryParameters["deadline3"] = deadline3
	    queryParameters["wifi"] = wifi
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self) as! AWSTask<AWSSupervision>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param caregroupid 
     
     return type: AWSSupervision
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidGet(memberid: String, userid: String, supervisionid: String, caregroupid: String) -> AWSTask<AWSSupervision> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self) as! AWSTask<AWSSupervision>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param caregroupid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidDelete(memberid: String, userid: String, supervisionid: String, caregroupid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param locationinterval 
     @param periodstart 
     @param supervisionid 
     @param supervisiontype 
     @param locationlongitude 
     @param periodend 
     @param locationlatitude 
     @param countdowntime 
     @param status 
     @param interval 
     @param memberid 
     @param endingtype 
     @param deadline1 
     @param deadline2 
     @param deadline3 
     @param wifi 
     @param userid 
     @param caregroupid 
     
     return type: AWSSupervision
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidPatch(locationinterval: String, periodstart: String, supervisionid: String, supervisiontype: String, locationlongitude: String, periodend: String, locationlatitude: String, countdowntime: String, status: String, interval: String, memberid: String, endingtype: String, deadline1: String, deadline2: String, deadline3: String, wifi: String, userid: String, caregroupid: String) -> AWSTask<AWSSupervision> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["locationinterval"] = locationinterval
	    queryParameters["periodstart"] = periodstart
	    queryParameters["supervisiontype"] = supervisiontype
	    queryParameters["locationlongitude"] = locationlongitude
	    queryParameters["periodend"] = periodend
	    queryParameters["locationlatitude"] = locationlatitude
	    queryParameters["countdowntime"] = countdowntime
	    queryParameters["status"] = status
	    queryParameters["interval"] = interval
	    queryParameters["endingtype"] = endingtype
	    queryParameters["deadline1"] = deadline1
	    queryParameters["deadline2"] = deadline2
	    queryParameters["deadline3"] = deadline3
	    queryParameters["wifi"] = wifi
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSSupervision.self) as! AWSTask<AWSSupervision>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param caregroupid 
     
     return type: AWSAlarm
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidAlarmsGet(memberid: String, userid: String, supervisionid: String, caregroupid: String) -> AWSTask<AWSAlarm> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/alarms", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSAlarm.self) as! AWSTask<AWSAlarm>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param caregroupid 
     
     return type: AWSCheckpoints
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidCheckpointsGet(memberid: String, userid: String, supervisionid: String, caregroupid: String) -> AWSTask<AWSCheckpoints> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/checkpoints", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSCheckpoints.self) as! AWSTask<AWSCheckpoints>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param caregroupid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidCheckpointsPost(memberid: String, userid: String, supervisionid: String, caregroupid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/checkpoints", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param caregroupid 
     
     return type: AWSTracking
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidTrackingsGet(memberid: String, userid: String, supervisionid: String, caregroupid: String) -> AWSTask<AWSTracking> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSTracking.self) as! AWSTask<AWSTracking>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param trackingid 
     @param caregroupid 
     
     return type: AWSTracking
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidTrackingsTrackingidGet(memberid: String, userid: String, supervisionid: String, trackingid: String, caregroupid: String) -> AWSTask<AWSTracking> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["trackingid"] = trackingid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSTracking.self) as! AWSTask<AWSTracking>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param trackingid 
     @param caregroupid 
     
     return type: AWSTracking
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidTrackingsTrackingidPut(memberid: String, userid: String, supervisionid: String, trackingid: String, caregroupid: String) -> AWSTask<AWSTracking> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["trackingid"] = trackingid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("PUT", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSTracking.self) as! AWSTask<AWSTracking>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param trackingid 
     @param caregroupid 
     
     return type: AWSTracking
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidTrackingsTrackingidDelete(memberid: String, userid: String, supervisionid: String, trackingid: String, caregroupid: String) -> AWSTask<AWSTracking> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["trackingid"] = trackingid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSTracking.self) as! AWSTask<AWSTracking>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param trackingid 
     @param caregroupid 
     
     return type: AWSTracking
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidTrackingsTrackingidPatch(memberid: String, userid: String, supervisionid: String, trackingid: String, caregroupid: String) -> AWSTask<AWSTracking> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["trackingid"] = trackingid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSTracking.self) as! AWSTask<AWSTracking>
	}

	
    /*
     
     
     @param memberid 
     @param userid 
     @param supervisionid 
     @param trackingid 
     @param caregroupid 
     
     return type: AWSLocation
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidTrackingsTrackingidLocationsGet(memberid: String, userid: String, supervisionid: String, trackingid: String, caregroupid: String) -> AWSTask<AWSLocation> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["trackingid"] = trackingid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLocation.self) as! AWSTask<AWSLocation>
	}

	
    /*
     
     
     @param memberid 
     @param locationid 
     @param userid 
     @param supervisionid 
     @param trackingid 
     @param caregroupid 
     
     return type: Empty
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidTrackingsTrackingidLocationsLocationidGet(memberid: String, locationid: String, userid: String, supervisionid: String, trackingid: String, caregroupid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["locationid"] = locationid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["trackingid"] = trackingid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations/{locationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param memberid 
     @param locationid 
     @param userid 
     @param supervisionid 
     @param trackingid 
     @param caregroupid 
     
     return type: AWSLocation
     */
    public func usersUseridCaregroupsCaregroupidMembersMemberidSupervisionsSupervisionidTrackingsTrackingidLocationsLocationidPut(memberid: String, locationid: String, userid: String, supervisionid: String, trackingid: String, caregroupid: String) -> AWSTask<AWSLocation> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["memberid"] = memberid
	    pathParameters["locationid"] = locationid
	    pathParameters["userid"] = userid
	    pathParameters["supervisionid"] = supervisionid
	    pathParameters["trackingid"] = trackingid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("PUT", urlString: "/users/{userid}/caregroups/{caregroupid}/members/{memberid}/supervisions/{supervisionid}/trackings/{trackingid}/locations/{locationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLocation.self) as! AWSTask<AWSLocation>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: AWSMessage
     */
    public func usersUseridCaregroupsCaregroupidMessagesGet(userid: String, caregroupid: String) -> AWSTask<AWSMessage> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/messages", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSMessage.self) as! AWSTask<AWSMessage>
	}

	
    /*
     
     
     @param messagetext 
     @param userid 
     @param caregroupid 
     
     return type: AWSMessage
     */
    public func usersUseridCaregroupsCaregroupidMessagesPost(messagetext: String, userid: String, caregroupid: String) -> AWSTask<AWSMessage> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["messagetext"] = messagetext
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/caregroups/{caregroupid}/messages", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSMessage.self) as! AWSTask<AWSMessage>
	}

	
    /*
     
     
     @param userid 
     @param caregroupid 
     
     return type: AWSGroupMember
     */
    public func usersUseridCaregroupsCaregroupidSearchGet(userid: String, caregroupid: String) -> AWSTask<AWSGroupMember> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["caregroupid"] = caregroupid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/caregroups/{caregroupid}/search", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSGroupMember.self) as! AWSTask<AWSGroupMember>
	}

	
    /*
     
     
     @param userid 
     
     return type: AWSInvitation
     */
    public func usersUseridInvitesGet(userid: String) -> AWSTask<AWSInvitation> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/invites", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self) as! AWSTask<AWSInvitation>
	}

	
    /*
     
     
     @param userid 
     @param invitationid 
     
     return type: AWSInvitation
     */
    public func usersUseridInvitesInvitationidGet(userid: String, invitationid: String) -> AWSTask<AWSInvitation> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["invitationid"] = invitationid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/invites/{invitationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self) as! AWSTask<AWSInvitation>
	}

	
    /*
     
     
     @param userid 
     @param groupname 
     @param invitationid 
     @param sendername 
     
     return type: AWSInvitation
     */
    public func usersUseridInvitesInvitationidPost(userid: String, groupname: String, invitationid: String, sendername: String) -> AWSTask<AWSInvitation> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["groupname"] = groupname
	    queryParameters["sendername"] = sendername
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["invitationid"] = invitationid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/invites/{invitationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self) as! AWSTask<AWSInvitation>
	}

	
    /*
     
     
     @param userid 
     @param invitationid 
     
     return type: AWSInvitation
     */
    public func usersUseridInvitesInvitationidDelete(userid: String, invitationid: String) -> AWSTask<AWSInvitation> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["invitationid"] = invitationid
	    
	    return self.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/invites/{invitationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self) as! AWSTask<AWSInvitation>
	}

	
    /*
     
     
     @param userid 
     @param status 
     @param invitationid 
     
     return type: AWSInvitation
     */
    public func usersUseridInvitesInvitationidPatch(userid: String, status: String, invitationid: String) -> AWSTask<AWSInvitation> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    var queryParameters:[String:Any] = [:]
	    queryParameters["status"] = status
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    pathParameters["invitationid"] = invitationid
	    
	    return self.invokeHTTPRequest("PATCH", urlString: "/users/{userid}/invites/{invitationid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSInvitation.self) as! AWSTask<AWSInvitation>
	}

	
    /*
     
     
     @param userid 
     
     return type: AWSLifesign
     */
    public func usersUseridLifesignsGet(userid: String) -> AWSTask<AWSLifesign> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/lifesigns", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLifesign.self) as! AWSTask<AWSLifesign>
	}

	
    /*
     
     
     @param userid 
     
     return type: AWSLifesign
     */
    public func usersUseridLifesignsPost(userid: String) -> AWSTask<AWSLifesign> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/lifesigns", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSLifesign.self) as! AWSTask<AWSLifesign>
	}

	
    /*
     
     
     @param messageid 
     @param userid 
     
     return type: AWSMessage
     */
    public func usersUseridMessagesMessageidGet(messageid: String, userid: String) -> AWSTask<AWSMessage> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["messageid"] = messageid
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/messages/{messageid}", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSMessage.self) as! AWSTask<AWSMessage>
	}

	
    /*
     
     
     @param userid 
     
     return type: Empty
     */
    public func usersUseridSnsmessagePost(userid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("POST", urlString: "/users/{userid}/snsmessage", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

	
    /*
     
     
     @param userid 
     
     return type: AWSStatus
     */
    public func usersUseridStatusGet(userid: String) -> AWSTask<AWSStatus> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("GET", urlString: "/users/{userid}/status", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSStatus.self) as! AWSTask<AWSStatus>
	}

	
    /*
     
     
     @param userid 
     
     return type: AWSStatus
     */
    public func usersUseridStatusPut(userid: String) -> AWSTask<AWSStatus> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("PUT", urlString: "/users/{userid}/status", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: AWSStatus.self) as! AWSTask<AWSStatus>
	}

	
    /*
     
     
     @param userid 
     
     return type: Empty
     */
    public func usersUseridStatusDelete(userid: String) -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   
	            ]
	    
	    let queryParameters:[String:Any] = [:]
	    
	    var pathParameters:[String:Any] = [:]
	    pathParameters["userid"] = userid
	    
	    return self.invokeHTTPRequest("DELETE", urlString: "/users/{userid}/status", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}




}
