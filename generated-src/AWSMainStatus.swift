/*
 Copyright 2010-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */


import Foundation
import AWSCore


@objcMembers
public class AWSMainStatus : AWSModel {
    
    var partitionkey: String?
    var sortkey: String?
    var dataSk: String?
    var username: String?
    var supervisiontype: String?
    var caregroupname: String?
    var memberid: String?
    var countdowntime: String?
    var deadlinetime: String?
    var endedtime: String?
    var periodend: String?
    var trackingstatus: String?
    var leftsafe: String?
    var onsafe: String?
    var backsafe: String?
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["partitionkey"] = "partitionkey"
		params["sortkey"] = "sortkey"
		params["dataSk"] = "data-sk"
		params["username"] = "username"
		params["supervisiontype"] = "supervisiontype"
		params["caregroupname"] = "caregroupname"
		params["memberid"] = "memberid"
		params["countdowntime"] = "countdowntime"
		params["deadlinetime"] = "deadlinetime"
		params["endedtime"] = "endedtime"
		params["periodend"] = "periodend"
		params["trackingstatus"] = "trackingstatus"
		params["leftsafe"] = "leftsafe"
		params["onsafe"] = "onsafe"
		params["backsafe"] = "backsafe"
		
        return params
	}
}
