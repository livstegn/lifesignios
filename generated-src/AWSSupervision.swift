/*
 Copyright 2010-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */


import Foundation
import AWSCore


@objcMembers
public class AWSSupervision : AWSModel {
    
    var partitionkey: String?
    var sortkey: String?
    var dataSk: String?
    var deadline1: String?
    var deadline2: String?
    var deadline3: String?
    var countdowntime: String?
    var interval: String?
    var supervisiontype: String?
    var periodstart: String?
    var periodend: String?
    var caregroupid: String?
    var userid: String?
    var endingtype: String?
    var wifi: String?
    var locationinterval: String?
    var locationlatitude: String?
    var locationlongitude: String?
    var trackingid: String?
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["partitionkey"] = "partitionkey"
		params["sortkey"] = "sortkey"
		params["dataSk"] = "data-sk"
		params["deadline1"] = "deadline1"
		params["deadline2"] = "deadline2"
		params["deadline3"] = "deadline3"
		params["countdowntime"] = "countdowntime"
		params["interval"] = "interval"
		params["supervisiontype"] = "supervisiontype"
		params["periodstart"] = "periodstart"
		params["periodend"] = "periodend"
		params["caregroupid"] = "caregroupid"
		params["userid"] = "userid"
		params["endingtype"] = "endingtype"
		params["wifi"] = "wifi"
		params["locationinterval"] = "locationinterval"
		params["locationlatitude"] = "locationlatitude"
		params["locationlongitude"] = "locationlongitude"
		params["trackingid"] = "trackingid"
		
        return params
	}
}
