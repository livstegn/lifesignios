/*
 Copyright 2010-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */


import Foundation
import AWSCore


@objcMembers
public class AWSTracking : AWSModel {
    
    var partitionkey: String?
    var sortkey: String?
    var dataSk: String?
    var onsafe: NSNumber?
    var leftsafe: NSNumber?
    var backsafe: NSNumber?
    var userid: String?
    var caregroupid: String?
    var memberid: String?
    var supervisionid: String?
    var latestlocationid: String?
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["partitionkey"] = "partitionkey"
		params["sortkey"] = "sortkey"
		params["dataSk"] = "data-sk"
		params["onsafe"] = "onsafe"
		params["leftsafe"] = "leftsafe"
		params["backsafe"] = "backsafe"
		params["userid"] = "userid"
		params["caregroupid"] = "caregroupid"
		params["memberid"] = "memberid"
		params["supervisionid"] = "supervisionid"
		params["latestlocationid"] = "latestlocationid"
		
        return params
	}
}
